﻿<?php  

  /*Verificação de acessibilidade */
    $acessibilidade = "";

    if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

          $acessibilidadeTxt = null;
          if ($acessibilidade == "sim") { 
            $acessibilidadeTxt = "?ac=sim";
          };    

  /*Configura parametros de navegação
      @Título menu  
      @Se 'exibir' exibe o menu
      @Número da aula
      @Número da página
      @Número de páginas da aula
      @Link da aula anterior
      @Link da próxima aula
      @Título da página    
  */
  function configHeader($nomeMenu, $flagExibirMenu, $aula, $pagina, $numeroAulas, $linkAnterior, $linkProximo,$headerName) {
?>

<?php   
  //Estrutura do curso

    $acessibilidade = "";

    if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

          $acessibilidadeTxt = null;
          if ($acessibilidade == "sim") { 
            $acessibilidadeTxt = "?ac=sim";
          };    


 ?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta charset="utf-8" />
    <title><?php echo 'A'.$aula.'P'.$pagina.' - '.$headerName ?></title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Curso do STF de Produção de Conteúdos para a EaD" name="description" />
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="../include/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../include/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <!-- BEGIN CSS TEMPLATE -->
    <link href="../include/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="../include/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="../include/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
    <!-- START STF CSS TEMPLATE -->
    <link href="../include/css/custom_queries.css" rel="stylesheet" type="text/css"/>    
    <link href="../include/css/custom.css" rel="stylesheet" type="text/css"/>
    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->



  </head>
  <body>
    
    <?php  if ($acessibilidade == "sim") { ?> 
        <div id="acessibilidade" style="display:none"><?php echo  $acessibilidade; ?></div>
    <?php   } ?>
    <section >  
      <div class="header navbar navbar-inverse ">
        <div class="navbar-inner">
          <!--topo all-screens -->
          <div class="header-quick-nav" >
            <a class="pull-left semi-bold" href="index.php"><img src="../include/img/logo.png" class="logo" alt="logo do STF" title="logo do stf" data-src="../include/img/logo.png"  /></a>
              <div class="pull-right chat-toggler">
                 <a href="http://ead.stf.jus.br" target="_blank" style="float:right">
                    <div class="user-details" >
                       <div class="username" style="margin-top:20px;"> Cursos a distância do <span class="bold">Supremo Tribunal Federal</span> </div>
                    </div>
                 </a>
				         <a href="#" style="float:right">
                    <div class="user-details" style="margin-top:20px">
                       <div class="username"> 
                          <ul class="menu-aulas" role="menubar" style="margin-top:-12px; margin-right:20px;">
                            <li role="menuitem"><a href="../include/tutorial.php" class="btn btn-default btn-small " title="Dicas de navegação">Dicas de navegação</a> </li> 
                            <li role="menuitem"><a href="?"  title="Retorne o texto ao tamanho normal"  alt="Retorne o texto ao tamanho normal" class="btn btn-default btn-small diminuirFonte">A</a> </li> 
                            <li role="menuitem"><a href="?ac=sim"  title="Aumente o tamanho do texto"  alt="Aumente o tamanho do texto" class="btn btn-default btn-small aumentarFonte">A+</a> </li> 
                          </ul>  
                        </div>
                    </div>
                 </a> 
              </div>  
          </div>
          <!--end of topo all-screens -->
          <!--topo só large -->
          <div class="header-quick-nav visible-lg pull-left semi-bold" style="margin-top:20px;" >Produção de Conteúdos para a EaD
          </div>
          <!--end of só large -->



          <!--topo xs -->
          <div class="header-seperation visible-xs">
            <a href="index.php"><img src="../include/img/logo_black.png" class="logo" alt="Logo do STF"  data-src="../include/img/logo_black.png"  /></a>
             <a href="javascript:void(0);" class="btn btn-white btn-small" style="float:right; margin:15px; cursor:default;">Página <?php echo $pagina; ?>/<?php echo $numeroAulas; ?></a>
          </div>
          <!-- end of topo xs -->        
        </div>
      </div>
    </section>

    <main role="main" >
      <div class="page-content" > 
        <div class="content" style="margin-bottom:30px">  

            <section id="banner">
              <!--header all-screens -->
              <header id="profile-cover" class="hidden-xs">
                <section id="usuario-resumo" class="row">
                  <div class="col-lg-9 col-md-9 col-sm-9">
                    <img src="../include/img/profiles-background/computador.png" alt="Foto do EaD" style="max-height:83px; margin-right:120px; float:right" class="hidden-xs">
                    <img id="usuario-imagem" alt="Curso de Licitações e Contratos Administrativos" class="img-circle pull-left" alt="Ícone do curso"  src="imagens/avatar<?php echo rand(0,9) ?>.jpg" data-toggle="tooltip" data-placement="top" />
                    <div id="usuario-identificacao">
                      <h4 style="font-weight:bold"> Produção de Conteúdos para a EaD</h4>
                      <h5>Autora: Mariana Serejo</h5>
                    </div>

                  </div>
                </section>
              </header>
              <!--end of header all-screens -->
              <!--header xs -->        
              <header id="profile-cover-xs" class="visible-xs">
                <section id="usuario-resumo-xs" class="row">
                  <div class="col-xs-12">
                    <img id="usuario-imagem-xs" alt="Curso de Licitações e Contratos Administrativos" class="img-circle" alt="Ícone do curso"  src="imagens/avatar<?php echo rand(0,9) ?>.jpg" data-toggle="tooltip" data-placement="top" title="Alterar foto" />

                    <div id="usuario-identificacao-xs">
                      <h4>Produção de Conteúdos para a EaD</h4>
                      <h5>Autora: Mariana Serejo</h5>
                    </div>
                  </div>
                </section>
              </header>  
              <!--end of header xs -->  
            </section> 

<?php  if ($flagExibirMenu == "exibir") { ?>
            <nav role="navigation" accesskey="1"> 
             <div id="menu" class="row">
                <div class="white-title"><h3 style="font-size:30px; border-bottom:2px dashed #346B94; padding-left:20px; border-left:2px dashed #346B94; border-bottom-left-radius:20px; border-top-left-radius:20px;padding-bottom: 5px; color: #F19121">Aula <?php echo $aula ?> - <?php echo $nomeMenu ?></h3 ></div>
                <div class="col-lg-12" style="margin:20px">
                  <div class="page-title">                    
                      <ul class="menu-aulas" role="menubar">
					  <li role="menuitem"> </li> 
<?php if ( $pagina != 1) { ?>
                            <li role="menuitem"><a href="<?php echo $linkAnterior.$acessibilidadeTxt ?>" class="btn btn-white btn-small" title="Voltar para página anterior">Anterior</a> </li> 
<?php } ?>
<?php     
  for ($i=1; $i <= $numeroAulas ; $i++) { 
      if ( $i == $pagina) {
          $corMenu = "primary";
          $titleMenu = "";          
          $href = "javascript:void(0);";
      } else {

          $corMenu = "white";
          $href = "href='aula".$aula."pagina".$i.".php".$acessibilidadeTxt."'";
          $titleMenu = "Ir para página ". $i;
      }

?>         
                            <li role="menuitem"><a <?php echo $href; ?>  class="btn btn-<?php echo $corMenu ?> btn-small " title="<?php echo $titleMenu; ?>"><?php echo $i ?></a> </li> 
<?php } ?> 
<?php if ( $pagina != $numeroAulas) { ?>
                            <li role="menuitem"><a href="<?php echo $linkProximo.$acessibilidadeTxt ?>" class="btn btn-white btn-small" title="Ir para próxima página">Próxima</a>  </li> 
<?php } ?>                           
                      </ul>            
                  </div>
                </div> 
              </div>
            </nav>
<div class="container" style="margin-top:0;padding-top:0">
<article id="aula" accesskey="2">
  <div class="row area-padding ">
    <div class="col-md-12">
      <div class="grid simple ">
        <div class="grid-body no-border area-padding-top">

<?php  
  } //end of flagExibirMenu
}// end of configHeader
?>
<?php  
function configNavegacaoRodape($flagExibirMenu, $aulaAnterior, $proximaAula) {
    $acessibilidade = "";

    if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

      $acessibilidadeTxt = null;
      if ($acessibilidade == "sim") { 
        $acessibilidadeTxt = "?ac=sim";
      };

if ($flagExibirMenu == "exibir") {
?>
                    <div class="clear"></div>
                  </div>
                </div>   
              </div>
            </div>
          </article>   
</div>
<div class="container" style="margin-top:0;padding-top:0">
          <footer>  
            <nav role="navigation" class="area-padding" >
                <div class="textAlignCenter area-padding-bottom ">
                  <div  style="padding:15px 0px 0px 0px; " >
<?php if($aulaAnterior != 'fim'){ ?>
                     <a href="<?php echo $aulaAnterior.$acessibilidadeTxt ?>" class="btn btn-lg btn-success espaco-mobile-menu" title="Voltar para página anterior"><i class="fa fa-arrow-circle-o-left"></i> Voltar </a> 
<?php } ?>                
               
<?php if($proximaAula != 'fim'){ ?>
                    <a href="<?php echo $proximaAula.$acessibilidadeTxt ?>" class="btn btn-lg btn-success espaco-mobile-menu" title="Ir para próxima página" >Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
<?php } ?>
                  </div>
                </div>
            </nav>
          </footer>
</div>
<?php  
  }//end of flagExibirMenu
}// end of configNavegacaoRodape
?>
<?php  
function configFooter() {
?>    

        </div>
      </div>
    </main>
    <script src="../include/js/jquery-1.8.3.min.js" type="text/javascript"></script> 
    <script src="../include/js/timeline2.js"></script>
    <script src="../include/js/bootstrap.min.js" type="text/javascript"></script> 
    <script src="../include/js/fitvids.js" type="text/javascript"></script> 

    <script type="text/javascript">
        $(document).ready(function () {
            $('.responsive-timeline').rTimeline({theme: 'light', url: 'loadedtest.html'});
        });
    </script>

    <script src="../include/js/custom.js" type="text/javascript"></script> 

    <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-68010757-1', 'auto');
          ga('send', 'pageview');

        </script>
    
  </body>
</html>
<?php } ?>
