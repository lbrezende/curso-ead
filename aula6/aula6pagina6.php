<?php 
//Configurar Aula
$aula = 6; 
$pagina = 6; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
  	<div class="row">
  		<div class="col-lg-6">
  			<p><span class="semi-bold">c.  Organização (da informação):</span> demonstram relações qualitativas entre fatos, conceitos ou princípios. São usadas para explicar procedimentos, atributos (características de algo) e conceitos. Exemplos: <em>ilustrações de passo a passo, ilustrações que retratam relações entre fatos, conceitos ou princípios</em>.</p>
                          <p class="text-center" style="text-indent:0em; margin-top:20px"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Imagem para expressar as relações entre o aluno, o material e o tutor (Aula 2)</p>
			<p class="text-center" style="text-indent:0em; margin-top:25px"><a href="imagens/esquema.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/esquema.jpg" alt="esquema entre aluno-material didático" class="img-responsiva"></a></p>
			<p style="margin-top:30px"><span class="semi-bold">d.  Relacionais:</span> “transformam informação numérica em informação visual exibindo relações quantitativas entre duas ou mais variáveis”. Exemplos: <em>gráficos de barras ou de pizza</em>.</p>
			
			<p style="text-indent:0em; margin-top:20px"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Na Aula 1, nós apresentamos uma tabela com números que representavam o crescimento da oferta de cursos e de participantes ao longo dos anos.  A título de exemplo, vamos propor a transformação dessas informações em um gráfico. Veja:</p>
			<p class="text-center" style="text-indent:0em; margin-top:25px"><a href="imagens/grafico1.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/grafico1.jpg" alt="grafico da quantidade de cursos ao longo dos anos" class="img-responsiva"></a></p>
			<p class="text-center" style="text-indent:0em"><a href="imagens/grafico2.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/grafico2.jpg" alt="" class="img-responsiva"></a></p>
  		</div>
  		<div class="col-lg-6">
			<p><span class="semi-bold">e.  Transformativa:</span> “mostram mudanças em procedimentos, processos e princípios ao longo do tempo ou espaço, comunicando movimento”.</p>
			<p class="text-center" style="text-indent:0em; margin-top:20px"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> linha do tempo com informações da aula 1.</p>
			<p class="text-center" style="text-indent:0em; margin-top:25px"><a href="imagens/linha-tempo-1.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/linha-tempo-1.jpg" alt="Linha do tempo ead pelo mundo" class="img-responsiva"></a></p>
			<p class="text-center" style="text-indent:0em"><a href="imagens/linha-tempo-2.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/linha-tempo-2.jpg" alt="Linha do tempo ead no Brasil" class="img-responsiva"></a></p>
  		</div>
  	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

