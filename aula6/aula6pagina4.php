<?php 
//Configurar Aula
$aula = 6; 
$pagina = 4; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
	<div class="row">
		<div class="col-lg-3 text-right"><img src="imagens/aspastop.jpg" style="margin-top:-50px;"></div>
		<div class="col-lg-5 text-left" style="padding-left:20px;">
			<h3 class="titulo titulo-primary">Uma imagem vale mais que mil palavras.</h3>
			<div class="row">
				<div class="col-lg-12">
					<p class="titulo titulo-primary fonteMenor text-right"><em>(Confúcio)</em></p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 text-left"><img src="imagens/aspasbottom.jpg" style="margin-top:50px"></div>
	</div>
	<p class="text-center"><img src="imagens/imagens.png" alt="imagens bem conhecidas do Superman, Monalisa, Coca-cola e bandeira do Brasil" style="max-width:400px;" /></p>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Essas imagens podem ser rapidamente identificadas por uma grande parcela de pessoas, não é mesmo?</h3></div>

	<div class="row">
		<div class="col-lg-6">
			<p>Muitas vezes, as imagens representam tão bem situações, pensamentos, emoções, informações, marcas, eventos etc. que, de fato, valem mais que mil palavras, como nos sugere o filósofo.</p>
			<p>Cardoso (2013) assinala que: </p>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-10"><p class="fonteMenor" style="text-indent:0em;">Os estudos no campo da neurociência apontam que a metade do cérebro humano estaria comprometida com o processamento de imagens. Imagens têm acesso direto à memória de longo prazo, e cada imagem é armazenada com sua própria informação como um coerente bloco ou conceito, de forma que processamos a informação visual 60 mil vezes mais rápido do que o texto.</p></div>
			</div>

		</div>
		<div class="col-lg-6">
			<p>No caso da educação a distância, as imagens também podem ser grandes aliadas. Mayer (2009), citado por Mattar (2014, p.116), postula que a aprendizagem é mais significativa quando há a combinação de imagens e palavras. Lohr complementa, ainda segundo Mattar, que isso pode ser potencializado quando as figuras são relacionadas com a informação textual. </p>
			<div class="tituloEspecialEadAula6">
				<p><span class="semi-bold ">Em síntese:</span> Aplicar imagens que apresentam correlação com o texto auxilia o aluno a construir uma aprendizagem mais efetiva.</p>
			</div>
		</div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

