<?php 
//Configurar Aula
$aula = 6; 
$pagina = 12; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
        <div style="text-align:left"><h3 class="titulo titulo-primary">6.3 Vídeos</h3></div>
	  <div class="row">
	  	<div class="col-lg-6">
	  		<p>O exposto até aqui ratifica o entendimento de que os vídeos são grandes aliados do conteudista. Você, autor, pode realizar buscas de mídias na internet conforme os assuntos a serem desenvolvidos em suas aulas. Outra alternativa é a gravação de vídeos pelo próprio conteudista. </p>
			<p>Mencionaremos algumas formas de aplicação, no texto didático, de vídeos encontrados no <em>YouTube</em>. Observe exemplos utilizados no nosso curso:</p>
			<ul>
				<li>Para <span class="semi-bold">aprofundar</span> algum assunto.
					<p style="margin-top:5px;"><span class="semi-bold">Aula 1.</span> Contextualização da Educação a Distância: </p>
					<p style="line-height:1.5;">Vídeo sobre o contexto do <a href="https://www.youtube.com/watch?v=Mz0gtsk0gCs" target="_blank">Projeto Minerva</a>.</p>

				</li>
				<li>Para <span class="semi-bold">sintetizar os conteúdos</span> abordados.
					<p style="margin-top:5px;"><span class="semi-bold">Aula 1.</span> Contextualização da Educação a Distância:</p>
					<p style="line-height:1.5;">Vídeo do <a href="https://www.youtube.com/watch?v=5OKybl7vTqo" target="_blank">Jornal Nacional</a>, apresentado para sintetizar a evolução histórica da EaD.</p>
				</li>
				<li>Para <span class="semi-bold">apresentar a visão de autores renomados</span> na área.
					<p style="margin-top:5px;"><span class="semi-bold">Aula 2.</span> Aspectos relacionados à aprendizagem a distância: </p>
					<p style="line-height:1.5;">Vídeo de <a href="https://www.youtube.com/watch?v=bUjWkm30nGw" target="_blank">entrevista</a> com a professora Daniela Melaré, sobre os estilos de aprendizagem.</p>
				</li>
				<li>Para <span class="semi-bold">inspirar reflexões</span> sobre o tema.
					<p style="margin-top:5px;"><span class="semi-bold">Aula 3.</span> Por que planejar a escrita?: </p>
					<p style="line-height:1.5;">Vídeo “<a href="https://www.youtube.com/watch?v=ISt-Dx7nBNE" target="_blank">Alice no país das maravilhas</a>” para refletir sobre os caminhos que devemos recorrer para escrever materiais didáticos consistentes.</p>
				</li>
			</ul>
	  	</div>
	  	<div class="col-lg-6">
	  		<p>Essas são apenas algumas das possibilidades de uso, que não se esgotam aqui. Você pode pensar em outras maneiras de agregar vídeos ao texto, de acordo com as especificidades temáticas do seu curso. </p>
			<p><span class="semi-bold">Observação:</span> A professora Filatro (2008) recomenda que evitemos vídeos longos, que exigem períodos extensos em frente ao computador, uma vez que a experiência dos navegantes da web é altamente interativa. </p>
			
			<div class="caixaAmarelaAula6" style="padding:20px 30px;">
				<p>Para finalizar a nossa conversa sobre as mídias audiovisuais, temos algumas orientações:</p>
				<p style="text-indent:0em !important;margin-left:30px;">● Quando selecionar algum vídeo para inserir em suas aulas, assista com atenção, de preferência mais de uma vez antes de decidir incluí-lo no material didático. </p>
				<p style="text-indent:0em !important;margin-left:30px;">● Reflita: Essa mídia é significativa? Atende aos meus objetivos didáticos? O vídeo demanda muito tempo da atenção do aluno? Apresenta qualidade visual (boas imagens) e auditiva (o volume é adequado)?</p>
				<p style="text-indent:0em !important;margin-left:30px;">● Coloque-se no lugar do aluno e tente pensar como ele se sentiria ao assistir ao vídeo. </p>	
			</div>
			<p style="padding-top:10px">O importante é você compreender que os vídeos são recursos didáticos com grande potencial para fomentar aprendizagens significativas e, ainda, para diversificar o seu material. Certo? </p>

	  	</div>
	  </div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

