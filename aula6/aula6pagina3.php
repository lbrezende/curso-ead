<?php 
//Configurar Aula
$aula = 6; 
$pagina = 3; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div style="text-align:left">
    <h3 class="titulo titulo-primary">6.1 Orientações iniciais</h3></div>
	<div class="row">
	  <div class="col-lg-6">
	  	<p>Com base em tudo o que estudamos ao longo do curso, percebemos que a educação a distância exige do autor o estabelecimento de diversas conexões no texto. Significa que precisamos associar recursos à escrita, com vistas a favorecer a construção do conhecimento pelos discentes e a tornar o conteúdo mais atrativo, interativo e interessante para o estudo <em>online</em>. </p>
		<p>A esse respeito, Filatro (2008) salienta que, “quando apresentada de maneira verbal e não verbal, a informação é mais facilmente processada pela memória do que quando apresentada de uma única forma”.</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="margin-top:20px">Diante disso, como escolher os recursos para o seu material didático?</h3></div>
		<p>A seleção deve observar, primordialmente, os objetivos específicos de aprendizagem que você definiu na fase de planejamento. Devemos pensar da seguinte maneira, conforme nos orienta Zanetti (2009), citando Moore e Kearsley (2007):</p>
		<p>Quais elementos podem <span class="semi-bold">“contribuir de maneira mais significativa para o processo de ensino/aprendizagem”</span> dos alunos?</p>
	  </div>
	  <div class="col-lg-6">
		<p>Com esse foco, você pode começar a fazer algumas pesquisas. Existem muitas opções. Para facilitar um pouco o processo, sugerimos que você recorra, de início, às suas experiências pessoais. Por exemplo, se aprecia música, artes visuais ou literatura, procure algo nessas áreas que poderia contribuir para o entendimento dos conteúdos ou estimular reflexões sobre os temas das aulas.</p>
		<p>Além disso, busque recursos na internet que tenham relação com os assuntos a serem discutidos no material didático. Como o ciberespaço é vasto, avalie com cuidado a pertinência conceitual e contextual de cada elemento. Você também pode procurar em outros meios, fique à vontade. </p>
		<p>Perceba, porém, que não se trata de apenas “rechear fartamente” a sua produção.  Cada ferramenta selecionada precisa guardar estrita relação com o texto e com o conteúdo proposto. O excesso, aqui, pode comprometer a qualidade do seu trabalho. Portanto, é importante utilizar o bom senso. </p>
		<p>Conversaremos, logo adiante, sobre recursos que podem ser incorporados no seu texto. Enquanto estiver lendo os tópicos, tente vislumbrar possibilidades de aplicação na aula que você escreverá ao final deste estudo. Tudo bem?</p>  	
	  </div>
	</div>

	
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

