<?php 
//Configurar Aula
$aula = 6; 
$pagina = 17; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
       <div style="text-align:left"><h3 class="titulo titulo-primary">6.6 Um pouco sobre direitos autorais na EaD</h3></div>
       <div class="row">
		<div class="col-lg-6">
			<p>Pelo apresentado no vídeo, devemos ter especial cuidado com a utilização de imagens. Comentamos a esse respeito no tópico 6.2. </p>
			<p>Outro ponto importante, que merece nossa atenção, é o <span class="semi-bold">plágio</span> (falamos rapidamente sobre esse assunto na Aula 5, no item referente ao desenvolvimento da aula). </p>
			<p>Na cartilha <a href="http://www.noticias.uff.br/arquivos/cartilha-sobre-plagio-academico.pdf" target="_blank" title="acesso a cartilha sobre plágio">“Nem tudo que parece é: entenda o que é plágio”</a>, elaborada pela Universidade Federal Fluminense, o plágio acadêmico: </p>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-10"><p class="fonteMenor" style="text-indent:0em">se configura quando um aluno retira, seja de livros ou da Internet, ideias, conceitos ou frases de outro autor (que as formulou e as publicou), sem lhe dar o devido crédito, sem citá-lo como fonte de pesquisa. Trata-se de uma violação dos direitos autorais de outrem. Isso tem implicações cíveis e penais. E o “desconhecimento da lei” não serve de desculpa, pois a lei é pública e explícita.</p></div>
			</div>	

			<p>Embora o trecho retrate a questão do plágio no meio universitário as explicações são totalmente aplicáveis ao caso do conteudista do STF. De igual modo, devemos citar as fontes dos autores que utilizamos no nosso material didático, visto que estamos sujeitos às sanções legais cabíveis em caso de violação. Então, é importante cuidado durante o processo de escrita.</p>
			<p>Em relação aos direitos autorais, no caso da elaboração de materiais didáticos para educação a distância no âmbito do Tribunal, cabe ressaltar que os conteudistas cedem esses direitos ao STF, conforme dispõe o artigo 10 da Instrução Normativa 169/2014:</p>
		</div>
		<div class="col-lg-6">

			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-10"><p class="fonteMenor" style="text-indent:0em"><span class="semi-bold">O servidor responsável pela elaboração</span> ou transposição do material didático fica obrigado a revisar o material desenvolvido pelo período de 2 (dois) anos, sem direito a nova remuneração, e <span class="semi-bold">deve ceder ao STF os direitos patrimoniais dos materiais produzidos</span>, conforme modelo disponível no servidor NORMAT e na intranet, após a conclusão da obra. (grifos nossos)</p></div>
			</div>	
			<p><a href="imagens/INSTRUTORIA_INTERNA_-_CESSAO_DE_DIREITOS_AUTORAIS.doc" target="_blank" title="acesse o termo de cessão de direitos autorais do STF">Clique aqui</a> para conhecer o Termo de Cessão de Direitos Autorais de Obra Intelectual.</p>
			<p>Isso significa que o STF detém os direitos de utilização do material produzido, mas o autor sempre será referenciado nos créditos de elaboração.</p>
			<p>Podemos dizer que a relação contratual entre o conteudista e a Suprema Corte privilegia os princípios da economicidade e do interesse público. Isso porque, com a cessão de direitos, o Tribunal tem a possibilidade de realizar diversas ofertas do curso para os seus servidores, sem a necessidade de remunerar o autor a cada edição. Além disso, pode compartilhar o curso com outros órgãos com os quais firmou acordo de cooperação, o que auxilia na gestão do conhecimento no âmbito do serviço público.</p>
			<p>Ademais, o trabalho de produção do material didático é realizado em parceria entre o conteudista e a equipe da Seção de Educação a Distância. Um depende do outro para a concretização de um produto de qualidade do ponto de vista conceitual, pedagógico e gráfico. Portanto, a cessão de direitos faz todo o sentido para a Administração Pública.</p>
			<p>De todo o exposto, esperamos que você, conteudista, diversifique a sua escrita e estabeleça conexões significativas no texto. Contudo, sempre tome o cuidado de resguardar os direitos dos autores que ajudaram a compor o seu trabalho.</p>
		</div>

	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

