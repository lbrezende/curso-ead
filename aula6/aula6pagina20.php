<?php 
//Configurar Aula
$aula = 6; 
$pagina = 20; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div><h3 class="titulo titulo-primary">REFERÊNCIAS</h3></div>
	<p style="text-indent:0em;">CARDOSO, Carlos Adriano. <span class="semi-bold">O vídeo instrucional como recurso digital em educação a distância</span>.  Sociedade Brasileira de Metrologia (SBM). <span class="semi-bold">Revista Trilha Digital</span>, v. 1, n. 1, 2013. Disponível em: <<a href="http://editorarevistas.mackenzie.br/index.php/TDig/article/view/5888/4250" target="_blank" title="acesso a referencia">http://editorarevistas.mackenzie.br/index.php/TDig/article/view/5888/4250</a>>. Acesso em: ago. 2015.  </p>
	<p style="text-indent:0em;">FILATRO, Andrea. <span class="semi-bold">Design instrucional na prática</span>. São Paulo: Pearson Education do Brasil, 2008.  </p>
	<p style="text-indent:0em;">MATTAR, João. <span class="semi-bold">Design educacional:</span> educação a distância na prática. São Paulo: Artesanato Educacional, 2014.</p>
	<p style="text-indent:0em;">MATTAR, João. <span class="semi-bold">YouTube na educação:</span> o uso de vídeos em EAD, 2009.  Disponível em: <<a href="http://www.joaomattar.com/YouTube%20na%20Educa%C3%A7%C3%A3o%20o%20uso%20de%20v%C3%ADdeos%20em%20EaD.pdf" target="_blank" title="acesso a referencia">http://www.joaomattar.com/YouTube%20na%20Educa%C3%A7%C3%A3o%20o%20uso%20de%20v%C3%ADdeos%20em%20EaD.pdf</a>>. Acesso em: set. 2015.</p>
	<p style="text-indent:0em;">NILDO, N. <span class="semi-bold">A função da imagem na construção de material didático utilizado em EAD:</span> uma contribuição para a área de design instrucional. Disponível em:  <<a href="http://reposital.cuaed.unam.mx:8080/jspui/bitstream/123456789/1429/1/2-ProjetoVirtualEduca.pdf" target="_blank" title="acesso a referencia">http://reposital.cuaed.unam.mx:8080/jspui/bitstream/123456789/1429/1/2-ProjetoVirtualEduca.pdf</a>>. Acesso em: ago. 2015.</p>
	<p style="text-indent:0em;">OLIVEIRA, Débora S. <span class="semi-bold">O uso do vídeo em EaD:</span> desafios no processo de ensino aprendizagem. <span class="semi-bold">Revista Cesuca Virtual:</span> conhecimento sem fronteiras, v.1, n. 1, jul. 2013. Disponível em: <<a href="http://ojs.cesuca.edu.br/index.php/cesucavirtual/article/view/422/207" target="_blank" title="acesso a referencia">http://ojs.cesuca.edu.br/index.php/cesucavirtual/article/view/422/207</a>>. Acesso em: ago. 2015.</p> 
	<p style="text-indent:0em;">SANTOS, A. S. S.; Reis, G. T. de S. <span class="semi-bold">Por uma política de direitos autorais para a EaD.</span> Disponível em: <<a href="http://www.abed.org.br/congresso2007/tc/55200771442PM.pdf" target="_blank" title="acesso a referencia">http://www.abed.org.br/congresso2007/tc/55200771442PM.pdf</a>>. Acesso em: ago. 2015.</p>
	<p style="text-indent:0em;">SANTOS, W. <span class="semi-bold">A utilização de imagens na construção do material didático na EaD</span>. Disponível em: <<a href="http://geces.com.br/simposio/anais/anais-2012/Anais-229-240.pdf" target="_blank" title="acesso a referencia">http://geces.com.br/simposio/anais/anais-2012/Anais-229-240.pdf</a>>.  Acesso em: jul. 2015.</p>
	<p style="text-indent:0em;">SIMÃO NETO, Antônio. <span class="semi-bold">Cenários e modalidade de EAD.</span> 1. ed. Curitiba: IESDE Brasil, 2012.</p>
	<p style="text-indent:0em;">ZANETTI, Alexsandra. <span class="semi-bold">Elaboração de materiais para a educação a distância.</span> Disponível em: <<a href="http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf" target="_blank" title="acesso a referencia">http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf</a>>. Acesso em: jul. 2015.</p>
	<p style="text-indent:0em;">Imagens: <<a href="http://www.pixabay.com" target="_blank" title="acesso a referencia">http://www.pixabay.com</a>> Licença CCO Public Domain.</http:>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

