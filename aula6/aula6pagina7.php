<?php 
//Configurar Aula
$aula = 6; 
$pagina = 7; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
	<p>Com esse rol, percebemos que os elementos visuais podem ser utilizados de maneiras variadas no texto didático. O importante é manter o foco nos objetivos de aprendizagem e evitar imagens decorativas que não contribuem com a aprendizagem, como nos orienta Mattar (2014).</p>
	<p>Lembre-se da variedade de recursos com os quais você poderá trabalhar: fotografias, ilustrações, <a href="http://www.cienciasecognicao.org/pdf/v12/m347187.pdf" target="_blank">mapas conceituais</a>, gráficos, tabelas, pinturas, desenhos, tirinhas, charges, entre outros. </p>
	<p>Sugerimos alguns sites para busca de imagens:</p>
	<p><a href="http://www.pixabay.com" target="_blank" title="site para busca de imagens" class="btn btn-info btn-small" style="text-indent:0em;">http://www.pixabay.com</a></p>
	<p><a href="http://www.dreamstime.com/free-photos" target="_blank" title="site para busca de imagens" class="btn btn-info btn-small" style="text-indent:0em;">http://www.dreamstime.com/free-photos</a></p>
	<p><a class="btn btn-info btn-small" style="text-indent:0em;" href="http://www.freeimages.com" target="_blank" title="">http://www.freeimages.com</a></p>
	<p><a class="btn btn-info btn-small" style="text-indent:0em;" href="http://www.morguefile.com" target="_blank" title="">http://www.morguefile.com</a></p>
	<p><a class="btn btn-info btn-small" style="text-indent:0em;" href="http://www.gratisography.com" target="_blank" title="">http://www.gratisography.com</a></p>
	<p><a class="btn btn-info btn-small" style="text-indent:0em;" href="http://br.123rf.com/freeimages.php" target="_blank" title="">http://br.123rf.com/freeimages.php</a></p>
	<p><a class="btn btn-info btn-small" style="text-indent:0em;" href="http://freerangestock.com" target="_blank" title="">http://freerangestock.com</a></p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

