<?php 
//Configurar Aula
$aula = 6; 
$pagina = 13; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <div style="text-align:left"><h3 class="titulo titulo-primary">6.4 <em>Hyperlinks</em></h3></div>
	<div class="row">
		<div class="col-lg-6">
			<p>A internet é um universo de informações. Navegamos nela por meio de páginas eletrônicas, cada qual com o seu próprio endereço, denominado <em>link</em> ou <em>hyperlink</em>. </p>
			<p>O texto que produzimos para a educação a distância encontra-se na categoria de hipertexto, pelo fato de permitir conexões internas com o ambiente externo por meio de <em>links</em>.</p>
			<p>Filatro (2008, p. 93) aponta que “um dos grandes atrativos do hipertexto é o fato de ele apoiar um método natural de processamento da informação, que, ao tratar a informação por associação de idéias (<em>sic</em>) em vez de linearmente, assemelha-se muito ao funcionamento da mente humana”.</p>
			<p>Por isso, ao longo da sua produção textual, indique <em>links</em> que direcionem o aluno para páginas que você julgar relevantes e que, de alguma maneira, agreguem valor às informações apresentadas. </p>
		</div>
		<div class="col-lg-6">
        			<p>Veja algumas sugestões de uso de <em>hyperlinks</em> no material didático:</p>
			<ul>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="http://www.abed.org.br/congresso2003/docs/anais/TC42.pdf" target="_blank">Textos complementares sobre o assunto.</a></p></li>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="https://www.youtube.com/watch?v=iphEbL4KS2o" target="_blank">Entrevistas com profissionais da área.</a></p></li>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="http://michaelis.uol.com.br/moderno/portugues/index.php?lingua=portugues-portugues&palavra=hipertexto" target="_blank">Dicionários.</a></p></li>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="http://educacao.globo.com/portugues/assunto/estudo-do-texto/hipertexto.html" target="_blank"><em>Sites</em> para explicar termos.</a></p></li>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2014/lei/l12965.htm" target="_blank">Legislações.</a></p></li>
				<li style="margin-bottom:7px;" ><p style="text-indent:0em;"><a href="https://pt.wikipedia.org/wiki/Theodor_Nelson" target="_blank">Biografia de autores.</a></p></li>
			</ul>
			<div class="caixaLaranjaAula6">
				<p><span class="semi-bold">Alguns cuidados:</span> antes de entregar o seu material didático, certifique-se de que os links estejam funcionando corretamente. Se o endereço remeter a um texto, é interessante copiar o arquivo para o seu computador e salvar o endereço eletrônico para evitar que o link saia do ar e você perca a referência. Como em todos os recursos, recomendamos a cautela em relação à quantidade de <em>hyperlinks</em> no material.</p>
			</div>
		</div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

