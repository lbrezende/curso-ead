<?php 
//Configurar Aula
$aula = 6; 
$pagina = 2; 
$totalPaginas = 12;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', '6','1', '12', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div style="text-align:center"><h3 class="titulo titulo-primary">Olá! Chegamos à última aula!</h3></div>
	<p>Vivenciamos uma caminhada repleta de aprendizagens. Cada passo dado nos aproxima do nosso objetivo maior: produzir conteúdos para a educação a distância.</p>
	<p>Como você se sente ao participar deste projeto?</p>
	<p>Bom, antes de começarmos o nosso estudo final, vamos a uma breve retrospectiva:</p>
	[to-do] Leandro, não consegui arrumar as imagens do quebra cabeça pra caberem no espaço
	<div class="row">
		<div class="col-lg-4" style="background-image: url('imagens/quebraCabeca-1.png');background-repeat: no-repeat;"><p>Na Aula 1, conversamos um pouco a respeito dos aspectos históricos da educação a distância. Ela surgiu como prática educativa no século XVIII e evoluiu no tempo, com a utilização das correspondências, do rádio e da televisão, até chegar aos dias atuais, com o uso intensivo da internet. Dentro do STF não foi diferente, percebemos o crescimento da EaD no STF e, com isso, a necessidade de desenvolvermos materiais didáticos de qualidade, específicos para a modalidade.</p></div>
		<div class="col-lg-4"><p>Na Aula 2, compreendemos que o objetivo maior da produção de conteúdos na EaD é a aprendizagem dos alunos, por isso a importância de o autor conhecer os elementos fundamentais da Andragogia e como o tutor pode potencializar esse processo por meio do material.</p></div>
		<div class="col-lg-4"><p>Após a solidificação desses conhecimentos basilares, passamos ao planejamento do material didático (Aula 3). Entendemos a relevância dessa fase, pois precisamos saber aonde queremos chegar. Discutimos sobre cada um dos tópicos previstos no plano de elaboração do material didático e você teve a oportunidade de desenvolver o seu próprio planejamento.</p></div>
	</div>
	<div class="row">
		<div class="col-lg-4"><p>Já na Aula 4, nos voltamos à análise da escrita para a educação a distância. Percebemos que é necessário cuidado com a escolha das palavras, diante do fato de que nosso leitor, na atualidade, lê muito além de textos. Além disso, ressaltamos a importância da linguagem dialógica, tendo em vista a relação equânime entre os atores do processo educativo.</p></div>
		<div class="col-lg-4"><p>E, na recente Aula 5, para auxiliar na organização da escrita, tratamos da estrutura interna das aulas e seus pontos fundamentais: introdução, desenvolvimento, conclusão, atividades avaliativas e referências.</p></div>
		<div class="col-lg-4"><p>Aula 6 - Recursos para complementar o texto: - imagens - vídeos - hyperlinks - destaques no conteúdo e direitos autorais</p></div>
	</div>
	<div class="bloco-pontilhado" style="">
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Percebeu quantos conhecimentos construímos neste curso?</h3></div>
		<p>Agora, na Aula 6, vamos dialogar sobre a aplicação de recursos para complementar o seu material didático. Esperamos, com isso, que você seja capaz de utilizar elementos diferenciados para enriquecer a apresentação dos conteúdos que, por conseguinte, facilitem a aprendizagem dos alunos.</p>
		<p>Ao final, você terá a oportunidade de colocar a “mão da massa”. Isso mesmo. Na última atividade do curso, a proposta é que você escreva uma aula completa, tendo por base o conteúdo programático definido no Plano de Elaboração do Material Didático. </p>
		<p>Mas, antes disso, vamos aos estudos!</p>
	</div>

	<div style="text-align:center"><h3 class="titulo titulo-primary">Orientações iniciais</h3></div>
	<p>Com base em tudo o que estudamos ao longo do curso, percebemos que a educação a distância exige do autor o estabelecimento de diversas conexões no texto. Significa que precisamos associar recursos à escrita, com vistas a favorecer a construção do conhecimento pelos discentes e a tornar o conteúdo mais atrativo, interativo e interessante para o estudo <em>online</em>. </p>
	<p>A esse respeito, Filatro (2008) salienta que, “quando apresentada de maneira verbal e não verbal, a informação é mais facilmente processada pela memória do que quando apresentada de uma única forma”.</p>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Diante disso, como escolher os recursos para o seu material didático?</h3></div>
	<p>A seleção deve observar, primordialmente, os objetivos específicos de aprendizagem que você definiu na fase de planejamento. Devemos pensar da seguinte maneira, conforme nos orienta Zanetti (2009), citando Moore e Kearsley (2007):</p>
	<p>Quais elementos podem <span class="semi-bold">“contribuir de maneira mais significativa para o processo de ensino/aprendizagem”</span> dos alunos?</p>
	<p>Com esse foco, você pode começar a fazer algumas pesquisas. Existem muitas opções. Para facilitar um pouco o processo, sugerimos que você recorra, de início, às suas experiências pessoais. Por exemplo, se aprecia música, artes visuais ou literatura, procure algo nessas áreas que poderia contribuir para o entendimento dos conteúdos ou estimular reflexões sobre os temas das aulas.</p>
	<p>Além disso, busque recursos na internet que tenham relação com os assuntos a serem discutidos no material didático. Como o ciberespaço é vasto, avalie com cuidado a pertinência conceitual e contextual de cada elemento. Você também pode procurar em outros meios, fique à vontade. </p>
	<p>Perceba, porém, que não se trata de apenas “rechear fartamente” a sua produção.  Cada ferramenta selecionada precisa guardar estrita relação com o texto e com o conteúdo proposto. O excesso, aqui, pode comprometer a qualidade do seu trabalho. Portanto, é importante utilizar o bom senso. </p>
	<p>Conversaremos, logo adiante, sobre recursos que podem ser incorporados no seu texto. Enquanto estiver lendo os tópicos, tente vislumbrar possibilidades de aplicação na aula que você escreverá ao final deste estudo. Tudo bem?</p>

	<div style="text-align:center"><h3 class="titulo titulo-primary">Imagens</h3></div>
	<div class="row">
		<div class="col-lg-3 text-right"><img src="imagens/aspastop.jpg" style="margin-top:-50px;"></div>
		<div class="col-lg-6 text-left" style="padding-left:20px;"><h3 class="titulo titulo-primary">Uma imagem vale mais que mil palavras<p class="fonteMenor text-right">(Confúcio)</p></div>
		<div class="col-lg-3 text-left"><img src="imagens/aspasbottom.jpg" style="margin-top:50px"></div>
	</div>
	<p class="text-center"><img src="imagens/imagens.png" alt="imagens bem conhecidas do Superman, Monalisa, Coca-cola e bandeira do Brasil" style="max-width:400px;" /></p>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Essas imagens podem ser rapidamente identificadas por uma grande parcela de pessoas, não é mesmo?</h3></div>
	<p>Muitas vezes, as imagens representam tão bem situações, pensamentos, emoções, informações, marcas, eventos etc. que, de fato, valem mais que mil palavras, como nos sugere o filósofo.</p>
	<p>Cardoso (2013) assinala que: </p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p class="fonteMenor">Os estudos no campo da neurociência apontam que a metade do cérebro humano estaria comprometida com o processamento de imagens. Imagens têm acesso direto à memória de longo prazo, e cada imagem é armazenada com sua própria informação como um coerente bloco ou conceito, de forma que processamos a informação visual 60 mil vezes mais rápido do que o texto.</p></div>
	</div>
	<p>No caso da educação a distância, as imagens também podem ser grandes aliadas. Mayer (2009), citado por Mattar (2014, p.116), postula que a aprendizagem é mais significativa quando há a combinação de imagens e palavras. Lohr complementa, ainda segundo Mattar, que isso pode ser potencializado quando as figuras são relacionadas com a informação textual. </p>
	<div class="tituloEspecialEadAula6">
		<p><span class="semi-bold ">Em síntese:</span> Aplicar imagens que apresentam correlação com o texto auxilia o aluno a construir uma aprendizagem mais efetiva.</p>
	</div>
	<p>Nesse sentido, em relação aos materiais didáticos, as imagens podem ser utilizadas observando-se as seguintes funções instrucionais (ou pedagógicas), de acordo com Filatro (2008):</p>

    <div class="botao-andrea"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Andrea Filatro?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-andrea btn btn-info">Conheça Andrea FILATRO</a></p></div>
    <div class="conteudo-andrea" style="display:none">
      <p style="background-color: #30BEC2; border-radius:10px; color: white; padding: 20px;"><img src="imagens/andrea-filatro.jpg" alt="Filatro" style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
      <span class="semi-bold">Andrea FILATRO</span> é mestra e doutora em Educação pela Faculdade de Educação da Universidade de São Paulo. Formada em Pedagogia pela FE/USP e em Gestão de Projetos pela FIA/USP.
	  <br />Disponível em: <a href="https://www.pearson.com.br/autores.asp?pag_id=41&area_pai=41&id_p=&filtro=F" target="_blank">https://www.pearson.com.br/autores.asp?pag_id=41&area_pai=41&id_p=&filtro=F</a> </p>
     </div>

	<p><span class="semi-bold">a.  Decoração:</span> a intenção é motivacional, decorativa. Esse tipo de imagem pode ser utilizado em aberturas de estudos, apresentações de conceitos e pausas motivacionais. Exemplos: charges, banners, arte na abertura do curso.</p>
	<div class="row">
		<div class="col-lg-6"><p style="padding-top:150px;"><span class="semi-bold"<em>Exemplo no nosso curso:</em></span> Capa do material de estudo em PDF</p></div>
		<div class="col-lg-6"><a href="imagens/capa-aula6.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/capa-aula6.jpg" alt="capa da aula 6" class="img-responsiva"></a></div>
	</div>
	<div class="clear">&nbsp;</div>
	<p><span class="semi-bold">b.  Representação:</span> são imagens que “oferecem uma referência concreta, para tornar a informação verbal mais fácil e significativa (...) são apropria[das] para apresentar conceitos concretos e informação factual”. Exemplos: <em>captura de tela e fotografia de equipamento</em>. </p>
	<div class="row">
		<div class="col-lg-6"><p style="padding-top:150px;"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Captura de tela do Moodle</p></div>
		<div class="col-lg-6"><a href="imagens/tela-moodle.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/tela-moodle.jpg" alt="captura de tela do moodle" class="img-responsiva"></a></div>
	</div>
	<div class="clear">&nbsp;</div>
	<p><span class="semi-bold">c.  Organização (da informação):</span> demonstram relações qualitativas entre fatos, conceitos ou princípios. São usadas para explicar procedimentos, atributos (características de algo) e conceitos. Exemplos: <em>ilustrações de passo a passo, ilustrações que retratam relações entre fatos, conceitos ou princípios</em>.</p>
	<div class="row">
		<div class="col-lg-6"><p style="padding-top:50px;"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span></p></div>
		<div class="col-lg-6"><a href="imagens/esquema.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/esquema.jpg" alt="esquema entre aluno-material didático" class="img-responsiva"></a></div>
	</div>
	<div class="clear">&nbsp;</div>
	<p><span class="semi-bold">d.  Relacionais:</span> “transformam informação numérica em informação visual exibindo relações quantitativas entre duas ou mais variáveis”. Exemplos: <em>gráficos de barras ou de pizza</em>.</p>
	<div class="row">
		<div class="col-lg-6"><p style="padding-top:150px;"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Na Aula 1, nós apresentamos uma tabela com números que representavam o crescimento da oferta de cursos e de participantes ao longo dos anos.  A título de exemplo, vamos propor a transformação dessas informações em um gráfico. Veja:</p></div>
		<div class="col-lg-6">
			<a href="imagens/grafico1.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/grafico1.jpg" alt="grafico da quantidade de cursos ao longo dos anos" class="img-responsiva"></a>
			<br /><a href="imagens/grafico2.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/grafico2.jpg" alt="" class="img-responsiva"></a>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
	<p><span class="semi-bold">e.  Transformativa:</span> “mostram mudanças em procedimentos, processos e princípios ao longo do tempo ou espaço, comunicando movimento”.</p>
	<div class="row">
		<div class="col-lg-6"><p style="padding-top:350px;"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> linha do tempo com informações da aula 1.</p></div>
		<div class="col-lg-6">
			<a href="imagens/linha-tempo-1.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/linha-tempo-1.jpg" alt="Linha do tempo ead pelo mundo" class="img-responsiva"></a>
			<br /><a href="imagens/linha-tempo-2.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/linha-tempo-2.jpg" alt="Linha do tempo ead no Brasil" class="img-responsiva"></a>
		</div>
	</div>
	<div class="clear">&nbsp;</div>

	<p>Com esse rol, percebemos que os elementos visuais podem ser utilizados de maneiras variadas no texto didático. O importante é manter o foco nos objetivos de aprendizagem e evitar imagens decorativas que não contribuem com a aprendizagem, como nos orienta Mattar (2014).</p>
	<p>Lembre-se da variedade de recursos com os quais você poderá trabalhar: fotografias, ilustrações, <a href="http://www.cienciasecognicao.org/pdf/v12/m347187.pdf" target="_blank">mapas conceituais</a>, gráficos, tabelas, pinturas, desenhos, tirinhas, charges, entre outros. </p>
	<p>Sugerimos alguns sites para busca de imagens:</p>
	<p><a href="http://www.pixabay.com" target="_blank" title="site para busca de imagens">http://www.pixabay.com</a></p>
	<p><a href="http://www.dreamstime.com/free-photos" target="_blank" title="site para busca de imagens">http://www.dreamstime.com/free-photos</a></p>
	<p><a href="http://www.freeimages.com" target="_blank" title="">http://www.freeimages.com</a></p>
	<p><a href="http://www.morguefile.com" target="_blank" title="">http://www.morguefile.com</a></p>
	<p><a href="http://www.gratisography.com" target="_blank" title="">http://www.gratisography.com</a></p>
	<p><a href="http://br.123rf.com/freeimages.php" target="_blank" title="">http://br.123rf.com/freeimages.php</a></p>
	<p><a href="http://freerangestock.com" target="_blank" title="">http://freerangestock.com</a></p>
	<p>Na sua prática como conteudista do STF, algumas observações são importantes em relação ao uso de imagens, vejamos: </p>
	<p><span class="semi-bold">1.</span>  Ao pesquisar na internet, fique atento às licenças das imagens : </p>
	<p class="semi-bold text-center">Tipos de licença</p>
	<p class="semi-bold"><span style="background-color:#EB6A44;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">a</span> Royalty Free</p>
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-11"><p class="">São imagens sem direito controlado, o tipo mais popular de licença em bancos de imagens. Você pode escolher e comprar na hora a imagem em formato digital, para qualquer tipo de uso (exceto para pornografia, difamação, violência e algumas outras restrições, dependendo do fornecedor). Sobre a veiculação, o tempo é indeterminado, ficando por conta do comprador. No entanto, a imagem não é produzida exclusivamente para o cliente, isto é, outra pessoa pode utilizá-la. O que pode ser vantajoso no caso de verbas reduzidas, em que pagar a produção de uma foto com manipulação seria inviável.</p></div>
	</div>
	<p class="semi-bold"><span style="background-color:#EABC67;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">b</span> Rights-managed (Direito controlado)</p>
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-11">
			<p>Neste caso, existe um acordo entre o comprador e o banco de imagem em relação ao uso do produto, cujo preço final pode variar de acordo com prazo, território, finalidade e tipo de mídia na qual será veiculado. </p>
			<p>A grande vantagem do direito controlado é que, enquanto você está utilizando a imagem, esta fica bloqueada para outros clientes do mesmo segmento.</p>
		</div>
	</div>
	<p class="semi-bold"><span style="background-color:#4EC0BF;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">c</span> Creative Commons</p>
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-11">
			<p>Creative-Commons (CC) é uma organização sem fins lucrativos fundada em 2001 na Califórnia com o objetivo de ajudar a categorizar o uso de imagens sem direitos reservados. Em outras palavras, caso tenha tirado determinada foto e publicado em uma rede social, como poderia certificar-se de que sua imagem não será usada por outra pessoa?</p>
			<p>Os principais tipos de licenças CC são:</p>
			<p style="text-indent:0em;"><img src="imagens/ccby.png" class="img-responsiva" style="max-width:100px;"><span  class="semi-bold"> Atribuição CC BY (Attribution)</span>  <br>
			Qualquer pessoa ou marca pode usar, mixar, editar, distribuir sua imagem, inclusive comercialmente, contanto que os créditos sejam citados. </p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/ccby-nd.png" class="img-responsiva" style="max-width:100px;"><span class="semi-bold"> Atribuição-SemDerivações CC BY ND (Attribution-NoDerivs)</span><br>
			O uso comercial, não comercial e a disseminação são liberados, desde que a imagem não sofra modificações e os créditos sejam citados. </p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/ccby-nc-sa.png" class="img-responsiva" style="max-width:100px;"><span class="semi-bold"> Atribuição-NãoComercial-CompartilhaIgual CC BY-NC-SA (Attribution-NonCommercial-ShareAlike)</span><br />
			Permite que outros remixem e usem sua imagem para criar outras imagens não comerciais, contanto que citem os créditos e cadastrem as novas imagens criadas sob os mesmos termos.</p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/ccby-sa.png" class="img-responsiva" style="max-width:100px;"><span class="semi-bold"> Atribuição-CompartilhaIgual (CC BY-SA) (Attribution-ShareAlike)</span><br />
			Esta licença permite que outros remixem, adaptem e criem obras derivadas, ainda que para fins comerciais, desde que atribuam crédito ao autor e licenciem as novas criações sob os mesmos termos. Esta licença é geralmente comparada àquelas usadas em software livre e aberto (copyleft). Por exemplo, é a licença utilizada pela Wikipedia.</p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/ccby-nc.png" class="img-responsiva" style="max-width:100px;"><span class="semi-bold"> Atribuição- NãoComercial CC BY-NC (Attribution-NonCommercial)</span>  <br />
			 Permite que outros remixem, adaptem e criem obras de forma não comercial. Apesar de suas novas obras também precisarem ser não comerciais e conterem créditos, os autores não têm de licenciar seus trabalhos derivados nos mesmos termos.</p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/ccby-nc-nd.png" class="img-responsiva" style="max-width:100px;"><span class="semi-bold"> Atribuição-SemDerivações-SemDerivados CC BY-NC-ND (Attribution-NonCommercial-NoDerivs)</span> <br />
			A mais restritiva de todas as outras licenças, só permite o download da imagem e o compartilhamento com créditos. Não permite mudanças nem uso comercial.</p>
			<br>
			<p style="text-indent:0em;"><img src="imagens/public.png" class="img-responsiva" style="max-height:40px;"><span class="semi-bold"> Domínio Público</span><br />
			São obras de livre uso, pois não estão sob domínio de direito autoral. Os direitos autorais duram até 70 anos contando do primeiro ano de falecimento do autor. </p>		</div>
	</div>

	<p><span class="semi-bold">Observação:</span> As imagens utilizadas ao longo do nosso curso que foram extraídas de sites da internet estão licenciadas em creative commons, na categoria CC BY. Os demais materiais visuais foram produzidos pela equipe de diagramação da Seção de Educação a Distância do STF.</p>

	<p><span class="semi-bold">2.</span> O “Google Imagens” apresenta inúmeros resultados de busca, porém não garante imagens livres de proteção de direitos autorais. Então, evite indicar materiais desse canal de pesquisa.</p>
	<p><span class="semi-bold">3.</span>  Registre o endereço eletrônico de onde a imagem foi retirada no próprio arquivo da aula que estiver escrevendo. E, quando possível, indique o autor da imagem.</p>
	<p><span class="semi-bold">4.</span> Você pode criar suas próprias imagens, caso tenha habilidade específica para isso. Agora, se não dominar essa arte, descreva com detalhes a imagem que deseja inserir no seu texto. No momento da diagramação, realizada pela equipe Educação a Distância, serão analisadas as possibilidades de se elaborar as peças gráficas com base nas suas explicações.</p>

	<div class="text-center"><h3 class="titulo titulo-primary">Vídeos</h3></div>
	<p class="semi-bold text-center">Você gosta de cinema?</p>
	<p class="semi-bold text-center">Existe alguma cena de filme que você considera inesquecível?</p>
	<div class="bloco-pontilhado" style="">
	  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
	  <p style="text-align:center; font-weight:bold;">Assista ao vídeo “20 cenas marcantes do cinema” e veja se encontra coincidências:</p>
	  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
	    <iframe width="420" height="315" src="https://www.youtube.com/embed/87sVqDJewv4" frameborder="0" allowfullscreen></iframe>                    
	  </div>
	</div>
	<p>Além do cinema, podemos perceber que as mídias audiovisuais estão presentes em nossas vidas de maneira muito intensa. Hoje, quase tudo é gravado. Parece que estamos em um constante filme da vida real. O avanço das tecnologias tem colaborado com esse processo e as redes sociais aceleram a divulgação e facilitam o compartilhamento desses conteúdos. </p>
	<h3 class="text-center titulo titulo-secondary">Mas por que os vídeos exercem tanto fascínio sobre nós? </h3>
	<p>A autora Débora Oliveira (2013) nos ajuda a responder esse questionamento ao registrar as seguintes palavras de Moran (1995):</p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p>O vídeo é sensorial, visual, linguagem falada, linguagem musical e escrita. (...) o vídeo nos seduz, informa, entretém, projeta outras realidades (no imaginário), em outros tempos e espaços. O vídeo combina a comunicação sensorial-cinestésica com a audiovisual, a intuição com a lógica, a emoção com a razão. Combina, mas começa pelo sensorial, pelo emocional e pelo intuitivo, para atingir posteriormente o racional.</p></div>
	</div>
	<p>Com fundamento nessa contribuição, podemos afirmar que a utilização de vídeos nos materiais didáticos pode ser enriquecedor para o aluno, além de respeitar os diferentes estilos de aprendizagem e as múltiplas inteligências, segundo Mattar (2009). Ademais, o autor destaca que muitos alunos aprendem melhor quando entram em contato com estímulos visuais e sonoros.</p>
	<p>Ao acompanhar essa perspectiva, Simão Neto (2012) revela algumas vantagens de se empregar vídeos na educação a distância, vejamos:</p>
	<ul>
		<li style="margin-bottom:7px;" ><span class="semi-bold">Riqueza de estímulos audiovisuais</span>, tornando essas mídias mais atrativas para os estudantes.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Dinamicidade</span>– as mídias audiovisuais são moventes e dinâmicas, abrindo múltiplos caminhos para a comunicação e a educação, possibilitados pela dimensão do movimento.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Familiaridade</span> do aluno com a linguagem audiovisual, especialmente a da TV, presente na vida cotidiana de pessoas de todas as idades e classes sociais.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Estímulos auditivo e visual integrados</span> – importantes para a aprendizagem de muitas pessoas para as quais tanto a inteligência “auditiva” quanto a “visual” representam papel essencial na aprendizagem.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Memória auditivo/visual</span> – da mesma forma, os estímulos audiovisuais ajudam na retenção de informações.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Diversidade</span> – inúmeros formatos e linguagens podem ser adotados para a produção audiovisual para a EAD, especialmente nos dias de hoje, quando contamos com recursos de edição digital.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Facilidade</span> de manuseio da tecnologia de recepção e de reprodução.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Grande potencial</span> comunicativo da mídia audiovisual, envolvendo não somente a razão, mas também a emoção e a sensibilidade.</li>
	</ul>
	<p>O exposto até aqui ratifica o entendimento de que os vídeos são grandes aliados do conteudista. Você, autor, pode realizar buscas de mídias na internet conforme os assuntos a serem desenvolvidos em suas aulas. Outra alternativa é a gravação de vídeos pelo próprio conteudista. </p>
	<p>Mencionaremos algumas formas de aplicação, no texto didático, de vídeos encontrados no <em>YouTube</em>. Observe exemplos utilizados no nosso curso:</p>
	<ul>
		<li>Para <span class="semi-bold">aprofundar</span> algum assunto.
			<p><span class="semi-bold">Aula 1.</span> Contextualização da Educação a Distância: </p>
			<p>Vídeo sobre o contexto do <a href="https://www.youtube.com/watch?v=Mz0gtsk0gCs" target="_blank">Projeto Minerva</a>.</p>

		</li>
		<li>Para <span class="semi-bold">sintetizar os conteúdos</span> abordados.
			<p><span class="semi-bold">Aula 1.</span> Contextualização da Educação a Distância:</p>
			<p>Vídeo do <a href="https://www.youtube.com/watch?v=5OKybl7vTqo" target="_blank">Jornal Nacional</a>, apresentado para sintetizar a evolução histórica da EaD.</p>
		</li>
		<li>Para apresentar <span class="semi-bold">a visão de autores renomados</span> na área.
			<p><span class="semi-bold">Aula 2.</span> Aspectos relacionados à aprendizagem a distância: </p>
			<p>Vídeo de <a href="https://www.youtube.com/watch?v=bUjWkm30nGw" target="_blank">entrevista</a> com a professora Daniela Melaré, sobre os estilos de aprendizagem.</p>
		</li>
		<li>Para <span class="semi-bold">inspirar reflexões</span> sobre o tema.
			<p><span class="semi-bold">Aula 3.</span> Por que planejar a escrita?: </p>
			<p>Vídeo “<a href="https://www.youtube.com/watch?v=ISt-Dx7nBNE" target="_blank">Alice no país das maravilhas</a>” para refletir sobre os caminhos que devemos recorrer para escrever materiais didáticos consistentes.</p>
		</li>
	</ul>


	<p>Essas são apenas algumas das possibilidades de uso, que não se esgotam aqui. Você pode pensar em outras maneiras de agregar vídeos ao texto, de acordo com as especificidades temáticas do seu curso. </p>
	<p><span class="semi-bold">Observação:</span> A professora Filatro (2008) recomenda que evitemos vídeos longos, que exigem períodos extensos em frente ao computador, uma vez que a experiência dos navegantes da web é altamente interativa. </p>
	
	<div class="caixaAmarelaAula6">
		<p>Para finalizar a nossa conversa sobre as mídias audiovisuais, temos algumas orientações:</p>
		<p>● Quando selecionar algum vídeo para inserir em suas aulas, assista com atenção, de preferência mais de uma vez antes de decidir incluí-lo no material didático. </p>
		<p>● Reflita: Essa mídia é significativa? Atende aos meus objetivos didáticos? O vídeo demanda muito tempo da atenção do aluno? Apresenta qualidade visual (boas imagens) e auditiva (o volume é adequado)?</p>
		<p>● Coloque-se no lugar do aluno e tente pensar como ele se sentiria ao assistir ao vídeo. </p>	
	</div>
	<p>O importante é você compreender que os vídeos são recursos didáticos com grande potencial para fomentar aprendizagens significativas e, ainda, para diversificar o seu material. Certo? </p>
	<div class="text-center"><h3 class="titulo titulo-primary">Hyperlinks</h3></div>

	<p>A internet é um universo de informações. Navegamos nela por meio de páginas eletrônicas, cada qual com o seu próprio endereço, denominado <em>link</em> ou <em>hyperlink</em>. </p>
	<p>O texto que produzimos para a educação a distância encontra-se na categoria de hipertexto, pelo fato de permitir conexões internas com o ambiente externo por meio de <em>links</em>.</p>
	<p>Filatro (2008, p. 93) aponta que “um dos grandes atrativos do hipertexto é o fato de ele apoiar um método natural de processamento da informação, que, ao tratar a informação por associação de idéias (<em>sic</em>) em vez de linearmente, assemelha-se muito ao funcionamento da mente humana”.</p>
	<p>Por isso, ao longo da sua produção textual, indique <em>links</em> que direcionem o aluno para páginas que você julgar relevantes e que, de alguma maneira, agreguem valor às informações apresentadas. </p>
	<p>Veja algumas sugestões de uso de <em>hyperlinks</em> no material didático:</p>
	<ul>
		<li style="margin-bottom:7px;" ><a href="http://www.abed.org.br/congresso2003/docs/anais/TC42.pdf" target="_blank">Textos complementares sobre o assunto.</a></li>
		<li style="margin-bottom:7px;" ><a href="https://www.youtube.com/watch?v=iphEbL4KS2o" target="_blank">Entrevistas com profissionais da área.</a></li>
		<li style="margin-bottom:7px;" ><a href="http://michaelis.uol.com.br/moderno/portugues/index.php?lingua=portugues-portugues&palavra=hipertexto" target="_blank">Dicionários.</a></li>
		<li style="margin-bottom:7px;" ><a href="http://educacao.globo.com/portugues/assunto/estudo-do-texto/hipertexto.html" target="_blank"><em>Sites</em> para explicar termos.</a></li>
		<li style="margin-bottom:7px;" ><a href="http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2014/lei/l12965.htm" target="_blank">Legislações.</a></li>
		<li style="margin-bottom:7px;" ><a href="https://pt.wikipedia.org/wiki/Theodor_Nelson" target="_blank">Biografia de autores.</a></li>
	</ul>
	<div class="caixaLaranjaAula6">
		<p><span class="semi-bold">Alguns cuidados:</span> antes de entregar o seu material didático, certifique-se de que os links estejam funcionando corretamente. Se o endereço remeter a um texto, é interessante copiar o arquivo para o seu computador e salvar o endereço eletrônico para evitar que o link saia do ar e você perca a referência. Como em todos os recursos, recomendamos a cautela em relação à quantidade de hyperlinks no material.</p>
	</div>

	<div class="text-center"><h3 class="titulo titulo-primary">Destaques no conteúdo</h3></div>

	<p>Durante o processo de escrita, além da inserção de recursos diversos, muitas vezes desejamos chamar a atenção do leitor para algum ponto do texto. Logo pensamos em <span class="semi-bold">negrito</span> ou <em>itálico</em>, não é mesmo? Essas são boas opções, mas podemos pensar em outras maneiras. </p>
	<p>Mattar (2014, p. 127) registra que:</p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p class="fonteMenor">Formas simples (retângulos, quadrados, triângulos, linhas etc.) (…) podem se tornar poderosos recursos para transmitir mensagens visuais. Formas podem ser usadas para indicar unidade, processo, sistemas, implicar harmonia, focar a atenção, conter informação, facilitar comparações, mostrar hierarquia, separar e definir, traçar conexões, denotar emoções e volume, mostrar movimento e direção e tornar uma imagem atraente, dentre outras funções.</p></div>
	</div>

	<p>Como o autor sugere, você também pode utilizar formas simples para destacar determinada informação. Por exemplo: colocar dentro de um retângulo a definição de um conceito, o artigo de uma norma ou uma frase que sintetiza um pensamento; utilizar setas para mostrar o passo a passo de situações, entre outras. </p>
	<p>Ainda que você não domine a arte do desenho gráfico, expresse no seu texto como espera que a informação seja apresentada. Isso facilita o trabalho de diagramação visual que será realizado no seu material. </p>
	<p>Além desses destaques, podemos inserir no texto seções específicas para organizar visualmente as informações, de acordo com o tipo de conteúdo. Geralmente, quando se repetem ao longo do material, associamos esses espaços a um ícone, para facilitar a identificação pelo aluno. </p>
	<p><span class="semi-bold">Exemplos:</span> </p>

	<div class="bloco-pontilhado" style="margin-top:40px" class="">
	  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
	      <p><span class="semi-bold">“Saiba mais”:</span> O objetivo é a indicação de materiais complementares ou sites para o aprofundamento do estudo. No nosso curso, utilizamos essa seção em vários momentos, relembre o ícone.</p>
	    </div>
	  </div>
	</div>
	<div class="bloco-pontilhado" style="margin-top:40px" class="">
	  <img src="imagens/refletir.jpg" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
	      <p><span class="semi-bold">“Para refletir”:</span> Questões propostas para estimular a reflexão sobre pontos específicos do conteúdo.</p>
	    </div>
	  </div>
	</div>
	<div class="bloco-pontilhado" style="margin-top:40px" class="">
	  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
	      <p><span class="semi-bold">“Dica”:</span> Indicação de situações/questões que poderão facilitar a ação do estudante de alguma maneira.</p>
	    </div>
	  </div>
	</div>
	<div class="bloco-pontilhado" style="margin-top:40px" class="">
	  <img src="imagens/curiosidade.jpg" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
	      <p><span class="semi-bold">“Curiosidade”:</span> Utilizada para apontar aspectos interessantes/lúdicos do conteúdo.</p>
	    </div>
	  </div>
	</div>

	<p>Você pode pensar em outras seções, de acordo com as necessidades do seu material didático. O importante é ajudar a guiar a leitura do aluno, de maneira que ele perceba como as informações estão organizadas.</p>

	<div class="text-center"><h3 class="titulo titulo-primary">Um pouco sobre direitos autorais na EaD</h3></div>
	<p>Após conversarmos a respeito dos recursos que podem enriquecer a produção textual educativa, devemos alertá-lo sobre a importância dos direitos autorais. Nossa intenção não é aprofundar a análise, mas, sobretudo, pretendemos ressaltar o impacto dessa questão na prática dos conteudistas.</p>
	<p>No Brasil, a legislação sobre os direitos autorais encontra-se consolidada na <a href="http://www.planalto.gov.br/ccivil_03/leis/L9610.htm" target="_blank" title="acesso a lei 9610">Lei 9.610/1998</a>. O principal objetivo da norma é proteger a expressão das ideias e garantir aos autores o direito exclusivo sobre a reprodução dos seus trabalhos.</p>
	<p>Para esclarecer pontos importantes do assunto, recomendamos a leitura do texto <a href="http://www.aprendebrasil.com.br/pesquisa/swf/DireitoAutoral.pdf" target="_blank" tile="acesso a apostila direito autoral">“Entenda o Direito Autoral”</a> – que possui uma abordagem bastante acessível –, escrito pela Doutora Flávia Lubieska N. Kischelewski, com direitos cedidos exclusivamente para @Positivo Informática S. A. </p>
	<div class="bloco-pontilhado" style="">
	  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
	  <p style="text-align:center; font-weight:bold;">No caso da educação a distância, o pesquisador Paulo Rená nos auxilia a compreender as repercussões dos direitos autorais no campo específico dos materiais didáticos na entrevista.</p>
	  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
	    <iframe width="420" height="315" src="https://www.youtube.com/embed/GrGkP4Crjww" frameborder="0" allowfullscreen></iframe>                    
	  </div>
	</div>

	<p>Pelo apresentado no vídeo, devemos ter especial cuidado com a utilização de imagens. Comentamos a esse respeito no tópico 6.2. </p>
	<p>Outro ponto importante, que merece nossa atenção, é o <span class="semi-bold">plágio</span> (falamos rapidamente sobre esse assunto na Aula 5, no item referente ao desenvolvimento da aula). </p>
	<p>Na cartilha <a href="http://www.noticias.uff.br/arquivos/cartilha-sobre-plagio-academico.pdf" target="_blank" title="acesso a cartilha sobre plágio">“Nem tudo que parece é: entenda o que é plágio”</a>, elaborada pela Universidade Federal Fluminense, o plágio acadêmico </p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p class="fonteMenor">se configura quando um aluno retira, seja de livros ou da Internet, ideias, conceitos ou frases de outro autor (que as formulou e as publicou), sem lhe dar o devido crédito, sem citá-lo como fonte de pesquisa. Trata-se de uma violação dos direitos autorais de outrem. Isso tem implicações cíveis e penais. E o “desconhecimento da lei” não serve de desculpa, pois a lei é pública e explícita.</p></div>
	</div>	

	<p>Embora o trecho retrate a questão do plágio no meio universitário as explicações são totalmente aplicáveis ao caso do conteudista do STF. De igual modo, devemos citar as fontes dos autores que utilizamos no nosso material didático, visto que estamos sujeitos às sanções legais cabíveis em caso de violação. Então, é importante cuidado durante o processo de escrita.</p>
	<p>Em relação aos direitos autorais, no caso da elaboração de materiais didáticos para educação a distância no âmbito do Tribunal, cabe ressaltar que os conteudistas cedem esses direitos ao STF, conforme dispõe o artigo 10 da Instrução Normativa 169/2014:</p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p class="fonteMenor"><span class="semi-bold">O servidor responsável pela elaboração</span> ou transposição do material didático fica obrigado a revisar o material desenvolvido pelo período de 2 (dois) anos, sem direito a nova remuneração, e <span class="semi-bold">deve ceder ao STF os direitos patrimoniais dos materiais produzidos</span>, conforme modelo disponível no servidor NORMAT e na intranet, após a conclusão da obra. (grifos nossos)</p></div>
	</div>	
	<p><a href="https://goo.gl/887TtV" target="_blank" title="acesse o termo de cessão de direitos autorais do STF">Clique aqui</a> para conhecer o Termo de Cessão de Direitos Autorais de Obra Intelectual.</p>
	<p>Isso significa que o STF detém os direitos de utilização do material produzido, mas o autor sempre será referenciado nos créditos de elaboração.</p>
	<p>Podemos dizer que a relação contratual entre o conteudista e a Suprema Corte privilegia os princípios da economicidade e do interesse público. Isso porque, com a cessão de direitos, o Tribunal tem a possibilidade de realizar diversas ofertas do curso para os seus servidores, sem a necessidade de remunerar o autor a cada edição. Além disso, pode compartilhar o curso com outros órgãos com os quais firmou acordo de cooperação, o que auxilia na gestão do conhecimento no âmbito do serviço público.</p>
	<p>Ademais, o trabalho de produção do material didático é realizado em parceria entre o conteudista e a equipe da Seção de Educação a Distância. Um depende do outro para a concretização de um produto de qualidade do ponto de vista conceitual, pedagógico e gráfico. Portanto, a cessão de direitos faz todo o sentido para a Administração Pública.</p>
	<p>De todo o exposto, esperamos que você, conteudista, diversifique a sua escrita e estabeleça conexões significativas no texto. Contudo, sempre tome o cuidado de resguardar os direitos dos autores que ajudaram a compor o seu trabalho.</p>

	<div class="text-center"><h3 class="titulo titulo-primary">CONSIDERAÇÕES FINAIS</h3></div>
	<p>Nesta aula, dialogamos sobre a importância de se utilizar recursos para enriquecer a apresentação do material didático, com vistas a potencializar a concretização da aprendizagem dos alunos. Vimos, também, algumas possibilidades de destaques no conteúdo para auxiliar a leitura dos alunos.  Por fim, lançamos uma breve reflexão sobre os direitos autorais na educação a distância.</p>
	<p>Encerramos também, neste momento, a jornada de aprendizagens do curso de <em>Produção de Conteúdos para a EaD</em>. Esperamos que tenha sido uma oportunidade relevante para você.</p>
	<p>Selecionamos os assuntos com a intenção de preparar um sólido alicerce para a construção do seu material. Passo a passo, da contextualização da EaD até a utilização de recursos para complementar o texto, você entrou em contato com as principais nuances que envolvem a escrita de materiais didáticos.</p>
	<p>Sabemos que foi o começo. As possibilidades não se exaurem aqui. Lançamos várias sementes e desejamos que floresçam em breve: esperamos poder contar com você, conteudista, como nosso parceiro no crescimento da educação a distância do STF. </p>
	<p>Ainda são muitos os desafios, mas acreditamos que a produção de materiais por servidores da nossa própria instituição é um caminho fundamental para a consolidação de estudos mais contextualizados à nossa realidade organizacional.</p>
	<p>Foi um prazer contar com a sua participação! </p>
	<p>Para fechar o curso, vamos à nossa atividade avaliativa final, descrita logo na sequência.</p>

	<div class="text-center"><h3 class="titulo titulo-primary">Vamos Praticar</h3></div>
	<p>Na nossa última tarefa, você deverá aplicar todos os conhecimentos construídos ao longo do curso. A proposta é: <span class="semi-bold">escrever o texto de uma aula completa</span>, tendo por base o Plano de Elaboração do Material Didático que você desenvolveu na Aula 3.</p>
	<p>Pontos a serem observados na sua produção textual: </p>
	<ul>
		<li style="margin-bottom:7px;">Especificidades da educação a distância (Aula 1).</li>
		<li style="margin-bottom:7px;">Escrita com foco no aluno, leitor do seu material (Aula 2).</li>
		<li style="margin-bottom:7px;">Observância dos objetivos desenhados no planejamento (Aula 3).</li>
		<li style="margin-bottom:7px;">Emprego da linguagem dialógica (Aula 4).</li>
		<li style="margin-bottom:7px;">Estrutura interna das aulas (Aula 5).</li>
		<li style="margin-bottom:7px;">Aplicação de recursos para complementar o texto e utilizar destaques no conteúdo (Aula 6).</li>
	</ul>

	<p>Compreendido? Então, é hora de praticar! </p>
	<p>Se surgirem dúvidas durante o desenvolvimento da atividade, o tutor estará à disposição para auxiliá-lo.</p>
	<p class="text-center semi-bold" style="color:#638BAB;">Um excelente trabalho!</p>
	
	<div class="text-center"><h3 class="titulo titulo-primary">REFERÊNCIAS</h3></div>
	<p>CARDOSO, Carlos Adriano. <span class="semi-bold">O vídeo instrucional como recurso digital em educação a distância</span>.  Sociedade Brasileira de Metrologia (SBM). <span class="semi-bold">Revista Trilha Digital</span>, v. 1, n. 1, 2013. Disponível em: <<a href="http://editorarevistas.mackenzie.br/index.php/TDig/article/view/5888/4250" target="_blank" title="acesso a referencia">http://editorarevistas.mackenzie.br/index.php/TDig/article/view/5888/4250</a>>. Acesso em: ago. 2015.  </p>
	<p>FILATRO, Andrea. <span class="semi-bold">Design instrucional na prática</span>. São Paulo: Pearson Education do Brasil, 2008.  </p>
	<p>MATTAR, João. <span class="semi-bold">Design educacional:</span> educação a distância na prática. São Paulo: Artesanato Educacional, 2014.</p>
	<p>MATTAR, João. <span class="semi-bold">YouTube na educação:</span> o uso de vídeos em EAD, 2009.  Disponível em: <<a href=""http://www.joaomattar.com/YouTube%20na%20Educa%C3%A7%C3%A3o%20o%20uso%20de%20v%C3%ADdeos%20em%20EaD.pdf target="_blank" title="acesso a referencia">http://www.joaomattar.com/YouTube%20na%20Educa%C3%A7%C3%A3o%20o%20uso%20de%20v%C3%ADdeos%20em%20EaD.pdf</a>>. Acesso em: set. 2015.</p>
	<p>NILDO, N. <span class="semi-bold">A função da imagem na construção de material didático utilizado em EAD:</span> uma contribuição para a área de design instrucional. Disponível em:  <<a href="http://reposital.cuaed.unam.mx:8080/jspui/bitstream/123456789/1429/1/2-ProjetoVirtualEduca.pdf" target="_blank" title="acesso a referencia">http://reposital.cuaed.unam.mx:8080/jspui/bitstream/123456789/1429/1/2-ProjetoVirtualEduca.pdf</a>>. Acesso em: ago. 2015.</p>
	<p>OLIVEIRA, Débora S. <span class="semi-bold">O uso do vídeo em EaD:</span> desafios no processo de ensino aprendizagem. <span class="semi-bold">Revista Cesuca Virtual:</span> conhecimento sem fronteiras, v.1, n. 1, jul. 2013. Disponível em: <<a href="http://ojs.cesuca.edu.br/index.php/cesucavirtual/article/view/422/207" target="_blank" title="acesso a referencia">http://ojs.cesuca.edu.br/index.php/cesucavirtual/article/view/422/207</a>>. Acesso em: ago. 2015.</p> 
	<p>SANTOS, A. S. S.; Reis, G. T. de S. <span class="semi-bold">Por uma política de direitos autorais para a EaD.</span> Disponível em: <<a href="http://www.abed.org.br/congresso2007/tc/55200771442PM.pdf" target="_blank" title="acesso a referencia">http://www.abed.org.br/congresso2007/tc/55200771442PM.pdf</a>>. Acesso em: ago. 2015.</p>
	<p>SANTOS, W. <span class="semi-bold">A utilização de imagens na construção do material didático na EaD</span>. Disponível em: <<a href="http://geces.com.br/simposio/anais/anais-2012/Anais-229-240.pdf" target="_blank" title="acesso a referencia">http://geces.com.br/simposio/anais/anais-2012/Anais-229-240.pdf</a>>.  Acesso em: jul. 2015.</p>
	<p>SIMÃO NETO, Antônio. <span class="semi-bold">Cenários e modalidade de EAD.</span> 1. ed. Curitiba: IESDE Brasil, 2012.</p>
	<p>ZANETTI, Alexsandra. <span class="semi-bold">Elaboração de materiais para a educação a distância.</span> Disponível em: <<a href="http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf" target="_blank" title="acesso a referencia">http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf</a>>. Acesso em: jul. 2015.</p>
	<p>Imagens: <<a href="http://www.pixabay.com" target="_blank" title="acesso a referencia">http://www.pixabay.com</a>> Licença CCO Public Domain.</http:>
  </div>
</div>
<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

