<?php 
//Configurar Aula
$aula = 6; 
$pagina = 9; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
	<p><span class="semi-bold">Observação:</span> As imagens utilizadas ao longo do nosso curso que foram extraídas de sites da internet estão licenciadas em <a href="https://br.creativecommons.org/sobre/" target="_blank" title="link para o site sobre creative commons">creative commons</a>, na categoria CC BY. Os demais materiais visuais foram produzidos pela equipe de diagramação da Seção de Educação a Distância do STF.</p>

	<p><span class="semi-bold">2.</span> O “Google Imagens” apresenta inúmeros resultados de busca, porém não garante imagens livres de proteção de direitos autorais. Então, evite indicar materiais desse canal de pesquisa.</p>
	<p><span class="semi-bold">3.</span>  Registre o endereço eletrônico de onde a imagem foi retirada no próprio arquivo da aula que estiver escrevendo. E, quando possível, indique o autor da imagem.</p>
	<p><span class="semi-bold">4.</span> Você pode criar suas próprias imagens, caso tenha habilidade específica para isso. Agora, se não dominar essa arte, descreva com detalhes a imagem que deseja inserir no seu texto. No momento da diagramação, realizada pela equipe da Seção de Educação a Distância, serão analisadas as possibilidades de se elaborar as peças gráficas com base nas suas explicações.</p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

