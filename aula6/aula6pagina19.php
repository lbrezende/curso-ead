<?php 
//Configurar Aula
$aula = 6; 
$pagina = 19; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div><h3 class="titulo titulo-primary">VAMOS PRATICAR</h3></div>
	<p>Na nossa última tarefa, você deverá aplicar todos os conhecimentos construídos ao longo do curso. A proposta é: <span class="semi-bold">escrever o texto de uma aula completa</span>, tendo por base o Plano de Elaboração do Material Didático que você desenvolveu na Aula 3.</p>
	<p>Pontos a serem observados na sua produção textual: </p>
	<ul>
		<li style="margin-bottom:7px;">Especificidades da educação a distância (Aula 1).</li>
		<li style="margin-bottom:7px;">Escrita com foco no aluno, leitor do seu material (Aula 2).</li>
		<li style="margin-bottom:7px;">Observância dos objetivos desenhados no planejamento (Aula 3).</li>
		<li style="margin-bottom:7px;">Emprego da linguagem dialógica (Aula 4).</li>
		<li style="margin-bottom:7px;">Estrutura interna das aulas (Aula 5).</li>
		<li style="margin-bottom:7px;">Aplicação de recursos para complementar o texto e utilizar destaques no conteúdo (Aula 6).</li>
	</ul>

	<p class="text-center">Compreendido? Então, é hora de praticar! </p>
	<p>Se surgirem dúvidas durante o desenvolvimento da atividade, o tutor estará à disposição para auxiliá-lo.</p>
	<p class="text-center semi-bold" style="color:#638BAB; font-size:20px"">Um excelente trabalho!</p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

