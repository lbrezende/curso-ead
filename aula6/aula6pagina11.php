<?php 
//Configurar Aula
$aula = 6; 
$pagina = 11; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
      	<div style="text-align:left"><h3 class="titulo titulo-primary">6.3 Vídeos</h3></div>
	<p>Com fundamento nessa contribuição, podemos afirmar que a utilização de vídeos nos materiais didáticos pode ser enriquecedor para o aluno, além de respeitar os diferentes estilos de aprendizagem e as múltiplas inteligências, segundo Mattar (2009). Ademais, o autor destaca que muitos alunos aprendem melhor quando entram em contato com estímulos visuais e sonoros.</p>
	<p>Ao acompanhar essa perspectiva, Simão Neto (2012) revela algumas vantagens de se empregar vídeos na educação a distância, vejamos:</p>
	<ul>
		<li style="margin-bottom:7px;" ><span class="semi-bold">Riqueza de estímulos audiovisuais</span>, tornando essas mídias mais atrativas para os estudantes.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Dinamicidade</span>– as mídias audiovisuais são moventes e dinâmicas, abrindo múltiplos caminhos para a comunicação e a educação, possibilitados pela dimensão do movimento.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Familiaridade</span> do aluno com a linguagem audiovisual, especialmente a da TV, presente na vida cotidiana de pessoas de todas as idades e classes sociais.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Estímulos auditivo e visual integrados</span> – importantes para a aprendizagem de muitas pessoas para as quais tanto a inteligência “auditiva” quanto a “visual” representam papel essencial na aprendizagem.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Memória auditivo/visual</span> – da mesma forma, os estímulos audiovisuais ajudam na retenção de informações.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Diversidade</span> – inúmeros formatos e linguagens podem ser adotados para a produção audiovisual para a EAD, especialmente nos dias de hoje, quando contamos com recursos de edição digital.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Facilidade</span> de manuseio da tecnologia de recepção e de reprodução.</li>
		<li style="margin-bottom:7px;"><span class="semi-bold">Grande potencial</span> comunicativo da mídia audiovisual, envolvendo não somente a razão, mas também a emoção e a sensibilidade.</li>
	</ul>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

