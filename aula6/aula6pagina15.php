<?php 
//Configurar Aula
$aula = 6; 
$pagina = 15; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
            <div style="text-align:left"><h3 class="titulo titulo-primary">6.5 Destaques no conteúdo</h3></div>
	<p><span class="semi-bold">Exemplos:</span> </p>
	<div class="row">
		<div class="col-lg-6">
			<div class="bloco-pontilhado" style="margin-top:40px;margin-bottom:50px;" class="">
			  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:10px; padding:10px">
			      <p><span class="semi-bold">“Saiba mais”:</span> O objetivo é a indicação de materiais complementares ou sites para o aprofundamento do estudo. No nosso curso, utilizamos essa seção em vários momentos.</p>
			    </div>
			  </div>
			</div>
			<div class="bloco-pontilhado" style="margin-top:40px;margin-bottom:20px;" class="">
			  <img src="imagens/refletir.jpg" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:10px; padding:10px">
			      <p><span class="semi-bold">“Para refletir”:</span> Questões propostas para estimular a reflexão sobre pontos específicos do conteúdo.</p>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="bloco-pontilhado" style="margin-top:40px;margin-bottom:50px;" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:10px; padding:10px">
			      <p><span class="semi-bold">“Dica”:</span> Indicação de situações/questões que poderão facilitar a ação do estudante de alguma maneira.</p>
			    </div>
			  </div>
			</div>
			<div class="bloco-pontilhado" style="margin-top:80px" class="">
			  <img src="imagens/curiosidade.jpg" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:10px; padding:10px">
			      <p><span class="semi-bold">“Curiosidade”:</span> Utilizada para apontar aspectos interessantes/lúdicos do conteúdo.</p>
			    </div>
			  </div>
			</div>
		</div>
	</div>

	<p>Você pode pensar em outras seções, de acordo com as necessidades do seu material didático. O importante é ajudar a guiar a leitura do aluno, de maneira que ele perceba como as informações estão organizadas.</p>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

