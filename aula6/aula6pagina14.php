<?php 
//Configurar Aula
$aula = 6; 
$pagina = 14; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <div style="text-align:left"><h3 class="titulo titulo-primary">6.5 Destaques no conteúdo</h3></div>
    <div class="row">
    	<div class="col-lg-6">
			<p>Durante o processo de escrita, além da inserção de recursos diversos, muitas vezes desejamos chamar a atenção do leitor para algum ponto do texto. Logo pensamos em <span class="semi-bold">negrito</span> ou <em>itálico</em>, não é mesmo? Essas são boas opções, mas podemos pensar em outras maneiras. </p>
			<p>Mattar (2014, p. 127) registra que:</p>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-10"><p class="fonteMenor" style="text-indent:0em">Formas simples (retângulos, quadrados, triângulos, linhas etc.) (…) podem se tornar poderosos recursos para transmitir mensagens visuais. Formas podem ser usadas para indicar unidade, processo, sistemas, implicar harmonia, focar a atenção, conter informação, facilitar comparações, mostrar hierarquia, separar e definir, traçar conexões, denotar emoções e volume, mostrar movimento e direção e tornar uma imagem atraente, dentre outras funções.</p></div>
			</div>
		</div>
		<div class="col-lg-6">
			<p>Como o autor sugere, você também pode utilizar formas simples para destacar determinada informação. Por exemplo: colocar dentro de um retângulo a definição de um conceito, o artigo de uma norma ou uma frase que sintetiza um pensamento; utilizar setas para mostrar o passo a passo de situações, entre outras. </p>
			<p>Ainda que você não domine a arte do desenho gráfico, expresse no seu texto como espera que a informação seja apresentada. Isso facilita o trabalho de diagramação visual que será realizado no seu material. </p>
			<p>Além desses destaques, podemos inserir no texto seções específicas para organizar visualmente as informações, de acordo com o tipo de conteúdo. Geralmente, quando se repetem ao longo do material, associamos esses espaços a um ícone, para facilitar a identificação pelo aluno. </p>
		</div>
	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

