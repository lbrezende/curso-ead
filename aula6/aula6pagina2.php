<?php 
//Configurar Aula
$aula = 6; 
$pagina = 2; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div class="bloco-pontilhado" style="">
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Percebeu quantos conhecimentos construímos neste curso?</h3></div>
		<p style="text-indent:1.3em !important;">Agora, na Aula 6, vamos dialogar sobre a aplicação de recursos para complementar o seu material didático. Esperamos, com isso, que você seja capaz de utilizar elementos diferenciados para enriquecer a apresentação dos conteúdos que, por conseguinte, facilitem a aprendizagem dos alunos.</p>
		<p style="text-indent:1.3em !important;">Ao final, você terá a oportunidade de colocar a “mão na massa”. Isso mesmo. Na última atividade do curso, a proposta é que você escreva uma aula completa, tendo por base o conteúdo programático definido no Plano de Elaboração do Material Didático. </p>
		<p style="text-indent:1.3em !important;">Mas, antes disso, vamos aos estudos!</p>
	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

