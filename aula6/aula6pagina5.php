<?php 
//Configurar Aula
$aula = 6; 
$pagina = 5; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
        <p>Nesse sentido, em relação aos materiais didáticos, as imagens podem ser utilizadas observando-se as seguintes funções instrucionais (ou pedagógicas), de acordo com Filatro (2008):</p>
	    <div class="botao-andrea"><h3 class="titulo titulo-secondary" style="text-align:center; margin-top:20px">Já conhece Andrea Filatro?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-andrea btn btn-info">Conheça Andrea Filatro</a></p></div>
	    <div class="conteudo-andrea" style="display:none">
	      <p style="background-color: #FBB570; border-radius:10px; color: #127575; padding: 20px; text-indent:0em"><img src="imagens/andrea-filatro.jpg" alt="Filatro" style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
	      <span class="semi-bold">Andrea Filatro</span><br /><span style="color:#555859" class="semi-bold"> É mestra e doutora em Educação pela Faculdade de Educação da Universidade de São Paulo. Formada em Pedagogia pela FE/USP e em Gestão de Projetos pela FIA/USP.
		  <br /><span class="fonteMenor">Disponível em: <a href="https://www.pearson.com.br/autores.asp?pag_id=41&area_pai=41&id_p=&filtro=F" target="_blank">https://www.pearson.com.br/autores.asp?pag_id=41&area_pai=41&id_p=&filtro=F</a></span></span> </p>
	    </div>
	    <div class="clear"></div>
		<div class="row">
			<div class="col-lg-6">
				<p><span class="semi-bold">a.  Decoração:</span> a intenção é motivacional, decorativa. Esse tipo de imagem pode ser utilizado em aberturas de estudos, apresentações de conceitos e pausas motivacionais. Exemplos: <em>charges, banners, arte na abertura do curso</em>.</p>
              <p class="text-center" style="text-indent:0em; margin-top:20px"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Capa do material de estudo em PDF</p>
			  <p class="text-center" style="text-indent:0em; margin-top:25px"><a href="imagens/capa-aula6.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/capa-aula6.jpg" alt="capa da aula 6" class="img-responsiva"></a></p>
			</div>
			<div class="col-lg-6">
				<p><span class="semi-bold">b.  Representação:</span> são imagens que “oferecem uma referência concreta, para tornar a informação verbal mais fácil e significativa (...) são apropria[das] para apresentar conceitos concretos e informação factual”. Exemplos: <em>captura de tela e fotografia de equipamento</em>.				</p>
                <p class="text-center" style="text-indent:0em; margin-top:20px"><span class="semi-bold"><em>Exemplo no nosso curso:</em></span> Captura de tela do Moodle</p>
				<p class="text-center" style="text-indent:0em; margin-top:25px"><a href="imagens/tela-moodle.jpg" target="_blank" title="link para aumentar a imagem"><img src="imagens/tela-moodle.jpg" alt="captura de tela do moodle" class="img-responsiva"></a></p>
			</div>
		</div>
	</div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

