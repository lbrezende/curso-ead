<?php 
//Configurar Aula
$aula = 6; 
$pagina = 1; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
  $paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
  $paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div style="text-align:center"><h3 class="titulo titulo-primary">Olá! Chegamos à última aula!</h3></div>
	<p>Vivenciamos uma caminhada repleta de aprendizagens. Cada passo dado nos aproxima do nosso objetivo maior: produzir conteúdos para a educação a distância.</p>
	<p>Como você se sente ao participar deste projeto?</p>
	<p>Bom, antes de começarmos o nosso estudo final, vamos a uma breve retrospectiva dos principais aspectos abordados nas aulas:</p>
	<div class=" text-center"><img src="imagens/quebraCabeca.png"  alt="Aula 1:aspectos historicos, ead e formação continuada, crescimento da ead no stf; Aula 2: aprendizagem do aluno, andragogia, estilos de aprendizagem, tutor; Aula3:planejamento, aonde queremos chegar, articulação; Aula4: comunicação na ead, uso de palavras, linguagem dialógica; Aula 5:organização de material, estrutura das aulas; Aula 6: recursos para complementar o texto" class="img-responsiva" /></div>
	<!--<div class="row">
	  <div class="col-lg-4" style="text-align:center;"><img src="imagens/aula1-2.png"  alt="Na Aula 1, conversamos sobre aspectos históricos nos meios de veiculação (correspondências, rádio, tv e internet), sobre a EaD e formação continuada e o crescimento da EaD no STF"></div>
		<div class="col-lg-4" style="text-align:center;"><img src="imagens/aula2-1.png"  alt="Na Aula 2, conversamos sobre aprendizagem do aluno (foco no material didático), andragogia, estilos de aprendizagem e tutor (porte entre o material e o aluno)"></div>
		<div class="col-lg-4" style="text-align:center;"><img src="imagens/aula3-2.png"  alt="Na Aula 3, conversamos sobre planejamento, aonde queremos chegar com a elaboração do material didático, articulação entre os principais tópicos do planejamento"></div>
	</div>
	<div class="row">
		<div class="col-lg-4" style="text-align:center;"><img src="imagens/aula4.png"  alt="Na Aula 1, conversamos sobre aspectos históricos nos meios de veiculação (correspondências, rádio, tv e internet), sobre a EaD e formação continuada e o crescimento da EaD no STF"></div>
		<div class="col-lg-4" style="text-align:center;"><img src="imagens/aula5.png"  alt="Na Aula 2, conversamos sobre aprendizagem do aluno (foco no material didático), andragogia, estilos de aprendizagem e tutor (porte entre o material e o aluno)"></div>
		<div class="col-lg-4" style="text-align:center;"><img src="imagens/aula6.png"  alt="Na Aula 3, conversamos sobre planejamento, aonde queremos chegar com a elaboração do material didático, articulação entre os principais tópicos do planejamento"></div>
	</div>-->

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'fim', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

