<?php 
//Configurar Aula
$aula = 6; 
$pagina = 18; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div><h3 class="titulo titulo-primary">CONSIDERAÇÕES FINAIS</h3></div>
	<p>Nesta aula, dialogamos sobre a importância de se utilizar recursos para enriquecer a apresentação do material didático, com vistas a potencializar a concretização da aprendizagem dos alunos. Vimos, também, algumas possibilidades de destaques no conteúdo para auxiliar a leitura dos alunos.  Por fim, lançamos uma breve reflexão sobre os direitos autorais na educação a distância.</p>
	<p>Encerramos também, neste momento, a jornada de aprendizagens do curso de <em>Produção de Conteúdos para a EaD</em>. Esperamos que tenha sido uma oportunidade relevante para você.</p>
	<p>Selecionamos os assuntos com a intenção de preparar um sólido alicerce para a construção do seu material. Passo a passo, da contextualização da EaD até a utilização de recursos para complementar o texto, você entrou em contato com as principais nuances que envolvem a escrita de materiais didáticos.</p>
	<p>Sabemos que foi o começo. As possibilidades não se exaurem aqui. Lançamos várias sementes e desejamos que floresçam em breve: esperamos poder contar com você, conteudista, como nosso parceiro no crescimento da educação a distância do STF. </p>
	<p>Ainda são muitos os desafios, mas acreditamos que a produção de materiais por servidores da nossa própria instituição é um caminho fundamental para a consolidação de estudos mais contextualizados à nossa realidade organizacional.</p>
	<p>Foi um prazer contar com a sua participação! </p>
	<p>Para fechar o curso, vamos à nossa atividade avaliativa final, descrita logo na sequência.</p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

