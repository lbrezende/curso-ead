<?php 
//Configurar Aula
$aula = 6; 
$pagina = 10; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    	<div style="text-align:left"><h3 class="titulo titulo-primary">6.3 Vídeos</h3></div>
  	<div class="row">
	  	<div class="col-lg-6">
			<p class="semi-bold text-center">Você gosta de cinema?</p>
			<p class="semi-bold text-center">Existe alguma cena de filme que você considera inesquecível?</p>
			<div class="bloco-pontilhado" style="">
			  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
			  <p style="text-align:center; font-weight:bold;">Assista ao vídeo “20 cenas marcantes do cinema” e veja se encontra coincidências:</p>
			  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
			    <iframe width="420" height="315" src="https://www.youtube.com/embed/87sVqDJewv4" frameborder="0" allowfullscreen></iframe>                    
			  </div>
			</div>
	  	</div>
	  	<div class="col-lg-6">
	  		<p>Além do cinema, podemos perceber que as mídias audiovisuais estão presentes em nossas vidas de maneira muito intensa. Hoje, quase tudo é gravado. Parece que estamos em um constante filme da vida real. O avanço das tecnologias tem colaborado com esse processo e as redes sociais aceleram a divulgação e facilitam o compartilhamento desses conteúdos. </p>
			<h3 class="text-center titulo titulo-secondary" style="margin-top:20px">Mas por que os vídeos exercem tanto fascínio sobre nós? </h3>
			<p>A autora Débora Oliveira (2013) nos ajuda a responder esse questionamento ao registrar as seguintes palavras de Moran (1995):</p>
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-10"><p style="text-indent:0em" class="fonteMenor">O vídeo é sensorial, visual, linguagem falada, linguagem musical e escrita. (...) o vídeo nos seduz, informa, entretém, projeta outras realidades (no imaginário), em outros tempos e espaços. O vídeo combina a comunicação sensorial-cinestésica com a audiovisual, a intuição com a lógica, a emoção com a razão. Combina, mas começa pelo sensorial, pelo emocional e pelo intuitivo, para atingir posteriormente o racional.</p></div>
			</div>

	  	</div>

  	</div>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

