<?php 
//Configurar Aula
$aula = 6; 
$pagina = 16; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div class="row">
	  <div class="col-lg-6">
          <div style="text-align:left"><h3 class="titulo titulo-primary">6.6 Um pouco sobre direitos autorais na EaD</h3></div>
		<p>Após conversarmos a respeito dos recursos que podem enriquecer a produção textual educativa, devemos alertá-lo sobre a importância dos direitos autorais. Nossa intenção não é aprofundar a análise, mas, sobretudo, pretendemos ressaltar o impacto dessa questão na prática dos conteudistas.</p>
		<p>No Brasil, a legislação sobre os direitos autorais encontra-se consolidada na <a href="http://www.planalto.gov.br/ccivil_03/leis/L9610.htm" target="_blank" title="acesso a lei 9610">Lei 9.610/1998</a>. O principal objetivo da norma é proteger a expressão das ideias e garantir aos autores o direito exclusivo sobre a reprodução dos seus trabalhos.</p>
		<p>Para esclarecer pontos importantes do assunto, recomendamos a leitura do texto <a href="http://www.aprendebrasil.com.br/pesquisa/swf/DireitoAutoral.pdf" target="_blank" tile="acesso a apostila direito autoral">“Entenda o Direito Autoral”</a> – que possui uma abordagem bastante acessível –, escrito pela Doutora Flávia Lubieska N. Kischelewski, com direitos cedidos exclusivamente para @Positivo Informática S. A.</p>
		<p>No caso da educação a distância, o pesquisador Paulo Rená nos auxilia a compreender as repercussões dos direitos autorais no campo específico dos materiais didáticos na entrevista ao lado.</p>
	  </div>
	  <div class="col-lg-6">
		<div class="bloco-pontilhado" style="">
		  <p style="margin-top:20px"><img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;"></p>
		  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
		    <iframe width="420" height="315" src="https://www.youtube.com/embed/GrGkP4Crjww" frameborder="0" allowfullscreen></iframe>                    
		  </div>
		</div>
	  	
	  </div>
	</div>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

