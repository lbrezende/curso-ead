<?php 
//Configurar Aula
$aula = 6; 
$pagina = 8; 
$totalPaginas = 20;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Utilização de recursos para complementar o texto', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<div style="text-align:left"><h3 class="titulo titulo-primary">6.2 Imagens</h3></div>
  	<div class="row">
	  <div class="col-lg-6">
	  		<p>Na sua prática como conteudista do STF, algumas observações são importantes em relação ao uso de imagens, vejamos: </p>
			<p><span class="semi-bold">1.</span>  Ao pesquisar na internet, fique atento às licenças das imagens<span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span></p>
			<div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  ">Fonte: <a href="http://www.cutedrop.com.br/2013/07/entendendo-as-licencas-de-uso-de-imagens/" target="_blank">http://www.cutedrop.com.br/2013/07/entendendo-as-licencas-de-uso-de-imagens/</a>  Com adaptações.</div> 
			<p class="semi-bold text-center">Tipos de licença</p>
			<p class="semi-bold"><!--<span style="background-color:#EB6A44;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">a</span>--><img src="imagens/letraA.png" /> <em>Royalty Free</em></p>
			<div class="row">
				<div class="col-lg-1"></div>
				<div class="col-lg-11"><p class="" style="text-indent:0em">São imagens sem direito controlado, o tipo mais popular de licença em bancos de imagens. Você pode escolher e comprar na hora a imagem em formato digital, para qualquer tipo de uso (exceto para pornografia, difamação, violência e algumas outras restrições, dependendo do fornecedor). Sobre a veiculação, o tempo é indeterminado, ficando por conta do comprador. No entanto, a imagem não é produzida exclusivamente para o cliente, isto é, outra pessoa pode utilizá-la. O que pode ser vantajoso no caso de verbas reduzidas, em que pagar a produção de uma foto com manipulação seria inviável.</p></div>
			</div>
			<p class="semi-bold"><!--<span style="background-color:#EABC67;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">b</span>--><img src="imagens/letraB.png" /> <em>Rights-managed</em> (Direito controlado)</p>
			<div class="row">
				<div class="col-lg-1"></div>
				<div class="col-lg-11">
					<p style="text-indent:0em">Neste caso, existe um acordo entre o comprador e o banco de imagem em relação ao uso do produto, cujo preço final pode variar de acordo com prazo, território, finalidade e tipo de mídia na qual será veiculado. </p>
					<p style="text-indent:0em">A grande vantagem do direito controlado é que, enquanto você está utilizando a imagem, esta fica bloqueada para outros clientes do mesmo segmento.</p>
				</div>
			</div>
	  		<p class="semi-bold"><!--<span style="background-color:#4EC0BF;border-radius: 100px;border: 5px solid #fff;padding:10px;font-family:Century Gothic;">c</span>--><img src="imagens/letraC.png" /> <em>Creative Commons</em></p>
			<div class="row">
				<div class="col-lg-1"></div>
				<div class="col-lg-11">
					<p style="text-indent:0em">Creative-Commons (CC) é uma organização sem fins lucrativos fundada em 2001 na Califórnia com o objetivo de ajudar a categorizar o uso de imagens sem direitos reservados. Em outras palavras, caso tenha tirado determinada foto e publicado em uma rede social, como poderia certificar-se de que sua imagem não será usada por outra pessoa?</p>
				</div>
			</div>

	  </div>
	  <div class="col-lg-6">
	  	<p style="text-indent:0em">Os principais tipos de licenças CC são:</p>
		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span  class="semi-bold"> Atribuição CC BY</span> (<em>Attribution</em>)</div>
			</div>
		Qualquer pessoa ou marca pode usar, mixar, editar, distribuir sua imagem, inclusive comercialmente, contanto que os créditos sejam citados. </p>			
		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby-nd.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span class="semi-bold"> Atribuição-SemDerivações CC BY ND</span> (<em>Attribution-NoDerivs</em>)</div>
			</div>
		O uso comercial, não comercial e a disseminação são liberados, desde que a imagem não sofra modificações e os créditos sejam citados. </p>
		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby-nc-sa.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span class="semi-bold"> Atribuição-NãoComercial-CompartilhaIgual CC BY-NC-SA</span> (<em>Attribution-NonCommercial-ShareAlike</em>)</div>
			</div>
		Permite que outros remixem e usem sua imagem para criar outras imagens não comerciais, contanto que citem os créditos e cadastrem as novas imagens criadas sob os mesmos termos.</p>

		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby-sa.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span class="semi-bold"> Atribuição-CompartilhaIgual (CC BY-SA)</span> (<em>Attribution-ShareAlike</em>)</div>
			</div>
		Esta licença permite que outros remixem, adaptem e criem obras derivadas, ainda que para fins comerciais, desde que atribuam crédito ao autor e licenciem as novas criações sob os mesmos termos. Esta licença é geralmente comparada àquelas usadas em software livre e aberto (copyleft). Por exemplo, é a licença utilizada pela Wikipedia.</p>

		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby-nc.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span class="semi-bold"> Atribuição- NãoComercial CC BY-NC</span> (<em>Attribution-NonCommercial</em>)</div>
			</div>
		Permite que outros remixem, adaptem e criem obras de forma não comercial. Apesar de suas novas obras também precisarem ser não comerciais e conterem créditos, os autores não têm de licenciar seus trabalhos derivados nos mesmos termos.</p>

		<p style="text-indent:0em;">
			<div class="row" style="margin-bottom:5px;">
				<div class="col-lg-3"><img src="imagens/ccby-nc-nd.png" class="img-responsiva" style="max-width:100px;"></div>
				<div class="col-lg-9"><span class="semi-bold"> Atribuição-SemDerivações-SemDerivados CC BY-NC-ND </span>(<em>Attribution-NonCommercial-NoDerivs</em>)</div>
			</div>
		A mais restritiva de todas as outras licenças, só permite o download da imagem e o compartilhamento com créditos. Não permite mudanças nem uso comercial.</p>

		<p style="text-indent:0em;"><img src="imagens/public.png" class="img-responsiva" style="max-height:40px;"><span class="semi-bold"> Domínio Público</span><br />
		São obras de livre uso, pois não estão sob domínio de direito autoral. Os direitos autorais duram até 70 anos contando do primeiro ano de falecimento do autor. </p>
	  </div>
	 </div>
	</div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

