<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','11', '15', 'aula2pagina10.php', 'aula2pagina12.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>

		<p>Voltando às considerações sobre o tema:</p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual a importância desse assunto na elaboração de materiais didáticos para a EaD? </h3> </div>		
		<p>Primeiro, se aprendemos de maneiras diversas, podemos concluir que não conseguiremos despertar os processos que desencadeiam a aprendizagem nos diferentes tipos de sujeitos escrevendo materiais pautados apenas em uma proposta.</p>
		<p>Segundo, os estilos de aprendizagem são preferências, portanto, não são determinantes. É interessante, por conseguinte, proporcionar oportunidades para que os alunos experimentem atividades diversas daquelas pelas quais têm maior inclinação; tentar fazer com que mobilizem outras estruturas mentais. </p>

    <div class="col-lg-6">
    <p>Logo, o principal desafio é: utilizar, na escrita dos materiais didáticos, estratégias e recursos que a virtualidade nos oferece para ampliar as possibilidades de concretização da aprendizagem nos alunos, e não apenas focar em atividades vinculadas especificamente a um estilo de aprendizagem. Para tanto, precisamos ir além do texto linear.</p>
        <div style="text-align:center"><h3 class="titulo titulo-secondary">Como assim? </h3> </div>
        <p>Além do texto em si, precisamos compor o material com recursos complementares – <i class="italico">hiperlinks</i>, vídeos, músicas e outras mídias, ilustrações e imagens –, efetuar destaques de conteúdo, inserir questionamentos, indicar referências para aprofundamento, enfim, o que a sua criatividade permitir. </p>
        <p>Devemos diversificar a produção. Todavia, sem excessos, pois é importante selecionar aquilo que, de fato, agrega valor às informações apresentadas.</p>
        <p>Para terminar nossa conversa sobre os estilos de aprendizagem, assista a dois trechos de uma entrevista com a professora Daniela Melaré, concedida à <i class="italico">TV FIB</i> – Programa “Diálogos do Saber”. Como o vídeo é extenso, recortamos os momentos em que a docente retrata o tema, mas fique à vontade se desejar assistir ao vídeo por completo: </p>
  

    </div> 

    <div class="col-lg-6">
        <div class="bloco-pontilhado" style="">
          <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
          <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
            
    <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                    <p class="textAlignCenter">
                    <video width="100%" height="" controls>
                      <source src="videos/aula2_video_dialogos_do_saber.mp4" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                    </p>
          <div >
            <p>Você pode acessar a versão completa do vídeo clicando <a href="https://youtu.be/bUjWkm30nGw" target="_blank">aqui.</a></p>
          </div>
              </div>
            </div>   
           </div>
        </div>
    </article>  


          </div>
        </div>  
    </div>  

    <div class="clear"></div>

    <div class="col-lg-12">
        <div class="bloco-pontilhado" style="" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; font-weight:bold;">No site indicado a seguir, encontramos um livro digital gratuito
que trata do tema na atualidade, em diversos aspectos:   <br><a href="http://estilosdeaprendizagem-vol01.blogspot.com.br/ " target="_blank" class="btn btn-info " style="text-align:center; text-indent:0em; margin-top:20px; ">Acesse Estilos de Aprendizagem</a> </p>
          <div style="text-align:center">
            Vale a pena navegar por ele!<br /><br />
          </div>
        </div>    
    </div>

    <div class="clear" ></div>

		<p style="margin-top:30px"> Em relação ao que vimos até o momento, um personagem fundamental na educação a distância poderá nos auxiliar a facilitar os processos de aprendizagem: o tutor. Vamos dialogar um pouco sobre ele no próximo tópico.</p>


  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina10.php', 'aula2pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
