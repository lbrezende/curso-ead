<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','14', '15', 'aula2pagina13.php', 'aula2pagina15.php', 'Produção de Conteúdos para a EaD');
?> 
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Considerações Finais</h3>
		<p>Nesta aula, conversamos sobre a aprendizagem de adultos e suas peculiaridades, assim como o papel central do aluno em relação à aprendizagem. Vimos também que somos pessoas diferentes, portanto, possuímos estilos de aprendizagem diversos. Por fim, observamos que o tutor será parceiro do conteudista, no sentido de tentar “traduzir” para os alunos aquilo que foi delineado na nossa escrita. Em todos os casos, refletimos acerca da influência dos temas na escrita de materiais educativos para a EaD. </p>
		<p>Finalizamos também, aqui, o Módulo I, no qual tratamos de questões relacionadas à contextualização da EaD e à aprendizagem, nossos pontos de partida.</p>
		<p>Solidificados esses assuntos, seguiremos para o Módulo II, em que estudaremos o planejamento da escrita dos materiais didáticos na educação a distância do STF.</p>
		<p style="text-align:center">Encontro vocês no próximo módulo, então!</p>
		<p style="text-align:center"><strong>Até breve!</strong></p>

    <h3 class="titulo titulo-primary">Vamos praticar</h3>
		<p>Como foi a experiência de utilizar o diário para anotar, no Moodle, as suas observações sobre a Aula 1?</p>
		<p>Novamente, convidamos você a registrar no espaço <strong>“Meu percurso de Aprendizagem”</strong>, agora, referente à Aula 2, tudo aquilo que considerou relevante nesta aula.</p>
		<p>Aguardamos suas impressões lá no nosso AVA.</p>
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula2pagina13.php', 'aula2pagina15.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
