<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','1', '15', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<?php $i = 1; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Olá Novamente,</h3>

		<p>Na aula anterior, conversamos sobre o histórico da EaD no cenário mundial e no Brasil, bem como as características da modalidade nos dias atuais.</p>
		<p>Consolidadas essas informações, estudaremos, nesta segunda aula, questões que versam sobre aprendizagem na educação a distância. </p>
		<p>Você, conteudista, deve se atentar para o fato de que sua produção textual será um dos principais meios de acesso do aluno aos conhecimentos selecionados para a ação educativa. E, nesse sentido, todas as ideias, reflexões, conceitos e atividades propostas precisam se concentrar em promover a aproximação com os discentes.</p>
		<p>Assim, esperamos que você, por meio do estudo desta aula, reconheça os elementos fundamentais da aprendizagem de adultos; identifique os principais estilos de aprendizagem descritos na literatura e sua aplicação na escrita de materiais para EaD; e, por fim, perceba que o tutor é o principal mediador entre o aluno e o material didático produzido.</p>

  </div>
</div>


<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual o significado da aprendizagem?</h3> </div>
		<img src="imagens/bebe.jpg" title="Bebe usando o computador" style="max-width:300px; float:right; padding-left:20px; padding-bottom:20px " alt="">
		<p>Antes de discorrer a respeito, gostaríamos de convidá-lo a pensar em uma criança nos primeiros anos de vida. A essência da palavra "aprender" manifesta-se nitidamente em cada ação, não é mesmo?</p>
		<p>Se você tem filhos, sobrinhos ou já observou crianças na primeira infância, pode perceber como nos admiramos com pequenas atitudes e comportamentos que elas apresentam: "<i class="italico">Que lindo! Que bebê inteligente! Como aprende rápido!</i>".</p>
		<p>O fato é que, por trás de cada uma dessas manifestações no comportamento infantil, existem diversos processos mentais, cognitivos, biológicos, psicológicos, emocionais, sociais, entre outros, que contribuem para cada evolução. É um processo complexo e integrado que se articula em favor do desenvolvimento humano e que se modifica à medida que crescemos ou amadurecemos. Assim, podemos dizer que nascemos programados para aprender.</p>

  </div>
</div>


<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>  
  </div>
</div>
<div class="row">
  <div class="col-lg-7">
		<p>Quando pensamos em produzir materiais didáticos, torna-se fundamental refletir sobre a aprendizagem, uma vez que o principal objetivo desses recursos é, justamente, proporcionar caminhos que auxiliem a construção do conhecimento pelos alunos.</p>
		<p>Em termos conceituais, Moraes (2008, p. 49) assinala:</p>

	    <div class="col-lg-offset-2 col-lg-10 quote">
	        <p>Aprendizagem é compreendida como um fenômeno interpretativo da realidade. É um fenômeno complexo, relacional, dialético e compartilhado, produto de um sistema auto-organizador que possui como características fundamentais as interações provocadoras de mudanças estruturais na organização viva. (...) envolve processos de auto-organização e de reorganização mental e emocional dos seres aprendentes. </p>
	    </div> 		

	    <div class="clear"></div>

	    

  </div>
  <div class="col-lg-5">

        <div class="bloco-pontilhado" style="margin-top:40px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; font-weight:bold;">O que ocorre no cérebro quando aprendemos?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
          <div id="conteudo-cerebro" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
              <p style="text-align:center; text-indent:0em;"><img src="imagens/cerebro.jpg" style="max-width:40%; text-align:center;" alt=""> <br> Alguns textos poderão nos ajudar a compreender essa dinâmica. Navegue à vontade pelos links a seguir: <br><br> <a href="http://www.cerebromente.org.br/n11/mente/eisntein/rats-p.html" class="btn btn-info btn-small" target="_blank" style="margin-bottom:20px">Mudanças no Cérebro</a> <a href="http://www.profissionalenegocios.com.br/artigos/artigo.asp?cod_materia=131" class="btn btn-info btn-small" target="_blank">Aprendizagem e Neurociência</a> </p>                       
            </div>
          </div>
        </div>  	    

  </div>  
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
	<div class="row">
	  <div class="col-lg-12">
	    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>  
	  </div>
	</div>
  <div class="col-lg-7">
		<p>Nesse sentido, é notório que, na fase adulta da vida, os processos de aprendizagem são diferentes daqueles que ocorrem na infância. Crescidos, as interjeições de admiração acerca do desenvolvimento dos adultos não são recorrentes como na infância, concorda?</p>
		<p>Ao pensar nas especificidades do processo de aprendizagem dos adultos, <strong>Malcom Knowles</strong> consolidou o termo <a href="http://pt.wikipedia.org/wiki/Andragogia" target="_blank" title="Conheça a Andragogia">Andragogia</a> – arte ou ciência de orientar adultos a aprender. Ela é vista como:</p>
	    <div class="col-lg-offset-2 col-lg-10 quote">
			<p>(...) um caminho educacional que busca compreender o adulto, podendo ser considerada uma teoria, mas também um método de ensino, que se reflete em um somatório de trocas de conhecimentos entre o facilitador do conhecimento e o estudante adulto e suas experiências de vida.</p>
			<p>(...) na andragogia a aprendizagem tem uma particularidade mais centrada no aluno, na independência e na autogestão da aprendizagem, para a aplicação prática na vida diária. (CARVALHO et al, 2010 apud HAMZE, 2008)</p>
	    </div> 	
		
	
  </div>
  <div class="col-lg-5">
      <div class="botao-malcom"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Malcom Knowles?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-malcom btn btn-info" style="margin-bottom:30px">Conheça Malcom Knowles</a></p></div>
      <div class="conteudo-malcom" style="display:none"> 
        <p style="background-color: #138DAC; border-radius:10px; color: white; padding: 20px;"><img src="imagens/malcom.jpg"  alt="Malcom" title="Foto de Malcom"  style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          O norte-americano Ph.D. <strong style="color:#EECA71">Malcolm Sheperd Knowles</strong> (1913-1997) é considerado o pai da Andragogia.</i>
          
          <br>

        </p>
      </div>  
	  <img src="imagens/mulher.jpg" title="Mulher segurando o tablet" style="max-width:100%; " alt="">	    

  </div>   
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<p>Com fundamento em sua definição, a Andragogia pode ser sintetizada em seis princípios básicos. Significa dizer que, para aprendermos, nós, adultos, nos apoiamos em algumas questões, vejamos <span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span> :</p>
		<div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  "> 
			Pocket Learning 3. Andragogia: aprendizagem efetiva para o desenvolvimento de adultos (Lab-SSJ). Disponível em: <a href="http://pt.calameo.com/read/002317991c7ece68b6499?authid=T2bSIXRwFmsW" target="_blank">http://pt.calameo.com/read/002317991c7ece68b6499?authid=T2bSIXRwFmsW</a>. Acesso em março de 2015.   
		</div>
  </div>

  <div class="col-lg-12">


    <div class="balao-esquerdo">1. A necessidade de saber: <a href="javascript:void(0)" style="float:right;" class="botao-questao-1 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-1" style="display:none">
    	<p class="" style=" margin-bottom:0px; padding-bottom:0px">Por que você está me ensinado isso?</p>
    </div>

    <div class="balao-esquerdo">2. O autoconceito do aprendiz: <a href="javascript:void(0)" style="float:right;" class="botao-questao-2 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-2" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Eu sou responsável pelas minhas escolhas. Sou capaz de tomar minhas próprias decisões.</p>
	</div>

    <div class="balao-esquerdo">3. O papel das experiências: <a href="javascript:void(0)" style="float:right;" class="botao-questao-3 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-3" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Minhas experiências são a base para meu aprendizado.</p>
	</div>

    <div class="balao-esquerdo">4. Prontidão para aprender: <a href="javascript:void(0)" style="float:right;" class="botao-questao-4 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-4" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Qual problema vou resolver com isso que você quer que eu aprenda?</p>
	</div>

    <div class="balao-esquerdo">5. Orientação para a aprendizagem centrada na vida:  <a href="javascript:void(0)" style="float:right;" class="botao-questao-5 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-5" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Estou aprendendo matérias ou ganhando ferramentas para resolver problemas do meu cotidiano?</p>
	</div>          		

    <div class="balao-esquerdo">6. Motivação:  <a href="javascript:void(0)" style="float:right;" class="botao-questao-6 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-6" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Aprendo aquilo pelo qual me interesso e que promove o meu crescimento.</p>
	</div>

  </div>  
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
  </div>

  <div class="col-lg-6">

        <div class="bloco-pontilhado" style="margin-top:40px" class="">
          <img src="imagens/pequenapausa.png" alt="" title="pequena pausa" style="width:80px; margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; "><strong>Uma pequena pausa:</strong> <br /> <br /> Reflita a respeito do seu próprio processo de aprendizagem:  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-pausa-1" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver as questões</a> </p>
          <div id="conteudo-pausa-1" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:0px 30px">
              <div style="text-align:center; text-indent:0em;" class="well">
				<p>•	Por que escolheu participar deste curso de formação de conteudistas? </p>
				<p>•	Qual é sua motivação para aprender sobre o processo de elaboração de materiais para a EaD?</p>
				<p>•	Quais são suas experiências com a elaboração de materiais didáticos ou com a docência? </p>
				<p>•	Você vislumbra possibilidades de aplicar o que está aprendendo?</p>
				<p>•	Os princípios da Andragogia fizeram sentido para você?</p>
			  </div>                       
            </div>
          </div>
        </div>  	    

  </div>  

  <div class="col-lg-6">

      <div class="bloco-pontilhado" style="">
        <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Agora, convidamos você para assistir a um pequeno vídeo, que retrata de maneira bem objetiva o que estamos discutindo sobre a Andragogia:  </p>
        <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
          <iframe width="420" height="315" src="https://www.youtube.com/embed/8I_0jXipUcI" frameborder="0" allowfullscreen></iframe>                    
        </div>
      </div>    

  </div>    


</div>


<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<p>Toda essa visão correlaciona-se, intrinsecamente, com o momento atual da EaD, que utiliza a internet como meio de veiculação. Ela traz subjacente às suas especificidades a mudança do papel do estudante: aqui, ele (e você!) exerce(m) um papel de destaque em relação ao seu próprio processo de aprendizagem. </p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Quem é o aluno na EaD?</h3> </div>
		<p>O aluno é visto como sujeito participativo, atuante, crítico e reflexivo; alguém que tem sua história de vida, interesses e motivações pessoais, que atribui significados diferentes às experiências e tem formas e tempos diversos de aprender.</p>
		<p>Com base nisso, devemos concentrar a escrita didática nas necessidades do discente. Dessa forma, é importante reconhecer que não somos transmissores de verdades. Sobretudo, como autores de conteúdos, temos o papel de selecionar cuidadosamente os materiais e organizá-los para facilitar os percursos de aprendizagem escolhidos por cada um.</p>
		<p>Como o processo pedagógico ocorrerá no ambiente virtual de aprendizagem, ressaltamos também o aspecto coletivo da construção de conhecimentos. Vimos na aula anterior, inclusive, que a EaD online possui esse diferencial em relação aos momentos históricos anteriores: a possibilidade de interação entre todos os participantes no espaço educativo virtual.</p>
		<p>Nesse sentido, “as trocas intelectuais e os diálogos atuam como fatores necessários ao desenvolvimento do pensamento e da aprendizagem”. (MORAES et al, 2008, p. 49).</p>

		
      <div class="col-lg-7 ">
          <p>Por isso, muito se fala em aprendizagem colaborativa nos contextos online: </p>
          <div class="col-lg-offset-2 col-lg-10 quote">
            <p>Aprendizagem colaborativa pode ser definida como o processo de construção do conhecimento decorrente da participação, do envolvimento e da contribuição ativa dos alunos na aprendizagem uns dos outros. (TORRES e AMARAL, 2011)</p>
          </div>
          <p>Então, ao retratarmos a aprendizagem na educação a distância, devemos pensar em duas frentes: o aluno (estudo individual) e os alunos (aprendizagem colaborativa na plataforma). </p>
      </div> 
      <div class="col-lg-5">
           <img src="imagens/planta.jpg" title="Pessoas trabalhando juntas" style="float:left; max-width:100%; margin-bottom:20px;">
      </div>      

      <div class="clear"></div>
		
		

  </div>
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>

		<p>Você pode estar se questionando: </p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Como faço, então, para privilegiar as necessidades dos alunos por meio da minha escrita?</h3> </div>
		<p>Ao longo do curso, discorreremos sobre vários pontos que nos auxiliarão nesse sentido. Por agora, vamos ressaltar algumas dicas:</p>
		<p><strong>• Transformar a sua escrita em um diálogo.</strong> Converse com o aluno, troque ideias durante a sua explanação. Utilize perguntas para incentivar a reflexão. Quebre a rigidez do discurso sem perder a profundidade. </p>
		<p><strong>• Tratar o aluno de maneira pessoal.</strong> Use linguagem simples, chame o aluno de “você”. Sempre que possível, procure incluí-lo no discurso e nas reflexões.</p>
		<p><strong>• Colocar-se na posição de aluno.</strong> Tente trazer exemplos próximos da realidade dele; proponha questionamentos contextualizados e procure imaginar quais dúvidas ele pode vir a ter ao ler o que você escreveu.</p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">E, ainda, como promover a aprendizagem colaborativa?</h3> </div>
		<p>Esse tipo de aprendizagem pode ser incentivado com a criação de atividades relacionadas ao conteúdo, a serem efetivadas no ambiente virtual, como, por exemplo, a discussão de temas específicos em Fórum.</p>
		<p>Você já pode pensar em maneiras de promover, na sua prática de conteudista, as questões que levantamos neste estudo. Sinta-se à vontade para compartilhar suas ideias com o grupo. Disponibilizamos, no nosso AVA, o espaço denominado <strong>“Compartilhando ideias”</strong>, especialmente para registros e trocas entre os participantes sobre os assuntos do curso. Você gostaria de começar? Aguardamos suas colaborações.</p>
		<p>Finalizando esse tópico, outro assunto emerge: se somos pessoas diferentes, diversas também são as formas de consolidação da aprendizagem. Firmados nessa assertiva, no item a seguir, conversaremos sobre os estilos de aprendizagem.</p>
		
  </div>
</div>



<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-7">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">

		<p>Antes de tudo, uma reflexão:</p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Você consegue identificar de que maneira aprende e fixa com mais facilidade determinado conteúdo? </h3> </div>
		<p>Alguns exemplos: por meio de leituras, realizando exercícios, fazendo resumos, refletindo ou analisando situações, sintetizando ideias em gráficos, tabelas ou mapas mentais, entre outros.</p>

		<div style="text-align:center"><h3 class="titulo titulo-secondary">Conseguiu listar? </h3> </div>
    <div style="text-align:center"><a href="javascript:void(0);" class="btn btn-info botao-listei" style="margin-bottom:20px">Clique após listar para continuar</a></div>
    <div style="display:none" class="conteudo-listei">
  		<p>É bem provável que as suas indicações não sejam idênticas às de seus colegas, pois, como ressaltamos anteriormente, somos pessoas diferentes.</p>
  		<p>Com base nas formas pelas quais cada um de nós encontra maior facilidade para aprender, surgiram os estudos acerca dos <strong>estilos de aprendizagem</strong> (EAs). </p>
  		<p>Eles são definidos, conforme Veraszto et al (2011) citando Kolb (1984), como:</p>

	    <div class="col-lg-offset-2 col-lg-10 quote">
	        <p>(...) formas preferenciais pelas quais o ser humano adquire e processa o conhecimento de forma mais eficiente. Esses EAs são tendências, mais ou menos acentuadas, e podem modificar-se ao longo do tempo em um mesmo indivíduo; a maioria das pessoas pode ter preferências variáveis dependendo das circunstâncias. Além disso, algumas capacidades de aprender se destacam mais que outras por consequência de fatores hereditários, experiências prévias e exigências do ambiente.</p>
	    </div> 
    </div>      

		

		
  </div>


</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>
  </div>
</div>

<div class="row">

  <div class="col-lg-7">
		<p>Segundo a professora Daniela Melaré <span id="biblio-2" class="fonte-bibliografica-numero " name="biblio-abrir">2</span> , referência no assunto, Alonso, Gallego e Honey (2002) definiram quatro estilos de aprendizagem, a saber: ativo, reflexivo, teórico e pragmático.</p>    
    <div class="fonte-bibliografica biblio-2-conteudo alert alert-warning" style="display:none;  "> 
      MATTAR, J. Design educacional: educação a distância na prática. São Paulo: Artesanato Educacional, 2014. p. 58.   
    </div>
  </div> 

  <div class="col-lg-5">
      <div class="botao-daniela"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Daniela Melaré?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-daniela btn btn-info" style="margin-bottom:30px">Conheça Daniela Melaré</a></p></div>
      <div class="conteudo-daniela" style="display:none"> 
        <p style="background-color: #138DAC; border-radius:10px; text-indent:0em; color: white; padding: 20px;"><img src="imagens/daniela.jpg"  alt="Daniela" title="Foto de Daniela"  style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          <strong>Prof.ª Dra. Daniela Melaré Vieira Barros</strong>: pedagoga, especialista em Design Instrucional e em Administração em Educação a Distância; mestre em Engenharia das Mídias para a Educação; doutora em Educação, com Pós-Doutorado pela UNICAMP e pela UNED; colaboradora da Open University; editora da Revista Estilos de Aprendizaje; docente da Universidade Aberta de Portugal-Lisboa; autora de 5 livros.
          <br /><br />
            <a href="http://dmelare.blogspot.com.br/" target="_blank" class="btn btn-white" style="text-indent:0em">Conheça mais</a>
        </p>
      </div> 
  </div> 

</div>  

<div class="row">

  <div class="col-lg-12">
  		<p>Vejamos o perfil das pessoas em cada tipo <span id="biblio-3" class="fonte-bibliografica-numero " name="biblio-abrir">3</span>: </p>
      <div class="fonte-bibliografica biblio-3-conteudo alert alert-warning" style="display:none;  "> 
        Com adaptações. Disponível em: <a href="http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf" target="_blank">http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf</a>. Acesso em abril de 2015.   
      </div>      

      <div class="row well cor-ativo">
        <div style="text-align:center"><a href="javascript:void(0);"  class="botao-ativo btn btn-info">Conheça o perfil Ativo</a></div>
        <div class="conteudo-ativo" style="display:none">
            <div class="col-lg-4">
              <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
              <p style="text-indent:0em">
                <p style="text-indent:0em">• Gosta de novas experiências, possui mente aberta, é entusiasmado por tarefas novas; </p>
                <p style="text-indent:0em">• Pensa no momento presente;</p>
                <p style="text-indent:0em">• Busca desafios e não gosta de longos prazos; </p>
                <p style="text-indent:0em">• Participa de grupos, envolve-se com os assuntos dos demais e centra ao seu redor todas as atividades. </p>
              </p>
            </div>
            <div class="col-lg-4" style="text-align:center">
              <img src="imagens/ativo.jpg">
            </div>
            <div class="col-lg-4">
              <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
              <p style="text-indent:0em">
                <p style="text-indent:0em"><strong>Principais:</strong> animador, improvisador, descobridor, que se arrisca, espontâneo. </p>
                <p style="text-indent:0em"><strong>Secundárias:</strong> criativo, aventureiro, renovador, vital, vive experiências, traz novidades, impetuoso, protagonista, chocante, inovador, conversador, líder, voluntarioso, divertido, participativo, competitivo, desejoso de aprender, solucionador de problemas e modificador.</p>
               </p>
            </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-reflexivo">
        <div style="text-align:center"><a href="javascript:void(0);"  class="botao-reflexivo btn btn-info">Conheça o perfil Reflexivo</a></div>
        <div class="conteudo-reflexivo" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style="text-indent:0em">• Atualiza e reúne dados, analisa com detalhamento antes de chegar a uma conclusão;</p>
              <p style="text-indent:0em">•  Estuda, reflete e analisa; </p>
              <p style="text-indent:0em">• Considera a experiência e a observa com diferentes perspectivas; </p>
              <p style="text-indent:0em">• Sua filosofia tende a ser prudente: gosta de considerar todas as alternativas possíveis antes de realizar algo;</p>
              <p style="text-indent:0em">• Gosta de observar a atuação dos demais e cria ao seu redor um ar ligeiramente distante. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/reflexivo.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p><strong>Principais:</strong> ponderado, consciente, receptivo, analítico e exaustivo.</p>
              <p><strong>Secundárias:</strong> observador, recompilador, paciente, cuidadoso, detalhista, elaborador de argumentos, previsor de alternativas, estudioso de comportamentos, pesquisador, registrador de dados, assimilador, escritor de informes ou declarações, lento, distante, prudente, inquisidor.</p>
            </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-teorico">
        <div style="text-align:center"><a href="javascript:void(0);"  class="botao-teorico btn btn-info">Conheça o perfil Teórico</a></div>
        <div class="conteudo-teorico" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style="text-indent:0em;">• É lógico, estabelece teorias, princípios, modelos, busca a estrutura, gosta de analisar e sintetiza; </p>
              <p style="text-indent:0em;">• Adapta-se e integra teses dentro de teorias lógicas e complexas;</p>
              <p style="text-indent:0em;">• Enfoca problemas de forma vertical, por etapas lógicas;</p>
              <p style="text-indent:0em;">• Tende a ser perfeccionista;</p>
              <p style="text-indent:0em;">• Integra o que faz em teorias coerentes;</p>
              <p style="text-indent:0em;">• É profundo em seu sistema de pensamento e na hora de estabelecer princípios, teorias e modelos;</p>
              <p style="text-indent:0em;">• Para ele, se é lógico, é bom. Busca a racionalidade e objetividade; distancia-se do subjetivo e do ambíguo. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/teorico.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'><strong>Principais:</strong> metódico, lógico, objetivo, crítico e estruturado. </p>
              <p style='text-indent:0em'><strong>Secundárias:</strong> disciplinado, planejador, sistemático, ordenador, sintético, raciocina, pensador, relacionador, perfeccionista, generalizador; busca hipóteses, modelos, perguntas, conceitos, finalidade clara, racionalidade, o porquê, sistemas de valores, de critérios; é inventor de procedimentos, explorador.</p>
             </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-pragmatico">
        <div style="text-align:center"><a href="javascript:void(0);"  class="botao-pragmatico btn btn-info">Conheça o perfil Pragmático</a></div>
        <div class="conteudo-pragmatico" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'>• Aplica a ideia e faz experimentos;  </p>
              <p style='text-indent:0em'>• Descobre o aspecto positivo das novas ideias e aproveita a primeira oportunidade para experimentá-las; </p>
              <p style='text-indent:0em'>• Gosta de atuar rapidamente e com seguridade com aquelas ideias (sic) e projetos que o atrai;</p>
              <p style='text-indent:0em'>• Tende a ser impaciente quando existem pessoas que teorizam;</p>
              <p style='text-indent:0em'>• É realista quando tem que tomar uma decisão e resolvê-la;</p>
              <p style='text-indent:0em'>• Sua filosofia é “sempre se pode fazer melhor” e, “se funciona, significa que é bom”. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/pragmatico.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'><strong>Principais:</strong> experimentador, prático, direto, eficaz e realista. </p>
              <p style='text-indent:0em'><strong>Secundárias:</strong> técnico, útil, rápido, decidido, planejador, positivo, concreto, objetivo, claro, seguro de si, organizador, atual, solucionador de problemas, aplicador do que aprendeu, planeja ações.</p>
            </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>                  



		<div style="text-align:center"><h3 class="titulo titulo-secondary">Apenas ao ler esses estilos de aprendizagem, você conseguiu se identificar com algum deles? </h3> </div>
		<p>Para auxiliar esse processo de descoberta, disponibilizamos em nosso ambiente virtual um questionário desenvolvido por Alonso, Gallego e Honey (2002), para a identificação do estilo de aprendizagem predominante em cada pessoa. Vamos respondê-lo? </p>
		<p>Após concluir o questionário, registre o seu resultado na enquete que também está lançada no Moodle. Assim poderemos conhecer o perfil da turma quanto às preferências de aprendizagem.</p>


  </div>


</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>

		<p>Voltando às considerações sobre o tema:</p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual a importância desse assunto na elaboração de materiais didáticos para a EaD? </h3> </div>		
		<p>Primeiro, se aprendemos de maneiras diversas, podemos concluir que não conseguiremos despertar os processos que desencadeiam a aprendizagem nos diferentes tipos de sujeitos escrevendo materiais pautados apenas em uma proposta.</p>
		<p>Segundo, os estilos de aprendizagem são preferências, portanto, não são determinantes. É interessante, por conseguinte, proporcionar oportunidades para que os alunos experimentem atividades diversas daquelas pelas quais têm maior inclinação; tentar fazer com que mobilizem outras estruturas mentais. </p>

    <div class="col-lg-6">
    <p>Logo, o principal desafio é: utilizar, na escrita dos materiais didáticos, estratégias e recursos que a virtualidade nos oferece para ampliar as possibilidades de concretização da aprendizagem nos alunos, e não apenas focar em atividades vinculadas especificamente a um estilo de aprendizagem. Para tanto, precisamos ir além do texto linear.</p>
        <div style="text-align:center"><h3 class="titulo titulo-secondary">Como assim? </h3> </div>
        <p>Além do texto em si, precisamos compor o material com recursos complementares – hiperlinks, vídeos, músicas e outras mídias, ilustrações e imagens –, efetuar destaques de conteúdo, inserir questionamentos, indicar referências para aprofundamento, enfim, o que a sua criatividade permitir. </p>
        <p>Devemos diversificar a produção. Todavia, sem excessos, pois é importante selecionar aquilo que, de fato, agrega valor às informações apresentadas.</p>
        <p>Para terminar nossa conversa sobre os estilos de aprendizagem, assista a dois trechos de uma entrevista com a professora Daniela Melaré, concedida à TV FIB – Programa “Diálogos do Saber”. Como o vídeo é extenso, especificamos os momentos em que a docente retrata o tema, mas fique à vontade se desejar assistir ao vídeo por completo: </p>
  

    </div> 

    <div class="col-lg-6">
        <div class="bloco-pontilhado" style="">
          <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
          <div >
            <p><strong>• Trecho 1:</strong> início – 10 minutos até 15 minutos e 20 segundos. </p>
            <p><strong>• Trecho 2:</strong> início – 18 minutos e 12 segundos até 20 minutos e 20 segundos.</p>
          </div>
          <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
            <iframe width="420" height="315" src="https://www.youtube.com/embed/bUjWkm30nGw" frameborder="0" allowfullscreen></iframe>                    
          </div>
        </div>  
    </div>  

    <div class="clear"></div>

    <div class="col-lg-12">
        <div class="bloco-pontilhado" style="" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; font-weight:bold;">No site indicado a seguir, encontramos um livro digital gratuito
que trata do tema na atualidade, em diversos aspectos:   <br><a href="http://estilosdeaprendizagem-vol01.blogspot.com.br/ " class="btn btn-info " style="text-align:center; text-indent:0em; margin-top:20px; ">Acesse Estilos de Aprendizagem</a> </p>
          <div style="text-align:center">
            Vale a pena navegar por ele!<br /><br />
          </div>
        </div>    
    </div>

    <div class="clear" ></div>

		<p style="margin-top:30px"> Em relação ao que vimos até o momento, um personagem fundamental na educação a distância poderá nos auxiliar a facilitar os processos de aprendizagem: o tutor. Vamos dialogar um pouco sobre ele no próximo tópico.</p>


  </div>
</div>


<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">O tutor e a mediação</h3>

		<p>De acordo com Souza et al (2004), “[N]a modalidade de Educação a Distância existem três elementos fundamentais em interação: aluno, material didático e professor”.</p>
		<p>Para Sales e Nonato (2007), no contexto da EaD:</p>

	    <div class="col-lg-offset-2 col-lg-10 quote">
	        <p>(...) o material didático é alçado a uma posição de grande importância, pois é ele que, ao lado do mediador, poderá possibilitar ao sujeito-aprendente um lugar de autonomia e criticidade que permita desenvolver-se como sujeito autônomo e crítico ao tempo em que constrói o conhecimento objetivo a que se propôs. </p>
	    </div> 	

		<p>Tendo em vista esses apontamentos, constatamos que o tutor tem um papel de grande relevância em contextos de educação online, notadamente no que concerne aos materiais didáticos utilizados.</p>
	</div>
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
      <h3 class="titulo titulo-primary">O tutor e a mediação</h3>
    <p>Cabe ressaltar que o tutor é o “professor” da EaD. Contudo, em comparação com o ensino presencial, tem a sua posição ressignificada, devido a todas as características da modalidade, sobre as quais já discorremos. Principalmente pelo fato de o aluno ganhar destaque no seu próprio processo de aprendizagem, o tutor atua muito mais como orientador e facilitador.</p>

  </div>
  <div class="col-lg-8">
		
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual será o papel do tutor especificamente em relação ao material didático que você irá desenvolver? </h3> </div>
		<p>Após a conclusão do material didático, o tutor será seu principal parceiro, uma vez que, no decorrer do curso, ele será o mediador entre o conteúdo produzido e o aluno. Ele auxiliará o discente a compreender o material e potencializará a aprendizagem dos conteúdos que você selecionou. Será o porta-voz da sua obra, na medida em que criará o ambiente para as interações e discussões sobre os temas, direcionará os diálogos e trocará ideias a respeito dos assuntos que você, cuidadosamente, indicou.</p>
		<p><strong>Observação:</strong> Em muitas situações, o conteudista exercerá o papel de tutor do curso que ele elaborou. Há ocasiões, porém, em que o tutor será uma pessoa diversa. De qualquer maneira, o papel da tutoria em relação ao material didático é o mesmo.</p>
		<p>Vejamos as relações expressas no esquema a seguir:</p>

		<div style="text-align:center"><img src="imagens/esquema.jpg" style="max-width:100%;"></div>

		<p>Dessa forma, vislumbramos que o conhecimento não está pronto e acabado. No ambiente virtual de aprendizagem, a sua elaboração de texto didático estará sujeita a construções e reconstruções. Isso porque a imersão crítica dos alunos e do tutor poderá conferir uma dinâmica enriquecedora e potencialmente fértil para a aprendizagem e, ainda, para a formação e para a transformação. Em cada nova turma, novos olhares, novos questionamentos e novos significados podem emergir. Isso é fantástico! Portanto, o que você escreve, propõe e solidifica é a base. Por isso, o seu trabalho é tão relevante!</p>

  </div>
  <div class="col-lg-4">
    <div class="well">
      <p style="text-indent:0em; text-align:center"><strong>Recapitulando...</strong></p>
      <p style="text-indent:0em; text-align:center"><strong>Você se lembra do conceito de mediação?</strong></p>
      <p style="text-indent:0em; text-align:center"><a href="javascript:void(0);" class="btn btn-info botao-relembrar">Clique para relembrar</a></p>
      <div style="display:none" class="conteudo-relembrar">
        <p>Falamos a respeito dela na primeira aula. Vamos relembrar?</p>
        <p>“É um processo comunicacional, conversacional, de construção de significados, cujo objetivo é ampliar as possibilidades de diálogo e desenvolver a negociação significativa de processos e conteúdos a serem trabalhados nos  ambientes  educacionais,  bem  como  incentivar  a  construção de um saber relacional, contextual, gerado na interação professor-aluno.</p>
        <p>A mediação pedagógica pressupõe, dessa forma, a ação de um docente que ajuda a desenvolver no aluno a curiosidade, a motivação, a autonomia e o gosto pelo aprender, seja no ambiente presencial ou no ambiente virtual”. (MACHADO et al, 2010)</p>
        <p style="text-indent:0em; text-align:center"> <strong>Neste momento, o entendimento sobre mediação fez mais sentido, não é mesmo?</strong></p>
      </div>
    </div>
  </div>
</div>

<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Considerações Finais</h3>
		<p>Nesta aula, conversamos sobre a aprendizagem de adultos e suas peculiaridades, assim como o papel central do aluno em relação à aprendizagem. Vimos também que somos pessoas diferentes, portanto, possuímos estilos de aprendizagem diversos. Por fim, observamos que o tutor será parceiro do conteudista, no sentido de tentar “traduzir” para os alunos aquilo que foi delineado na nossa escrita. Em todos os casos, refletimos acerca da influência dos temas na escrita de materiais educativos para a EaD. </p>
		<p>Finalizamos também, aqui, o Módulo I, no qual tratamos de questões relacionadas à contextualização da EaD e à aprendizagem, nossos pontos de partida.</p>
		<p>Solidificados esses assuntos, seguiremos para o Módulo II, em que estudaremos o planejamento da escrita dos materiais didáticos na educação a distância do STF.</p>
		<p>Encontro vocês no próximo módulo, então!</p>
		<p style="text-align:center"><strong>Até breve!</strong></p>

    <h3 class="titulo titulo-primary">Vamos praticar</h3>
		<p>Como foi a experiência de utilizar o diário para anotar, no Moodle, as suas observações sobre a Aula 1?</p>
		<p>Novamente, convidamos você a registrar no espaço <strong>“Meu percurso de Aprendizagem”</strong>, agora, referente à Aula 2, tudo aquilo que considerou relevante nesta aula.</p>
		<p>Aguardamos suas impressões lá no nosso AVA.</p>
  </div>
</div>


<?php $i++ ; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Referências</h3>

	<p style="text-indent:0em">AMARAL, S. F.; BARROS, D. M. V. B. Estilos de aprendizagem no contexto educativo de uso das tecnologias digitais interativas. Disponível em: <a href="http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf" target="_blank">http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf</a>. Acesso em: abr. 2015.</p>
	<p style="text-indent:0em">CARVALHO, J. A.; PEDROTE, M. C. et al. Andragogia: considerações sobre a aprendizagem do adulto. REMPEC, Ensino Saúde e Ambiente, v. 3, n. 1, p. 78-90, abr. 2010.</p>
	<p style="text-indent:0em">MACHADO, M.; FERREIRA, S. M. B.; AQUINO, V. A mediação pedagógica à luz do pensar complexo: uma análise comparativa da aula em ambiente virtual e presencial. Col@bora. Revista Digital da CVA. Ricesu, v. 6, n. 23, jul. 2010. Disponível em: <a href="http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116" target="_blank">http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116</a>. Acesso em: mar. 2015.</p>
	<p style="text-indent:0em">MORAES, M. A.; PESCE, L.; BRUNO, A. R. Pesquisando fundamentos para novas práticas na educação online. São Paulo: RG Editores, 2008.</p>
	<p style="text-indent:0em">SALES, Mary; NONATO, Emanuel. EaD e material didático: reflexões sobre mediação pedagógica. Mai. 2007. Disponível em: <a href="http://www.abed.org.br/congresso2007/tc/552007104704PM.pdf" target="_blank">http://www.abed.org.br/congresso2007/tc/552007104704PM.pdf</a>. Acesso em: abr. 2015. </p>
	<p style="text-indent:0em">SOUZA, Carlos Alberto et al.  Tutoria na educação a distância. Disponível em: <a href="http://www.abed.org.br/congresso2004/por/htm/088-TC-C2.htm" target="_blank">http://www.abed.org.br/congresso2004/por/htm/088-TC-C2.htm</a>. Acesso em: abr. 2015.</p>
	<p style="text-indent:0em">TORRES, T. Z.; AMARAL, S. F. Aprendizagem colaborativa e Web 2.0: proposta de modelos de organização de conteúdos interativos. ETD – Educ.Tem. Dig.,Campinas, v. 12, n. esp., p. 49-72, mar. 2011. Disponível em: <a href="https://www.fe.unicamp.br/revistas/ged/etd/article/view/2281/pdf_51" target="_blank">https://www.fe.unicamp.br/revistas/ged/etd/article/view/2281/pdf_51</a>. Acesso em: mar. 2015.</p>
	<p style="text-indent:0em">VERASZTO, E. V. et al. Educação a distância e estilos de aprendizagem: estratégicas educativas apoiadas pelas TIC. In: BARROS, Daniela Melaré Vieira (Org.). Estilos de aprendizagem na atualidade. Lisboa, 2011. v. 1, (E-book online). </p>

  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
