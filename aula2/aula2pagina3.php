<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','3', '15', 'aula2pagina2.php', 'aula2pagina4.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>  
  </div>
</div>
<div class="row">
  <div class="col-lg-7">
		<p>Quando pensamos em produzir materiais didáticos, torna-se fundamental refletir sobre a aprendizagem, uma vez que o principal objetivo desses recursos é, justamente, proporcionar caminhos que auxiliem a construção do conhecimento pelos alunos.</p>
		<p>Em termos conceituais, Moraes (2008, p. 49) assinala:</p>

	    <div class="col-lg-offset-2 col-lg-10 quote">
	        <p>Aprendizagem é compreendida como um fenômeno interpretativo da realidade. É um fenômeno complexo, relacional, dialético e compartilhado, produto de um sistema auto-organizador que possui como características fundamentais as interações provocadoras de mudanças estruturais na organização viva. (...) envolve processos de auto-organização e de reorganização mental e emocional dos seres aprendentes. </p>
	    </div> 		

	    <div class="clear"></div>

	    

  </div>
  <div class="col-lg-5">

        <div class="bloco-pontilhado" style="margin-top:40px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; font-weight:bold;">O que ocorre no cérebro quando aprendemos?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
          <div id="conteudo-cerebro" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
              <p style="text-align:center; text-indent:0em;"><img src="imagens/cerebro.jpg" style="max-width:40%; text-align:center;" alt=""> <br> Alguns textos poderão nos ajudar a compreender essa dinâmica. Navegue à vontade pelos <em>links</em> a seguir: <br><br> <a href="http://www.cerebromente.org.br/n11/mente/eisntein/rats-p.html" class="btn btn-info btn-small" target="_blank" style="margin-bottom:20px">Mudanças no Cérebro</a> <a href="http://www.profissionalenegocios.com.br/artigos/artigo.asp?cod_materia=131" class="btn btn-info btn-small" target="_blank">Aprendizagem e Neurociência</a> </p>                       
            </div>
          </div>
        </div>  	    

  </div>  
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina2.php', 'aula2pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
