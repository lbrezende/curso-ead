<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','7', '15', 'aula2pagina6.php', 'aula2pagina8.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<p>Toda essa visão correlaciona-se, intrinsecamente, com o momento atual da EaD, que utiliza a internet como meio de veiculação. Ela traz subjacente às suas especificidades a mudança do papel do estudante: aqui, ele (e você!) exerce(m) um papel de destaque em relação ao seu próprio processo de aprendizagem. </p>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Quem é o aluno na EaD?</h3> </div>
		<p>O aluno é visto como sujeito participativo, atuante, crítico e reflexivo; alguém que tem sua história de vida, interesses e motivações pessoais, que atribui significados diferentes às experiências e tem formas e tempos diversos de aprender.</p>
		<p>Com base nisso, devemos concentrar a escrita didática nas necessidades do discente. Dessa forma, é importante reconhecer que não somos transmissores de verdades. Sobretudo, como autores de conteúdos, temos o papel de selecionar cuidadosamente os materiais e organizá-los para facilitar os percursos de aprendizagem escolhidos por cada um.</p>
		<p>Como o processo pedagógico ocorrerá no ambiente virtual de aprendizagem, ressaltamos também o aspecto coletivo da construção de conhecimentos. Vimos na aula anterior, inclusive, que a EaD <i class="italico">online</i> possui esse diferencial em relação aos momentos históricos anteriores: a possibilidade de interação entre todos os participantes no espaço educativo virtual.</p>
		<p>Nesse sentido, “as trocas intelectuais e os diálogos atuam como fatores necessários ao desenvolvimento do pensamento e da aprendizagem”. (MORAES et al, 2008, p. 49).</p>

		
      <div class="col-lg-7 ">
          <p>Por isso, muito se fala em aprendizagem colaborativa nos contextos <i class="italico">online</i>: </p>
          <div class="col-lg-offset-2 col-lg-10 quote">
            <p>Aprendizagem colaborativa pode ser definida como o processo de construção do conhecimento decorrente da participação, do envolvimento e da contribuição ativa dos alunos na aprendizagem uns dos outros. (TORRES e AMARAL, 2011)</p>
          </div>
          <p>Então, ao retratarmos a aprendizagem na educação a distância, devemos pensar em duas frentes: o aluno (estudo individual) e os alunos (aprendizagem colaborativa na plataforma). </p>
      </div> 
      <div class="col-lg-5">
           <img src="imagens/planta.jpg" title="Pessoas trabalhando juntas" style="float:left; max-width:100%; margin-bottom:20px;">
      </div>      

      <div class="clear"></div>
		
		

  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina6.php', 'aula2pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
