<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','1', '15', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Olá, novamente!</h3>

		<p>Na aula anterior, conversamos sobre o histórico da EaD no cenário mundial e no Brasil, bem como as características da modalidade nos dias atuais.</p>
		<p>Consolidadas essas informações, estudaremos, nesta segunda aula, questões que versam sobre aprendizagem na educação a distância. </p>
		<p>Você, conteudista, deve se atentar para o fato de que sua produção textual será um dos principais meios de acesso do aluno aos conhecimentos selecionados para a ação educativa. E, nesse sentido, todas as ideias, reflexões, conceitos e atividades propostas precisam se concentrar em promover a aproximação com os discentes.</p>
		<p>Assim, esperamos que você, por meio do estudo desta aula, reconheça os elementos fundamentais da aprendizagem de adultos; identifique os principais estilos de aprendizagem descritos na literatura e sua aplicação na escrita de materiais para EaD; e, por fim, perceba que o tutor é o principal mediador entre o aluno e o material didático produzido.</p>

  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
