<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','13', '15', 'aula2pagina12.php', 'aula2pagina14.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  <div class="col-lg-12">
      <h3 class="titulo titulo-primary">O tutor e a mediação</h3>
    <p>Cabe ressaltar que o tutor é o “professor” da EaD. Contudo, em comparação com o ensino presencial, tem a sua posição ressignificada, devido a todas as características da modalidade, sobre as quais já discorremos. Principalmente pelo fato de o aluno ganhar destaque no seu próprio processo de aprendizagem, o tutor atua muito mais como orientador e facilitador.</p>

  </div>
  <div class="col-lg-8">
		
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual será o papel do tutor especificamente em relação ao material didático que você irá desenvolver? </h3> </div>
		<p>Após a conclusão do material didático, o tutor será seu principal parceiro, uma vez que, no decorrer do curso, ele será o mediador entre o conteúdo produzido e o aluno. Ele auxiliará o discente a compreender o material e potencializará a aprendizagem dos conteúdos que você selecionou. Será o porta-voz da sua obra, na medida em que criará o ambiente para as interações e discussões sobre os temas, direcionará os diálogos e trocará ideias a respeito dos assuntos que você, cuidadosamente, indicou.</p>
		<p><strong>Observação:</strong> Em muitas situações, o conteudista exercerá o papel de tutor do curso que ele elaborou. Há ocasiões, porém, em que o tutor será uma pessoa diversa. De qualquer maneira, o papel da tutoria em relação ao material didático é o mesmo.</p>
		<p>Vejamos as relações expressas no esquema a seguir:</p>

		<div style="text-align:center"><img src="imagens/esquema.jpg" style="max-width:100%;"></div>

		<p>Dessa forma, vislumbramos que o conhecimento não está pronto e acabado. No ambiente virtual de aprendizagem, a sua elaboração de texto didático estará sujeita a construções e reconstruções. Isso porque a imersão crítica dos alunos e do tutor poderá conferir uma dinâmica enriquecedora e potencialmente fértil para a aprendizagem e, ainda, para a formação e para a transformação. Em cada nova turma, novos olhares, novos questionamentos e novos significados podem emergir. Isso é fantástico! Portanto, o que você escreve, propõe e solidifica é a base. Por isso, o seu trabalho é tão relevante!</p>

  </div>
  <div class="col-lg-4">
    <div class="well">
      <p style="text-indent:0em; text-align:center"><strong>Recapitulando...</strong></p>
      <p style="text-indent:0em; text-align:center"><strong>Você se lembra do conceito de mediação?</strong></p>
      <p style="text-indent:0em; text-align:center"><a href="javascript:void(0);" class="btn btn-info botao-relembrar">Clique para relembrar</a></p>
      <div style="display:none" class="conteudo-relembrar">
        <p>Falamos a respeito dela na primeira aula. Vamos relembrar?</p>
        <p>“É um processo comunicacional, conversacional, de construção de significados, cujo objetivo é ampliar as possibilidades de diálogo e desenvolver a negociação significativa de processos e conteúdos a serem trabalhados nos  ambientes  educacionais,  bem  como  incentivar  a  construção de um saber relacional, contextual, gerado na interação professor-aluno.</p>
        <p>A mediação pedagógica pressupõe, dessa forma, a ação de um docente que ajuda a desenvolver no aluno a curiosidade, a motivação, a autonomia e o gosto pelo aprender, seja no ambiente presencial ou no ambiente virtual”. (MACHADO et al, 2010)</p>
        <p style="text-indent:0em; text-align:center"> <strong>Neste momento, o entendimento sobre mediação fez mais sentido, não é mesmo?</strong></p>
      </div>
    </div>
  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina12.php', 'aula2pagina14.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
