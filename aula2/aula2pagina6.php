<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','6', '15', 'aula2pagina5.php', 'aula2pagina7.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
  </div>

  <div class="col-lg-6">

        <div class="bloco-pontilhado" style="margin-top:40px" class="">
          <img src="imagens/pequenapausa.png" alt="" title="pequena pausa" style="width:80px; margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; "><strong>Uma pequena pausa:</strong> <br /> <br /> Reflita a respeito do seu próprio processo de aprendizagem:  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-pausa-1" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver as questões</a> </p>
          <div id="conteudo-pausa-1" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:0px 30px">
              <div style="text-align:center; text-indent:0em;" class="well">
				<p>•	Por que escolheu participar deste curso de formação de conteudistas? </p>
				<p>•	Qual é sua motivação para aprender sobre o processo de elaboração de materiais para a EaD?</p>
				<p>•	Quais são suas experiências com a elaboração de materiais didáticos ou com a docência? </p>
				<p>•	Você vislumbra possibilidades de aplicar o que está aprendendo?</p>
				<p>•	Os princípios da Andragogia fizeram sentido para você?</p>
			  </div>                       
            </div>
          </div>
        </div>  	    

  </div>  

  <div class="col-lg-6">

      <div class="bloco-pontilhado" style="">
        <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Agora, convidamos você para assistir a um pequeno vídeo, que retrata de maneira bem objetiva o que estamos discutindo sobre a Andragogia:  </p>
        <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
          <iframe width="420" height="315" src="https://www.youtube.com/embed/8I_0jXipUcI" frameborder="0" allowfullscreen></iframe>                    
        </div>
      </div>    

  </div>    


</div>


<?php  configNavegacaoRodape('exibir', 'aula2pagina5.php', 'aula2pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
