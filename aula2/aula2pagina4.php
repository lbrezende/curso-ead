<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','4', '15', 'aula2pagina3.php', 'aula2pagina5.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
	<div class="row">
	  <div class="col-lg-12">
	    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>  
	  </div>
	</div>
  <div class="col-lg-7">
		<p>Nesse sentido, é notório que, na fase adulta da vida, os processos de aprendizagem são diferentes daqueles que ocorrem na infância. Crescidos, as interjeições de admiração acerca do desenvolvimento dos adultos não são recorrentes como na infância, concorda?</p>
		<p>Ao pensar nas especificidades do processo de aprendizagem dos adultos, <strong>Malcom Knowles</strong> consolidou o termo <a href="http://pt.wikipedia.org/wiki/Andragogia" target="_blank" title="Conheça a Andragogia">Andragogia</a> – arte ou ciência de orientar adultos a aprender. Ela é vista como:</p>
	    <div class="col-lg-offset-2 col-lg-10 quote">
			<p>(...) um caminho educacional que busca compreender o adulto, podendo ser considerada uma teoria, mas também um método de ensino, que se reflete em um somatório de trocas de conhecimentos entre o facilitador do conhecimento e o estudante adulto e suas experiências de vida.</p>
			<p>(...) na andragogia a aprendizagem tem uma particularidade mais centrada no aluno, na independência e na autogestão da aprendizagem, para a aplicação prática na vida diária. (CARVALHO et al, 2010 apud HAMZE, 2008)</p>
	    </div> 	
		
	
  </div>
  <div class="col-lg-5">
      <div class="botao-malcom"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Malcom Knowles?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-malcom btn btn-info" style="margin-bottom:30px">Conheça Malcom Knowles</a></p></div>
      <div class="conteudo-malcom" style="display:none"> 
        <p style="background-color: #138DAC; border-radius:10px; color: white; padding: 20px;"><a href="http://web.utk.edu/~start6/knowles/malcolm_knowles.html" target="_blank"><img src="imagens/malcom1.jpg"  alt="Malcom" title="Foto de Malcom"  style="float: left; margin-right:15px;  border: 3px solid #fff; max-width:125px"></a>
          O norte-americano Ph.D. <strong style="color:#EECA71">Malcolm Sheperd Knowles</strong> (1913-1997) é considerado o pai da Andragogia.</i>
          
          <br>

        </p>
      </div>  
	  <img src="imagens/mulher.jpg" title="Mulher segurando o tablet" style="max-width:100%; " alt="">	    

  </div>   
</div>


<?php  configNavegacaoRodape('exibir', 'aula2pagina3.php', 'aula2pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
