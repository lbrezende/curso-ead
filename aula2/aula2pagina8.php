<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','8', '15', 'aula2pagina7.php', 'aula2pagina9.php', 'Produção de Conteúdos para a EaD');
?> 
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>

    <p>Você pode estar se questionando: </p>
    <div style="text-align:center"><h3 class="titulo titulo-secondary">Como faço, então, para privilegiar as necessidades dos alunos por meio da minha escrita?</h3> </div>
    <p>Ao longo do curso, discorreremos sobre vários pontos que nos auxiliarão nesse sentido. Por agora, vamos ressaltar algumas dicas:</p>
    <p><strong>• Transformar a sua escrita em um diálogo.</strong> Converse com o aluno, troque ideias durante a sua explanação. Utilize perguntas para incentivar a reflexão. Quebre a rigidez do discurso sem perder a profundidade. </p>
    <p><strong>• Tratar o aluno de maneira pessoal.</strong> Use linguagem simples, chame o aluno de “você”. Sempre que possível, procure incluí-lo no discurso e nas reflexões.</p>
    <p><strong>• Colocar-se na posição de aluno.</strong> Tente trazer exemplos próximos da realidade dele; proponha questionamentos contextualizados e procure imaginar quais dúvidas ele pode vir a ter ao ler o que você escreveu.</p>
    <div style="text-align:center"><h3 class="titulo titulo-secondary">E, ainda, como promover a aprendizagem colaborativa?</h3> </div>
    <p>Esse tipo de aprendizagem pode ser incentivado com a criação de atividades relacionadas ao conteúdo, a serem efetivadas no ambiente virtual, como, por exemplo, a discussão de temas específicos em Fórum.</p>
    <p>Você já pode pensar em maneiras de promover, na sua prática de conteudista, as questões que levantamos neste estudo. Sinta-se à vontade para compartilhar suas ideias com o grupo. Disponibilizamos, no nosso AVA, o espaço denominado <strong>“Compartilhando ideias”</strong>, especialmente para registros e trocas entre os participantes sobre os assuntos do curso. Você gostaria de começar? Aguardamos suas colaborações.</p>
    <p>Finalizando esse tópico, outro assunto emerge: se somos pessoas diferentes, diversas também são as formas de consolidação da aprendizagem. Firmados nessa assertiva, no item a seguir, conversaremos sobre os estilos de aprendizagem.</p>
    
  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina7.php', 'aula2pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
