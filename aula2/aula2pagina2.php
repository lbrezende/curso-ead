<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','2', '15', 'aula2pagina1.php', 'aula2pagina3.php', 'Produção de Conteúdos para a EaD');
?> 
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual o significado da aprendizagem?</h3> </div>
		<img src="imagens/bebe.jpg" title="Bebe usando o computador" style="max-width:300px; float:right; padding-left:20px; padding-bottom:20px " alt="">
		<p>Antes de discorrer a respeito, gostaríamos de convidá-lo a pensar em uma criança nos primeiros anos de vida. A essência da palavra "aprender" manifesta-se nitidamente em cada ação, não é mesmo?</p>
		<p>Se você tem filhos, sobrinhos ou já observou crianças na primeira infância, pode perceber como nos admiramos com pequenas atitudes e comportamentos que elas apresentam: "<i class="italico">Que lindo! Que bebê inteligente! Como aprende rápido!</i>".</p>
		<p>O fato é que, por trás de cada uma dessas manifestações no comportamento infantil, existem diversos processos mentais, cognitivos, biológicos, psicológicos, emocionais, sociais, entre outros, que contribuem para cada evolução. É um processo complexo e integrado que se articula em favor do desenvolvimento humano e que se modifica à medida que crescemos ou amadurecemos. Assim, podemos dizer que nascemos programados para aprender.</p>

  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina1.php', 'aula2pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
