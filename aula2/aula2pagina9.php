<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','9', '15', 'aula2pagina8.php', 'aula2pagina10.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-7">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">

    <p>Antes de tudo, uma reflexão:</p>
    <div style="text-align:center"><h3 class="titulo titulo-secondary">Você consegue identificar de que maneira aprende e fixa com mais facilidade determinado conteúdo? </h3> </div>
    <p>Alguns exemplos: por meio de leituras, realizando exercícios, fazendo resumos, refletindo ou analisando situações, sintetizando ideias em gráficos, tabelas ou mapas mentais, entre outros.</p>

    <div style="text-align:center"><h3 class="titulo titulo-secondary">Conseguiu listar? </h3> </div>
    <div style="text-align:center"><a href="javascript:void(0);" class="btn btn-info botao-listei" style="margin-bottom:20px">Clique após listar para continuar</a></div>
    <div style="display:none" class="conteudo-listei">
      <p>É bem provável que as suas indicações não sejam idênticas às de seus colegas, pois, como ressaltamos anteriormente, somos pessoas diferentes.</p>
      <p>Com base nas formas pelas quais cada um de nós encontra maior facilidade para aprender, surgiram os estudos acerca dos <strong>estilos de aprendizagem</strong> (EAs). </p>
      <p>Eles são definidos, conforme Veraszto et al (2011) citando Kolb (1984), como:</p>

      <div class="col-lg-offset-2 col-lg-10 quote">
          <p>(...) formas preferenciais pelas quais o ser humano adquire e processa o conhecimento de forma mais eficiente. Esses EAs são tendências, mais ou menos acentuadas, e podem modificar-se ao longo do tempo em um mesmo indivíduo; a maioria das pessoas pode ter preferências variáveis dependendo das circunstâncias. Além disso, algumas capacidades de aprender se destacam mais que outras por consequência de fatores hereditários, experiências prévias e exigências do ambiente.</p>
      </div> 
    </div>      

    

    
  </div>


</div>


<?php  configNavegacaoRodape('exibir', 'aula2pagina8.php', 'aula2pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
