<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','5', '15', 'aula2pagina4.php', 'aula2pagina6.php', 'Produção de Conteúdos para a EaD');
?> 
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">A Aprendizagem: um processo construtivo</h3>
		<p>Com fundamento em sua definição, a Andragogia pode ser sintetizada em seis princípios básicos. Significa dizer que, para aprendermos, nós, adultos, nos apoiamos em algumas questões, vejamos<span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span>:</p>
		<div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  "> 
			Pocket Learning 3. Andragogia: aprendizagem efetiva para o desenvolvimento de adultos (Lab-SSJ). Disponível em: <a href="http://pt.calameo.com/read/002317991c7ece68b6499?authid=T2bSIXRwFmsW" target="_blank">http://pt.calameo.com/read/002317991c7ece68b6499?authid=T2bSIXRwFmsW</a>. Acesso em março de 2015.   
		</div>
  </div>

  <div class="col-lg-12">


    <div class="balao-esquerdo">1. A necessidade de saber: <a href="javascript:void(0)" style="float:right;" class="botao-questao-1 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-1" style="display:none">
    	<p class="" style=" margin-bottom:0px; padding-bottom:0px">Por que você está me ensinado isso?</p>
    </div>

    <div class="balao-esquerdo">2. O autoconceito do aprendiz: <a href="javascript:void(0)" style="float:right;" class="botao-questao-2 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-2" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Eu sou responsável pelas minhas escolhas. Sou capaz de tomar minhas próprias decisões.</p>
	</div>

    <div class="balao-esquerdo">3. O papel das experiências: <a href="javascript:void(0)" style="float:right;" class="botao-questao-3 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-3" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Minhas experiências são a base para meu aprendizado.</p>
	</div>

    <div class="balao-esquerdo">4. Prontidão para aprender: <a href="javascript:void(0)" style="float:right;" class="botao-questao-4 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-4" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Qual problema vou resolver com isso que você quer que eu aprenda?</p>
	</div>

    <div class="balao-esquerdo">5. Orientação para a aprendizagem centrada na vida:  <a href="javascript:void(0)" style="float:right;" class="botao-questao-5 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-5" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Estou aprendendo matérias ou ganhando ferramentas para resolver problemas do meu cotidiano?</p>
	</div>          		

    <div class="balao-esquerdo">6. Motivação:  <a href="javascript:void(0)" style="float:right;" class="botao-questao-6 btn btn-info btn-small">Clique para ver o significado</a></div>
    <div class="balao-direito conteudo-questao-6" style="display:none">
		<p class="" style=" margin-bottom:0px; padding-bottom:0px">Aprendo aquilo pelo qual me interesso e que promove o meu crescimento.</p>
	</div>

  </div>  
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina4.php', 'aula2pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
