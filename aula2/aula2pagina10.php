<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','10', '15', 'aula2pagina9.php', 'aula2pagina11.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Estilos de aprendizagem</h3>
  </div>
</div>

<div class="row">

  <div class="col-lg-7">
    <p>Segundo a professora Daniela Melaré<span id="biblio-2" class="fonte-bibliografica-numero " name="biblio-abrir">2</span>, referência no assunto, Alonso, Gallego e Honey (2002) definiram quatro estilos de aprendizagem, a saber: ativo, reflexivo, teórico e pragmático.</p>    
    <div class="fonte-bibliografica biblio-2-conteudo alert alert-warning" style="display:none;  "> 
      MATTAR, J. Design educacional: educação a distância na prática. São Paulo: Artesanato Educacional, 2014. p. 58.   
    </div>
  </div> 

  <div class="col-lg-5">
      <div class="botao-daniela"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Daniela Melaré?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-daniela btn btn-info" style="margin-bottom:30px">Conheça Daniela Melaré</a></p></div>
      <div class="conteudo-daniela" style="display:none"> 
        <p style="background-color: #138DAC; border-radius:10px; text-indent:0em; color: white; padding: 20px;"><img src="imagens/daniela.jpg"  alt="Daniela" title="Foto de Daniela"  style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          <strong>Prof.ª Dra. Daniela Melaré Vieira Barros</strong>: pedagoga, especialista em Design Instrucional e em Administração em Educação a Distância; mestre em Engenharia das Mídias para a Educação; doutora em Educação, com Pós-Doutorado pela UNICAMP e pela UNED; colaboradora da Open University; editora da Revista Estilos de Aprendizaje; docente da Universidade Aberta de Portugal-Lisboa; autora de 5 livros.
          <br /><br />
            <a href="http://dmelare.blogspot.com.br/" target="_blank" class="btn btn-white" style="text-indent:0em">Conheça mais</a>
        </p>
      </div> 
  </div> 

</div>  

<div class="row">

  <div class="col-lg-12">
      <p>Clique nas imagens para conhecer o perfil das pessoas em cada tipo<span id="biblio-3" class="fonte-bibliografica-numero " name="biblio-abrir">3</span>: </p>
      <div class="fonte-bibliografica biblio-3-conteudo alert alert-warning" style="display:none;  "> 
        Com adaptações. Disponível em: <a href="http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf" target="_blank">http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf</a>. Acesso em abril de 2015.   
      </div>      

      <div class="row well cor-ativo">
        <div class="botao-ativo" style="text-align:center"><img src="imagens/ativo.jpg"><Br /><Br /><a href="javascript:void(0);"  class=" btn btn-info">Conheça o perfil Ativo</a></div>
        <div class="conteudo-ativo" style="display:none">
            <div class="col-lg-4">
              <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
              <p style="text-indent:0em">
                <p style="text-indent:0em">• Gosta de novas experiências, possui mente aberta, é entusiasmado por tarefas novas; </p>
                <p style="text-indent:0em">• Pensa no momento presente;</p>
                <p style="text-indent:0em">• Busca desafios e não gosta de longos prazos; </p>
                <p style="text-indent:0em">• Participa de grupos, envolve-se com os assuntos dos demais e centra ao seu redor todas as atividades. </p>
              </p>
            </div>
            <div class="col-lg-4" style="text-align:center">
              <img src="imagens/ativo.jpg">
            </div>
            <div class="col-lg-4">
              <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
              <p style="text-indent:0em">
                <p style="text-indent:0em"><strong>Principais:</strong> animador, improvisador, descobridor, que se arrisca, espontâneo. </p>
                <p style="text-indent:0em"><strong>Secundárias:</strong> criativo, aventureiro, renovador, vital, vive experiências, traz novidades, impetuoso, protagonista, chocante, inovador, conversador, líder, voluntarioso, divertido, participativo, competitivo, desejoso de aprender, solucionador de problemas e modificador.</p>
               </p>
            </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-reflexivo">
        <div class="botao-reflexivo" style="text-align:center"><img src="imagens/reflexivo.jpg"><Br /><Br /><a href="javascript:void(0);"  class=" btn btn-info">Conheça o perfil Reflexivo</a></div>
        <div class="conteudo-reflexivo" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style="text-indent:0em">• Atualiza e reúne dados, analisa com detalhamento antes de chegar a uma conclusão;</p>
              <p style="text-indent:0em">• Estuda, reflete e analisa; </p>
              <p style="text-indent:0em">• Considera a experiência e a observa com diferentes perspectivas; </p>
              <p style="text-indent:0em">• Sua filosofia tende a ser prudente: gosta de considerar todas as alternativas possíveis antes de realizar algo;</p>
              <p style="text-indent:0em">• Gosta de observar a atuação dos demais e cria ao seu redor um ar ligeiramente distante. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/reflexivo.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p style="text-indent:0em;"><strong>Principais:</strong> ponderado, consciente, receptivo, analítico e exaustivo.</p>
              <p style="text-indent:0em;"><strong>Secundárias:</strong> observador, recompilador, paciente, cuidadoso, detalhista, elaborador de argumentos, previsor de alternativas, estudioso de comportamentos, pesquisador, registrador de dados, assimilador, escritor de informes ou declarações, lento, distante, prudente, inquisidor.</p>
            </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-teorico">
        <div class="botao-teorico" style="text-align:center"><img src="imagens/teorico.jpg"><Br /><Br /><a href="javascript:void(0);"  class=" btn btn-info">Conheça o perfil Teórico</a></div>
        <div class="conteudo-teorico" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style="text-indent:0em;">• É lógico, estabelece teorias, princípios, modelos, busca a estrutura, gosta de analisar e sintetiza; </p>
              <p style="text-indent:0em;">• Adapta-se e integra teses dentro de teorias lógicas e complexas;</p>
              <p style="text-indent:0em;">• Enfoca problemas de forma vertical, por etapas lógicas;</p>
              <p style="text-indent:0em;">• Tende a ser perfeccionista;</p>
              <p style="text-indent:0em;">• Integra o que faz em teorias coerentes;</p>
              <p style="text-indent:0em;">• É profundo em seu sistema de pensamento e na hora de estabelecer princípios, teorias e modelos;</p>
              <p style="text-indent:0em;">• Para ele, se é lógico, é bom. Busca a racionalidade e objetividade; distancia-se do subjetivo e do ambíguo. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/teorico.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'><strong>Principais:</strong> metódico, lógico, objetivo, crítico e estruturado. </p>
              <p style='text-indent:0em'><strong>Secundárias:</strong> disciplinado, planejador, sistemático, ordenador, sintético, raciocina, pensador, relacionador, perfeccionista, generalizador; busca hipóteses, modelos, perguntas, conceitos, finalidade clara, racionalidade, o porquê, sistemas de valores, de critérios; é inventor de procedimentos, explorador.</p>
             </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="row well cor-pragmatico">
        <div class="botao-pragmatico" style="text-align:center"><img src="imagens/pragmatico.jpg"><Br /><Br /><a href="javascript:void(0);"  class=" btn btn-info">Conheça o perfil Pragmático</a></div>
        <div class="conteudo-pragmatico" style="display:none">
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Perfil </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'>• Aplica a ideia e faz experimentos;  </p>
              <p style='text-indent:0em'>• Descobre o aspecto positivo das novas ideias e aproveita a primeira oportunidade para experimentá-las; </p>
              <p style='text-indent:0em'>• Gosta de atuar rapidamente e com seguridade com aquelas ideias (sic) e projetos que o atrai;</p>
              <p style='text-indent:0em'>• Tende a ser impaciente quando existem pessoas que teorizam;</p>
              <p style='text-indent:0em'>• É realista quando tem que tomar uma decisão e resolvê-la;</p>
              <p style='text-indent:0em'>• Sua filosofia é “sempre se pode fazer melhor” e, “se funciona, significa que é bom”. </p>
            </p>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="imagens/pragmatico.jpg">
          </div>
          <div class="col-lg-4">
            <div style="text-align:center"><h3 class="titulo titulo-secondary">Características </h3> </div>
            <p style="text-indent:0em">
              <p style='text-indent:0em'><strong>Principais:</strong> experimentador, prático, direto, eficaz e realista. </p>
              <p style='text-indent:0em'><strong>Secundárias:</strong> técnico, útil, rápido, decidido, planejador, positivo, concreto, objetivo, claro, seguro de si, organizador, atual, solucionador de problemas, aplicador do que aprendeu, planeja ações.</p>
            </p>
          </div>
        </div>
      </div>
      <div class="clear"></div>                  



    <div style="text-align:center"><h3 class="titulo titulo-secondary">Apenas ao ler esses estilos de aprendizagem, você conseguiu se identificar com algum deles? </h3> </div>
    <p>Para auxiliar esse processo de descoberta, disponibilizamos em nosso ambiente virtual um questionário desenvolvido por Alonso, Gallego e Honey (2002), para a identificação do estilo de aprendizagem predominante em cada pessoa. Vamos respondê-lo? </p>
    <p>Após concluir o questionário, registre o seu resultado na enquete que também está lançada no Moodle. Assim poderemos conhecer o perfil da turma quanto às preferências de aprendizagem.</p>


  </div>


</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina9.php', 'aula2pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
