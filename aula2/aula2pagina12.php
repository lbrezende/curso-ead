<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','12', '15', 'aula2pagina11.php', 'aula2pagina13.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">O tutor e a mediação</h3>

    <p>De acordo com Souza et al (2004), “[N]a modalidade de Educação a Distância existem três elementos fundamentais em interação: aluno, material didático e professor”.</p>
    <p>Para Sales e Nonato (2007), no contexto da EaD:</p>

      <div class="col-lg-offset-2 col-lg-10 quote">
          <p>(...) o material didático é alçado a uma posição de grande importância, pois é ele que, ao lado do mediador, poderá possibilitar ao sujeito-aprendente um lugar de autonomia e criticidade que permita desenvolver-se como sujeito autônomo e crítico ao tempo em que constrói o conhecimento objetivo a que se propôs. </p>
      </div>  

    <p>Tendo em vista esses apontamentos, constatamos que o tutor tem um papel de grande relevância em contextos de educação <em>online</em>, notadamente no que concerne aos materiais didáticos utilizados.</p>
  </div>
</div>

<?php  configNavegacaoRodape('exibir', 'aula2pagina11.php', 'aula2pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
