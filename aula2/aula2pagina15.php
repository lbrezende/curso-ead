<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aspectos relacionados à aprendizagem a distância', 'exibir', '2','15', '15', 'aula2pagina14.php', 'aula2pagina15.php', 'Produção de Conteúdos para a EaD');
?> 
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Referências</h3>

	<p style="text-indent:0em">AMARAL, S. F.; BARROS, D. M. V. B. <strong>Estilos de aprendizagem no contexto educativo de uso das tecnologias digitais interativas.</strong> Disponível em: <a href="http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf" target="_blank">http://www.lantec.fe.unicamp.br/lantec/pt/tvdi_portugues/daniela.pdf</a>. Acesso em: abr. 2015.</p>
	<p style="text-indent:0em">CARVALHO, J. A.; PEDROTE, M. C. et al. <strong>Andragogia</strong>: considerações sobre a aprendizagem do adulto. REMPEC, Ensino Saúde e Ambiente, v. 3, n. 1, p. 78-90, abr. 2010.</p>
	<p style="text-indent:0em">MACHADO, M.; FERREIRA, S. M. B.; AQUINO, V. <strong>A mediação pedagógica à luz do pensar complexo:</strong> uma análise comparativa da aula em ambiente virtual e presencial. Col@bora. Revista Digital da CVA. Ricesu, v. 6, n. 23, jul. 2010. Disponível em: <a href="http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116" target="_blank">http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116</a>. Acesso em: mar. 2015.</p>
	<p style="text-indent:0em">MORAES, M. A.; PESCE, L.; BRUNO, A. R. <strong>Pesquisando fundamentos para novas práticas na educação online.</strong> São Paulo: RG Editores, 2008.</p>
	<p style="text-indent:0em">SALES, Mary; NONATO, Emanuel. <strong>EaD e material didático:</strong> reflexões sobre mediação pedagógica. Mai. 2007. Disponível em: <a href="http://www.abed.org.br/congresso2007/tc/552007104704PM.pdf" target="_blank">http://www.abed.org.br/congresso2007/tc/552007104704PM.pdf</a>. Acesso em: abr. 2015. </p>
	<p style="text-indent:0em">SOUZA, Carlos Alberto et al.  <strong>Tutoria na educação a distância.</strong> Disponível em: <a href="http://www.abed.org.br/congresso2004/por/htm/088-TC-C2.htm" target="_blank">http://www.abed.org.br/congresso2004/por/htm/088-TC-C2.htm</a>. Acesso em: abr. 2015.</p>
	<p style="text-indent:0em">TORRES, T. Z.; AMARAL, S. F. <strong>Aprendizagem colaborativa e Web 2.0:</strong> proposta de modelos de organização de conteúdos interativos. ETD – Educ.Tem. Dig.,Campinas, v. 12, n. esp., p. 49-72, mar. 2011. Disponível em: <a href="https://www.fe.unicamp.br/revistas/ged/etd/article/view/2281/pdf_51" target="_blank">https://www.fe.unicamp.br/revistas/ged/etd/article/view/2281/pdf_51</a>. Acesso em: mar. 2015.</p>
	<p style="text-indent:0em">VERASZTO, E. V. et al. Educação a distância e estilos de aprendizagem: estratégicas educativas apoiadas pelas TIC. In: BARROS, Daniela Melaré Vieira (Org.). <strong>Estilos de aprendizagem na atualidade.</strong> Lisboa, 2011. v. 1, (E-book online). </p>
	<p style="text-indent:0em">Imagens: &lt;<a href="http://www.pixabay.com" target="_blank">http://www.pixabay.com</a>&gt; Licença CCO Public Domain.</p>

  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula2pagina14.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
