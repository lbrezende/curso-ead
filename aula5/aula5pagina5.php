<?php 
//Configurar Aula
$aula = 5; 
$pagina = 5; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
		<h3 class="titulo titulo-primary">5.3 Quantidade de páginas </h3>
		<p>Cada uma das aulas previstas no conteúdo programático deve conter, no máximo, 20 páginas de texto escrito. </p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Mas por quê?</h3></div>
		<p>Você há de concordar conosco que textos muito longos podem se tornar enfadonhos e dispersar a atenção do leitor. Por isso, procure seguir a nossa recomendação. </p>
		<p>Caso uma aula ultrapasse o limite, avalie a possibilidade de retirar alguns pontos ou de dividir o assunto, isto é, de criar uma nova aula ou de transformar alguns desses itens em texto complementar. O importante é manter o bom senso e focar no aprendizado do aluno. </p>
		<p><span class="semi-bold">Observação:</span> A utilização de gráficos e tabelas, entre outros recursos, em uma quantidade considerável repercute na quantidade de páginas. Nessas situações, o número poderá ser ampliado, sem maiores inconvenientes.</p>
		<p>Neste momento, vamos à temática central da nossa conversa?</p>
      </div>
 </div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

