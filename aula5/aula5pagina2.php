<?php 
//Configurar Aula
$aula = 5; 
$pagina = 2; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
  $paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
  $paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<div class="bloco-pontilhado" style="margin-right: 15px;">
		<p style="text-indent: 1.3em !important;"><span class="semi-bold">Exemplo</span>: No curso “Produção de Conteúdo para a Educação a Distância”, pensamos assim:</p>
		<p style="text-indent: 1.3em !important;">Nos primeiros momentos, o aluno precisa conhecer as características da educação a distância, já que ela é o pano de fundo para todas as discussões. Ele também precisa compreender as particularidades da aprendizagem nessa modalidade de ensino, posto que esse é o foco da escrita. </p>
		<p style="text-indent: 1.3em !important;">Então, criamos um módulo para tratar essas questões, com o título:</p>
		<p style="text-indent: 1.3em !important;">Módulo I: EaD e aprendizagem (tema amplo). </p>
		<p style="text-indent: 1.3em !important;">Subdividimos esse tema em duas partes, para organizar melhor a nossa escrita e facilitar o estudo dos alunos: </p>
		<p style="text-indent: 1.3em !important;"><span class="semi-bold">a) Aula 1:</span> Contextualização da Educação a Distância </p>
		<p style="text-indent: 1.3em !important;"><span class="semi-bold">b) Aula 2:</span> Aspectos relacionados à aprendizagem a distância</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="margin-top:20px">Mas por que essa explanação?</h3></div>
		<p style="text-indent: 1.3em !important;">Para você perceber que, na prática, produzirá o texto das aulas previstas no conteúdo programático. Por isso, o enfoque neste momento do nosso estudo é a estrutura interna das aulas.</p>
		<p style="text-indent: 1.3em !important;">Ao final, esperamos que você produza conteúdos em formato de aulas de acordo com as necessidades da educação a distância.</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Tudo bem? Vamos começar?</h3></div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'fim', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

