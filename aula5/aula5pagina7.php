<?php 
//Configurar Aula
$aula = 5; 
$pagina = 7; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.4.1 Introdução da aula</h3>
	<p>Na introdução, você estabelece o primeiro contato com o aluno e ele com você. Então, esse é o momento de apresentar a proposta da aula e de esclarecer quais temas serão tratados. Com isso, o leitor pode “saber onde está pisando” e formular as próprias expectativas em relação ao estudo.</p>
	<p>Para auxiliar a sua escrita, indicamos alguns aspectos que devem ser inseridos no texto introdutório. Clique nos itens para visualizar a explicação:</p>
    <div><a href="javascript:void(0);" class="botao-letraA" style="margin-bottom:20px"><img src="imagens/letraA.png" class="img-responsiva" alt="Saudação ao leitor" width="277px" height="50px" /></a></div>
    <div style="display:none" class="conteudo-letraA">
    	<img src="imagens/letraA.png" class="img-responsiva" alt="Saudação ao leitor" width="277px" height="50px" />
		<p style="margin-top:15px">Antes de tudo, estamos em um diálogo com nosso estudante, certo? Por essa razão, iniciamos a nossa escrita com uma breve saudação ao aluno. </p>
		<p>Você pode ficar à vontade para cumprimentar de acordo com o seu próprio estilo. Não precisa ser algo elaborado demais. Seja simples e direto: <em>Olá!, Seja bem-vindo!, Olá novamente!, Bem-vindo à nossa primeira aula!</em></p>
    </div>      
	
    <div><a href="javascript:void(0);" class="botao-letraB" style="margin-bottom:20px"><img src="imagens/letraB.png" class="img-responsiva" alt="B) Apresentação e contextualização do tema da aula" width="612px" height="50px"  /></a></div>
    <div style="display:none" class="conteudo-letraB">
    	<img src="imagens/letraB.png" class="img-responsiva" alt="B) Apresentação e contextualização do tema da aula" width="612px" height="50px"  />
		<p style="margin-top:15px">Registre os assuntos que serão desenvolvidos e procure contextualizar esses temas: aproximar o aluno da proposta da aula logo no início. </p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Como assim?</h3></div>
		<p>Elabore alguma situação com a finalidade de trazer o leitor para o universo temático da aula. Significa que você deve dizer algo mais que apenas: <em>“Estudaremos, nesse momento, a estrutura das aulas”</em>. Agregue mais elementos à sua escrita. Como exemplo, sugerimos que reveja as introduções das aulas do nosso curso. <a href="Aula_5_Introducao_das_aulas.pdf" target="_blank" title="clique aqui para ver as introduções das aulas">Clique aqui</a> para visualizá-las.</p>
		<p>Vários recursos podem ser utilizados para essa finalidade, tais como: vídeos, imagens, perguntas reflexivas, histórias, fatos sobre a realidade de trabalho dos estudantes, explicação de conceitos, enfim, o importante é criar um “clima” favorável para o estudo.</p>
		<p>Com isso, você poderá envolver o aluno no diálogo e ajudá-lo a começar a refletir sobre a proposta da aula. </p>
		<p><span class="semi-bold">Observação:</span> A partir da segunda aula do seu curso, antes de contextualizar o assunto, retome rapidamente o que foi tratado na aula anterior. Isso para que o aluno recorde o que foi desenvolvido previamente e perceba a sequência dos estudos.</p>
    </div>      
    <div><a href="javascript:void(0);" class="botao-letraC" style="margin-bottom:20px"><img src="imagens/letraC.jpg" class="img-responsiva" alt="C) Objetivos em relação à aprendizagem dos estudantes" height="50px"  /></a></div>
    <div style="display:none;" class="conteudo-letraC">
    	<img src="imagens/letraC.jpg" class="img-responsiva" alt="C) Objetivos em relação à aprendizagem dos estudantes" height="50px"  />
		<p style="margin-top:15px">Escreva o que você espera do aluno ao final daquela aula. Você já definiu o objetivo no seu plano de trabalho; agora, é só trazê-lo para a introdução. </p>
		<p><span class="semi-bold">Note:</span> Algumas vezes, a transcrição literal desse objetivo não se mostra adequada no contexto da introdução. Nesses casos, para manter a coerência, efetue os ajustes necessários para incluir o objetivo no texto.</p>
		<p>Ao conhecer a meta da aula, o aluno poderá direcionar os próprios caminhos de aprendizagem. Ainda, poderá se preparar para realizar a atividade avaliativa relacionada ao tema. </p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="margin-top:20px">Ficou claro como a introdução deve ser redigida?</h3></div>
	</div>      
	<div style="margin-top:20px;" class="text-center semi-bold">Recapitulando...</div>
	<div style="text-align:center; margin-top:20px; "><h3 style="font-size:26px"><span class="semi-bold tituloCinza" >Introdução da aula:</span></h3></div>
	<div class="row">
		<div class="col-lg-2" style="text-align:center;"><span class="semi-bold verdeClaro">Saudação</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-4" style="text-align:center;"><span class="semi-bold verdeClaro">Apresentação/Contextualização</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-4" style="text-align:center;"><span class="semi-bold verdeClaro">Objetivo específico da aula</span></div>
	</div>

	<p>&nbsp;</p>
	<p>Seguiremos, então, para o desenvolvimento da aula.</p>    	
	

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

