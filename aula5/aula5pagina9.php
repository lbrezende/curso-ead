<?php 
//Configurar Aula
$aula = 5; 
$pagina = 9; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.2 Desenvolvimento da aula</h3>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Então, conseguiu estabelecer pontos de contato com o assunto da nossa aula?</h3></div>
	<p>Nós encontramos alguns, analise:</p>
	<img src="imagens/letraA_2.png" class="img-responsiva" height="50px" alt="É necessário ter em mãos os elementos que serão utilizados no trabalho">
	<div class="row" style="margin-top:20px;">
		<div class="col-lg-6">
			<p style="text-indent:0em !important;">1) Antes de começar a escrever sua aula, pesquise materiais bibliográficos que poderão dar sustentação teórica à sua produção. </p>
			<p style="text-indent:0em !important;">Verifique se a fonte de dados é confiável e opte por referências atualizadas.</p>
			<p style="text-indent:0em !important;">Durante a escrita da aula, ao parafrasear ou transcrever o trabalho de outros autores, aplique as regras de citação da Associação Brasileira de Normas Técnicas (ABNT).  </p>
			<p style="text-indent:0em !important;">Veja as principais formas de <a href="https://goo.gl/ZHY5V4" target="_blank">citação</a> descritas no Manual de Orientações para o trabalho do conteudista aqui do STF.</p>
			<p style="text-indent:0em !important;">Além disso, indicamos alguns sites para consulta a esse respeito:</p>
			<div style="text-align:center;margin-bottom:20px;"><a href="http://www.bu.ufsc.br/design/Citacao1.htm" target="_blank" class="btn btn-info">UFSC - Citação</a></div>
			<div style="text-align:center;"><a href="http://www.leffa.pro.br/textos/abnt.htm#citacoes" target="_blank" class="btn btn-info">Leffa - Citações</a></div>
			<div class="bloco-pontilhado" style="margin:40px 0px 20px 0px;" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:30px;padding:0px 10px;">
			      <p style="text-indent:1.3em !important;">Caso não tenha todas as informações recomendadas pela ABNT, procure citar, pelo menos, autor e ano da obra consultada, entre parênteses, por exemplo: (POSSARI, 2009).</p>
			    </div>
			  </div>
			</div>		
			<p><span class="semi-bold">Observação:</span> Podemos basear nossa redação nas ideias de outros autores, mas a eles devemos dar os devidos créditos. Então, cuidado com o <a href="http://www.producao.ufrgs.br/arquivos/arquivos/PLAGIO_EsclarecimentoSobre.pdf" target="_blank">plágio</a>.</p>
		</div>
		<div class="col-lg-6">
			<div class="bloco-pontilhado" style="margin:40px 0px 20px 0px;" class="">
			  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-20px; margin-bottom:10px;">
			  <div id="conteudo-cerebro">
			    <div class="row" style="text-align:center; margin-bottom:30px;padding:0px 10px; ">
					<p class="text-center">Você conhece o Google Livros?</p>
					<p class="text-center">Esta ferramenta pode auxiliá-lo nas pesquisas de material bibliográfico. Nela, você pode encontrar obras de diversos assuntos em meio eletrônico. Navegue em:</p>
					<p class="text-center"> <a href="https://books.google.com.br/" target="_blank" class="btn btn-info">Google Livros</a> </p>

			    </div>
			  </div>
			</div>

			<p style="text-indent:0em !important;">2) Além disso, busque outros recursos que poderão ser utilizados para enriquecer o texto: sites, textos complementares, artigos, vídeos, imagens, quadrinhos, poemas, letras de música, frases, histórias, entrevistas com profissionais da área, entre outros.</p>
			<div class="bloco-pontilhado" style="margin:40px 0px 20px 0px;" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-20px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:30px; padding:0px 10px;">
			      <p style="text-indent:1.3em !important;">Sempre registre as referências dos recursos que pesquisou. Você pode criar um arquivo exclusivo para isso. Caso decida incluir algum deles na sua aula, deverá citar a respectiva fonte para preservar a questão dos direitos autorais (conversaremos mais a esse respeito na próxima aula).</p>
			    </div>
			  </div>
			</div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

