<?php 
//Configurar Aula
$aula = 5; 
$pagina = 17; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.4.5 Referências </h3>
	<p>Todos os materiais bibliográficos utilizados na escrita da sua aula deverão ser indicados nesse momento, tanto os trechos que foram citados como os que foram consultados e que embasaram a sua escrita. Isso inclui, também, páginas da internet, manuais, dicionários e referências contidas nas notas de rodapé.</p>
	<p>Siga, para tanto, as normas da ABNT.  Veja alguns exemplos <a href="imagens/Referencias_Bibliograficas_Manual_do_Conteudista_STF.docx" target="_blank" title="arquivo com exemplos com as normas da ABNT">aqui</a> (as informações foram extraídas do Manual de Orientações do Conteudista do STF). </p>
	<p>Você também pode consultar o tópico “4 – Modelos de Referências” do site: <a href="http://www.bu.ufsc.br/framerefer.html" target="_blank" title="acesse os modelos de referências">http://www.bu.ufsc.br/framerefer.html</a>.</p>

	<p><span class="semi-bold">Observação:</span> As notas de rodapé devem ser utilizadas, preferencialmente, para pequenas explicações ou comentários do autor. Evite usá-las para indicar referências bibliográficas, pois, quando estas são registradas nas notas de rodapé de maneira excessiva, compromete-se o aspecto visual do texto.</p>

  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
