<?php 
//Configurar Aula
$aula = 5; 
$pagina = 12; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.4.3 Conclusão da aula</h3>
	<p>Depois de escrever todos os tópicos temáticos previstos para a aula, é necessário realizar o fechamento das ideias. Para tanto, retome brevemente os principais pontos discutidos. </p>
	<p>Em seguida, estabeleça uma ponte com os assuntos da próxima unidade de estudo. Isto é, retrate as “cenas dos próximos capítulos”, como forma de aguçar a curiosidade dos alunos e de situá-los na trilha de aprendizagem do curso.</p>
	<p>Aqui no nosso curso, denominamos essa etapa como “Considerações Finais”. Como exemplo, vamos revisitar a conclusão das aulas anteriores? Observe:

	<div class="bloco-pontilhado" style="margin-top:50px" class="">
	  <img src="imagens/etiquetaAula1.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;">    </p>
	  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; padding:10px">
	      <p style="text-indent:1.3em !important;">Para resgatarmos rapidamente os temas lançados, nesta aula: conversamos sobre os aspectos históricos da EaD até chegarmos aos dias de hoje. Percebemos que a modalidade é fundamental para promover ações educativas desde épocas anteriores. Ainda, observamos que ela evoluiu no tempo, acompanhou os avanços das tecnologias e, hoje, vivencia grande expansão, notadamente no campo da educação continuada de profissionais. </p>
		  <p style="text-indent:1.3em !important;">Feitas essas considerações, na próxima aula trocaremos ideias a respeito do estudo online e das particularidades da aprendizagem na educação a distância. </p>
	    </div>
	  </div>
	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

