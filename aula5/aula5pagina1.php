<?php 
//Configurar Aula
$aula = 5; 
$pagina = 1; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
  $paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
  $paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">

<div style="text-align:center"><h3 class="titulo titulo-primary">Bem-vindo à nossa quinta aula!</h3></div>
<p>Agora que você já conhece a linguagem que deve ser utilizada na escrita do material didático, vamos dialogar sobre a apresentação das suas ideias no texto das aulas. </p>
<p>Você se recorda do <a href="https://goo.gl/uiFrkM" target="_blank" title="link para o Plano de Elaboração do Material Didático">Plano de Elaboração do Material Didático</a>, criado no Módulo III (colocar link para o Modelo de Plano)? Nele, foram organizados os tópicos do conteúdo programático que você delimitou para o seu curso, certo?</p>
<p>Aqui, no nosso treinamento, distribuímos os assuntos em módulos e em aulas. No seu planejamento, outra maneira pode ter sido escolhida, sem problemas.</p>
<p>A título de informação, os <span class="semi-bold">módulos</span> são temas amplos, dentro do conteúdo programático, que podem ser subdivididos em outros pontos para compor o entendimento sobre a temática central abordada. </p>
<p>Cada subdivisão é considerada uma <span class="semi-bold">aula</span>, cuja temática delimitada será desenvolvida em função do módulo. As aulas ainda podem ser divididas em tópicos para facilitar o encadeamento das ideias. </p>
<p>Visualize novamente o conteúdo programático do curso: </p>
			
			<table class="table-ead  table-hover" cellpadding="10">
			  <thead>
			    <tr>
			      <th style="width:25%; font-size:22px; text-align:center;background:#B5C2CB;color:#000 !important" class='table-title semi-bold' >MÓDULO I</th>
			      <th align="center" class='table-title' style="width:25%; font-size:22px; text-align:center;color:#000 !important">MÓDULO II</th>
			      <th style="width:50%; font-size:22px; text-align:center;color:#000 !important" class='table-title' colspan="2">MÓDULO III</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td style="background:#CCDDEE; border-radius:20px;" ><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 1</p>
			        <strong>CONTEXTUALIZAÇÃO DA EDUCAÇÃO A DISTÂNCIA</strong><br />
			        <strong>1.1</strong> Breve histórico mundial da EaD <br />
			        <strong>1.2</strong> A EaD no Brasil <br />
			        <strong>1.3</strong> A EaD na atualidade</td>
			      <td style="background:#9FC3E2; border-radius:20px;" valign="top" rowspan="2"><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 3</p>
			        <strong>POR QUE PLANEJAR A ESCRITA </strong><br />
			        <strong>3.1</strong> Elaboração do material didático no STF <br />
			        <strong>3.2</strong> Plano de elaboração do material didático <br />
			        <strong>3.2.1</strong> Principais itens do planejamento</td>
			      <td style="background:#8CA5C1; border-radius:20px;"><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 4</p>
			        <strong>LINGUAGUEM PARA A EaD </strong><br />
			        <strong>4.1</strong> A importância da comunicação na EaD <br />
			        <strong>4.2</strong> O leitor na EaD <br />
			        <strong>4.3</strong> Linguagem dialógica <br />
			        <strong>4.4</strong> Correção gramatical</td>
			      <td style="background:#8CA5C1; border-radius:20px;" valign="top" rowspan="2"><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 5</p>
			        <strong>ESTRUTURA DA AULA </strong><br />
			        <strong>5.1</strong> Organização geral do material didático <br />
			        <strong>5.2</strong> Formatação dos arquivos de texto <br />
			        <strong>5.3</strong> Quantidade de páginas <br />
			        <strong>5.4</strong> Estrutura básica da aula <br />
			        <strong>5.4.1</strong> Introdução da aula <br />
			        <strong>5.4.2 </strong>Desenvolvimento da aula <br />
			        <strong>5.4.3</strong> Conclusão da aula <br />
			        <strong>5.4.4</strong> Atividade avaliativa <br />
			        <strong>5.4.5</strong> Referências</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4; border-radius:20px;"><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 2</p>
		          <strong>ASPECTOS RELACIONADOS À APRENDIZAGEM A DISTÂNCIA</strong><br />
		          <strong>2.1</strong> A Aprendizagem: um processo construtivo <br />
		          <strong>2.2</strong> Estilos de aprendizagem <br />
		          <strong>2.3</strong> O tutor e a mediação</td>
			      <td style="background:#8CA5C1; border-radius:20px;"><p style="color:#396E94;text-align:center !important;text-indent:0em; font-size:20px" class="semi-bold">Aula 6</p>
		          <strong>UTILIZAÇÃO DE RECURSOS PARA COMPLEMENTAR O TEXTO</strong><br />
		          <strong>6.1</strong> Orientações iniciais <br />
		          <strong>6.2</strong> Imagens <br />
		          <strong>6.3</strong> Vídeos <br />
		          <strong>6.4</strong> <em>Hyperlinks </em><br />
		          <strong>6.5</strong> Destaques no conteúdo <br />
		          <strong>6.6</strong> Um pouco sobre direitos autorais na EaD</td>
			    </tr>
			  </tbody>


			</table>



  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'fim', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

