<?php 
//Configurar Aula
$aula = 5; 
$pagina = 19; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">REFERÊNCIAS</h3>

	<p style="text-indent:0em !important;">FIOCRUZ. Escola Nacional de Saúde Pública. Programa de Educação a distância. <span class="semi-bold">Elaboração de material didático impresso para programas de formação a distância:</span> orientações aos autores. Disponível em: <<a href="http://www.ufrgs.br/nucleoead/documentos/ENSPMaterial.pdf" target="_blank">http://www.ufrgs.br/nucleoead/documentos/ENSPMaterial.pdf</a>>. Acesso em: ago. 2015.</p>

	<p style="text-indent:0em !important;">POSSARI, L. H. V. <span class="semi-bold">Material didático para a EaD:</span> processo de produção. EdUFMT, 2009.</p>

	<p style="text-indent:0em !important;">ZANETTI, Alexsandra. <span class="semi-bold">Elaboração de materiais para a educação a distância</span>. Disponível em: <<a href="http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf" target="_blank">http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf</a>>. Acesso em: jul. 2015.</p>
	<p style="text-indent:0em !important;"><span style="text-indent:0em">Imagens: &lt;<a href="http://www.pixabay.com" target="_blank">http://www.pixabay.com</a>&gt; Licença CCO Public Domain.</span></p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
