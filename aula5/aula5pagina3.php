<?php 
//Configurar Aula
$aula = 5; 
$pagina = 3; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.1 Organização geral do material didático</h3>
	<p>Antes de tudo, você precisa ter uma visão geral do trabalho a ser desenvolvido.</p>
	<p>Nesse sentido, o material didático é formado, basicamente, pela apresentação do curso e pelas aulas escritas: </p>
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-11"><p style="text-indent:0em;"><span class="semi-bold">a.  Texto de apresentação do curso</span>, que contempla: considerações gerais sobre o curso; histórico e cenário atual do tema, quando apropriado; objetivos; conteúdo programático; nível de aprofundamento da abordagem; questões que o curso pretende esclarecer ou responder; relevância dos temas tratados e/ou benefícios esperados pela instituição com o treinamento; entre outras questões. </p></div>
	</div>	
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10"><p style="text-indent:0em;"><span class="semi-bold">Observação:</span> Esse texto pode ser elaborado ou revisado após a conclusão do material propriamente dito, quando você terá mais elementos para redigi-lo.</p></div>
	</div>	
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-11"><p style="text-indent:0em;"><span class="semi-bold">b.  Texto de cada uma das aulas</span>, conforme o conteúdo programático definido. Os itens que devem ser contemplados no texto das aulas serão abordados logo adiante, no tópico 5.4. </p></div>
	</div>	
	<p>&nbsp;</p>
	
	<div class="row">
	  <div class="col-lg-3" style="text-align:center;"><span class="semi-bold">Apresentação do curso</span><br /><img src="imagens/apresentacao_curso.png" style="width:75%;" /></div>
		<div class="col-lg-3" style="padding-top:50px;text-align:center;"><span class="semi-bold">Aula 1</span><br /><img src="imagens/aula1.png" style="width:75%;" /></div>
		<div class="col-lg-3" style="text-align:center;"><span class="semi-bold">Aula 2</span><br /><img src="imagens/aula2.png" style="width:75%;" /></div>
		<div class="col-lg-3" style="padding-top:50px;text-align:center;"><span class="semi-bold">Aula 3</span><br /><img src="imagens/aula3.png" style="width:75%;" /></div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

