<?php 
//Configurar Aula
$aula = 5; 
$pagina = 2; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', '4','1', '12', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Título</h3>

<p>Bem-vindo à nossa quinta aula!</p>
<p>Agora que você já conhece a linguagem que deve ser utilizada na escrita do material didático, vamos dialogar sobre a apresentação das suas ideias no texto das aulas. </p>
<p>Você se recorda do Plano de Elaboração do Material Didático, criado no Módulo III (colocar link para o Modelo de Plano)? Nele, foram organizados os tópicos do conteúdo programático que você delimitou para o seu curso, certo?</p>
<p>Aqui, no nosso treinamento, distribuímos os assuntos em módulos e em aulas. No seu planejamento, outra maneira pode ter sido escolhida, sem problemas.</p>
<p>A título de informação, os módulos são temas amplos, dentro do conteúdo programático, que podem ser subdivididos em outros pontos para compor o entendimento sobre a temática central abordada. </p>
<p>Cada subdivisão é considerada uma aula, cuja temática delimitada será desenvolvida em função do módulo. As aulas ainda podem ser divididas em tópicos para facilitar o encadeamento das ideias. </p>
<p>Clique aqui para visualizar novamente o conteúdo programático do curso: </p>
<p>(colocar o conteúdo para abrir em nova janela. como ele vai sofrer alguns ajustes, é bom colocarmos quando o curso estiver finalizado)</p>
<p>Exemplo: No curso “Produção de Conteúdo para a Educação a Distância”, pensamos assim:</p>
<p>Nos primeiros momentos, o aluno precisa conhecer as características da educação a distância, já que ela é o pano de fundo para todas as discussões. Ele também precisa compreender as particularidades da aprendizagem nessa modalidade de ensino, posto que esse é o foco da escrita. </p>
<p>Então, criamos um módulo para tratar essas questões, com o título:</p>
<p>Módulo I: EaD e aprendizagem (tema amplo). </p>
<p>Subdividimos esse tema em duas partes, para organizar melhor a nossa escrita e facilitar o estudo dos alunos: </p>
<p>a) Aula 1: Contextualização da Educação a Distância </p>
<p>b) Aula 2: Aspectos relacionados à aprendizagem a distância</p>
<p>Mas por que essa explanação?</p>
<p>Para você perceber que, na prática, produzirá o texto das aulas previstas no conteúdo programático. Por isso, o enfoque neste momento do nosso estudo é a estrutura interna das aulas.</p>
<p>Ao final, esperamos que você produza conteúdos em formato de aulas de acordo com as necessidades da educação a distância.</p>
<p>Tudo bem? Vamos começar?</p>

<p>5.1 Organização geral do material didático</p>
<p>Antes de tudo, você precisa ter uma visão geral do trabalho a ser desenvolvido.</p>
<p>Nesse sentido, o material didático é formado, basicamente, pela apresentação do curso e pelas aulas escritas: </p>
<p>a.  Texto de apresentação do curso, que contempla: considerações gerais sobre o curso; histórico e cenário atual do tema, quando apropriado; objetivos; conteúdo programático; nível de aprofundamento da abordagem; questões que o curso pretende esclarecer ou responder; relevância dos temas tratados e/ou benefícios esperados pela instituição com o treinamento; entre outras questões. </p>
<p>Dica: Esse texto pode ser elaborado ou revisado após a conclusão do material propriamente dito, quando você terá mais elementos para redigi-lo.</p>

<p>b.  Texto de cada uma das aulas, conforme o conteúdo programático definido. Os itens que devem ser contemplados no texto das aulas serão abordados logo adiante, no tópico 5.4. </p>


<p>A intenção aqui é criar um esquema visual para o conteudista entender como é o material didático. Pensei em criar um desenho como se fossem arquivos do word, um próximo do outro, com os títulos correspondentes.</p>
<p>Apresentação</p>
<p>do curso</p>
<p>  Aula 1  Aula 2  Aula 3</p>

<p>5.2 Formatação dos arquivos de texto</p>
<p>Os arquivos do material devem ser apresentados com as seguintes especificações de formatação:</p>
<p>Conferir o que decidimos sobre isso e colocar um desenho de folha word com as especificações dentro dela. O texto das formatações no lado esquerdo e o desenho no lado direito (lado a lado):</p>

<p>2cm</p>

<p>parágrafo</p>
<p>2cm                                              2cm</p>

<p>                           </p>

<p>                            2cm</p>
<p>1</p>

<p>• Papel: formato A4, com dimensões de 21 x 29,7 cm. </p>
<p>• Editor de texto: Word 2010. </p>
<p>• Margens: superior - 2 cm; inferior - 2 cm; esquerda - 2 cm; direita - 2 cm. </p>
<p>• Fonte: arial, tamanho 12, cor preta.</p>
<p>• Alinhamento: justificado.</p>
<p>• Espaçamento entre linhas: 1,5 cm.</p>
<p>• Espaçamento entre parágrafos: antes - 6 pt; depois - 6 pt.</p>
<p>• Recuo esquerdo da primeira linha do parágrafo: 2 cm. </p>
<p>• Nomear o arquivo com a sequência: número da aula – nome da aula.</p>
<p>o Exemplo: Aula 1 – Contextualização da EaD</p>
<p>• Numeração de páginas: inferior e à direita. </p>

<p>Observação: Esse é o padrão utilizado atualmente pela Seção de Educação a Distância do Supremo Tribunal Federal. Ao exercer a atividade de conteudista para cursos do STF, você deverá entregar o material de acordo com essa formatação. </p>

<p>5.3 Quantidade de páginas </p>
<p>Cada uma das aulas previstas no conteúdo programático deve conter, no máximo, 20 páginas de texto escrito. </p>
<p>Mas por quê?</p>
<p>Você há de concordar conosco que textos muito longos podem se tornar enfadonhos e dispersar a atenção do leitor. Por isso, procure seguir a nossa recomendação. </p>
<p>Caso uma aula ultrapasse o limite, avalie a possibilidade de retirar alguns pontos ou de dividir o assunto, isto é, de criar uma nova aula ou de transformar alguns desses itens em texto complementar. O importante é manter o bom senso e focar no aprendizado do aluno. </p>
<p>Observação: A utilização de gráficos e tabelas, entre outros recursos, em uma quantidade considerável repercute na quantidade de páginas. Nessas situações, o número poderá ser ampliado, sem maiores inconvenientes.</p>
<p>Neste momento, vamos à temática central da nossa conversa?</p>

<p>5.4 Estrutura básica da aula</p>
<p>De maneira geral, cada aula é composta por: introdução, desenvolvimento, conclusão, atividades avaliativas e referências bibliográficas. Essas etapas integram o todo textual e, por conseguinte, precisam demonstrar sintonia entre si. </p>
<p>Falando em sintonia, você já teve a oportunidade de assistir a um concerto musical?</p>
<p>Convido você para ouvir a Orquestra Sinfônica de Brasília tocando o tema do filme </p>
<p>“Star Wars”: https://www.youtube.com/watch?v=_DN19SOcihM  </p>
<p>Notou a harmonia entre as diferentes partes da música?</p>
<p>Essa apresentação nos inspira a compreender que as aulas precisam de articulação. Em outras palavras, as ideias do texto devem estar ligadas umas às outras para que ocorra, de fato, a construção de significado por parte do aluno.</p>
<p>Além disso, destacamos que a subdivisão interna da aula tem finalidade didática, pois confere uma organização textual que ajuda o estudante a se situar durante a leitura.</p>
<p>Conversaremos acerca das etapas intrínsecas à aula logo na sequência.</p>
<p>  Título da aula</p>
<p>           Introdução da aula</p>
<p>           Breve retomada dos assuntos anteriores, quando for o caso.</p>
<p>           Apresentação e contextualização do tema da aula.</p>
<p>           Objetivos da aula.</p>

<p>           Desenvolvimento da aula</p>
<p>           Texto da aula propriamente dito.</p>
<p>                           </p>
<p>          Conclusão da aula</p>
<p>           Síntese das principais ideias do texto.</p>
<p>           Ponte entre as aulas.</p>
<p>                            </p>
<p>           Atividade avaliativa</p>
<p>           Atividade para fixar a aprendizagem dos alunos.</p>
<p>                          </p>
<p>            Referências bibliográficas</p>
<p>           Indicação do material bibliográfico utilizado na escrita.</p>


<p>5.4.1 Introdução da aula</p>
<p>Na introdução, você estabelece o primeiro contato com o aluno e ele com você. Então, esse é o momento de apresentar a proposta da aula e de esclarecer quais temas serão tratados. Com isso, o leitor pode “saber onde está pisando” e formular as próprias expectativas em relação ao estudo.</p>
<p>Para auxiliar a sua escrita, indicamos alguns aspectos que devem ser inseridos no texto introdutório:</p>
<p>a.  Saudação ao leitor </p>
<p>Antes de tudo, estamos em um diálogo com nosso estudante, certo? Por essa razão, iniciamos a nossa escrita com uma breve saudação ao aluno. </p>
<p>Você pode ficar à vontade para cumprimentar de acordo com o seu próprio estilo. Não precisa ser algo elaborado demais. Seja simples e direto: Olá!, Seja bem-vindo!, Olá novamente!, Bem-vindo à nossa primeira aula!</p>

<p> b.  Apresentação e contextualização do tema da aula</p>
<p>Registre os assuntos que serão desenvolvidos e procure contextualizar esses temas: aproximar o aluno da proposta da aula logo no início. </p>
<p>Como assim?</p>
<p>Elabore alguma situação com a finalidade de trazer o leitor para o universo temático da aula. Significa que você deve dizer algo mais que apenas: “Estudaremos, nesse momento, a estrutura das aulas”. Agregue mais elementos à sua escrita. Como exemplo, sugerimos que reveja as introduções das aulas do nosso curso. ( criar um arquivo com as introduções das aulas para colocar como link; na aula interativa, poderia abrir um balao  para mostrar os textos de introdução)</p>
<p>Vários recursos podem ser utilizados para essa finalidade, tais como: vídeos, imagens, perguntas reflexivas, histórias, fatos sobre a realidade de trabalho dos estudantes, explicação de conceitos, enfim, o importante é criar um “clima” favorável para o estudo.</p>
<p>Com isso, você poderá envolver o aluno no diálogo e ajudá-lo a começar a refletir sobre a proposta da aula. </p>
<p>Observação: A partir da segunda aula do seu curso, antes de contextualizar o assunto, retome rapidamente o que foi tratado na aula anterior. Isso para que o aluno recorde o que foi desenvolvido previamente e perceba a sequência dos estudos.</p>

<p>c. Objetivos em relação à aprendizagem dos estudantes</p>
<p>Escreva o que você espera do aluno ao final daquela aula. Você já definiu o objetivo no seu plano de trabalho; agora, é só trazê-lo para a introdução. </p>
<p>Note: Algumas vezes, a transcrição literal desse objetivo não se mostra adequada no contexto da introdução. Nesses casos, para manter a coerência, efetue os ajustes necessários para incluir o objetivo no texto.</p>
<p>Ao conhecer a meta da aula, o aluno poderá direcionar os próprios caminhos de aprendizagem. Ainda, poderá se preparar para realizar a atividade avaliativa relacionada ao tema. </p>
<p>Ficou claro como a introdução deve ser redigida?</p>
<p>Recapitulando...</p>
<p>INTRODUÇÃO DA AULA:</p>
<p>Saudação – Apresentação/Contextualização do tema da aula – Objetivo específico da aula </p>


<p>Seguiremos, então, para o desenvolvimento da aula.</p>

<p>5.4.2 Desenvolvimento da aula</p>
<p>Nessa etapa, você escreverá o texto da aula propriamente dito, observando o conteúdo programático previsto no seu planejamento. </p>
<p>É a hora de articular as ideias, expor os principais conceitos da matéria, propor exemplos, relacionar o tema com a realidade dos alunos e incorporar os recursos que você pesquisou.</p>
<p>Atenção! O emprego da linguagem dialógica e de todas as recomendações da nossa Aula 4 são imprescindíveis no desenvolvimento das suas aulas. </p>
<p>Não se esqueça disso!</p>

<p>Por onde começar?</p>
<p>Bom, existem várias maneiras de iniciar essa etapa da aula. </p>
<p>Você deve ter em mente o seguinte: de que forma posso provocar (estimular, despertar) reflexões sobre o tema da aula?</p>
<p>Dica: Para responder a esse questionamento, você pode utilizar a técnica “Tempestade de ideias” ou “Brainstorming”.</p>
<p>Aqui, a nossa pergunta-chave foi: de que forma podemos despertar reflexões sobre a fase de desenvolvimento da escrita das aulas que compõem o material didático?</p>
<p>Entendemos que uma analogia com o trabalho da costureira poderia ser interessante. Então, chegamos à seguinte proposta, veja:</p>
<p>Convidamos você para ler sobre o trabalho da costureira, no quadro abaixo. Após a leitura, reflita: </p>
<p>Quais as possíveis relações entre o trabalho da costureira e a fase de desenvolvimento da escrita das aulas do material didático?</p>

<p>O ofício da costureira</p>
<p>A costureira, antes de colocar a “mão na massa”, isto é, de começar a costurar, precisa de:</p>
<p>● Modelo da roupa. </p>
<p>● Tecido escolhido e cortado. </p>
<p>● Aviamentos (linha, zíper, botões etc).</p>
<p>Com esses elementos definidos, planeja como montar as peças de tecido cortadas.</p>
<p>Em seguida, inicia a costura: une as partes do tecido e, aos poucos, o modelo vai ganhando forma, até surgir a roupa idealizada. </p>
<p>Mas, ainda não acabou o trabalho, é necessário o acabamento: cortar linhas sobressalentes, pregar botões etc. </p>
<p>Agora, sim, obra concluída, roupa pronta!</p>

<p>Então, conseguiu estabelecer pontos de contato com o assunto da nossa aula?</p>
<p>Nós encontramos alguns, analise:</p>
<p>● É necessário ter em mãos os elementos que serão utilizados no trabalho.</p>
<p>a) Antes de começar a escrever sua aula, pesquise materiais bibliográficos que poderão dar sustentação teórica à sua produção. </p>
<p>Verifique se a fonte de dados é confiável e opte por referências atualizadas.</p>
<p>Durante a escrita da aula, ao parafrasear ou transcrever o trabalho de outros autores, aplique as regras de citação da Associação Brasileira de Normas Técnicas (ABNT).  </p>
<p>Veja as principais formas de citação descritas no Manual de Orientações para o trabalho do conteudista aqui do STF: (colocar link para parte no Manual de conteudista que trata das citações, coloquei o arquivo na pasta: P:\SEEAD\Cursos 2015\Validação - 2015\356.861 - Mariana - Produção de Conteúdos para a EaD\Aulas em validação\Aula 05)</p>
<p>Além disso, indicamos alguns sites para consulta a esse respeito:  http://www.bu.ufsc.br/design/Citacao1.htm e http://www.leffa.pro.br/textos/abnt.htm#citacoes. </p>
<p> Dica: Caso não tenha todas as informações recomendadas pela ABNT, procure citar, pelo menos, autor e ano da obra consultada, entre parênteses, por exemplo: (POSSARI, 2009).</p>
<p>Observação: Podemos basear nossa redação nas ideias de outros autores, mas a eles devemos dar os devidos créditos. Então, cuidado com o plágio.</p>
<p>Saiba mais</p>
<p>Você conhece o Google Livros?</p>
<p>Esta ferramenta pode auxiliá-lo nas pesquisas de material bibliográfico. Nela, você pode encontrar obras de diversos assuntos em meio eletrônico. </p>
<p>Navegue em: https://books.google.com.br/ </p>

<p>b) Além disso, busque outros recursos que poderão ser utilizados para enriquecer o texto: sites, textos complementares, artigos, vídeos, imagens, quadrinhos, poemas, letras de música, frases, histórias, entrevistas com profissionais da área, entre outros.</p>

<p>Dica: Sempre registre as referências dos recursos que pesquisou. Você pode criar um arquivo exclusivo para isso. Caso decida incluir algum deles na sua aula, deverá citar a respectiva fonte para preservar a questão dos direitos autorais (conversaremos mais a esse respeito na próxima aula).</p>

<p>● É necessário escrever aos poucos, para se chegar àquilo que foi planejado.</p>
<p>Você já leu bastante sobre o tema e pesquisou elementos para agregá-los à produção. Agora, é hora de começar a escrever:</p>
<p>a) Procure desenvolver o seu texto aos poucos, partindo dos assuntos mais simples, passo a passo, até chegar aos mais complexos. Costure as peças com cuidado: monte o texto unindo assuntos que mantêm coerência entre si. </p>
<p>b) Decida quais recursos utilizará para enriquecer a aula. Eles podem impactar a sua escrita.</p>
<p>c) Inclua exemplos sempre que oportuno. Com isso, podemos tornar as ideias mais concretas e, assim, facilitar o entendimento dos estudantes.</p>
<p>d) À medida que escrever, busque envolver o seu leitor. Algumas questões poderão ajudá-lo nesse aspecto:</p>
<p>. Posso trazer algum fato do ambiente de trabalho para ilustrar o tema?</p>
<p>. Posso fazer algum esquema comparativo?</p>
<p>. Quais pontos do conteúdo devem ser destacados para chamar a atenção do aluno?</p>
<p>. Quais questionamentos sobre o assunto devo propor para auxiliar o estudante a manter o foco no objetivo da aula?</p>
<p>. Quais perguntas podem estimular reflexões sobre o tema?</p>
<p>    </p>
<p>Inspire-se com Albert Einstein: </p>
<p>               “Não são as respostas que movem o mundo, são as perguntas”.</p>


<p>imagem e frase http://bibliotecactca.blogspot.com.br/2013/01/nao-sao-as-respostas-que-movem-o-mundo.html </p>
<p>  </p>
<p>● É necessária uma vistoria geral do trabalho para efetuar ajustes e melhorias.</p>
<p>Revise a sua produção. Ler o que você escreve, durante o desenvolvimento do conteúdo da aula, é muito importante. Isso permite verificar se as ideias estão se encaixando, se o texto está claro e coeso, se há repetição de palavras, se o sentido das expressões corresponde ao que você deseja exprimir, se você está aplicando a linguagem dialógica, se é necessário efetuar cortes, entre outras questões.</p>
<p>Quando considerar que o texto está finalizado, descanse um ou dois dias do trabalho. Depois, volte ao texto e leia tudo novamente. Em muitos casos, novas ideias podem surgir para aprimorar a sua produção. E, para terminar, faça mais uma revisão!</p>
<p>Dica: Ler o texto impresso ou ler em voz alta podem ser boas estratégias. Faça o teste! </p>
<p>Recapitulando...</p>

<p>DESENVOLVIMENTO DA AULA:</p>
<p>Pesquisa de elementos para enriquecer o texto – Linguagem dialógica – Iniciar com reflexões sobre o tema da aula – Escrever o texto propriamente dito – Revisar a produção</p>


<p>5.4.3 Conclusão da aula</p>
<p>Depois de escrever todos os tópicos temáticos previstos para a aula, é necessário realizar o fechamento das ideias. Para tanto, retome brevemente os principais pontos discutidos. </p>
<p>Em seguida, estabeleça uma ponte com os assuntos da próxima unidade de estudo. Isto é, retrate as “cenas dos próximos capítulos”, como forma de aguçar a curiosidade dos alunos e de situá-los na trilha de aprendizagem do curso.</p>
<p>Aqui no nosso curso, denominamos essa etapa como “Considerações Finais”. Como exemplo, vamos revisitar a conclusão das aulas anteriores? Observe:</p>
<p>Poderia colocar essas conclusões em quadros, podendo ser dois em cada linha para aproveitar o espaço.</p>
<p>Aula 1 – Contextualização da EaD</p>
<p>Para resgatarmos rapidamente os temas lançados, nesta aula: conversamos sobre os aspectos históricos da EaD até chegarmos aos dias de hoje. Percebemos que a modalidade é fundamental para promover ações educativas desde épocas anteriores. Ainda, observamos que ela evoluiu no tempo, acompanhou os avanços das tecnologias e, hoje, vivencia grande expansão, notadamente no campo da educação continuada de profissionais. </p>
<p>Feitas essas considerações, na próxima aula trocaremos ideias a respeito do estudo on-line e das particularidades da aprendizagem na educação a distância. </p>

<p>Aula 2 – Aspectos relacionados à aprendizagem a distância</p>
<p>Nesta aula, conversamos sobre a aprendizagem de adultos e suas peculiaridades, assim como o papel central do aluno em relação à aprendizagem. Vimos também que somos pessoas diferentes, portanto, possuímos estilos de aprendizagem diversos. Por fim, observamos que o tutor será parceiro do conteudista, no sentido de tentar “traduzir” para os alunos aquilo que foi delineado na nossa escrita. Em todos os casos, refletimos acerca da influência dos temas na escrita de materiais educativos para a EaD. </p>
<p>Finalizamos também, aqui, o Módulo I, no qual tratamos de questões relacionadas à contextualização da EaD e à aprendizagem, nossos pontos de partida.</p>
<p>Solidificados esses assuntos, seguiremos para o Módulo II, em que estudaremos o planejamento da escrita dos materiais didáticos na educação a distância do STF.</p>
<p>Encontro vocês no próximo módulo, então!</p>
<p>Até breve!</p>

<p>Aula 3 – O planejamento do material didático</p>
<p>Você consegue perceber a importância de cada item do planejamento e como eles estão intimamente relacionados, como em uma engrenagem?</p>
<p>Chegamos ao final desta aula e encerramos, juntamente com ela, o Módulo II. Nesta aula, conversamos sobre a importância do planejamento nas ações de cunho educativo, especialmente para a elaboração de materiais didáticos. Conhecemos o processo de produção de conteúdos para a EaD no STF e, por fim, analisamos cada um dos principais itens do Plano de Elaboração do Material Didático.</p>
<p>Ainda restam dúvidas?</p>
<p>Se precisar de algum esclarecimento a respeito daquilo que dialogamos nesta aula, compartilhe no ambiente virtual de aprendizagem.</p>

<p>Aula 4 – O processo de escrita</p>
<p>Chegamos ao final da nossa quarta aula!</p>
<p>Como você se sente até aqui? </p>
<p>Trocamos ideias sobre os principais pontos que devem ser observados em relação à escrita dos materiais didáticos para a educação a distância.</p>
<p>Salientamos que, devido a todas as especificidades dessa modalidade de ensino, devemos empregar a linguagem dialógica na produção dos textos. </p>
<p>Talvez essa proposta de escrita seja uma novidade para você. Contudo, poderemos praticá-la aqui no nosso curso em duas oportunidades:</p>
<p>a.  Veja a atividade avaliativa desta aula logo na sequência.</p>
<p>b.  No final do curso, está prevista, como atividade avaliativa, a escrita de uma aula completa de acordo com o conteúdo programático previsto no seu Plano de Elaboração do Material Didático.</p>
<p>Se surgirem dúvidas sobre o assunto, utilize os canais de comunicação no nosso ambiente virtual de aprendizagem, tudo bem?</p>

<p>  </p>
<p>Perceba que esses são exemplos. Você pode seguir o seu próprio estilo de escrita para finalizar a aula, certo? </p>

<p>5.4.4 Atividade avaliativa   </p>
<p>Você pensou que o trabalho tinha acabado? Ainda não, estamos quase lá!</p>
<p>Nesse tópico da escrita da aula, registre com detalhes a proposta de avaliação escolhida. Lembre-se do objetivo da aula: o que você espera do aluno ao final do estudo? Esse objetivo inicial orientará a sua decisão, uma vez que a atividade deve ser coerente com ele.</p>
<p>Dica: Releia a Aula 3 do nosso curso, no ponto que trata das atividades avaliativas. </p>
<p> </p>
<p>5.4.5 Referências </p>
<p>Todos os materiais bibliográficos utilizados na escrita da sua aula deverão ser indicados nesse momento, tanto os trechos que foram citados como os que foram consultados e que embasaram a sua escrita. Isso inclui, também, páginas da internet, manuais, dicionários e referências contidas nas notas de rodapé.</p>
<p>Siga, para tanto, as normas da ABNT.  Veja alguns exemplos aqui (as informações foram extraídas do Manual de Orientações do Conteudista do STF). (colocar link para parte no Manual de conteudista que trata das referências, coloquei o arquivo na pasta: P:\SEEAD\Cursos 2015\Validação - 2015\356.861 - Mariana - Produção de Conteúdos para a EaD\Aulas em validação\Aula 05 .)</p>
<p>Você também pode consultar o tópico “4 – Modelos de Referências” do site: http://www.bu.ufsc.br/framerefer.html.</p>

<p>Observação: As notas de rodapé devem ser utilizadas, preferencialmente, para pequenas explicações ou comentários do autor. Evite usá-las para indicar referências bibliográficas, pois, quando estas são registradas nas notas de rodapé de maneira excessiva, compromete-se o aspecto visual do texto.</p>

<p>CONSIDERAÇÕES FINAIS</p>
<p>Neste estudo, dialogamos sobre a organização geral do material didático, os aspectos da formatação do texto, a quantidade de páginas recomendadas e, principalmente, a estrutura interna das aulas.</p>
<p>Em relação à dinâmica de escrita das aulas, percebemos a relevância de cada uma das fases e como elas devem apresentar harmonia entre si para compor um texto coerente e didático.</p>
<p>Com essas constatações, aproximamo-nos do término do nosso curso. Na aula final, trataremos, especificamente, da utilização de recursos para complementar o texto, em observância às características do leitor na educação a distância. Além disso, conversaremos sobre a questão dos direitos autorais. Até a nossa última aula!</p>





<p>ATIVIDADE AVALIATIVA</p>
<p>Pesquise na internet materiais didáticos de cursos a distância e tente identificar como eles estão organizados. Eles apresentam estrutura semelhante à que propusemos aqui ou não? Encontrou algo interessante que pode ser aproveitado em suas aulas?</p>



<p>REFERÊNCIAS</p>

<p>FIOCRUZ. Escola Nacional de Saúde Pública. Programa de Educação a distância. Elaboração de material didático impresso para programas de formação a distância: orientações aos autores. Disponível em: <http://www.ufrgs.br/nucleoead/documentos/ENSPMaterial.pdf>. Acesso em: ago. 2015.</p>

<p>POSSARI, L. H. V. Material didático para a EaD: processo de produção. EdUFMT, 2009.</p>

<p>ZANETTI, Alexsandra. Elaboração de materiais para a educação a distância. Disponível em: <http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf>. Acesso em: jul. 2015.</p>


  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

