<?php 
//Configurar Aula
$aula = 5; 
$pagina = 6; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
		<h3 class="titulo titulo-primary">5.4 Estrutura básica da aula </h3>
<div class="row">
  	<div class="col-lg-6">
		<p>De maneira geral, cada aula é composta por: introdução, desenvolvimento, conclusão, atividades avaliativas e referências bibliográficas. Essas etapas integram o todo textual e, por conseguinte, precisam demonstrar sintonia entre si. </p>
		<p>Falando em sintonia, você já teve a oportunidade de assistir a um concerto musical?</p>
		<div class="bloco-pontilhado" style="">
		  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
		  <p style="text-align:center; font-weight:bold;">Convidamos você para ouvir a Orquestra Sinfônica de Brasília tocando o tema do filme “Star Wars”</p>
		  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
		    <iframe width="420" height="315" src="https://www.youtube.com/embed/_DN19SOcihM" frameborder="0" allowfullscreen></iframe>                    
		  </div>
		</div>
	</div>
	<div class="col-lg-6">
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Notou a harmonia entre as diferentes partes da música?</h3></div>
		<p>Essa apresentação nos inspira a compreender que as aulas precisam de articulação. Em outras palavras, as ideias do texto devem estar ligadas umas às outras para que ocorra, de fato, a construção de significado por parte do aluno.</p>
		<p>Além disso, destacamos que a subdivisão interna da aula tem finalidade didática, pois confere uma organização textual que ajuda o estudante a se situar durante a leitura.</p>
		<p>Conversaremos acerca das etapas intrínsecas à aula logo na sequência.</p>
    	<div class="text-center"><img src="imagens/estrutura.png" alt="Título da aula. Introdução: saudação, breve retomada de assuntos, apresentação e contextualização do tema, objetivos.Desenvolvimento:texto da aula.Conclusão:sintese das idéias, ponte entre as aulas. Avaliação. Referências Bibliográficas." class="img-responsiva"  /></div>

	</div>	
  </div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

