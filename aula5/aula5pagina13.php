<?php 
//Configurar Aula
$aula = 5; 
$pagina = 13; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.3 Conclusão da aula</h3>
	<div class="bloco-pontilhado" style="margin-top:50px" class="">
	  <img src="imagens/etiquetaAula2.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
	  <h3 class="titulo titulo-primary textoIndentado" >CONSIDERAÇÕES FINAIS</h3>
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; padding:10px">
			<p style="text-indent:1.3em !important;">Nesta aula, conversamos sobre a aprendizagem de adultos e suas peculiaridades, assim como o papel central do aluno em relação à aprendizagem. Vimos também que somos pessoas diferentes, portanto, possuímos estilos de aprendizagem diversos. Por fim, observamos que o tutor será parceiro do conteudista, no sentido de tentar “traduzir” para os alunos aquilo que foi delineado na nossa escrita. Em todos os casos, refletimos acerca da influência dos temas na escrita de materiais educativos para a EaD. </p>
			<p style="text-indent:1.3em !important;">Finalizamos também, aqui, o Módulo I, no qual tratamos de questões relacionadas à contextualização da EaD e à aprendizagem, nossos pontos de partida.</p>
			<p style="text-indent:1.3em !important;">Solidificados esses assuntos, seguiremos para o Módulo II, em que estudaremos o planejamento da escrita dos materiais didáticos na educação a distância do STF.</p>
			<p style="text-indent:1.3em !important;">Encontro vocês no próximo módulo, então!</p>
			<p style="text-indent:1.3em !important;" class="semi-bold text-center">Até breve!</p>
	    </div>
	  </div>
	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

