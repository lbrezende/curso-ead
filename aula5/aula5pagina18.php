<?php 
//Configurar Aula
$aula = 5; 
$pagina = 18; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">CONSIDERAÇÕES FINAIS</h3>
	<p>Neste estudo, dialogamos sobre a organização geral do material didático, os aspectos da formatação do texto, a quantidade de páginas recomendadas e, principalmente, a estrutura interna das aulas.</p>
	<p>Em relação à dinâmica de escrita das aulas, percebemos a relevância de cada uma das fases e como elas devem apresentar harmonia entre si para compor um texto coerente e didático.</p>
	<p>Com essas constatações, aproximamo-nos do término do nosso curso. Na aula final, trataremos, especificamente, da utilização de recursos para complementar o texto, em observância às características do leitor na educação a distância. Além disso, conversaremos sobre a questão dos direitos autorais. Até a nossa última aula!</p>
	<h3 class="titulo titulo-primary" style="padding-top:20px">VAMOS PRATICAR</h3>
	<p>Pesquise na internet materiais didáticos de cursos a distância e tente identificar como eles estão organizados. Eles apresentam estrutura semelhante à que propusemos aqui ou não? Encontrou algo interessante que pode ser aproveitado em suas aulas?</p>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

