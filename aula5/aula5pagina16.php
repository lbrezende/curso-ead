<?php 
//Configurar Aula
$aula = 5; 
$pagina = 16; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.4.4 Atividade avaliativa   </h3>
	<h3 class="titulo titulo-secondary text-center">Você pensou que o trabalho tinha acabado?</h3>
	<h3 class="titulo titulo-secondary text-center">Ainda não, estamos quase lá!</h3>
	<p>Nesse tópico da escrita da aula, registre com detalhes a proposta de avaliação escolhida. Lembre-se do objetivo da aula: o que você espera do aluno ao final do estudo? Esse objetivo inicial orientará a sua decisão, uma vez que a atividade deve ser coerente com ele.</p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
			<div class="bloco-pontilhado" style="margin-top:50px" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; padding:10px">
			      <p style="text-align:center">Releia a Aula 3 do nosso curso, no ponto que trata das atividades avaliativas. </p>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-2"></div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

