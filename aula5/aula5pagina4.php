<?php 
//Configurar Aula
$aula = 5; 
$pagina = 4; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.2 Formatação dos arquivos de texto</h3>
	<p>Os arquivos do material devem ser apresentados com as seguintes especificações de formatação:</p>
	<div style="text-align:center;padding-top:10px;padding-bottom:10px;"><img src="imagens/pagina-em-branco-word.png" style="max-width:75%;" class="img-responsiva"></div>

	<ul>
		<li style="line-height:1.5;">Papel: formato A4, com dimensões de 21 x 29,7 cm. </li>
		<li style="line-height:1.5;">Editor de texto: Word 2010. </li>
		<li style="line-height:1.5;">Margens: superior - 2 cm; inferior - 2 cm; esquerda - 2 cm; direita - 2 cm. </li>
		<li style="line-height:1.5;">Fonte: arial, tamanho 12, cor preta.</li>
		<li style="line-height:1.5;">Alinhamento: justificado.</li>
		<li style="line-height:1.5;">Espaçamento entre linhas: 1,5 cm.</li>
		<li style="line-height:1.5;">Espaçamento entre parágrafos: antes - 6 pt; depois - 6 pt.</li>
		<li style="line-height:1.5;">Recuo esquerdo da primeira linha do parágrafo: 2 cm. </li>
		<li style="line-height:1.5;">Nomear o arquivo com a sequência: número da aula – nome da aula:<br />
		Exemplo: Aula 1 – Contextualização da EaD</li>
		<li style="line-height:2;">Numeração de páginas: inferior e à direita. </li>
	</ul>
	<p><span class="semi-bold">Observação:</span> Esse é o padrão utilizado atualmente pela Seção de Educação a Distância do Supremo Tribunal Federal. Ao exercer a atividade de conteudista para cursos do STF, você deverá entregar o material de acordo com essa formatação. </p>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

