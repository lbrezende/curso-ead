<?php 
//Configurar Aula
$aula = 5; 
$pagina = 2; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', '5','1', '12', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Estrutura da Aula</h3>

<div style="text-align:center"><h3 class="titulo titulo-primary">Bem-vindo à nossa quinta aula!</h3></div>
<p>Agora que você já conhece a linguagem que deve ser utilizada na escrita do material didático, vamos dialogar sobre a apresentação das suas ideias no texto das aulas. </p>
<p>Você se recorda do <a href="https://goo.gl/uiFrkM" target="_blank" title="link para o Plano de Elaboração do Material Didático">Plano de Elaboração do Material Didático</a>, criado no Módulo III (colocar link para o Modelo de Plano)? Nele, foram organizados os tópicos do conteúdo programático que você delimitou para o seu curso, certo?</p>
<p>Aqui, no nosso treinamento, distribuímos os assuntos em módulos e em aulas. No seu planejamento, outra maneira pode ter sido escolhida, sem problemas.</p>
<p>A título de informação, os <span class="semi-bold">módulos</span> são temas amplos, dentro do conteúdo programático, que podem ser subdivididos em outros pontos para compor o entendimento sobre a temática central abordada. </p>
<p>Cada subdivisão é considerada uma <span class="semi-bold">aula</span>, cuja temática delimitada será desenvolvida em função do módulo. As aulas ainda podem ser divididas em tópicos para facilitar o encadeamento das ideias. </p>
<p>Visualize novamente o conteúdo programático do curso: </p>
			
			<table class="table-ead  table-hover" cellpadding="10">
			  <thead>
			    <tr>
			      <th style="width:25%; font-size:20px; text-align:center;background:#B5C2CB;color:#000 !important" class='table-title semi-bold' >MÓDULO I</th>
			      <th align="center" class='table-title' style="width:25%; font-size:20px; text-align:center;color:#000 !important">MÓDULO II</th>
			      <th style="width:50%; font-size:20px; text-align:center;color:#000 !important" class='table-title' colspan="2">MÓDULO III</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td style="background:#CCDDEE;"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 1</p>CONTEXTUALIZAÇÃO DA EDUCAÇÃO A DISTÂNCIA <br />1.1 Breve histórico mundial da EaD <br />1.2 A EaD no Brasil <br />1.3 A EaD na atualidade</td>
			      <td style="background:#9FC3E2;" valign="top" rowspan="2"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 3</p>POR QUE PLANEJAR A ESCRITA <br />3.1 Elaboração do material didático no STF <br />3.2 Plano de elaboração do material didático <br />3.2.1 Principais itens do planejamento</td>
			      <td style="background:#8CA5C1;"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 4</p>LINGUAGUEM PARA EaD <br />4.1 A importância da comunicação na EaD <br />4.2 O leitor na EaD <br />4.3 Linguagem dialógica <br />4.4 Correção gramatical</td>
			      <td style="background:#8CA5C1;" valign="top" rowspan="2"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 6</p>ESTRUTURA DA AULA <br />5.1 Organização geral do material didático <br />5.2 Formatação dos arquivos de texto <br />5.3 Quantidade de páginas <br />5.4. Estrutura básica da aula <br />5.4.1 Introdução da aula <br />5.4.2 Desenvolvimento da aula <br />5.4.3 Conclusão da aula <br />5.4.4 Atividade avaliativa <br />5.4.5 Referências</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 2</p>ASPECTOS RELACIONADOS À APRENDIZAGEM A DISTÂNCIA <br />2.1 A Aprendizagem: um processo construtivo <br />2.2 Estilos de aprendizagem <br />2.3 O tutor e a mediação</td>
			      <td style="background:#8CA5C1;"><p style="color:#396E94;text-align:center !important;text-indent:0em;" class="semi-bold">Aula 5</p>LINGUAGUEM PARA EaD <br />6.1 Orientações iniciais <br />6.2 Imagens <br />6.3 Vídeos <br />6.4 Hyperlinks <br />6.5 Destaques no conteúdo <br />6.6 Um pouco sobre direitos autorais na EaD</td>
			    </tr>
			  </tbody>


			</table>



<div class="bloco-pontilhado" style="">
	<p><span class="semi-bold">Exemplo</span>: No curso “Produção de Conteúdo para a Educação a Distância”, pensamos assim:</p>
	<p>Nos primeiros momentos, o aluno precisa conhecer as características da educação a distância, já que ela é o pano de fundo para todas as discussões. Ele também precisa compreender as particularidades da aprendizagem nessa modalidade de ensino, posto que esse é o foco da escrita. </p>
	<p>Então, criamos um módulo para tratar essas questões, com o título:</p>
	<p>Módulo I: EaD e aprendizagem (tema amplo). </p>
	<p>Subdividimos esse tema em duas partes, para organizar melhor a nossa escrita e facilitar o estudo dos alunos: </p>
	<p><span class="semi-bold">a) Aula 1:</span> Contextualização da Educação a Distância </p>
	<p><span class="semi-bold">b) Aula 2:</span> Aspectos relacionados à aprendizagem a distância</p>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Mas por que essa explanação?</h3></div>
	<p>Para você perceber que, na prática, produzirá o texto das aulas previstas no conteúdo programático. Por isso, o enfoque neste momento do nosso estudo é a estrutura interna das aulas.</p>
	<p>Ao final, esperamos que você produza conteúdos em formato de aulas de acordo com as necessidades da educação a distância.</p>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary">Tudo bem? Vamos começar?</h3></div>
</div>
<h3 class="titulo titulo-primary">5.1 Organização geral do material didático</h3>
<p>Antes de tudo, você precisa ter uma visão geral do trabalho a ser desenvolvido.</p>
<p>Nesse sentido, o material didático é formado, basicamente, pela apresentação do curso e pelas aulas escritas: </p>
<p><span class="semi-bold">a.  Texto de apresentação do curso</span>, que contempla: considerações gerais sobre o curso; histórico e cenário atual do tema, quando apropriado; objetivos; conteúdo programático; nível de aprofundamento da abordagem; questões que o curso pretende esclarecer ou responder; relevância dos temas tratados e/ou benefícios esperados pela instituição com o treinamento; entre outras questões. </p>
<p><span class="semi-bold">Observação:</span> Esse texto pode ser elaborado ou revisado após a conclusão do material propriamente dito, quando você terá mais elementos para redigi-lo.</p>

<p><span class="semi-bold">b.  Texto de cada uma das aulas</span>, conforme o conteúdo programático definido. Os itens que devem ser contemplados no texto das aulas serão abordados logo adiante, no tópico 5.4. </p>

[to-do]Leandro, se der, crie só as imagens pra mim por favor.


<div class="row">
	<div class="col-lg-3" style="text-align:center;"><span class="semi-bold">Apresentação do curso</span><br /><img src="imagens/apresentacao_curso.png" style="width:75%;" /></div>
	<div class="col-lg-3" style="padding-top:50px;text-align:center;"><span class="semi-bold">Aula 1</span><br /><img src="imagens/aula1.png" style="width:75%;" /></div>
	<div class="col-lg-3" style="text-align:center;"><span class="semi-bold">Aula 2</span><br /><img src="imagens/aula2.png" style="width:75%;" /></div>
	<div class="col-lg-3" style="padding-top:50px;text-align:center;"><span class="semi-bold">Aula 3</span><br /><img src="imagens/aula3.png" style="width:75%;" /></div>
</div>

<h3 class="titulo titulo-primary">5.2 Formatação dos arquivos de texto</h3>
<p>Os arquivos do material devem ser apresentados com as seguintes especificações de formatação:</p>
<div style="text-align:center;">Programa utilizado na escrita: <br />Microsoft Word <img src="imagens/icon-word.png"></div>
<div style="text-align:center;"><img src="imagens/estruturaArquivo.png" style="max-width:75%;" class="img-responsiva"></div>

<ul>
	<li>Papel: formato A4, com dimensões de 21 x 29,7 cm. </li>
	<li>Editor de texto: Word 2010. </li>
	<li>Margens: superior - 2 cm; inferior - 2 cm; esquerda - 2 cm; direita - 2 cm. </li>
	<li>Fonte: arial, tamanho 12, cor preta.</li>
	<li>Alinhamento: justificado.</li>
	<li>Espaçamento entre linhas: 1,5 cm.</li>
	<li>Espaçamento entre parágrafos: antes - 6 pt; depois - 6 pt.</li>
	<li>Recuo esquerdo da primeira linha do parágrafo: 2 cm. </li>
	<li>Nomear o arquivo com a sequência: número da aula – nome da aula:<br />
	Exemplo: Aula 1 – Contextualização da EaD</li>
	<li>Numeração de páginas: inferior e à direita. </li>
</ul>
<p><span class="semi-bold">Observação:</span> Esse é o padrão utilizado atualmente pela Seção de Educação a Distância do Supremo Tribunal Federal. Ao exercer a atividade de conteudista para cursos do STF, você deverá entregar o material de acordo com essa formatação. </p>

<h3 class="titulo titulo-primary">5.3 Quantidade de páginas </h3>
<p>Cada uma das aulas previstas no conteúdo programático deve conter, no máximo, 20 páginas de texto escrito. </p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Mas por quê?</h3></div>
<p>Você há de concordar conosco que textos muito longos podem se tornar enfadonhos e dispersar a atenção do leitor. Por isso, procure seguir a nossa recomendação. </p>
<p>Caso uma aula ultrapasse o limite, avalie a possibilidade de retirar alguns pontos ou de dividir o assunto, isto é, de criar uma nova aula ou de transformar alguns desses itens em texto complementar. O importante é manter o bom senso e focar no aprendizado do aluno. </p>
<p><span class="semi-bold">Observação:</span> A utilização de gráficos e tabelas, entre outros recursos, em uma quantidade considerável repercute na quantidade de páginas. Nessas situações, o número poderá ser ampliado, sem maiores inconvenientes.</p>
<p>Neste momento, vamos à temática central da nossa conversa?</p>

<h3 class="titulo titulo-primary">5.4 Estrutura básica da aula</h3>
<p>De maneira geral, cada aula é composta por: introdução, desenvolvimento, conclusão, atividades avaliativas e referências bibliográficas. Essas etapas integram o todo textual e, por conseguinte, precisam demonstrar sintonia entre si. </p>
<p>Falando em sintonia, você já teve a oportunidade de assistir a um concerto musical?</p>

<div class="bloco-pontilhado" style="">
  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
  <p style="text-align:center; font-weight:bold;">Convido você para ouvir a Orquestra Sinfônica de Brasília tocando o tema do filme “Star Wars”</p>
  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
    <iframe width="420" height="315" src="https://www.youtube.com/embed/_DN19SOcihM" frameborder="0" allowfullscreen></iframe>                    
  </div>
</div>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Notou a harmonia entre as diferentes partes da música?</h3></div>
<p>Essa apresentação nos inspira a compreender que as aulas precisam de articulação. Em outras palavras, as ideias do texto devem estar ligadas umas às outras para que ocorra, de fato, a construção de significado por parte do aluno.</p>
<p>Além disso, destacamos que a subdivisão interna da aula tem finalidade didática, pois confere uma organização textual que ajuda o estudante a se situar durante a leitura.</p>
<p>Conversaremos acerca das etapas intrínsecas à aula logo na sequência.</p>
<div class="notebook">
	<div class="lines"></div>
	<ul class="list">
		<li class="semi-bold text-center">Título da aula</li>
		<li class="marcador">•	Introdução da aula</li>
		<li>Breve retomada dos assuntos anteriores, quando for o caso.</li>
		<li>Apresentação e contextualização do tema da aula.</li>
		<li>Objetivos da aula.</li>
		<li>•	Desenvolvimento da aula</li>
		<li>Texto da aula propriamente dito.</li>
		<li>•	Conclusão da aula</li>
		<li>Síntese das principais ideias do texto.</li>
		<li>Ponte entre as aulas.</li>
		<li>•	Atividade avaliativa</li>
		<li>Atividade para fixar a aprendizagem dos alunos.</li>
		<li>•	Referências bibliográficas</li>
		<li>Indicação do material bibliográfico utilizado na escrita.</li>
	</ul>
</div>

<h3 class="titulo titulo-primary">5.4.1 Introdução da aula</h3>
<p>Na introdução, você estabelece o primeiro contato com o aluno e ele com você. Então, esse é o momento de apresentar a proposta da aula e de esclarecer quais temas serão tratados. Com isso, o leitor pode “saber onde está pisando” e formular as próprias expectativas em relação ao estudo.</p>
<p>Para auxiliar a sua escrita, indicamos alguns aspectos que devem ser inseridos no texto introdutório:</p>
<div class="tituloEspecialEadAula5 col-lg-3">
		<span style="color:#5EB39D;" class="semi-bold ">A) Saudação ao leitor</span>
</div>  	
<div class="clear"></div>

<p>Antes de tudo, estamos em um diálogo com nosso estudante, certo? Por essa razão, iniciamos a nossa escrita com uma breve saudação ao aluno. </p>
<p>Você pode ficar à vontade para cumprimentar de acordo com o seu próprio estilo. Não precisa ser algo elaborado demais. Seja simples e direto: <em>Olá!, Seja bem-vindo!, Olá novamente!, Bem-vindo à nossa primeira aula!</em></p>
<div class="tituloEspecialEadAula5 col-lg-6">
		<span style="color:#5EB39D;" class="semi-bold ">B) Apresentação e contextualização do tema da aula</span>
</div>  	
<div class="clear"></div>

<p>Registre os assuntos que serão desenvolvidos e procure contextualizar esses temas: aproximar o aluno da proposta da aula logo no início. </p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Como assim?</h3></div>
<p>Elabore alguma situação com a finalidade de trazer o leitor para o universo temático da aula. Significa que você deve dizer algo mais que apenas: <em>“Estudaremos, nesse momento, a estrutura das aulas”</em>. Agregue mais elementos à sua escrita. Como exemplo, sugerimos que reveja as introduções das aulas do nosso curso. <a href="Aula_5_Introducao_das_aulas.pdf" target="_blank" title="clique aqui para ver as introduções das aulas">Clique aqui</a> para visualizá-las.</p>
<p>Vários recursos podem ser utilizados para essa finalidade, tais como: vídeos, imagens, perguntas reflexivas, histórias, fatos sobre a realidade de trabalho dos estudantes, explicação de conceitos, enfim, o importante é criar um “clima” favorável para o estudo.</p>
<p>Com isso, você poderá envolver o aluno no diálogo e ajudá-lo a começar a refletir sobre a proposta da aula. </p>
<p><span class="semi-bold">Observação:</span> A partir da segunda aula do seu curso, antes de contextualizar o assunto, retome rapidamente o que foi tratado na aula anterior. Isso para que o aluno recorde o que foi desenvolvido previamente e perceba a sequência dos estudos.</p>
<div class="tituloEspecialEadAula5 col-lg-6">
		<span style="color:#5EB39D;" class="semi-bold ">C) Objetivos em relação à aprendizagem dos estudantes</span>
</div>  	
<div class="clear"></div>

<p>Escreva o que você espera do aluno ao final daquela aula. Você já definiu o objetivo no seu plano de trabalho; agora, é só trazê-lo para a introdução. </p>
<p><span class="semi-bold">Note:</span> Algumas vezes, a transcrição literal desse objetivo não se mostra adequada no contexto da introdução. Nesses casos, para manter a coerência, efetue os ajustes necessários para incluir o objetivo no texto.</p>
<p>Ao conhecer a meta da aula, o aluno poderá direcionar os próprios caminhos de aprendizagem. Ainda, poderá se preparar para realizar a atividade avaliativa relacionada ao tema. </p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Ficou claro como a introdução deve ser redigida?</h3></div>
<div class="text-center semi-bold">Recapitulando...</div>
<div style="text-align:center;"><h3><span class="semi-bold tituloCinza">Introdução da aula:</span></h3></div>
<div class="row">
	<div class="col-lg-3" style="text-align:center;"><span class="semi-bold verdeClaro">Saudação</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-3" style="text-align:center;"><span class="semi-bold verdeClaro">Apresentação/Contextualização do tema da aula</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-3" style="text-align:center;"><span class="semi-bold verdeClaro">Objetivo específico da aula</span></div>
	<div class="col-lg-1" style="text-align:center;"></div>
</div>

<p>Seguiremos, então, para o desenvolvimento da aula.</p>

<h3 class="titulo titulo-primary">5.4.2 Desenvolvimento da aula</h3>
<p>Nessa etapa, você escreverá o texto da aula propriamente dito, observando o conteúdo programático previsto no seu planejamento. </p>
<p>É a hora de articular as ideias, expor os principais conceitos da matéria, propor exemplos, relacionar o tema com a realidade dos alunos e incorporar os recursos que você pesquisou.</p>
<p><span class="semi-bold">Atenção!</span> O emprego da linguagem dialógica e de todas as recomendações da nossa Aula 4 são imprescindíveis no desenvolvimento das suas aulas. </p>
<p>Não se esqueça disso!</p>

<div style="text-align:center;"><h3 class="titulo titulo-secondary">Por onde começar?</h3></div>
<div class="bloco-pontilhado" style="margin-top:0px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <p style="text-align:center; font-weight:bold;">Procure escrever com simplicidade.    </p>
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">

      <p>Para responder a esse questionamento, você pode utilizar a técnica “Tempestade de ideias” ou “Brainstorming”.</p>

    </div>
  </div>
</div>
<p>Bom, existem várias maneiras de iniciar essa etapa da aula. </p>
<p>Você deve ter em mente o seguinte: <span class="semi-bold"><em>de que forma posso provocar (estimular, despertar) reflexões sobre o tema da aula?</em></span></p>
<p>Aqui, a nossa pergunta-chave foi: de que forma podemos despertar reflexões sobre a fase de desenvolvimento da escrita das aulas que compõem o material didático?</p>
<p>Entendemos que uma analogia com o trabalho da costureira poderia ser interessante. Então, chegamos à seguinte proposta, veja:</p>
<p>Convidamos você para ler sobre o trabalho da costureira, no quadro abaixo. Após a leitura, reflita: </p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Quais as possíveis relações entre o trabalho da costureira e a fase de desenvolvimento da escrita das aulas do material didático?</h3></div>

<div class="row well cor-balao-costureira">
	<div class="col-lg-6" >
		<p>O ofício da costureira</p>
		<p>A costureira, antes de colocar a “mão na massa”, isto é, de começar a costurar, precisa de:</p>
		<p>● Modelo da roupa. </p>
		<p>● Tecido escolhido e cortado. </p>
		<p>● Aviamentos (linha, zíper, botões etc).</p>
		<p>Com esses elementos definidos, planeja como montar as peças de tecido cortadas.</p>
	</div>
	<div class="col-lg-6" >
		<img src="imagens/costureira.png" alt="imagem de uma costureira trabalhando" style="width:50%" class="img-responsiva" />
	</div>
	<div class="clear"></div>
	<p>Em seguida, inicia a costura: une as partes do tecido e, aos poucos, o modelo vai ganhando forma, até surgir a roupa idealizada. </p>
	<p>Mas, ainda não acabou o trabalho, é necessário o acabamento: cortar linhas sobressalentes, pregar botões etc. </p>
	<p>Agora, sim, obra concluída, roupa pronta!</p>
</div>	

<div style="text-align:center;"><h3 class="titulo titulo-secondary">Então, conseguiu estabelecer pontos de contato com o assunto da nossa aula?</h3></div>
<p>Nós encontramos alguns, analise:</p>
<p>● É necessário ter em mãos os elementos que serão utilizados no trabalho.</p>
<p>a) Antes de começar a escrever sua aula, pesquise materiais bibliográficos que poderão dar sustentação teórica à sua produção. </p>
<p>Verifique se a fonte de dados é confiável e opte por referências atualizadas.</p>
<p>Durante a escrita da aula, ao parafrasear ou transcrever o trabalho de outros autores, aplique as regras de citação da Associação Brasileira de Normas Técnicas (ABNT).  </p>
<p>Veja as principais formas de <a href="https://goo.gl/ZHY5V4" target="_blank">citação</a> descritas no Manual de Orientações para o trabalho do conteudista aqui do STF: (colocar link para parte no Manual de conteudista que trata das citações, coloquei o arquivo na pasta: P:\SEEAD\Cursos 2015\Validação - 2015\356.861 - Mariana - Produção de Conteúdos para a EaD\Aulas em validação\Aula 05)</p>
<p>Além disso, indicamos alguns sites para consulta a esse respeito:</p>
<div style="text-align:center;"><a href="http://www.bu.ufsc.br/design/Citacao1.htm" target="_blank">http://www.bu.ufsc.br/design/Citacao1.htm</a></div>
<div style="text-align:center;"><a href="http://www.leffa.pro.br/textos/abnt.htm#citacoes" target="_blank">http://www.leffa.pro.br/textos/abnt.htm#citacoes</a></div>
<div class="bloco-pontilhado" style="margin-top:0px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p>Caso não tenha todas as informações recomendadas pela ABNT, procure citar, pelo menos, autor e ano da obra consultada, entre parênteses, por exemplo: (POSSARI, 2009).</p>
    </div>
  </div>
</div>

<p><span class="semi-bold">Observação:</span> Podemos basear nossa redação nas ideias de outros autores, mas a eles devemos dar os devidos créditos. Então, cuidado com o plágio.</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <div id="conteudo-cerebro">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		<p class="text-center">Você conhece o Google Livros?</p>
		<p class="text-center">Esta ferramenta pode auxiliá-lo nas pesquisas de material bibliográfico. Nela, você pode encontrar obras de diversos assuntos em meio eletrônico. </p>
		<p class="text-center">Navegue em: <a href="https://books.google.com.br/" target="_blank">https://books.google.com.br/</a> </p>

    </div>
  </div>
</div>

<p>b) Além disso, busque outros recursos que poderão ser utilizados para enriquecer o texto: sites, textos complementares, artigos, vídeos, imagens, quadrinhos, poemas, letras de música, frases, histórias, entrevistas com profissionais da área, entre outros.</p>
<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;">
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p>Sempre registre as referências dos recursos que pesquisou. Você pode criar um arquivo exclusivo para isso. Caso decida incluir algum deles na sua aula, deverá citar a respectiva fonte para preservar a questão dos direitos autorais (conversaremos mais a esse respeito na próxima aula).</p>
    </div>
  </div>
</div>

<p>● É necessário escrever aos poucos, para se chegar àquilo que foi planejado.</p>
<p>Você já leu bastante sobre o tema e pesquisou elementos para agregá-los à produção. Agora, é hora de começar a escrever:</p>
<p>a) Procure desenvolver o seu texto aos poucos, partindo dos assuntos mais simples, passo a passo, até chegar aos mais complexos. Costure as peças com cuidado: monte o texto unindo assuntos que mantêm coerência entre si. </p>
<p>b) Decida quais recursos utilizará para enriquecer a aula. Eles podem impactar a sua escrita.</p>
<p>c) Inclua exemplos sempre que oportuno. Com isso, podemos tornar as ideias mais concretas e, assim, facilitar o entendimento dos estudantes.</p>
<p>d) À medida que escrever, busque envolver o seu leitor. Algumas questões poderão ajudá-lo nesse aspecto:</p>
<p>. Posso trazer algum fato do ambiente de trabalho para ilustrar o tema?</p>
<p>. Posso fazer algum esquema comparativo?</p>
<p>. Quais pontos do conteúdo devem ser destacados para chamar a atenção do aluno?</p>
<p>. Quais questionamentos sobre o assunto devo propor para auxiliar o estudante a manter o foco no objetivo da aula?</p>
<p>. Quais perguntas podem estimular reflexões sobre o tema?</p>
<p>    </p>



<p>Inspire-se com Albert Einstein: </p>
<h3 class="titulo titulo-primary center"><img src="imagens/aspastop.jpg" style="margin-top:-50px;">“Não são as respostas que movem o mundo, são as perguntas”.<img src="imagens/aspasbottom.jpg" style="margin-top:50px"></h3>
<div class="text-center"><img src="imagens/einstein.jpg" alt="quadrinhos com albert einstein" class="img-responsiva"></div>
<p class="fonteMenor text-center">Imagem e frase <a href="http://bibliotecactca.blogspot.com.br/2013/01/nao-sao-as-respostas-que-movem-o-mundo.html" target="_blank">http://bibliotecactca.blogspot.com.br/2013/01/nao-sao-as-respostas-que-movem-o-mundo.html</a></p>



<p>● É necessária uma vistoria geral do trabalho para efetuar ajustes e melhorias.</p>
<p>Revise a sua produção. Ler o que você escreve, durante o desenvolvimento do conteúdo da aula, é muito importante. Isso permite verificar se as ideias estão se encaixando, se o texto está claro e coeso, se há repetição de palavras, se o sentido das expressões corresponde ao que você deseja exprimir, se você está aplicando a linguagem dialógica, se é necessário efetuar cortes, entre outras questões.</p>
<p>Quando considerar que o texto está finalizado, descanse um ou dois dias do trabalho. Depois, volte ao texto e leia tudo novamente. Em muitos casos, novas ideias podem surgir para aprimorar a sua produção. E, para terminar, faça mais uma revisão!</p>
<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;">
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p>Ler o texto impresso ou ler em voz alta podem ser boas estratégias. Faça o teste! </p>
    </div>
  </div>
</div>

<div class="text-center semi-bold">Recapitulando...</div>
<div style="text-align:center;"><h3><span class="semi-bold tituloCinza">Desenvolvimento da aula:</span></h3></div>
<div class="row">
	<div class="col-lg-2" style="text-align:center;"><span class="semi-bold verdeClaro">Pesquisa de elementos para enriquecer o texto</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-1" style="text-align:center;"><span class="semi-bold verdeClaro">Linguagem dialógica</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-2" style="text-align:center;"><span class="semi-bold verdeClaro">Iniciar com reflexões sobre o tema da aula</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-2" style="text-align:center;"><span class="semi-bold verdeClaro">Escrever o texto propriamente dito</span></div>
	<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
	<div class="col-lg-1" style="text-align:center;"><span class="semi-bold verdeClaro">Revisar a produção</span></div>
</div>


<h3 class="titulo titulo-primary">5.4.3 Conclusão da aula</h3>
<p>Depois de escrever todos os tópicos temáticos previstos para a aula, é necessário realizar o fechamento das ideias. Para tanto, retome brevemente os principais pontos discutidos. </p>
<p>Em seguida, estabeleça uma ponte com os assuntos da próxima unidade de estudo. Isto é, retrate as “cenas dos próximos capítulos”, como forma de aguçar a curiosidade dos alunos e de situá-los na trilha de aprendizagem do curso.</p>
<p>Aqui no nosso curso, denominamos essa etapa como “Considerações Finais”. Como exemplo, vamos revisitar a conclusão das aulas anteriores? Observe:</p>
<p>Poderia colocar essas conclusões em quadros, podendo ser dois em cada linha para aproveitar o espaço.</p>

<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/etiquetaAula1.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;">
  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-indent:1.3em !important;">Para resgatarmos rapidamente os temas lançados, nesta aula: conversamos sobre os aspectos históricos da EaD até chegarmos aos dias de hoje. Percebemos que a modalidade é fundamental para promover ações educativas desde épocas anteriores. Ainda, observamos que ela evoluiu no tempo, acompanhou os avanços das tecnologias e, hoje, vivencia grande expansão, notadamente no campo da educação continuada de profissionais. </p>
	  <p style="text-indent:1.3em !important;">Feitas essas considerações, na próxima aula trocaremos ideias a respeito do estudo on-line e das particularidades da aprendizagem na educação a distância. </p>
    </div>
  </div>
</div>

<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/etiquetaAula2.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
  <h3 class="titulo titulo-primary textoIndentado" >CONSIDERAÇÕES FINAIS</h3>
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		<p style="text-indent:1.3em !important;">Nesta aula, conversamos sobre a aprendizagem de adultos e suas peculiaridades, assim como o papel central do aluno em relação à aprendizagem. Vimos também que somos pessoas diferentes, portanto, possuímos estilos de aprendizagem diversos. Por fim, observamos que o tutor será parceiro do conteudista, no sentido de tentar “traduzir” para os alunos aquilo que foi delineado na nossa escrita. Em todos os casos, refletimos acerca da influência dos temas na escrita de materiais educativos para a EaD. </p>
		<p style="text-indent:1.3em !important;">Finalizamos também, aqui, o Módulo I, no qual tratamos de questões relacionadas à contextualização da EaD e à aprendizagem, nossos pontos de partida.</p>
		<p style="text-indent:1.3em !important;">Solidificados esses assuntos, seguiremos para o Módulo II, em que estudaremos o planejamento da escrita dos materiais didáticos na educação a distância do STF.</p>
		<p style="text-indent:1.3em !important;">Encontro vocês no próximo módulo, então!</p>
		<p style="text-indent:1.3em !important;" class="semi-bold text-center">Até breve!</p>
    </div>
  </div>
</div>

<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/etiquetaAula3.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		<p style="text-indent:1.3em !important;">Você consegue perceber a importância de cada item do planejamento e como eles estão intimamente relacionados, como em uma engrenagem?</p>
		<p style="text-indent:1.3em !important;">Chegamos ao final desta aula e encerramos, juntamente com ela, o Módulo II. Nesta aula, conversamos sobre a importância do planejamento nas ações de cunho educativo, especialmente para a elaboração de materiais didáticos. Conhecemos o processo de produção de conteúdos para a EaD no STF e, por fim, analisamos cada um dos principais itens do Plano de Elaboração do Material Didático.</p>
		<p style="text-indent:1.3em !important;"><h3 class="titulo titulo-secondary text-center">Ainda restam dúvidas?</h3></p>
		<p style="text-indent:1.3em !important;">Se precisar de algum esclarecimento a respeito daquilo que dialogamos nesta aula, compartilhe no ambiente virtual de aprendizagem.</p>
	</div>
  </div>
</div>

<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/etiquetaAula4.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		<p style="text-indent:1.3em !important;" class="text-center">Chegamos ao final da nossa quarta aula!</p>
		<p style="text-indent:1.3em !important;" class="semi-bold text-center">Como você se sente até aqui? </p>
		<p style="text-indent:1.3em !important;">Trocamos ideias sobre os principais pontos que devem ser observados em relação à escrita dos materiais didáticos para a educação a distância.</p>
		<p style="text-indent:1.3em !important;">Salientamos que, devido a todas as especificidades dessa modalidade de ensino, devemos empregar a linguagem dialógica na produção dos textos. </p>
		<p style="text-indent:1.3em !important;">Talvez essa proposta de escrita seja uma novidade para você. Contudo, poderemos praticá-la aqui no nosso curso em duas oportunidades:</p>
		<p style="text-indent:1.3em !important;"><span class="semi-bold">a.</span>  Veja a atividade avaliativa desta aula logo na sequência.</p>
		<p style="text-indent:1.3em !important;"><span class="semi-bold">b.</span>  No final do curso, está prevista, como atividade avaliativa, a escrita de uma aula completa de acordo com o conteúdo programático previsto no seu Plano de Elaboração do Material Didático.</p>
		<p style="text-indent:1.3em !important;">Se surgirem dúvidas sobre o assunto, utilize os canais de comunicação no nosso ambiente virtual de aprendizagem, tudo bem?</p>
	</div>
  </div>
</div>


<div style="text-align:center;"><h3 class="titulo titulo-secondary">Perceba que esses são exemplos. Você pode seguir o seu próprio estilo de escrita para finalizar a aula, certo? </h3></div>

<h3 class="titulo titulo-primary">5.4.4 Atividade avaliativa   </h3>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Você pensou que o trabalho tinha acabado? <br />Ainda não, estamos quase lá!</h3></div>
<p>Nesse tópico da escrita da aula, registre com detalhes a proposta de avaliação escolhida. Lembre-se do objetivo da aula: o que você espera do aluno ao final do estudo? Esse objetivo inicial orientará a sua decisão, uma vez que a atividade deve ser coerente com ele.</p>
<div class="bloco-pontilhado" style="margin-top:50px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;">
  <div id="conteudo-cerebro" >
    <div class="row" style="text-align:center; margin-bottom:30px; padding:10px">
      <p>Releia a Aula 3 do nosso curso, no ponto que trata das atividades avaliativas. </p>
    </div>
  </div>
</div>

<h3 class="titulo titulo-primary">5.4.5 Referências </h3>
<p>Todos os materiais bibliográficos utilizados na escrita da sua aula deverão ser indicados nesse momento, tanto os trechos que foram citados como os que foram consultados e que embasaram a sua escrita. Isso inclui, também, páginas da internet, manuais, dicionários e referências contidas nas notas de rodapé.</p>
<p>Siga, para tanto, as normas da ABNT.  Veja alguns exemplos aqui (as informações foram extraídas do Manual de Orientações do Conteudista do STF). (colocar link para parte no Manual de conteudista que trata das referências, coloquei o arquivo na pasta: P:\SEEAD\Cursos 2015\Validação - 2015\356.861 - Mariana - Produção de Conteúdos para a EaD\Aulas em validação\Aula 05 .)</p>
<p>Você também pode consultar o tópico “4 – Modelos de Referências” do site: <a href="http://www.bu.ufsc.br/framerefer.html" target="_blank" title="acesse os modelos de referências">http://www.bu.ufsc.br/framerefer.html</a>.</p>

<p><span class="semi-bold">Observação:</span> As notas de rodapé devem ser utilizadas, preferencialmente, para pequenas explicações ou comentários do autor. Evite usá-las para indicar referências bibliográficas, pois, quando estas são registradas nas notas de rodapé de maneira excessiva, compromete-se o aspecto visual do texto.</p>

<h3 class="titulo titulo-primary">CONSIDERAÇÕES FINAIS</h3>
<p>Neste estudo, dialogamos sobre a organização geral do material didático, os aspectos da formatação do texto, a quantidade de páginas recomendadas e, principalmente, a estrutura interna das aulas.</p>
<p>Em relação à dinâmica de escrita das aulas, percebemos a relevância de cada uma das fases e como elas devem apresentar harmonia entre si para compor um texto coerente e didático.</p>
<p>Com essas constatações, aproximamo-nos do término do nosso curso. Na aula final, trataremos, especificamente, da utilização de recursos para complementar o texto, em observância às características do leitor na educação a distância. Além disso, conversaremos sobre a questão dos direitos autorais. Até a nossa última aula!</p>





<h3 class="titulo titulo-primary">VAMOS PRATICAR</h3>
<p>Pesquise na internet materiais didáticos de cursos a distância e tente identificar como eles estão organizados. Eles apresentam estrutura semelhante à que propusemos aqui ou não? Encontrou algo interessante que pode ser aproveitado em suas aulas?</p>



<h3 class="titulo titulo-primary">REFERÊNCIAS</h3>

<p>FIOCRUZ. Escola Nacional de Saúde Pública. Programa de Educação a distância. <span class="semi-bold">Elaboração de material didático impresso para programas de formação a distância:</span> orientações aos autores. Disponível em: <<a href="http://www.ufrgs.br/nucleoead/documentos/ENSPMaterial.pdf" target="_blank">http://www.ufrgs.br/nucleoead/documentos/ENSPMaterial.pdf</a>>. Acesso em: ago. 2015.</p>

<p>POSSARI, L. H. V. <span class="semi-bold">Material didático para a EaD:</span> processo de produção. EdUFMT, 2009.</p>

<p>ZANETTI, Alexsandra. <span class="semi-bold">Elaboração de materiais para a educação a distância</span>. Disponível em: <<a href="http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf" target="_blank">http://www.cead.ufjf.br/wp-content/uploads/2009/02/media_biblioteca_elaboracao_materiais.pdf</a>>. Acesso em: jul. 2015.</p>


  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

