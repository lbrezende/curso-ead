<?php 
//Configurar Aula
$aula = 5; 
$pagina = 8; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
	<h3 class="titulo titulo-primary">5.4.2 Desenvolvimento da aula</h3>

	<div class="row">
		<div class="col-lg-6">
			<p>Nessa etapa, você escreverá o texto da aula propriamente dito, observando o conteúdo programático previsto no seu planejamento. </p>
			<p>É a hora de articular as ideias, expor os principais conceitos da matéria, propor exemplos, relacionar o tema com a realidade dos alunos e incorporar os recursos que você pesquisou.</p>
			<p><span class="semi-bold">Atenção!</span> O emprego da linguagem dialógica e de todas as recomendações da nossa Aula 4 são imprescindíveis no desenvolvimento das suas aulas. </p>
			<p>Não se esqueça disso!</p>

			<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:10px">Por onde começar?</h3></div>
			<div class="bloco-pontilhado" style="margin-top:40px;margin-bottom:20px;" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:0px;">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center; margin-bottom:0px; padding:10px">

			      <p>Para responder a esse questionamento, você pode utilizar a técnica “Tempestade de ideias” ou “Brainstorming”.</p>

			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-6">
        	<p>Bom, existem várias maneiras de iniciar essa etapa da aula. </p>
			<p>Você deve ter em mente o seguinte: <span class="semi-bold"><em>de que forma posso provocar (estimular, despertar) reflexões sobre o tema da aula?</em></span></p>
			<p>Aqui, a nossa pergunta-chave foi: de que forma podemos despertar reflexões sobre a fase de desenvolvimento da escrita das aulas que compõem o material didático?</p>
			<p>Entendemos que uma analogia com o trabalho da costureira poderia ser interessante. Então, chegamos à seguinte proposta, veja:</p>
			<p>Convidamos você para ler sobre o trabalho da costureira, no quadro abaixo. Após a leitura, reflita: </p>		
			<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:10px">Quais as possíveis relações entre o trabalho da costureira e a fase de desenvolvimento da escrita das aulas do material didático?</h3></div>
		    
		<div class="row">
		  <div class="col-lg-3"></div>
		  <div class="col-lg-6">
			<div id="balao-botao-costureira" class="row well cor-balao-costureira">
	            <div class="botao-costureira" style="text-align:center"><img src="imagens/costureira_pequena.png" class="img-responsiva" alt="Imagem de uma costureira"><br><a href="javascript:void(0);" class=" btn btn-info">Clique aqui para saber</a></div>
	       </div>
		  </div>
		  <div class="col-lg-3"></div>
		</div>

		</div>
	</div>
	    <div class="row">
	    	<div class="col-lg-2" ></div>
	    	<div class="col-lg-8" >
		    <div style="display:none" class="conteudo-costureira">
			<div class="row well cor-balao-costureira">
					<div class="col-lg-6" >
						<p><strong>O ofício da costureira</strong></p>
						<p>A costureira, antes de colocar a “mão na massa”, isto é, de começar a costurar, precisa de:</p>
						<p style="text-indent:0em !important">● Modelo da roupa. </p>
						<p style="text-indent:0em !important">● Tecido escolhido e cortado.</p>
						<p style="text-indent:0em !important">● Aviamentos (linha, zíper, botões etc).</p>
					</div>
					<div class="col-lg-6" >
						<img src="imagens/costureira.png" alt="imagem de uma costureira trabalhando" class="img-responsiva" />
					</div>
					<div class="clear"></div>
					<p>Com esses elementos definidos, planeja como montar as peças de tecido cortadas.</p>
					<p>Em seguida, inicia a costura: une as partes do tecido e, aos poucos, o modelo vai ganhando forma, até surgir a roupa idealizada. </p>
					<p>Mas ainda não acabou o trabalho, é necessário o acabamento: cortar linhas sobressalentes, pregar botões etc. </p>
					<p>Agora, sim, obra concluída, roupa pronta!</p>
				</div>			
	    	</div>
	    	</div>		
	    	<div class="col-lg-2" ></div>
	    </div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

