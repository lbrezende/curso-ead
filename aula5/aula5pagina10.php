<?php 
//Configurar Aula
$aula = 5; 
$pagina = 10; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.2 Desenvolvimento da aula</h3>
	<img src="imagens/letraB_2.png" class="img-responsiva" height="50px" alt="É necessário escrever aos poucos, para se chegar àquilo que foi planejado">
	<p style="padding-top:20px">Você já leu bastante sobre o tema e pesquisou elementos para agregá-los à produção. Agora, é hora de começar a escrever:</p>
	<p style="text-indent:0em;">1) Procure desenvolver o seu texto aos poucos, partindo dos assuntos mais simples, passo a passo, até chegar aos mais complexos. Costure as peças com cuidado: monte o texto unindo assuntos que mantêm coerência entre si. </p>
	<p style="text-indent:0em;">2) Decida quais recursos utilizará para enriquecer a aula. Eles podem impactar a sua escrita.</p>
	<p style="text-indent:0em;">3) Inclua exemplos sempre que oportuno. Com isso, podemos tornar as ideias mais concretas e, assim, facilitar o entendimento dos estudantes.</p>
	<p style="text-indent:0em;">4) À medida que escrever, busque envolver o seu leitor. Algumas questões poderão ajudá-lo nesse aspecto:</p>
	<ul>
		<li style="margin-bottom:3px;">Posso trazer algum fato do ambiente de trabalho para ilustrar o tema?</li>
		<li style="margin-bottom:3px;">Posso fazer algum esquema comparativo?</li>
		<li style="margin-bottom:3px;">Quais pontos do conteúdo devem ser destacados para chamar a atenção do aluno?</li>
		<li style="margin-bottom:3px;">Quais questionamentos sobre o assunto devo propor para auxiliar o estudante a manter o foco no objetivo da aula?</li>
		<li style="margin-bottom:3px;">Quais perguntas podem estimular reflexões sobre o tema?</li>
	</ul>
	<p>Inspire-se com Albert Einstein: </p>
	<h3 class="titulo titulo-primary center"><img src="imagens/aspastop.jpg" style="margin-top:-50px;">Não são as respostas que movem o mundo, são as perguntas.<img src="imagens/aspasbottom.jpg" style="margin-top:50px"></h3>
	<div class="text-center"><a href="imagens/einstein.jpg" target="_blank"><img src="imagens/einstein.jpg" alt="quadrinhos com albert einstein" class="img-responsiva"></a></div>
	<p class="fonteMenor text-center">Imagem e frase <a href="http://bibliotecactca.blogspot.com.br/2013/01/nao-sao-as-respostas-que-movem-o-mundo.html" target="_blank">http://bibliotecactca.blogspot.com.br/2013/01/nao-sao-as-respostas-que-movem-o-mundo.html</a></p>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

