<?php 
//Configurar Aula
$aula = 5; 
$pagina = 15; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.3 Conclusão da aula</h3>
	<div class="bloco-pontilhado" style="margin-top:50px" class="">
	  <img src="imagens/etiquetaAula4.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
	  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; padding:10px">
			<p style="text-indent:1.3em !important;" class="text-center">Chegamos ao final da nossa quarta aula!</p>
			<p style="text-indent:1.3em !important;" class="semi-bold text-center">Como você se sente até aqui? </p>
			<p style="text-indent:1.3em !important;">Trocamos ideias sobre os principais pontos que devem ser observados em relação à escrita dos materiais didáticos para a educação a distância.</p>
			<p style="text-indent:1.3em !important;">Salientamos que, devido a todas as especificidades dessa modalidade de ensino, devemos empregar a linguagem dialógica na produção dos textos. </p>
			<p style="text-indent:1.3em !important;">Talvez essa proposta de escrita seja uma novidade para você. Contudo, poderemos praticá-la aqui no nosso curso em duas oportunidades:</p>
			<p style="text-indent:1.3em !important;"><span class="semi-bold">a.</span>  Veja a atividade avaliativa desta aula logo na sequência.</p>
			<p style="text-indent:1.3em !important;"><span class="semi-bold">b.</span>  No final do curso, está prevista, como atividade avaliativa, a escrita de uma aula completa de acordo com o conteúdo programático previsto no seu Plano de Elaboração do Material Didático.</p>
		  <p style="text-indent:1.3em !important;">Se surgirem dúvidas sobre o assunto, utilize os canais de comunicação no nosso ambiente virtual de aprendizagem, tudo bem?</p>
			<p style="text-indent:1.3em !important;"> Vamos à atividade desta aula?</p>
	    </div>
	  </div>
	</div>
	<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:20px">Perceba que esses são exemplos. Você pode seguir o seu próprio estilo de escrita para finalizar a aula, certo? </h3></div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

