<?php 
//Configurar Aula
$aula = 5; 
$pagina = 11; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Estrutura da aula', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.2 Desenvolvimento da aula</h3>
	<img src="imagens/letraC_2.png" alt="É necessária uma vistoria geral do trabalho para efetuar ajustes e melhorias" height="50px" class="img-responsiva">
	<p style="padding-top:20px">Revise a sua produção. Ler o que você escreve, durante o desenvolvimento do conteúdo da aula, é muito importante. Isso permite verificar se as ideias estão se encaixando, se o texto está claro e coeso, se há repetição de palavras, se o sentido das expressões corresponde ao que você deseja exprimir, se você está aplicando a linguagem dialógica, se é necessário efetuar cortes, entre outras questões.</p>
	<p>Quando considerar que o texto está finalizado, descanse um ou dois dias do trabalho. Depois, volte ao texto e leia tudo novamente. Em muitos casos, novas ideias podem surgir para aprimorar a sua produção. E, para terminar, faça mais uma revisão!</p>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-8">
			<div class="bloco-pontilhado" style="margin-top:50px;margin-bottom:20px;" class="">
			  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px;margin-left:-30px; ">
			  <div id="conteudo-cerebro" >
			    <div class="row" style="text-align:center;padding:10px">
			      <p style="text-align:center">Ler o texto impresso ou ler em voz alta podem ser boas estratégias.</p>
			      <p style="text-align:center">Faça o teste!</p>
			    </div>
			  </div>
			</div>
		</div>
		<div class="col-lg-2"></div>
	</div>

	<div class="text-center semi-bold">Recapitulando...</div>
	<div style="text-align:center;magin-bottom:20px;"><h3 style="padding-bottom:10px"><span class="semi-bold tituloCinza">Desenvolvimento da aula:</span></h3></div>
	<div class="row">
		<div class="col-lg-2" style="text-align:center;"><span class="semi-bold titulo-primary">Pesquisa de elementos para enriquecer o texto</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-1" style="text-align:center;"><span class="semi-bold titulo-primary">Linguagem dialógica</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-2" style="text-align:center;"><span class="semi-bold titulo-primary">Iniciar com reflexões sobre o tema da aula</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-2" style="text-align:center;"><span class="semi-bold titulo-primary">Escrever o texto propriamente dito</span></div>
		<div class="col-lg-1" style="text-align:center;"><img src="imagens/seta.png" style="width:100%;" /></div>
		<div class="col-lg-1" style="text-align:center;"><span class="semi-bold titulo-primary">Revisar a produção</span></div>
	</div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

