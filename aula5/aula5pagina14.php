<?php 
//Configurar Aula
$aula = 5; 
$pagina = 14; 
$totalPaginas = 19;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
  	<h3 class="titulo titulo-primary">5.4.3 Conclusão da aula</h3>
 	<div class="bloco-pontilhado" style="margin-top:50px" class="">
	  <img src="imagens/etiquetaAula3.png" alt="" title="" style="margin-top:-40px;margin-left:-30px; margin-bottom:10px;max-width:150px;" />
	  <h3 class="titulo titulo-primary" style="text-indent:1.3em !important;">CONSIDERAÇÕES FINAIS</h3>
	  <div id="conteudo-cerebro" >
	    <div class="row" style="text-align:center; padding:10px">
			<p style="text-indent:1.3em !important;">Você consegue perceber a importância de cada item do planejamento e como eles estão intimamente relacionados, como em uma engrenagem?</p>
			<p style="text-indent:1.3em !important;">Chegamos ao final desta aula e encerramos, juntamente com ela, o Módulo II. Nesta aula, conversamos sobre a importância do planejamento nas ações de cunho educativo, especialmente para a elaboração de materiais didáticos. Conhecemos o processo de produção de conteúdos para a EaD no STF e, por fim, analisamos cada um dos principais itens do Plano de Elaboração do Material Didático.</p>
			<h3 class="titulo titulo-secondary text-center">Ainda restam dúvidas?</h3>
			<p style="text-indent:1.3em !important;">Se precisar de algum esclarecimento a respeito daquilo que dialogamos nesta aula, compartilhe no ambiente virtual de aprendizagem.</p>
			<p style="text-indent:1.3em !important;">No próximo módulo, conversaremos sobre as especificidades da escrita de materiais didáticos na educação a distância.</p>
			<p style="text-indent:1.3em !important;">Depois de todo esse estudo, você está preparado para colocar seus conhecimentos sobre planejamento em prática? Vamos à atividade de avaliação?</p>
		</div>
	  </div>
	</div>
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

