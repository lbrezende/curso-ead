$(document).ready(function() {		



/******************/
/*** Aula 1 Ead ***/
/******************/

	$("[id|='destaque']").hide();
	$('.cerebro').click(function(){
		$('#esconder').hide("slow");
		$('#destaque-1').show('slideDown');

	});
	
	$('.saibaMais').click(function(){
		$('#esconder-SaibaMais').hide("slow");
		$('#destaque-SaibaMais-1').show('slideDown');

	});

	$( "[name|='conteudo']" ).hide();
	$('.itemmenu').click(function(){
		var name = $(this).attr('name'); //pega o name para completar no conteúdo
		$( "[name|='conteudo']" ).attr('style','display:none');
		//$( "[name|='conteudo']" ).hide(); //oculta todos os outros conteúdos para exibir apenas o próximo
		$('#conteudo-'+name).attr('style','display:block'); //exibe o conteúdo clicado baseado no name do elemnto de onde partiu o clique
		//$('#conteudo-'+name).show(); //exibe o conteúdo clicado baseado no name do elemnto de onde partiu o clique
		$('.itemmenu').removeClass('selected'); //remove todas as classes selected dos itens de menu
		$('#'+name).addClass('selected'); //inclui a classe de seleção somente no item selecionado
	});
	
	$("[id|='caixa']").hide();
	$('.abreCaixa').click(function(){
		var name = $(this).attr('id');
		//alert(name);
		$('#'+name).hide();
		$('#caixa-'+name).toggle('slideDown');	
	});
	
	
    $("#ir-menu").click(function(){
      $("#menu").focus();
     });
    $("#ir-aula").click(function(){
      $("#aula").focus();
     });     	

    $("[rel='popover']").popover({
		html:true, 
		trigger: 'focus',
		placement: 'auto right',
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content referencia"></div></div>'
	});

	$(".diminuirFonte").click(function(){
		$('article').css('font-size','16px');
		$('.fonteMenor').css('font-size','14px');
		$('.referencia p').css('font-size','16px !important');
		$("[rel=popover]").attr("data-size","popover-small");
    $(".titulo").css('font-size','24px');
	})

	$(".aumentarFonte").click(function(){
		$('article').css('font-size','24px');
		$('.fonteMenor').css('font-size','16px');
		$('.referencia p').attr('css','font-size: 24px ');
		$('.referencia p').addClass('referenciaGrande');
		$("[rel=popover]").attr("data-size","popover-big");
    $(".titulo").css('font-size','30px');

	})

	if ($("#acessibilidade").html() == "sim") {
		$(".aumentarFonte").click();
	};
	
/*Curso EaD - aula 1*/

  $("#botao-correspondencia").click(function(){
     $("#abrir-correspondencia").hide('fade');
     $("#conteudo-correspondencia").show('slideUp');
  });
  $("#botao-radio").click(function(){
     $("#abrir-radio").hide('fade');
     $("#conteudo-radio").show('slideUp');
  });
  $("#botao-tv").click(function(){
     $("#abrir-tv").hide('fade');
     $("#conteudo-tv").show('slideUp');
  });
  $(".botao-tvaberta").click(function(){
     $("#abrir-tvaberta").hide('fade');
     $("#conteudo-tvaberta").show('slideUp');
  });
  $(".botao-saibamaisSeculo").click(function(){
     $(".botao-saibamaisSeculo").hide();
     $("#conteudo-saibamaisSeculo").show('slideUp');
  }); 
  $(".botao-videoMinerva").click(function(){
     $(".botao-videoMinerva").hide();
     $("#conteudo-videoMinerva").show('slideUp');
  });  

  $(".botao-mediacao").click(function(){
     $(".conteudo-mediacao").show('slideUp');
  });    

  $(".botao-alunosprofessores").click(function(){
     $(".conteudo-alunosprofessores").show('slideUp');
  });      

  $(".botao-tecnologia").click(function(){
     $(".conteudo-tecnologia").show('slideUp');
  });  

  $(".botao-cibercultura").click(function(){
     $(".conteudo-cibercultura").show('slideUp');
  });          

  $(".botao-maquina").click(function(){
     $(".botao-maquina").hide();
     $("#conteudo-maquina").show('slideUp');
  });  

  $(".botao-maiaMattar").click(function(){
    $(".botao-maiaMattar").hide();
     $(".conteudo-maiaMattar").show('slideUp');
  });   

  $(".abrir-maiaMattar").click(function(){
     $(".botao-maiaMattar").hide();    
     $(".conteudo-maiaMattar").show('slideUp');
  });     

  $(".botao-conteudista-1").click(function(){
     $(".botao-maiaMattar").hide();
     $(".botao-conteudista-1").hide();
     $(".conteudo-conteudista-1").show('slideUp');
  });  

  $(".botao-moodle").click(function(){
     $(".botao-moodle").hide();
     $("#conteudo-moodle").show('slideUp');
  }); 

  $(".botao-eadbrasil").click(function(){
     $(".botao-eadbrasil").hide();
     $("#conteudo-eadbrasil").show('slideUp');
  });   

  $(".botao-levy").click(function(){
    $(".botao-levy").hide();
     $(".conteudo-levy").show('slideUp');
  });   

  $(".abrir-levy").click(function(){
     $(".botao-levy").hide();    
     $(".conteudo-levy").show('slideUp');
  });             

  $(".fitvids").fitVids();

/*Bibliografia*/
  $("[name|='biblio-abrir']").click(function(){
     var notaNumero = ($(this).html());
     $(".biblio-"+notaNumero +"-conteudo").toggle();
  });  



/******************/
/*** Aula 2 Ead ***/
/******************/
  $(".botao-cerebro").click(function(){
     $(".botao-cerebro").hide();
     $("#conteudo-cerebro").show('slideUp');
  }); 

  $(".botao-pausa-1").click(function(){
     $(".botao-pausa-1").hide();
     $("#conteudo-pausa-1").show('slideUp');
  }); 


  $(".botao-malcom").click(function(){
    $(".botao-malcom").hide();
     $(".conteudo-malcom").show('slideUp');
  });   

  $(".botao-relembrar").click(function(){
    $(".botao-relembrar").hide();
     $(".conteudo-relembrar").show('slideUp');
  });    

  $(".botao-daniela").click(function(){
    $(".botao-daniela").hide();
     $(".conteudo-daniela").show('slideUp');
  });    

  $(".botao-questao-1").click(function(){
    $(".botao-questao-1").hide();
     $(".conteudo-questao-1").show('slideUp');
  });   

  $(".botao-questao-2").click(function(){
    $(".botao-questao-2").hide();
     $(".conteudo-questao-2").show('slideUp');
  });   

  $(".botao-questao-3").click(function(){
    $(".botao-questao-3").hide();
     $(".conteudo-questao-3").show('slideUp');
  });   

  $(".botao-questao-4").click(function(){
    $(".botao-questao-4").hide();
     $(".conteudo-questao-4").show('slideUp');
  });   

  $(".botao-questao-5").click(function(){
    $(".botao-questao-5").hide();
     $(".conteudo-questao-5").show('slideUp');
  });   

  $(".botao-questao-6").click(function(){
    $(".botao-questao-6").hide();
     $(".conteudo-questao-6").show('slideUp');
  });  

  $(".botao-questao-7").click(function(){
    $(".botao-questao-7").hide();
     $(".conteudo-questao-7").show('slideUp');
  });                 

  $(".botao-questao-8").click(function(){
    $(".botao-questao-8").hide();
     $(".conteudo-questao-8").show('slideUp');
  });      

  $(".botao-questao-9").click(function(){
    $(".botao-questao-9").hide();
     $(".conteudo-questao-9").show('slideUp');
  });      

  $(".botao-questao-10").click(function(){
    $(".botao-questao-10").hide();
     $(".conteudo-questao-10").show('slideUp');
  });      

  $(".botao-listei").click(function(){
    $(".botao-listei").hide();
     $(".conteudo-listei").show('slideUp');
  });  

 $(".botao-ativo").click(function(){
    $(".botao-ativo").hide('fadeOut');
     $(".conteudo-ativo").show('fadeIn');
  });   

 $(".botao-reflexivo").click(function(){
    $(".botao-reflexivo").hide('fadeOut');
     $(".conteudo-reflexivo").show('fadeIn');
  });   

 $(".botao-teorico").click(function(){
    $(".botao-teorico").hide('fadeOut');
     $(".conteudo-teorico").show('fadeIn');
  });   

 $(".botao-pragmatico").click(function(){
    $(".botao-pragmatico").hide('fadeOut');
     $(".conteudo-pragmatico").show('fadeIn');
  });  

/******************/
/*** Aula 3 Ead ***/
/******************/

 $(".botao-reflexao-1").click(function(){
    $(".botao-reflexao-1").hide('fadeOut');
     $(".conteudo-reflexao-1").show('fadeIn');
  });   

 $(".botao-reflexao-2").click(function(){
    $(".botao-reflexao-2").hide('fadeOut');
     $(".conteudo-reflexao-2").show('fadeIn');
  });

 $(".img-reflexao-2").click(function(){
     $(".botao-reflexao-2").click();
 })


 });

/*Aula 5*/
  $(".botao-estrutura").click(function(){
    $(".botao-estrutura").hide();
     $(".conteudo-estrutura").show('slideUp');
  });  
  $(".botao-letraA").click(function(){
    $(".botao-letraA").hide();
     $(".conteudo-letraA").show('slideUp');
  });
  $(".botao-letraB").click(function(){
    $(".botao-letraB").hide();
     $(".conteudo-letraB").show('slideUp');
  });
  $(".botao-letraC").click(function(){
    $(".botao-letraC").hide();
     $(".conteudo-letraC").show('slideUp');
  });
  $(".botao-costureira").click(function(){
    $(".botao-costureira").hide();
    $("#balao-botao-costureira").hide();
     $(".conteudo-costureira").show('slideUp');
  });
/******/

/*Aula 6*/
  $(".botao-andrea").click(function(){
    $(".botao-andrea").hide();
     $(".conteudo-andrea").show('slideUp');
  });   



