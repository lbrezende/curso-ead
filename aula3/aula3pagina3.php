<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','3', '26', 'aula3pagina2.php', 'aula3pagina4.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Elaboração do material didático no STF</h3>
			<p>No STF, a Seção de Educação a Distância – SEEAD (Coordenadoria de Desenvolvimento de Pessoas/Secretaria de Gestão de Pessoas) é a responsável por gerenciar os processos relacionados à produção de materiais didáticos para a EaD. </p>
			<p>Essa atividade é regulamentada pela <a href="docs/IN_169.14_INSTRUTORIA_INTERNA.doc" target="_blank" title="link para IN169/14">norma de Instrutoria Interna, IN 169/14</a>, na qual estão descritos todos os requisitos necessários para o servidor que deseja atuar nessa área, bem como os critérios para a retribuição pelo exercício dessa função. </p>
			<p>O processo de elaboração de materiais é composto pelas seguintes etapas:</p>
			<table class="table-ead  table-hover" cellpadding="10">
			  <thead>
			    <tr>
			      <th style="width:25%; font-size:20px; text-align:center;" class='table-title' >ETAPA</th>
			      <th align="center" class='table-title' style="width:25%; font-size:20px; text-align:center;">ATOR</th>
			      <th style="width:50%; font-size:20px; text-align:center;" class='table-title'>AÇÃO</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td style="background:#CCDDEE;">Entrega do “Plano de Elaboração do Material Didático”</td>
			      <td style="background:#9FC3E2;" align="center"><span class="semi-bold">CONTEUDISTA</span></td>
			      <td style="background:#8CA5C1;">Planejamento da escrita do conteúdo
		          do curso.</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;">Validação do “Plano de Elaboração do Material Didático”</td>
			      <td style="background:#C3DAED;" align="center">CDPE</td>
			      <td style="background:#B7C7D8;">Análise das informações apresentadas e indicação de ajustes, se necessário.</td>
			    </tr>
			    <tr>
			      <td style="background:#CCDDEE;">Conhecendo sua escrita</td>
			      <td style="background:#9FC3E2;" align="center"><span class="semi-bold">CONTEUDISTA</span></td>
			      <td style="background:#8CA5C1;">Aula demonstrativa para validação da CDPE, com a proposta de primeira aula do conteúdo para o curso pretendido, a fim de conhecer o estilo de escrita do conteudista e de verificar a correspondência com as necessidades específicas do texto para educação a distância.</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;">Validação da aula demonstrativa</td>
			      <td style="background:#C3DAED;" align="center">CDPE</td>
			      <td style="background:#B7C7D8;">Análise da escrita e do uso de recursos complementares à aprendizagem, tais como vídeos, imagens, <i>hyperlinks</i> etc.</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;">Contratação do conteudista</td>
			      <td style="background:#C3DAED;" align="center">CDPE</td>
			      <td style="background:#B7C7D8;">Escrita do projeto básico e juntada de documentos.<br />
		          Encaminhamento para trâmite, com vistas à aprovação do projeto pelo Diretor-Geral.</td>
			    </tr>
			    <tr>
			      <td style="background:#CCDDEE;">Escrita do conteúdo do curso</td>
			      <td style="background:#9FC3E2;" align="center"><span class="semi-bold">CONTEUDISTA</span></td>
			      <td style="background:#8CA5C1;">O conteudista desenvolverá o texto com o conteúdo programático proposto para o curso, que constituirá a apostila configurada em versão para ser impressa.</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;">Validação do conteúdo</td>
			      <td style="background:#C3DAED;" align="center">CDPE</td>
			      <td style="background:#B7C7D8;">Análise e avaliação do texto produzido pelo conteudista, com indicação de ajustes e melhorias no que diz respeito à linguagem, aos recursos propostos e ao conteúdo, para que todos os itens previstos sejam contemplados.</td>
			    </tr>
			    <tr>
			      <td style="background:#CCDDEE;">Entrega da versão final do conteúdo</td>
			      <td style="background:#9FC3E2;" align="center"><span class="semi-bold">CONTEUDISTA</span></td>
			      <td style="background:#8CA5C1;">Com base nas orientações da CDPE, o conteudista deverá efetuar os ajustes no material didático para entregar a versão final do trabalho.</td>
			    </tr>
			    <tr>
			      <td style="background:#DFEAF4;">Revisão gramatical</td>
			      <td style="background:#C3DAED;" align="center">REVISOR DE TEXTOS</td>
			      <td style="background:#B7C7D8;">Revisão dos aspectos relacionados à Língua Portuguesa.</td>
			    </tr>
			  </tbody>


			</table>
	</div>
</div>
<div class="row" style="margin-top:40px">
		<div class="col-lg-6">
			<p>Após a conclusão da etapa de escrita, o material passará por tratamento pedagógico e gráfico, para que seja adaptado ao ambiente virtual de aprendizagem. Somente depois disso, o curso estará pronto para ser ofertado aos alunos. Cabe salientar que essa fase é de responsabilidade exclusiva da SEEAD, isto é, não há a participação do conteudista.</p>
			<p>Depois dessa breve contextualização concernente ao processo, passaremos a analisar os principais tópicos previstos no Plano de Elaboração do Material Didático do STF.</p>
		</div>
		<div class="col-lg-6">
			<div class="bloco-pontilhado" style="margin-top:40px" class="">
			    <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
			    <p style="text-align:center; font-weight:bold;">Você conhece a Comunidade de Apoio aos Instrutores Internos EaD?  <br>
			    <a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para conhecer!</a> </p>
			    <div id="conteudo-cerebro" style="display:none">
			      <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
			        <p style="text-align:center; text-indent:0em;"><img src="imagens/comunidade.jpg" style="max-width:95%; text-align:center;" alt=""> <br /><br /> Para auxiliar o desenvolvimento do trabalho dos profissionais da educação a distância do STF, dentre os quais, os conteudistas, foram disponibilizados pela SEEAD manuais de orientação e  diversos materiais nesta comunidade. </p>
			        <p style="text-align:center; text-indent:0em;">Que tal visitar a comunidade?<br /><a href="https://ead.stf.jus.br/course/view.php?id=70"  target="_blank" title="visite a página da comunidade de apoio aos instrutores internos do STF">Página da comunidade</a></p>
			      </div>
			    </div>
			</div>
		</div>	
	</div>

<?php  configNavegacaoRodape('exibir', 'aula3pagina2.php', 'aula3pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
