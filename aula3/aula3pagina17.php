<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','17', '26', 'aula3pagina16.php', 'aula3pagina18.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/conclusao.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 63px; max-height:80px; margin-top:22px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/conclusao.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">E) CONTEÚDO PROGRAMÁTICO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quais assuntos são fundamentais <br /> para se atingir os objetivos traçados?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-6">
		
		<div class="bloco-pontilhado" style="margin-top:0px; padding:20px" class="">
			<p>Então, é hora de definir os assuntos. Pense em um quebra-cabeça:</p>
			<p style="text-indent:0em; text-align:center"><img src="imagens/maos.png" alt="maos remexendo um quebra-cabeça"></p>		
			<p class="semi-bold" style="color:#4476AF;"><strong>Quais são as peças-chave (assuntos) que ajudarão o aluno a construir, passo a passo, a própria aprendizagem acerca do tema central proposto para o curso? </strong></p>
			<p class="semi-bold" style="color:#4476AF;"><strong>O que é essencial e não pode ficar de fora? </strong></p>
			<p><img src="imagens/quebra_cabeca.png" alt="maos montando um quebra-cabeça"></p>
			<p class="semi-bold" style="color:#4476AF;"><strong>Qual ordem de apresentação dos conteúdos favorecerá o encadeamento das ideias pelos estudantes?</strong></p>
		</div>  
		
	</div>
  	<div class="col-lg-6" >
		<p>Após a escolha dos assuntos, estes devem ser estruturados de modo que obedeçam a uma sequência lógica – do mais simples para o mais complexo –, sempre estabelecendo relação com a temática principal do treinamento.</p>
			<p>Você tem a liberdade para organizar o seu curso da forma que julgar mais apropriada.  O importante é criar um esquema uniforme e subdividir os tópicos em módulos (ou unidades temáticas ou capítulos) e em aulas. </p>
			<p>O nosso curso, “Produção de Conteúdos para a EaD”, foi organizado em módulos e em aulas. Veja, por exemplo, o Módulo I:</p>
			<p class="semi-bold">Módulo I – EaD e aprendizagem </p>
			<ul>
			  <li><strong>Aula 1</strong> – Contextualização da Educação a Distância</li>
			  <ol>
			    <li style="list-style-type:none">1.1 Breve histórico mundial da EaD</li>
			    <li style="list-style-type:none">1.2 A EaD no Brasil</li>
			    <li style="list-style-type:none">1.3 A EaD na atualidade</li>
			  </ol>
			  <li><strong>Aula 2</strong> – Aspectos relacionados à aprendizagem a distância:</li>
			  <ol>
			    <li style="list-style-type:none">2.1 A aprendizagem: um processo construtivo </li>
			    <li style="list-style-type:none">2.2 Estilos de aprendizagem</li>
			    <li style="list-style-type:none">2.3 O tutor e a mediação</li>
			  </ol>
			</ul>
			<p>Por fim, seguem alguns aspectos a serem considerados no momento da definição do conteúdo programático do curso:</p>
			<ul>
			  <li>Selecionar assuntos de acordo com o nível de aprofundamento previsto (básico, intermediário ou avançado) e buscar informações atualizadas sobre o tema.</li>
			  <li>Determinar a estrutura dos conteúdos (módulos, aulas), além de observar uma sequência que favoreça o entendimento do conteúdo pelo aluno.</li>
			</ul>
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina16.php', 'aula3pagina18.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
