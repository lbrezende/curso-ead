<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','18', '26', 'aula3pagina17.php', 'aula3pagina19.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/check.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 22px;    max-height: 98px;   margin-top: 9px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/check.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">F) ATIVIDADES AVALIATIVAS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Como posso ajudar o aluno a fixar os <br /> conhecimentos abordados no curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p>Até aqui, estudamos que o trabalho do conteudista é todo direcionado para promover a aprendizagem dos alunos. Certo? </p>
		<p>Por conseguinte, as atividades avaliativas devem estar previstas no material didático, pois auxiliam o estudante a fixar os assuntos abordados. Essas estratégias de avaliação, por sua vez, devem estar alinhadas com os objetivos estabelecidos para o curso.  </p>
		<p>Para conversarmos sobre a avaliação no processo de aprendizagem, proponho um resgate de suas experiências como estudante:</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:10px">Você se recorda de algum período da escola básica, da faculdade ou de outras situações educativas nas quais tenha passado por alguma experiência interessante envolvendo a avaliação?</h3></div>
		<p>Em nossa cultura escolar, a avaliação da aprendizagem, geralmente, ocorre no final do processo e está muito ligada ao aspecto quantitativo, tendo em vista medir o desempenho do estudante, emitir notas e aprovar ou reprovar no período letivo. </p>
		<p>Contudo, avaliar vai muito além disso, ainda mais no caso da educação a distância para adultos. Temos uma visão diferenciada da atuação do aluno, considerado ativo no seu próprio percurso de aprendizagem; por consequência, diferentes também devem ser as práticas de avaliação.</p>
	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina17.php', 'aula3pagina19.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
