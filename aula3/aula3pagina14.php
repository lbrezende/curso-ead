<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','14', '26', 'aula3pagina13.php', 'aula3pagina15.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">

		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Quais verbos utilizar?</h3></div>
		<p>Muitos já podem ter lido sobre a <a href="http://www.biblioteconomiadigital.com.br/2012/08/a-taxonomia-de-bloom-verbos-e-os.html" target="_blank" title="acesse os verbos da taxonomia de Boom">Taxonomia de Bloom</a>, tradicionalmente difundida nesse assunto: lista de verbos organizados de acordo com domínios e níveis de complexidade que se deseja expressar em relação à aprendizagem esperada do aluno.</p>
		<p>Atualmente, encontramos essa proposta revisada em função dos avanços tecnológicos, denominada <span class="semi-bold">Taxonomia Digital de Bloom</span> (Mattar, 2014), conforme a figura abaixo:</p>
		<table class="table-ead table-striped table-hover" cellpadding="10">
		  <caption class='table-title' style="background:#5CC4BF; color:#000 !important">
		  <strong><span style="font-size:20px">TAXONOMIA DIGITAL DE BLOOM</span></strong>
		  </caption>
		  <thead>
		    <tr>
		      <th style="text-align:center; font-size:18px; width:15%; background:#FDCC82 !important; color:#000 !important" class='table-title' >Termos chave</th>
		      <th style="text-align:center; font-size:18px; width:45%; background:#FDCC82 !important; color:#000 !important" class='table-title' >Verbos</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td class='table-title' style="background:#D5E2EF; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Criar</strong></td>
		      <td class='table-title' style="background:#D5E2EF; text-align:center; color:#000 !important; text-shadow:none">Planejar, constuir, produzir, inventar, projetar, elaborar, programar, filmar, animar, mixar, remixar, wiki-ar (wiki-ing), publicar videocasting, podcasting, dirigir, transmitir programa de rádio ou tv, blogar, videoblogar.</td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Avaliar</strong></td>
		      <td class='table-title' style="background:#EFDAC1; text-align:center; color:#000 !important; text-shadow:none">Examinar, formular hipóteses, criticar, experimentar, julgar, testar, detectar, monitorar, comentar em um blog, revisar, postar, moderar, colaborar, participar em redes, reelaborar, provar.</td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#B0DED7; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Analisar</strong></td>
		      <td class='table-title' style="background:#B0DED7; text-align:center; color:#000 !important; text-shadow:none">Comparar, organizar, desconstruir, atribuir, delinear, encontrar, estruturar, integrar, misturar, linkar, validar, fazer engenharia reversa, crackear, recopiar informação de meios (media clipping)</td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#FDD3B1; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Aplicar</strong></td>
		      <td class='table-title' style="background:#FDD3B1; text-align:center; color:#000 !important; text-shadow:none">Implementar, realizar, usar, executar um programa, carregar um programa, jogar, operar, hackear, fazer uploads, compartilhar, editar.</td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#8AC9BE; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Compreender</strong></td>
		      <td class='table-title' style="background:#8AC9BE; text-align:center; color:#000 !important; text-shadow:none">Interpretar, resumir, inferir, parafrasear, classificar, comparar, explicar, exemplificar, fazer buscas avançadas, fazer buscas booleanas, fazer jornalismo em formato de blog, twittar, categorizar, taguear, comentar, fazer anotações, assinar (uma lista de e-mails, por exemplo).</td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#F5C984; text-align:center; color:#000 !important; text-shadow:none; font-size:18px; "><strong>Lembrar</strong></td>
		      <td class='table-title' style="background:#F5C984; text-align:center; color:#000 !important; text-shadow:none">Reconhecer, listar, descrever, identificar, recuperar, nomear, localizar, encontrar, criar listas com marcadores e numeração, realçar (highlighting), gravar endereços de sites em softwares sociais, participar de redes sociais, favoritar, googlar.</td>
		    </tr>
		  </tbody>
		</table>
		<p>Esses verbos são sugestões que podem auxiliar na definição dos seus objetivos. O mais importante de tudo é estruturá-los de maneira clara e coerente, uma vez que eles direcionarão a escolha dos conteúdos a serem desenvolvidos na ação educativa e, também, a proposta de avaliação que será utilizada em cada caso.</p>
	</div>
  	<div class="col-lg-12" >
		
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina13.php', 'aula3pagina15.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
