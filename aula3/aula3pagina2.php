<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','2', '26', 'aula3pagina1.php', 'aula3pagina3.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-6">

		
		<p>De acordo com nossos estudos até aqui, na área educativa, as atividades são estruturadas com vistas a promover a aprendizagem dos alunos. </p>
		<p>Agir para alcançar esse propósito, por conseguinte, exige dos profissionais envolvidos uma organização prévia, um trabalho de base para fortalecer as ações futuras: o planejamento.</p>
		<p>O professor Celso dos Santos Vasconcellos<span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span> ressalta que: “Planejar é antecipar ações para atingir certos objetivos, que vêm de necessidades criadas por uma determinada realidade, e, sobretudo, agir de acordo com essas ideias antecipadas”.</p>
		<div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  "> 
		  Entrevista à revista Nova Escola. Disponível em: <a href="http://revistaescola.abril.com.br/formacao/planejar-objetivos-427809.shtml" target="_blank">http://revistaescola.abril.com.br/formacao/planejar-objetivos-427809.shtml</a>. Acesso em abril de 2015.
		</div>
		<p>No que concerne aos materiais didáticos para a EaD, o planejamento nos ajudará a escolher uma direção e a estabelecer alguns parâmetros. Clique na imagem para conhecer as direções: </p>
	</div>		
  	<div class="col-lg-6" style="text-align:center">
		 
			<div style="text-align:center; ">
				<img src="imagens/icone-setas.png" class="botao-reflexao-2"> <br />
				<a href="javascript:void(0);" class="btn btn-info btn-small botao-reflexao-2">Clique para ver as direções</a>
			</div>
			<div style="text-align:center; display:none; background:url('imagens/icone-setas-big.png') center center no-repeat;" class="conteudo-reflexao-2" >
              <p style="text-indent:0em;">   <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Por onde começar? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Quais as necessidades da instituição? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Qual o problema diagnosticado na instituição que o treinamento objetiva solucionar? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Quais fatores motivaram a escrita do material? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Quais pessoas precisamos alcançar? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Quais objetivos pretendemos atingir? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Quais assuntos privilegiar? </h3></p>
			  <p style="text-indent:0em;"> <h3 class="titulo titulo-secondary; color:#666; padding:0px; margin:0px; font-size:14px !important; margin-bottom:5px;" >	 Qual a proposta de avaliação? </h3></p>
			</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 conteudo-reflexao-2" >
		<div class="well cor-ativo">
			<span class="semi-bold">Note</span>: 
			Se começarmos a pensar aonde queremos chegar e aonde esperamos que os alunos cheguem ao entrarem em contato com o nosso material, dificilmente qualquer caminho servirá. Assim, não ficaremos perdidos como a personagem Alice!</div>
			<p>Observadas essas questões, conversaremos nesta aula a respeito do processo de elaboração de material didático para a EaD aqui no Tribunal e sobre o Plano de Elaboração do Material Didático, documento utilizado nas contratações de autores de conteúdo. Com isso, esperamos que você planeje o seu próprio material, com base nos elementos essenciais que descrevemos adiante.</p>
			<h3 class="titulo titulo-secondary" style="text-align:center" >Vamos começar?</h3>
	</div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina1.php', 'aula3pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
