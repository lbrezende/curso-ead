<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','10', '26', 'aula3pagina9.php', 'aula3pagina11.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">
  	<div class="col-lg-6">
		<p>A definição dos objetivos é uma etapa importante do planejamento, pois eles retratam aonde queremos chegar com a realização do treinamento. E não é tarefa das mais simples!</p>
		<p>Essa fase está intimamente relacionada com a justificativa. Logo, para definirmos os objetivos, o olhar deve voltar-se para os fatores que motivaram a proposta.</p>
		Algumas questões podem nos orientar nesse sentido:
		<ul>
		  <li style="padding-top:10px;">Qual a finalidade do curso?</li>
		  <li style="padding-top:10px;">O que se espera do aluno após o estudo?</li>
		  <li style="padding-top:10px;">Quais competências ou habilidades pretendemos promover?</li>
		</ul>
		<p>Além disso, os objetivos devem ser redigidos de forma clara e detalhada, de modo a indicar exatamente aquilo que pretendemos em relação ao curso (objetivo geral) e o que o aprendiz será capaz de fazer após o estudo de cada aula ou módulo (objetivo específico).</p>
	</div>
  	<div class="col-lg-6" style="overflow:hidden;">


  		<div id="clouds" style="padding:20px; -webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;">
	  		
	  		<div>
				<div class="cloud x1"></div>
				<!-- Time for multiple clouds to dance around -->
				<div class="cloud x2"></div>
				<div class="cloud x3"></div>
				<div class="cloud x4"></div>
				<div class="cloud x5"></div>
			</div>
	  		<div style="z-index:9999999; position: absolute;    top: 0px;    padding: 30px 50px 0px 14px;">
	  			<p><strong>Para refletir</strong></p>
	  			<p>Sem clareza em relação aos objetivos, poderemos divagar, esquecer pontos importantes, perder o foco. No nosso caso, não há espaço para isso, pois, se não tivermos objetivos definidos, questionaremos a própria necessidade de se elaborar o material.</p>  		
	  		</div>

		</div>

  	</div>


 		 
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina9.php', 'aula3pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
