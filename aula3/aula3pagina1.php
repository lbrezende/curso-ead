<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','1', '26', 'aula3pagina1.php', 'aula3pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-6">
	  	<h3 class="titulo titulo-primary">Bem-vindos novamente!</h3>
		<p>Após conversarmos sobre assuntos fundamentais para a educação a distância, neste momento do curso, estudaremos temas relacionados ao planejamento da escrita do material didático. Cabe salientar que este módulo é composto apenas pela Aula 3.</p>
		<div style="text-align:center">
		  <h3 class="titulo titulo-secondary" style="padding-top:10px">Mas por que falar sobre planejamento?</h3> </div>
		<p>Na clássica história de “Alice no País das Maravilhas”, um trecho interessante nos convida a refletir a esse respeito. </p>
		<p>As ideias levantadas na história podem gerar diversas reflexões. Nesse sentido, perguntamos a você:</p>

		<div style="text-align:center; " class="conteudo-reflexao-1">
		    <h3 class="titulo titulo-secondary" style="padding-top:10px">Qual caminho devemos seguir para escrever materiais didáticos consistentes?</h3> 
		    <h3 class="titulo titulo-secondary">Qualquer caminho serve?</h3> 
	    </div>		
	</div>
  	<div class="col-lg-6">
		<div class="bloco-pontilhado" style="">
		  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
		  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
		    <iframe width="420" height="315" src="https://www.youtube.com/embed/ISt-Dx7nBNE" startTime="1" endTime="2" frameborder="0" allowfullscreen></iframe>                    
		  </div>
		</div>
	</div>	
</div>
<div class="row"><div class="col-lg-12" style="margin-top:20px">
	<p>Essas interrogações nos conduzem a perceber que o caminho do planejamento será fundamental para o trabalho do conteudista. </p>
	  <p>Vale ressaltar que planejar é uma prática inerente à vida. Se não organizarmos a nossa rotina minimamente, com certeza gastaremos tempo e nos perderemos em meio a todos os afazeres, não é mesmo?</p>

</div></div>

<?php  configNavegacaoRodape('exibir', 'fim', 'aula3pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
