<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','15', '26', 'aula3pagina14.php', 'aula3pagina16.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">

		<p><span class="semi-bold">Vamos refletir sobre uma situação hipotética:</span></p>
		<p>Em um curso de redação, desejamos que, ao final do estudo, o aluno seja capaz de: <em>posicionar-se diante de determinado assunto, expressando com clareza seus argumentos</em>. </p>
		<p>Qual objetivo seria mais apropriado?</p>
		<ol>
		  <li>Reconhecer a importância de se posicionar diante de determinado assunto…</li>
		  <li>Formular argumentos claros para expressar sua posição, a favor ou contra, sobre determinado assunto...  </li>
		</ol>

	</div>
  	<div class="col-lg-12" style="margin-top:20px" >
			<div class="bloco-pontilhado" style="margin-top:0px" class="">
				<p style="text-align:center; font-weight:bold;"><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:10px">Clique aqui para ver a resposta</a> </p>
				<div id="conteudo-cerebro" style="display:none">
					<p>Nesse caso, a alternativa “b” nos parece mais coerente, não é mesmo?</p>
					<p>Siga esse entendimento para elaborar os seus objetivos específicos.</p>
				</div>
			</div>  	

  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina14.php', 'aula3pagina16.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
