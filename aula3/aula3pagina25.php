<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','25', '26', 'aula3pagina24.php', 'aula3pagina26.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Vamos praticar</h3>
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<ul>
		  <li style="padding:10px">Você deverá planejar a elaboração do material didático de um curso a distância. Escolha um tema de seu domínio, relacionado com sua experiência e que seja de interesse do STF.</li>
		  <li style="padding:10px">Para escrever o seu Plano de Elaboração do Material Didático, você deverá utilizar o arquivo modelo que se encontra na plataforma Moodle. </li>
		  <li style="padding:10px">Siga as orientações que foram ressaltadas nesta aula para efetuar a sua atividade. Em caso de dúvidas, entre em contato com o tutor.</li>
		  <li style="padding:10px">O prazo para a realização da atividade encontra-se no ambiente virtual de aprendizagem.</li>
		</ul>
		<p><strong>Observação:</strong> O tema escolhido no seu planejamento também será utilizado na atividade final do nosso curso: escrita de uma aula completa a respeito do conteúdo delimitado no seu Plano de Elaboração do Material Didático. Então, capriche na sua proposta.</p>
		<p><strong>Lembre-se:</strong> Caso, futuramente, você seja contratado pelo Tribunal, precisará elaborar documento semelhante. Logo, aproveite a oportunidade para praticar agora!</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Preparados? Mãos à obra!</h3></div>
	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina24.php', 'aula3pagina26.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
