<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','7', '26', 'aula3pagina6.php', 'aula3pagina8.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/interroga.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 50px; max-height:80px; margin-top:18px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/interroga.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">B) JUSTIFICATIVA</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Por que este curso precisa ser realizado?</strong></p>
					<p style="font-size: 20px"><strong>O que motivou a escrita deste conteúdo?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">
  	<div class="col-lg-6">
		<p>Precisamos ter em mente os motivos institucionais que ensejaram a escrita de um material didático com tema específico. Isso porque a justificativa contempla informações importantes que ajudarão a nortear as decisões relacionadas ao treinamento.</p>
		<p>Desse modo, torna-se imprescindível compreender o contexto em que se identificou essa necessidade:</p>
		<ul>
		  <li style="padding-top:10px;">Qual problema foi levantado?</li>
		  <li style="padding-top:10px;">Quais as dificuldades atuais sobre o assunto observadas com relação ao trabalho realizado no STF?</li>
		  <li style="padding-top:10px;">Quais atividades específicas precisam de aperfeiçoamento? </li>
		  <li style="padding-top:10px;">Como o estudo do conteúdo poderá contribuir para o aprimoramento das atividades exercidas?</li>
		  <li style="padding-top:10px;">São requeridos conhecimentos de que nível (básico, intermediário ou avançado)? </li>
		  <li style="padding-top:10px;">São necessários conhecimentos teóricos e/ou práticos? </li>
		  <li style="padding-top:10px;">Quais unidades de trabalho estão envolvidas?</li>
		</ul>
		<p>Podemos dizer, com efeito, que a justificativa é o diagnóstico da situação. Em vista disso, quanto mais dados relevantes conseguirmos para elaborar a justificativa, mais bem fundamentado será o plano de trabalho. </p>
		<p>Muitas dessas informações serão fornecidas pela equipe responsável pela EaD, que define a programação de cursos a serem desenvolvidos em função dos resultados do Levantamento de Necessidades de Capacitação (LNC). Esse estudo é realizado com o objetivo de incentivar as unidades a refletir sobre os processos de trabalho e a identificar os conhecimentos necessários à promoção de melhorias nas atividades, com vistas ao alcance dos objetivos estratégicos do Tribunal. </p>

  	</div>
  	<div class="col-lg-6">
		<div class="bloco-pontilhado" style="margin-top:40px" class="">
		  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
		  <p style="text-align:center; font-weight:bold;">Que tal conhecer os eventos de capacitação definidos de acordo com os resultados do LNC?  </p>
		  <div id="conteudo-cerebro" >
		    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		      <p style="text-align:center; text-indent:0em;">Visite, na intranet do STF, a página do Plano de Capacitação Anual: <br /><br /> <a href="http://intranetstf/intranetsgp/?page_id=5483" class="btn btn-info btn-small" title="clique aqui para acessar o LNC" target="_blank">Plano de Capacitação Anual</a> </p>
		    </div>
		  </div>
		</div>
		<p style="margin-top:20px">Você, conteudista, deverá organizar os dados referentes à justificativa no seu Plano de Elaboração do Material Didático. Para tanto, não existe um texto padrão a ser seguido. O importante é escrever argumentos que deixem claro que o treinamento precisa ser realizado.</p>
        <p><span class="semi-bold" >Observação</span>: Em uma possível contratação do seu trabalho como conteudista do STF, a justificativa será utilizada para embasar o projeto básico. Este será encaminhado ao Diretor-Geral, que é a autoridade competente para aprovar a realização do processo. Além disso, o emprego de recursos públicos utilizados em uma contratação exige fundamentação convincente. Percebeu a responsabilidade?</p>
		<p>No nosso caso, para realizar a atividade avaliativa desta aula, você deverá criar uma justificativa sólida, capaz de convencer a instituição (STF) acerca da importância de realizar o curso com o tema que você escolheu. Certo?</p>		
  	</div>  	
</div>





<?php  configNavegacaoRodape('exibir', 'aula3pagina6.php', 'aula3pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
