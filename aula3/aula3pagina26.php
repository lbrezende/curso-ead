<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','26', '26', 'aula3pagina25.php', 'aula3pagina26.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">REFERÊNCIAS</h3>
	</div>
</div>

<div class="row">

  <div class="col-lg-12">
		<p style="text-indent:0em">BARRETO, C. et al. <strong>Planejamento e elaboração de material didático impresso para EaD: </strong>elementos instrucionais e estratégias de ensino. CEDERJ, curso de Formação da UAB para a Região Sudeste 1.  Disponível em: <<a href="http://teca.cecierj.edu.br" target="_blank">http://teca.cecierj.edu.br</a>>. Acesso em: jun. 2015.</p>
		<p style="text-indent:0em">INSTITUTO NACIONAL DO SEGURO SOCIAL (INSS). <strong>Curso de design educacional.</strong> Acesso com login e senha.</p>
	<p style="text-indent:0em">MATTAR, João. <strong>Design educacional:</strong> educação a distância na prática. São Paulo: Artesanato Educacional, 2014.</p>
	  <p style="text-indent:0em">TRACTENBERG, R. <strong>Curso teoria e prática do design instrucional.</strong> Disponível em: <<a href="http://www.livredocencia.com.br/home/" target="_blank">http://www.livredocencia.com.br/home/</a>>. Acesso com login e senha.</p>
		<p style="text-indent:0em">Imagens: <<a href="http://www.pixabay.com" target="_blank">http://www.pixabay.com</a>> Licença CCO Public Domain.</p>
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina25.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
