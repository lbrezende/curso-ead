<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','19', '26', 'aula3pagina18.php', 'aula3pagina20.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/check.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 22px;    max-height: 98px;   margin-top: 9px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/check.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">F) ATIVIDADES AVALIATIVAS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Como posso ajudar o aluno a fixar os <br /> conhecimentos abordados no curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-6">
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Lembra-se dos princípios da Andragogia estudados na Aula 2?</h3></div>
		<p>Eles indicam que os estudantes adultos desejam aprender questões que impactam a própria vida e que possam ser aplicadas na prática. </p>
		<p>Este é o nosso desafio: propor atividades avaliativas que estimulem o aluno a refletir e, ao mesmo tempo, ajudem a consolidar a própria aprendizagem de maneira significativa.</p>
		<p>Por essa perspectiva, a avaliação deve se fazer presente ao longo do material didático produzido, apresentada como estratégias de fixação, propostas de reflexão, entre outras formas que auxiliem os alunos a perceber os próprios avanços. </p>
		<p>No momento da elaboração do material, não precisaremos pensar na atribuição de notas para as atividades. Essa questão será estabelecida em período posterior, quando planejarmos a tutoria do curso. Assim, o tutor, em conjunto com a SEEAD, analisará a proposta de avaliação desenvolvida pelo conteudista e definirá os critérios para a composição das notas quando o treinamento, de fato, for oferecido. </p>
		
	</div>
  	<div class="col-lg-6" >
			<div class="bloco-pontilhado" style="margin-top:0px; padding:10px" class="">
			   <p><strong>Observação 1</strong>: Caso o tutor indicado para acompanhar o curso manifeste interesse em modificar as atividades que foram desenhadas pelo conteudista, a alteração somente será efetivada com o aval da SEEAD. Nessa situação, o aceite não acarretará incremento na carga horária total definida para o evento.</p>
				
			</div> 
			<div class="bloco-pontilhado" style="margin-top:20px; padding:10px" class="">
			   
				<p><strong>Observação 2</strong>: Por força normativa, nos cursos <em>online</em> oferecidos pelo STF, há exigência de um aproveitamento mínimo dos alunos para aprovação. Isso ocorre porque é necessário um critério para atestar a participação dos servidores, uma vez que são empregados recursos públicos nessas ações. </p>
			</div> 			 	
		
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina18.php', 'aula3pagina20.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
