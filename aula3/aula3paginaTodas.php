<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','1', '26', 'aula2pagina1.php', 'aula2pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<?php $i = 1; echo "*** Início aula ".$i." *** " ?>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Bem-vindos novamente!</h3>
<p>Após conversarmos sobre assuntos fundamentais para a educação a distância, neste momento do curso, estudaremos temas relacionados ao planejamento da escrita do material didático. Cabe salientar que este módulo é composto apenas pela Aula 3.</p>
<div style="text-align:center"><h3 class="titulo titulo-secondary">Qual o significado da aprendizagem?</h3> </div>
<p>Na clássica história de “Alice no País das Maravilhas”, um trecho interessante nos convida a refletir a esse respeito. </p>
<div class="bloco-pontilhado" style="">
  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
  <p style="text-align:center; font-weight:bold;">Agora, convidamos você para assistir a um pequeno vídeo, que retrata de maneira bem objetiva o que estamos discutindo sobre a Andragogia:  </p>
  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
    <iframe width="420" height="315" src="https://www.youtube.com/embed/8I_0jXipUcI" frameborder="0" allowfullscreen></iframe>                    
  </div>
</div>
<p>As ideias levantadas na história podem gerar diversas reflexões. Nesse sentido, perguntamos a você:</p>
<div class="bloco-pontilhado" style="">
  <img src="imagens/questionamento.jpg" alt="" title="imagem questionamento" witdh="191px" height="82px" style="margin-top:-40px; margin-left:-30px;">
  <div style="text-align:center"><h3 class="titulo titulo-secondary">Qual caminho devemos seguir para escrever materiais didáticos consistentes?</h3> </div>
  <div style="text-align:center"><h3 class="titulo titulo-secondary">Qualquer caminho serve?</h3> </div>
</div>
<p>Essas interrogações nos conduzem a perceber que o caminho do planejamento será fundamental para o trabalho do conteudista. </p>
<p>Vale ressaltar que planejar é uma prática inerente à vida. Se não organizarmos a nossa rotina minimamente, com certeza gastaremos tempo e nos perderemos em meio a todos os afazeres, não é mesmo?</p>
<p>De acordo com nossos estudos até aqui, na área educativa, as atividades são estruturadas com vistas a promover a aprendizagem dos alunos. Agir para alcançar esse propósito, por conseguinte, exige dos profissionais envolvidos uma organização prévia, um trabalho de base para fortalecer as ações futuras: o planejamento.</p>
<p>O professor Celso dos Santos Vasconcellos<span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span> ressalta que: “Planejar é antecipar ações para atingir certos objetivos, que vêm de necessidades criadas por uma determinada realidade, e, sobretudo, agir de acordo com essas ideias antecipadas”.</p>
<div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  "> 
  Entrevista à revista Nova Escola. Disponível em: <a href="http://revistaescola.abril.com.br/formacao/planejar-objetivos-427809.shtml" target="_blank">http://revistaescola.abril.com.br/formacao/planejar-objetivos-427809.shtml</a>. Acesso em abril de 2015.
</div>
<p>No que concerne aos materiais didáticos para a EaD, o planejamento nos ajudará a escolher uma direção e a estabelecer alguns parâmetros: </p>
<div style="text-align:center;margin-bottom:20px;">
[To-do]essa imagem não é free, gostaria de uma semelhante
  <img src="imagens/imagem1.png" alt="Por onde começar?
  Quais as necessidades da instituição?
  Qual o problema diagnosticado na instituição
  que o treinamento objetiva solucionar?
  Quais fatores motivaram a escrita do material?
  Quais pessoas precisamos alcançar?
  Quais objetivos pretendemos atingir?
  Quais assuntos privilegiar?
  Qual a proposta de avaliação?" />
</div>

<div class="row well cor-ativo"><span class="semi-bold">Note</span>: Se começarmos a pensar aonde queremos chegar e aonde esperamos que os alunos cheguem ao entrarem em contato com o nosso material, dificilmente qualquer caminho servirá. Assim, não ficaremos perdidos como a personagem Alice!</div>
<p>Observadas essas questões, conversaremos nesta aula a respeito do processo de elaboração de material didático para a EaD aqui no Tribunal e sobre o Plano de Elaboração do Material Didático, documento utilizado nas contratações de autores de conteúdo. Com isso, esperamos que você planeje o seu próprio material, com base nos elementos essenciais que descrevemos adiante.</p>
<p>Vamos começar?</p>
<h3 class="titulo titulo-primary">3.1 Elaboração do material didático no STF</h3>
<p>No STF, a Seção de Educação a Distância – SEEAD (Coordenadoria de Desenvolvimento de Pessoas/Secretaria de Gestão de Pessoas) é a responsável por gerenciar os processos relacionados à produção de materiais didáticos para a EaD. </p>
<p>Essa atividade é regulamentada pela norma de Instrutoria Interna, [To-do]verificar o link com a Mariana <a href="http://stf.jus.br/ARQUIVO/NORMA/INSTRUCAONORMATIVA169-2014.PDF" target="_blank" title="link para IN169/14">IN 169/14</a>, na qual estão descritos todos os requisitos necessários para o servidor que deseja atuar nessa área, bem como os critérios para a retribuição pelo exercício dessa função. </p>
<p>O processo de elaboração de materiais é composto pelas seguintes etapas:</p>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Etapa</th>
      <th>Ator</th>
      <th>Ação</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Entrega do “Plano de Elaboração do Material Didático”</td>
      <td><span class="semi-bold">Conteudista</span></td>
      <td>escrita </td>
    </tr>
    <tr>
      <td>Validação do “Plano de Elaboração do Material Didático”</td>
      <td>CDPE</td>
      <td>Análise das informações apresentadas e indicação de ajustes, se necessário.</td>
    </tr>
    <tr>
      <td>Conhecendo sua escrita</td>
      <td><span class="semi-bold">Conteudista</span></td>
      <td>Aula demonstrativa para validação da CDPE, com a proposta de primeira aula do conteúdo para o curso pretendido, a fim de conhecer o estilo de escrita do conteudista e de verificar a correspondência com as necessidades específicas do texto para educação a distância.</td>
    </tr>
    <tr>
      <td>Validação da aula demonstrativa</td>
      <td>CDPE</td>
      <td>Análise da escrita e do uso de recursos complementares à aprendizagem, tais como vídeos, imagens, <i>hiperlinks</i> etc.</td>
    </tr>
    <tr>
      <td>Contratação do conteudista</td>
      <td>CDPE</td>
      <td>Escrita do projeto básico e juntada de documentos.<br />
          Encaminhamento para trâmite, com vistas à aprovação do projeto pelo Diretor-Geral.</td>
    </tr>
    <tr>
      <td>Escrita do conteúdo do curso</td>
      <td><span class="semi-bold">Conteudista</span></td>
      <td>O conteudista desenvolverá o texto com o conteúdo programático proposto para o curso, que constituirá a apostila configurada em versão para ser impressa.</td>
    </tr>
    <tr>
      <td>Validação do conteúdo</td>
      <td>CDPE</td>
      <td>Análise e avaliação do texto produzido pelo conteudista, com indicação de ajustes e melhorias no que diz respeito à linguagem, aos recursos propostos e ao conteúdo, para que todos os itens previstos sejam contemplados.</td>
    </tr>
    <tr>
      <td>Entrega da versão final do conteúdo</td>
      <td><span class="semi-bold">Conteudista</span></td>
      <td>Com base nas orientações da CDPE, o conteudista deverá efetuar os ajustes no material didático para entregar a versão final do trabalho.</td>
    </tr>
    <tr>
      <td>Revisão gramatical</td>
      <td>Revisor de textos</td>
      <td>Revisão dos aspectos relacionados à Língua Portuguesa.</td>
    </tr>
  </tbody>


</table>

<p>Após a conclusão da etapa de escrita, o material passará por tratamento pedagógico e gráfico, para que seja adaptado ao ambiente virtual de aprendizagem. Somente depois disso, o curso estará pronto para ser ofertado aos alunos. Cabe salientar que essa fase é de responsabilidade exclusiva da SEEAD, isto é, não há a participação do conteudista.</p>
<p>Depois dessa breve contextualização concernente ao processo, passaremos a analisar os principais tópicos previstos no Plano de Elaboração do Material Didático do STF.</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
    <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
    <p style="text-align:center; font-weight:bold;">Você conhece a Comunidade de Apoio aos Instrutores Internos EaD?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
    <div id="conteudo-cerebro" style="display:none">
      <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
        <p style="text-align:center; text-indent:0em;"><img src="imagens/comunidadeEad.jpg" style="max-width:40%; text-align:center;" alt=""> <br /><br /> Para auxiliar o desenvolvimento do trabalho dos profissionais da educação a distância do STF, dentre os quais, os conteudistas, foram disponibilizados pela SEEAD manuais de orientação e  diversos materiais nesta comunidade. </p>
        <p style="text-align:center; text-indent:0em;">Que tal visitar a comunidade?<br /><a href="https://ead.stf.jus.br/course/view.php?id=70"  target="_blank" title="visite a página da comunidade de apoio aos instrutores internos do STF">https://ead.stf.jus.br/course/view.php?id=70</a></p>
      </div>
    </div>
</div>
<h3 class="titulo titulo-primary">3.2 Plano de elaboração do material didático</h3>
<p>Vimos no item anterior que a entrega do <a href="" title="acesse o plano de elaboração de material didático" target="_blank">Plano de Elaboração do Material Didático</a>[To-do]PEGAR O LINK é a primeira etapa do processo de produção de conteúdos no Tribunal.</p>
<p>Por isso, agora, detalharemos os principais tópicos relacionados a esse planejamento, a fim de que você possua as informações necessárias para atuar nessa fase. </p>
<p>Antes de seguirmos com os estudos, consideramos pertinente antecipar a descrição da Atividade Avaliativa deste módulo, tudo bem? Não se assuste! Apenas desejamos colocar a sua mente em alerta, vejamos:</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <div style="text-align:center"><h3 class="titulo titulo-secondary">Atividade Avaliativa – Módulo II</h3></div>
  <p>Ao final deste módulo, você deverá apresentar um Plano de Elaboração do Material Didático de um curso a distância, versando sobre tema de seu domínio e de interesse do STF. As orientações detalhadas sobre a atividade encontram-se no fim desta aula. Então, ao ler as explicações, aproveite para pensar em sua tarefa, ok?</p>
</div>
<h3 class="titulo titulo-primary">3.2.1 Principais itens do planejamento</h3>
[To-do]a) Nome do curso
Qual nome devo escolher para o curso? Qual a identidade dele?
<p>A título de inspiração inicial, indicamos que assista ao vídeo a seguir, que aborda a temática da criação de nomes para empresas:</p>
<div class="bloco-pontilhado" style="">
  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
    <iframe width="420" height="315" src="https://www.youtube.com/embed/RwplGixBcsc" frameborder="0" allowfullscreen></iframe>                    
  </div>
</div>
<p>Apesar das ressalvas sobre o tipo de trabalho que iremos desenvolver, isto é, materiais didáticos para cursos a distância, o filme apresenta alguns princípios que podem nos auxiliar na escolha do nome do nosso curso.</p>
<p>De acordo com o site “Consultório Etimológico”<span id="biblio-2" class="fonte-bibliografica-numero " name="biblio-abrir">2</span>, a palavra nome vem do Latim NOMEN e do Grego ONYMA ou ONOMA e significa: “palavra usada para identificar uma pessoa”.</p>
<div class="fonte-bibliografica biblio-2-conteudo alert alert-warning" style="display:none;  "> 
  Disponível em: <a href="http://origemdapalavra.com.br/site/pergunta/origem-da-palavra-nome/" title="acesse explicação da origem da palavra nome" target="_blank">http://origemdapalavra.com.br/site/pergunta/origem-da-palavra-nome/</a>. Acesso em abril de 2015.   
</div>
<p>No nosso caso, é de extrema relevância identificar com clareza a temática central do curso que pretendemos escrever e expressar de forma concisa as intenções do treinamento.</p>
<p>Por isso, precisamos pensar em nomes que chamem a atenção do leitor, de tal forma que o motive a participar do evento. Ademais, quando possível, devemos deixar claro o nível de aprofundamento do treinamento, pois isso ajudará o aluno a avaliar se possui os conhecimentos necessários para participar da ação educativa com aproveitamento. </p>
<p>Para definir a identificação do curso, podemos utilizar a criatividade, mas sempre atentos ao fato de que integramos uma instituição pública; assim, devemos ter cuidado com os exageros.</p>
<p>Vamos analisar, agora, alguns nomes de treinamento. Veja:</p>
<div class="balao-esquerdo">“Redação Oficial”<a href="javascript:void(0)" style="float:right;" class="botao-questao-1 btn btn-info btn-small">Clique para ver a análise do nome</a></div>
    <div class="balao-direito conteudo-questao-1" style="display:none">
      <p class="" style=" margin-bottom:0px; padding-bottom:0px">Esse é um tema específico e passível de ser tratado em apenas um curso a distância. Não há muito a acrescentar ao nome do curso. Ao nosso ver, ele atinge perfeitamente o público interessado.</p>
    </div>

    <div class="balao-esquerdo">“Direito Constitucional” <a href="javascript:void(0)" style="float:right;" class="botao-questao-2 btn btn-info btn-small">Clique para ver a análise do nome</a></div>
    <div class="balao-direito conteudo-questao-2" style="display:none">
    <p class="" style=" margin-bottom:0px; padding-bottom:0px">A identificação do curso é muito ampla. Sabemos que existem diversos assuntos que podem ser abordados dentro do campo do direito constitucional; contudo, dificilmente todos serão desenvolvidos em apenas um evento online. Por isso, delimitar o nível de aprofundamento e/ou os temas principais que serão estudados facilitará a compreensão dos servidores sobre a proposta do curso.</p>
  </div>

<p>Para exemplificar, apresentamos o nome de uma ação de treinamento a distância, da área do direito constitucional, trabalhada aqui do STF: </p>
<div style="text-align:center"><h3 class="titulo titulo-secondary">“Introdução ao Direito Constitucional e ao Controle de Constitucionalidade”.</h3></div>
<p>Perceba como fica evidente o foco do evento e o nível básico de aprofundamento. </p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <p style="text-align:center; font-weight:bold;">Já começaram a surgir ideias interessantes?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver a dica!</a> </p>
  <div id="conteudo-cerebro" style="display:none">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-align:center; text-indent:0em;"><img src="imagens/cerebro.jpg" style="max-width:40%; text-align:center;" alt=""> <br> Registre, sempre que possível, as ideias que aparecerem. Não confie apenas na sua memória, pois, em algumas situações, ela é volátil. Anote o que conseguir. Dessa maneira, você reunirá mais elementos para tomar a melhor decisão a esse respeito.</a> </p>                       
    </div>
  </div>
</div>
<p>[To-do] Justificativa</p>
<p>Precisamos ter em mente os motivos institucionais que ensejaram a escrita de um material didático com tema específico. Isso porque a justificativa contempla informações importantes que ajudarão a nortear as decisões relacionadas ao treinamento.</p>
<p>Desse modo, torna-se imprescindível compreender o contexto em que se identificou essa necessidade:</p>
<ul>
  <li>Qual problema foi levantado?</li>
  <li>Quais as dificuldades atuais sobre o assunto observadas com relação ao trabalho realizado no STF?</li>
  <li>Quais atividades específicas precisam de aperfeiçoamento? </li>
  <li>Como o estudo do conteúdo poderá contribuir para o aprimoramento das atividades exercidas?</li>
  <li>São requeridos conhecimentos de que nível (básico, intermediário ou avançado)? </li>
  <li>São necessários conhecimentos teóricos e/ou práticos? </li>
  <li>Quais unidades de trabalho estão envolvidas?</li>
</ul>
<p>Podemos dizer, com efeito, que a justificativa é o diagnóstico da situação. Em vista disso, quanto mais dados relevantes conseguirmos para elaborar a justificativa, mais bem fundamentado será o plano de trabalho. </p>
<p>Muitas dessas informações serão fornecidas pela equipe responsável pela EaD, que define a programação de cursos a serem desenvolvidos em função dos resultados do Levantamento de Necessidades de Capacitação (LNC). Esse estudo é realizado com o objetivo de incentivar as unidades a refletir sobre os processos de trabalho e a identificar os conhecimentos necessários à promoção de melhorias nas atividades, com vistas ao alcance dos objetivos estratégicos do Tribunal. </p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <p style="text-align:center; font-weight:bold;">Que tal conhecer os eventos de capacitação definidos de acordo com os resultados do LNC? <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para saber como conhecer!</a> </p>
  <div id="conteudo-cerebro" style="display:none">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-align:center; text-indent:0em;">Visite, na intranet do STF, a página do Plano de Capacitação Anual: <a href="http://intranetstf/intranetsgp/?page_id=5483" title="clique aqui para acessar o LNC" target="_blank">http://intranetstf/intranetsgp/?page_id=5483</a> </p>
    </div>
  </div>
</div>
<p>Você, conteudista, deverá organizar os dados referentes à justificativa no seu Plano de Elaboração do Material Didático. Para tanto, não existe um texto padrão a ser seguido. O importante é escrever argumentos que deixem claro que o treinamento precisa ser realizado.</p>
<p><span class="semi-bold">Observação</span>: Em uma possível contratação do seu trabalho como conteudista do STF, a justificativa será utilizada para embasar o projeto básico. Este será encaminhado ao Diretor-Geral, que é a autoridade competente para aprovar a realização do processo. Além disso, o emprego de recursos públicos utilizados em uma contratação exige fundamentação convincente. Percebeu a responsabilidade?</p>
<p>No nosso caso, para realizar a atividade avaliativa desta aula, você deverá criar uma justificativa sólida, capaz de convencer a instituição (STF) acerca da importância de realizar o curso com o tema que você escolheu. Certo?</p>
<p>[To-do]Caracterização</p>
<p>Imagine escrever um e-mail com vários destinatários definidos aleatoriamente. Agora, pense em redigir uma mensagem para pessoas que você conhece.</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Existe alguma diferença em relação à forma como você organizará a sua escrita? <br />Ou utilizará, em ambos os casos, as mesmas palavras?
</h3></div>
<p>Na primeira opção, você se empenhará para escrever um texto claro e dessa maneira alcançará seus objetivos, não temos dúvidas.</p>
<p>Na segunda, pelo fato de conhecer a quem se destina a mensagem, provavelmente o seu texto será mais personalizado, pois você buscará expressões que fazem parte do contexto em que estão inseridos os leitores. Com isso, poderá aplicar, inclusive, uma escrita mais afetiva e menos carregada de formalismos e, ainda assim, logrará êxito nos seus propósitos.</p>
<p>Ocorre algo semelhante na escrita de materiais didáticos: conhecer a quem se destina a produção textual poderá contribuir para a melhor escolha de palavras e expressões, bem como para o estabelecimento de um diálogo mais próximo e afetivo. Consequentemente, o seu texto poderá ser compreendido com mais facilidade pelos alunos.</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Você deseja desenvolver uma escrita alinhada com o perfil do público a que se destina o treinamento? </h3></div>
<p>O professor João Mattar (p. 57, 2014) nos ensina que: “Conhecer melhor nossos alunos permite adequar e modificar nossa estratégia de ensino em benefício da aprendizagem”.</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;" height="82px">
  <p style="text-align:center; font-weight:bold;">Antes de escrever, o que é necessário saber?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver a dica!</a> </p>
  <div id="conteudo-cerebro" style="display:none">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-align:center; text-indent:0em;">Qual o perfil dos estudantes? <br />
      Teremos alunos que precisam de alguma atenção especial (pessoas com deficiência)?<br />
      O que há de marcante neles que requer atenção especial?<br />
      Quais os conhecimentos anteriores sobre o tema?<br />
      Será necessário algum nivelamento de conhecimento?</p>                       
    </div>
  </div>
</div>
<p>Dessa forma, procure retratar com o máximo de detalhes as características do público, visto que isso ajudará a direcionar sua escrita nos seguintes termos: aprofundamento da abordagem, adequação da linguagem, enfoque que deverá ser dado ao curso e escolha dos recursos a serem empregados. </p>
<p>Precisamos ressaltar que, no STF, há servidores com deficiência. Por isso, ao planejar a escrita do seu material, tenha em mente a diversidade dos leitores e a questão da acessibilidade.</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <p style="text-align:center; font-weight:bold;">Você conhece o projeto “STF Sem barreiras”?  <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
  <div id="conteudo-cerebro" style="display:none">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-align:center; text-indent:0em;">Para obter mais informações a respeito, acesse a página do programa na intranet do Supremo: <br /><a href="http://intranetstf/intranetsis/?page_id=251"  target="_blank" title="visite a página do stf sem barreiras">http://intranetstf/intranetsis/?page_id=251</a></p>
    </div>
  </div>
</div>
<p>[To-do]Objetivos</p>
<p>A definição dos objetivos é uma etapa importante do planejamento, pois eles retratam aonde queremos chegar com a realização do treinamento. E não é tarefa das mais simples!</p>
<p>Essa fase está intimamente relacionada com a justificativa. Logo, para definirmos os objetivos, o olhar deve voltar-se para os fatores que motivaram a proposta.</p>
Algumas questões podem nos orientar nesse sentido:
<ul>
  <li>Qual a finalidade do curso?</li>
  <li>O que se espera do aluno após o estudo?</li>
  <li>Quais competências ou habilidades pretendemos promover?</li>
</ul>
<p>Além disso, os objetivos devem ser redigidos de forma clara e detalhada, de modo a indicar exatamente aquilo que pretendemos em relação ao curso (objetivo geral) e o que o aprendiz será capaz de fazer após o estudo de cada aula ou módulo (objetivo específico).</p>
<p>[To-do]Para refletir</p>
<p><span class="semi-bold">Objetivo geral</span></p>
<p>Diz respeito ao que esperamos com a realização do treinamento de maneira ampla.</p>
<div class="row well cor-ativo">
  <p style="text-align:center; ">Como exemplo, vamos citar o objetivo geral deste curso. <br /><a href="javascript:void(0);" class="btn btn-info btn-small botao-pausa-1" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver o objetivo geral</a> </p>
  <div id="conteudo-pausa-1" style="display:none">
    <p>Ao final do treinamento, os servidores deverão ser capazes de escrever material didático específico para a educação a distância, de acordo com a área temática delimitada, empregando linguagem dialógica e recursos da virtualidade, por meio de um planejamento consistente, com vistas à construção de conteúdos significativos, focados na aprendizagem dos alunos.</p>
  </div>
</div>
<p>Perceba os detalhes abordados na escrita desse objetivo geral: </p>
<ul>
  <li><span class="semi-bold">Público a que se destina o evento:</span> “servidores”.</li>
  <li><span class="semi-bold">O que se espera que o aluno seja capaz ao final do treinamento?</span> “escrever material didático específico para a educação a distância de acordo com a área temática delimitada”.</li>
  <li><span class="semi-bold">Em quais condições?</span> “empregar linguagem dialógica e recursos da virtualidade, por meio de um planejamento consistente”.</li>
  <li><span class="semi-bold">Enfoque do curso:</span> pela descrição, notamos que é um curso voltado para a prática, não se fixa apenas nos aspectos teóricos: “com vistas à construção de conteúdos significativos, focados na aprendizagem dos alunos”.</li>
</ul>
<p>Com esse exemplo, salientamos que o objetivo geral deve ser completo, além de  trazer, em seu bojo, elementos suficientes para retratar a proposta do seu curso. Certo? </p>

<p>Um método utilizado na definição de objetivos é o denominado ABCD, que possui quatro características, segundo o professor Regis T ractenberg em seu curso “Teoria e Prática do Design Instrucional”<span id="biblio-3" class="fonte-bibliografica-numero " name="biblio-abrir">3</span>:</p>
<div class="fonte-bibliografica biblio-3-conteudo alert alert-warning" style="display:none;  "> 
  <p><a href="http://www.livredocencia.com.br/home/" title="" target="_blank">http://www.livredocencia.com.br/home/</a>. Acesso com login e senha. </p>
</div>
<ol sytle="list-style-type:decimal;">
  <li>Audiência (Audience – aprendiz): Deixa claro quem são os aprendizes.</li>
  <li>Comportamento (Behavior): Indica um comportamento observável que, se realizado pelos aprendizes, fornece boa evidência de sua aprendizagem. </li>
  <li>Condições (Conditions): Descreve as condições sob as quais os aprendizes devem desempenhar o comportamento observável.</li>
  <li>Grau (Degree – critério): Especifica os critérios de avaliação do comportamento.</li>
</ol>
<p>O autor nos auxilia a compreender o modelo da seguinte maneira:</p>
<p><em>“Eis um exemplo impreciso de objetivo de aprendizagem: <br />
  Os alunos devem saber consertar um carro.<br />
  Vejamos a mesma tarefa traduzida em um objetivo instrucional conforme o modelo ABCD:</em></p>
<p><em><span style="color:#000080;">Os novos mecânicos contratados,</span><span style="color:#808000;"> dado um carro de fórmula 1 da Ferrari,</span><span style="color:#800000;"> consertarão qualquer tipo de problema no pneu.</span><span style="color:#808000;"> Eles contarão com todas as ferramentas necessárias mas não poderão consultar manuais técnicos nem deliberar sobre como realizar os consertos.</span><span style="color:#548DD4;text-decoration:underline;"> Devem ser capazes de efetivar os reparos dentro de 20 segundos em 90% das tentativas.</span><br />
  Legenda: <span style="color:#000080;">Aprendiz</span> – <span style="color:#808000;">comportamento</span> – <span style="color:#800000;">condições</span> – <span style="color:#548DD4;text-decoration:underline;">critério”</span></em></p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Então, ficou um pouco mais claro como o objetivo geral poderá ser escrito?</h3></div>
<p>É importante salientar que não há rigidez quanto à redação do objetivo geral. Contudo, devemos contemplar na escrita os aspectos que deixem evidente a proposta do curso.</p>
<p><span class="semi-bold">Objetivos específicos</span></p>
<p>Os objetivos específicos são um detalhamento do objetivo geral. Eles se referem ao que o aluno será capaz de realizar após o estudo de cada aula, isto é, a aprendizagem que ele deve apresentar ao final de cada tópico de conteúdo. Por isso, os objetivos específicos ajudam a definir o número de módulos e/ou aulas a serem escritas. </p>
<p><span class="semi-bold">Atenção:</span> Os <span class="semi-bold">objetivos específicos</span> não são ações que descrevem atividades do conteudista ou do professor. Eles dizem respeito a <span class="semi-bold">aprendizagens esperadas dos alunos</span>.</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Você pode estar se perguntando: como escrever esses objetivos específicos?</h3></div>
<ol sytle="list-style-type:decimal;">
  <li>Inicie o seu objetivo com verbos no modo infinitivo. </li>
  <li>Se possível, inclua condições e critérios: o que o aprendiz deve ser capaz de fazer? Em que condições deve fazê-lo? </li>
  <li>Registre aquilo que o aluno será capaz de realizar após o estudo.</li>
</ol>
<p>Veja alguns exemplos do nosso curso:</p>
<ul>
  <li>Identificar os principais fatos ligados à evolução histórica da EaD no Brasil e no mundo. </li>
  <li>Planejar o processo de elaboração de materiais para a EaD com base nos seus elementos essenciais.</li>
</ul>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Quais verbos utilizar?</h3></div>
<p>Muitos já podem ter lido sobre a <a href="http://www.biblioteconomiadigital.com.br/2012/08/a-taxonomia-de-bloom-verbos-e-os.html" target="_blank" title="acesse os verbos da taxonomia de Boom">Taxonomia de Bloom</a>, tradicionalmente difundida nesse assunto: lista de verbos organizados de acordo com domínios e níveis de complexidade que se deseja expressar em relação à aprendizagem esperada do aluno.</p>
<p>Atualmente, encontramos essa proposta revisada em função dos avanços tecnológicos, denominada <span class="semi-bold">Taxonomia Digital de Bloom</span> (Mattar, 2014), conforme a figura abaixo:</p>
<table class="table table-striped">
  <caption>TAXONOMIA DIGITAL DE BLOOM</caption>
  <thead>
    <tr>
      <th>Termos chave</th>
      <th>Verbos</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Criar</td>
      <td>Planejar, constuir, produzir, inventar, projetar, elaborar, programar, filmar, animar, mixar, remixar, wiki-ar (wiki-ing), públicar videocasting, podcasting, dirigir, transmitir, programa de rádio ou tv, blogar, vídeo blogar.</td>
    </tr>
    <tr>
      <td>Avaliar</td>
      <td>Examinar, formular hipóteses, criticar, experimentar, julgar, testar, detectar, monitorar, comentar em um blog, revisar, postar, moderar, colaborar, participar em redes, reelaborar, provar.</td>
    </tr>
    <tr>
      <td>Analisar</td>
      <td>Comparar, organizar, desconstruir, atribuir delinear, encontrar, estruturar, integrar, misturar, linkar, validar, fazer engenharia reversa, crackear, recopiar informação de meios (media clipping)</td>
    </tr>
    <tr>
      <td>Aplicar</td>
      <td>Implementar, realizar, usar, executar um programa, carregar um programa, jogar, operar, hackear, fazer uploads, compartilhar, editar.</td>
    </tr>
    <tr>
      <td>Compreender</td>
      <td>Interpretar, resumir, inferir, parafrasear, classificar, comparar, explicar, exemplificar, fazer buscas avançadas, fazer buscas booleanas, fazer jornalismo em formato de blog, twittar, categorizar, taguear, comentar, fazer anotações, assinar (uma lista de e-mails, por exemplo).</td>
    </tr>
    <tr>
      <td>Lembrar</td>
      <td>Reconhecer, listar, descrever, identificar, reculperar, nomear, localizar, encontrar, criar lista com marcadores e numeração, realçar (highlighting), gravar endereços de sites em softwares sociais, participar de redes sociais, favoritar, googlar.</td>
    </tr>
  </tbody>
</table>
<p>Esses verbos são sugestões que podem auxiliar na definição dos seus objetivos. O mais importante de tudo é estruturá-los de maneira clara e coerente, uma vez que eles direcionarão a escolha dos conteúdos a serem desenvolvidos na ação educativa e, também, a proposta de avaliação que será utilizada em cada caso.</p>
<p><span class="semi-bold">Vamos refletir sobre uma situação hipotética:</span></p>
<p>Em um curso de redação, desejamos que, ao final do estudo, o aluno seja capaz de: <em>posicionar-se diante de determinado assunto, expressando com clareza seus argumentos</em>. </p>
<p>Qual objetivo seria mais apropriado?</p>
<ol>
  <li>Reconhecer a importância de se posicionar diante de determinado assunto…</li>
  <li>Formular argumentos claros para expressar sua posição, a favor ou contra, sobre determinado assunto...  </li>
</ol>
<p>Nesse caso, a alternativa “b” nos parece mais coerente, não é mesmo?</p>
<p>Siga esse entendimento para elaborar os seus objetivos específicos.</p>
<p>[To-do]Conteudo Programatico</p>
<p>Esta parte do planejamento pode gerar alguns conflitos internos, pois, muitas vezes, não é possível abarcar todos os assuntos relacionados à temática no mesmo curso. </p>
<p>Aí você se depara com um dilema: </p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Quais tópicos de conteúdo privilegiar?</h3></div>
<p>Bom, as principais questões que devem permear seus pensamentos, nesse momento, são: </p>
<p class="semi-bold">Por que este treinamento precisa ser realizado (justificativa)? </p>
<p class="semi-bold">Quais objetivos foram delineados para esta ação educativa? </p>
<div class="row well cor-ativo"><span class="semi-bold">Note</span>: Se você já escreveu uma justificativa bem fundamentada e definiu os objetivos do curso com cuidado, delimitar os temas a serem abordados no curso será um pouco mais tranquilo.</div>
<p>Então, é hora de definir os assuntos. Pense em um quebra-cabeça:</p>
<p><img src="imagens/maos.png" alt="maos remexendo um quebra-cabeça"></p>
<p class="semi-bold" style="color:#4476AF;">Quais são as peças-chave (assuntos) que ajudarão o aluno a construir, passo a passo, a própria aprendizagem acerca do tema central proposto para o curso? </p>
<p class="semi-bold" style="color:#4476AF;">O que é essencial e não pode ficar de fora? </p>
<p><img src="imagens/quebra_cabeca.png" alt="maos montando um quebra-cabeça"></p>
<p class="semi-bold" style="color:#4476AF;">Qual ordem de apresentação dos conteúdos favorecerá o encadeamento das ideias pelos estudantes?</p>
<p>Após a escolha dos assuntos, estes devem ser estruturados de modo que obedeçam a uma sequência lógica – do mais simples para o mais complexo –, sempre estabelecendo relação com a temática principal do treinamento.</p>
<p>Você tem a liberdade para organizar o seu curso da forma que julgar mais apropriada.  O importante é criar um esquema uniforme e subdividir os tópicos em módulos (ou unidades temáticas ou capítulos) e em aulas. </p>
<p>O nosso curso, “Produção de Conteúdos para a EaD”, foi organizado em módulos e em aulas. Veja, por exemplo, o Módulo I:</p>
<p class="semi-bold">Módulo I – EaD e aprendizagem </p>
<ul>
  <li>Aula 1 – Contextualização da Educação a Distância</li>
  <ol>
    <li>Breve histórico mundial da EaD</li>
    <li>A EaD no Brasil</li>
    <li>A EaD na atualidade</li>
  </ol>
  <li>Aula 2 – Aspectos relacionados à aprendizagem a distância:</li>
  <ol>
    <li>A aprendizagem: um processo construtivo </li>
    <li>Estilos de aprendizagem</li>
    <li>O tutor e a mediação</li>
  </ol>
</ul>
<p>Por fim, seguem alguns aspectos a serem considerados no momento da definição do conteúdo programático do curso:</p>
<ul>
  <li>Selecionar assuntos de acordo com o nível de aprofundamento previsto (básico, intermediário ou avançado) e buscar informações atualizadas sobre o tema.</li>
  <li>Determinar a estrutura dos conteúdos (módulos, aulas), além de observar uma sequência que favoreça o entendimento do conteúdo pelo aluno.</li>
</ul>
<p>[To-do]Atividades avaliativas</p>
<p>Até aqui, estudamos que o trabalho do conteudista é todo direcionado para promover a aprendizagem dos alunos. Certo? </p>
<p>Por conseguinte, as atividades avaliativas devem estar previstas no material didático, pois auxiliam o estudante a fixar os assuntos abordados. Essas estratégias de avaliação, por sua vez, devem estar alinhadas com os objetivos estabelecidos para o curso.  </p>
<p>Para conversarmos sobre a avaliação no processo de aprendizagem, proponho um resgate de suas experiências como estudante:</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Você se recorda de algum período da escola básica, da faculdade ou de outras situações educativas nas quais tenha passado por alguma experiência interessante envolvendo a avaliação?</h3></div>
<p>Em nossa cultura escolar, a avaliação da aprendizagem, geralmente, ocorre no final do processo e está muito ligada ao aspecto quantitativo, tendo em vista medir o desempenho do estudante, emitir notas e aprovar ou reprovar no período letivo. </p>
<p>Contudo, avaliar vai muito além disso, ainda mais no caso da educação a distância para adultos. Temos uma visão diferenciada da atuação do aluno, considerado ativo no seu próprio percurso de aprendizagem; por consequência, diferentes também devem ser as práticas de avaliação.</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Lembra-se dos princípios da Andragogia estudados na Aula 2?</h3></div>
<p>Eles indicam que os estudantes adultos desejam aprender questões que impactam a própria vida e que possam ser aplicadas na prática. </p>
<p>Este é o nosso desafio: propor atividades avaliativas que estimulem o aluno a refletir e, ao mesmo tempo, ajudem a consolidar a própria aprendizagem de maneira significativa.</p>
<p>Por essa perspectiva, a avaliação deve se fazer presente ao longo do material didático produzido, apresentada como estratégias de fixação, propostas de reflexão, entre outras formas que auxiliem os alunos a perceber os próprios avanços. </p>
<p>No momento da elaboração do material, não precisaremos pensar na atribuição de notas para as atividades. Essa questão será estabelecida em período posterior, quando planejarmos a tutoria do curso. Assim, o tutor, em conjunto com a SEEAD, analisará a proposta de avaliação desenvolvida pelo conteudista e definirá os critérios para a composição das notas quando o treinamento, de fato, for oferecido. </p>
<p>Observação 1: Caso o tutor indicado para acompanhar o curso manifeste interesse em modificar as atividades que foram desenhadas pelo conteudista, a alteração somente será efetivada com o aval da SEEAD. Nessa situação, o aceite não acarretará incremento na carga horária total definida para o evento.</p>
<p>Observação 2: Por força normativa, nos cursos online oferecidos pelo STF, há exigência de um aproveitamento mínimo dos alunos para aprovação. Isso ocorre porque é necessário um critério para atestar a participação dos servidores, uma vez que são empregados recursos públicos nessas ações.  </p>
<p>Você pode estar se questionando:</p>
<div class="bloco-pontilhado" style="margin-top:40px" class="">
  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
  <p style="text-align:center; font-weight:bold;">E, agora, por onde começar? Quais atividades são mais interessantes ou mais apropriadas para o ambiente online? <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver a dica!</a> </p>
  <div id="conteudo-cerebro" style="display:none">
    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
      <p style="text-align:center; text-indent:0em;"><img src="imagens/cerebro.jpg" style="max-width:40%; text-align:center;" alt=""> <br> Para elaborar atividades coerentes, recorra aos objetivos específicos que você estabeleceu para o seu curso.</a> </p>                       
    </div>
  </div>
</div>
<p>Aqui, vamos retomar o objetivo que mencionamos anteriormente como exemplo, no item que trata da escrita dos objetivos, veja: </p>
<p><em>Formular argumentos claros para expressar sua posição, a favor ou contra, sobre determinado assunto.</em></p>
<p>Pela essência daquilo que se espera dos estudantes, qual atividade parece mais apropriada?</p>
<ol>
  <li>Exercício objetivo.</li>
  <li>Redação discursiva.</li>
</ol>
<p>Na nossa perspectiva, a letra “b” se aproxima mais do objetivo proposto. Você concorda?</p>
<p><span class="semi-bold">Para elaborar suas atividades avaliativas, pense no seguinte:</span> Qual a melhor maneira de os alunos fixarem os conteúdos estabelecidos, observando os objetivos específicos que foram delineados?</p>
<p>Vale ressaltar que não existem atividades certas ou erradas, e sim mais ou menos apropriadas em função das metas estabelecidas e do conteúdo estudado. </p>
<p>Há várias opções. Para ajudá-lo nesse aspecto, listamos algumas atividades que podem ser utilizadas em cursos a distância e as ferramentas do Moodle a serem empregadas para viabilizar as tarefas no ambiente virtual de aprendizagem. </p>
<div class="row well cor-ativo"><span class="semi-bold">Note</span>: Essas são apenas sugestões, você poderá criar novas propostas de acordo com o seu caso.</div>
[To-do]Fazer os ícones, no paint não rolou
<table class="table table-striped">
  <thead>
    <tr>
      <th>Tipo de atividade</th>
      <th>Ferramenta do Moodle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <ul>
          <li>Discussão/Debate de questões específicas:</li>
          <ul>
            <li>Expressar opiniões</li>
            <li>Comentar as opiniões dos colegas</li>
            <li>Defender um ponto de vista</li>
          </ul>
        </ul>
      </td>
      <td>Fórum</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Pesquisas rápidas</li>
          <li>Estimular reflexões em que o aluno escolha uma das alternativas propostas</li>
          <li>Divisão da turma em grupos</li>
        </ul>
      </td>
      <td>Escolha</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Exercícios objetivos:</li>
          <ul>
            <li>Verdadeiro ou Falso</li>
            <li>Múltipla escolha</li>
            <li>Associação, na qual o aluno deverá relacionar os itens pre¬sentes em duas colunas de opções </li>
            <li>Resposta numérica</li>
          </ul>
          <li>Prova objetiva</li>
          <li>Testes</li>
          <li>Questões dissertativas para correção do tutor individualmente</li>
        </ul>
      </td>
      <td>Questionário</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Atividades desenvolvidas pelo aluno, criadas em arquivo – formato digital (word, excel, power point, pdf), que, posteriormente, são inseridas no Moodle para avaliação do tutor: </li>
          <ul>
            <li>Estudo de caso</li>
            <li>Redação</li>
            <li>Análise crítica para comparações e diferenças</li>
            <li>Resumos</li>
            <li>Resenhas</li>
            <li>Artigos</li>
            <li>Projetos</li>
            <li>Relatórios</li>
            <li>Mapas mentais</li>
            <li>Portfólio</li>
          </ul>
        </ul>
      </td>
      <td>Tarefa</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Construção colaborativa de textos</li>
        </ul>
      </td>
      <td>Wiki</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Discussão/Debate de questões específicas em tempo real, com hora marcada (síncrona)</li>
        </ul>
      </td>
      <td>Chat</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Esclarecer termos relacionados aos conteúdos do curso com a criação de um minidicionário (os alunos ajudam a construir e a redigir os termos)</li>
        </ul>
      </td>
      <td>Glossário</td>
    </tr>
    <tr>
      <td>
        <ul>
          <li>Resumos individuais dos alunos sobre os temas das aulas</li>
          <li>Registros sobre o percurso de aprendizagem</li>
        </ul>
      </td>
      <td>Diário</td>
    </tr>
  </tbody>
</table>
<p>No planejamento da elaboração do material didático, é importante propor pelo menos uma atividade a cada aula ou a cada conjunto de aulas, com temas que são complementares entre si. Também podem ser propostas atividades por módulos ou unidades. </p>
<p>Durante o processo de escrita propriamente dito, novas ideias de atividades podem surgir em função daquilo que você escrever, não há óbice quanto a isso. O relevante é que a ideia da avaliação esteja com você desde o início do percurso de produção de materiais para a EaD.</p>
[To-do]g) Estimativa de carga horária
<p>Determinar a carga horária de um curso a distância não é tarefa simples, pois exige a consideração de alguns fatores: quantidade de material a ser estudado pelo aluno, de leituras complementares sugeridas e de tarefas avaliativas a serem realizadas.</p>
<p>De início, não conseguiremos propor números exatos, já que não teremos o material pronto para efetuar essa análise. Assim, na fase de planejamento, a carga horária será estimada vislumbrando-se a proposta do curso de maneira ampla. </p>
<p>Isso é importante para o processo de contratação do conteudista. A Administração precisa saber quanto será, aproximadamente, o investimento com a ação, para decidir a favor ou contra o projeto. Como na instrutoria interna os valores são calculados com base na carga horária do treinamento, nesse momento, apenas poderemos indicar a estimativa.</p>
<p>Ao final da produção do conteúdo, será, então, definida a carga horária que, de fato, será considerada para o curso. Nessa fase, também serão estabelecidos os valores pecuniários a serem pagos pela elaboração do material. </p>
<p><span class="semi-bold">Atenção:</span> A carga horária total do curso, definida e validada pela SEEAD no final da elaboração do material didático, será a referência para todas as etapas seguintes do processo (revisão gramatical e transposição para o formato multimídia) e, ainda, para a tutoria, no momento da realização do treinamento. </p>
<p>A Seção de Educação a Distância observará os seguintes critérios no cálculo final da carga horária:</p>
<table class="table table-striped">
  <caption>Aulas (conteúdo principal)</caption>
  <thead>
    <tr>
      <th>Item</th>
      <th>Parâmetro</th>
      <th>Valor</th>
      <th>Considerações</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Texto</td>
      <td>Número de páginas</td>
      <td>De 6 a 10 páginas = 1h</td>
      <td>A variação considera a velocidade de cada um para estudar e absorver o conteúdo, bem como o tipo de conteúdo a ser lido, se mais denso ou mais prático, por exemplo.</td>
    </tr>
    <tr>
      <td>Vídeos</td>
      <td>Minutos</td>
      <td>Mesma duração do vídeo</td>
      <td></td>
    </tr>
    <tr>
      <td>Textos complementares no corpo da aula (acórdãos, leis, decisões)</td>
      <td>Número de páginas</td>
      <td>Até 10 páginas = 10 min.<br />Mais de 11 páginas = 15 min.</td>
      <td>Por ser de natureza complementar, consideramos que o aluno não estudará com o mesmo aprofundamento do conteúdo principal e, portanto, conseguirá ler mais rápido o material.</td>
    </tr>
    <tr>
      <td>Links complementares (sites, biografias, etc.)</td>
      <td>Quantidade</td>
      <td>Cada link = 5 min. a 10 min.</td>
      <td>Consideramos que esses tipos de link (como o “Saiba Mais”, por exemplo) são acessados de forma rápida para complementar o estudo ou despertar a curiosidade do aluno.</td>
    </tr>
  </tbody>
</table>
<table class="table table-striped">
  <caption>Atividades avaliativas</caption>
  <thead>
    <tr>
      <th>Item</th>
      <th>Parâmetro</th>
      <th>Valor</th>
      <th>Considerações</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Tipo de atividade</td>
      <td>Nível de interação e planejamento necessários </td>
      <td>Fórum = 2h <br />
          Tarefa = 1h a 2h<br />
          Questionário = 30 min.<br />
          Wiki = 30 min. a 1h
      </td>
      <td>O tempo empregado em cada atividade deve considerar, além da tarefa em si, o planejamento e o acompanhamento por parte do aluno. Portanto, exercícios mais interativos levam, de forma geral, mais tempo para serem concluídos.</td>
    </tr>
  </tbody>
</table>
<h3 class="titulo titulo-primary">CONSIDERAÇÕES FINAIS</h3>
<p><em>Você consegue perceber a importância de cada item do planejamento e como eles estão intimamente relacionados, como em uma engrenagem?</em></p>
<p>Chegamos ao final desta aula e encerramos, juntamente com ela, o Módulo II. Nesta aula, conversamos sobre a importância do planejamento nas ações de cunho educativo, especialmente para a elaboração de materiais didáticos. Conhecemos o processo de produção de conteúdos para a EaD no STF e, por fim, analisamos cada um dos principais itens do Plano de Elaboração do Material Didático.</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Ainda restam dúvidas?</h3></div>
<p>Se precisar de algum esclarecimento a respeito daquilo que dialogamos nesta aula, compartilhe no ambiente virtual de aprendizagem.</p>
<p>No próximo módulo, conversaremos sobre as especificidades da escrita de materiais didáticos na educação a distância. </p>
<p>Depois de todo esse estudo, você está preparado para colocar seus conhecimentos sobre planejamento em prática? Vamos à atividade de avaliação?</p>
<h3 class="titulo titulo-primary">VAMOS PRATICAR</h3>
<ul>
  <li>Você deverá planejar a elaboração do material didático de um curso a distância. Escolha um tema de seu domínio, relacionado com sua experiência e que seja de interesse do STF.</li>
  <li>Para escrever o seu Plano de Elaboração do Material Didático, você deverá utilizar o arquivo modelo que se encontra na plataforma Moodle. </li>
  <li>Siga as orientações que foram ressaltadas nesta aula para efetuar a sua atividade. Em caso de dúvidas, entre em contato com o tutor.</li>
  <li>O prazo para a realização da atividade encontra-se no ambiente virtual de aprendizagem.</li>
</ul>
<p>Observação: O tema escolhido no seu planejamento também será utilizado na atividade final do nosso curso: escrita de uma aula completa a respeito do conteúdo delimitado no seu Plano de Elaboração do Material Didático. Então, capriche na sua proposta.</p>
<p>Lembre-se: Caso, futuramente, você seja contratado pelo Tribunal, precisará elaborar documento semelhante. Logo, aproveite a oportunidade para praticar agora!</p>
<div style="text-align:center;"><h3 class="titulo titulo-secondary">Preparados? Mãos à obra!</h3></div>
<h3 class="titulo titulo-primary">REFERÊNCIAS</h3>
<p style="text-indent:0em">BARRETO, C. et al. Planejamento e elaboração de material didático impresso para EaD: elementos instrucionais e estratégias de ensino. CEDERJ, curso de Formação da UAB para a Região Sudeste 1.  Disponível em: <<a href="http://teca.cecierj.edu.br" target="_blank">http://teca.cecierj.edu.br</a>>. Acesso em: jun. 2015.</p>
<p style="text-indent:0em">INSTITUTO NACIONAL DO SEGURO SOCIAL (INSS). Curso de design educacional. Acesso com login e senha.</p>
<p style="text-indent:0em">MATTAR, João. Design educacional: educação a distância na prática. São Paulo: Artesanato Educacional, 2014.</p>
<p style="text-indent:0em">TRACTENBERG, R. Curso teoria e prática do design instrucional. Disponível em: <<a href="http://www.livredocencia.com.br/home/" target="_blank">http://www.livredocencia.com.br/home/</a>>. Acesso com login e senha.</p>
</div>
</div>


<?php  configNavegacaoRodape('exibir', 'fim', 'aula2pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
