<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','23', '26', 'aula3pagina22.php', 'aula3pagina24.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/clock.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height: 100px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/clock.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">G) ESTIMATIVA DE CARGA HORÁRIA</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quanto tempo será necessário para o aluno estudar </br >todo o conteúdo e realizar as atividades do curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p>A Seção de Educação a Distância observará os seguintes critérios no cálculo final da carga horária:</p>
		<table class="table-ead table-striped table-hover" cellpadding="10">
		  <caption class='table-title' style="background:#0CAF9C; color:#FFF !important">
		  <strong><span style="font-size:20px">Aulas (conteúdo principal)</span></strong>
		  </caption>
		  <thead>
		    <tr>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Item</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Parâmetros</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Valor</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Considerações</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td align="center">Texto</td>
		      <td align="center">Número de páginas</td>
		      <td align="center">De 6 a 10 páginas = 1h</td>
		      <td>A variação considera a velocidade de cada um para estudar e absorver o conteúdo, bem como o tipo de conteúdo a ser lido, se mais denso ou mais prático, por exemplo.</td>
		    </tr>
		    <tr>
		      <td align="center">Vídeos</td>
		      <td align="center">Minutos</td>
		      <td align="center">Mesma duração do vídeo</td>
		      <td align="center">_______</td>
		    </tr>
		    <tr>
		      <td align="center">Textos complementares no corpo da aula (acórdãos, leis, decisões)</td>
		      <td align="center">Número de páginas</td>
		      <td align="center">Até 10 páginas = 10 min<br />
		        Mais de 11 páginas = 15 min</td>
		      <td>Por ser de natureza complementar, consideramos que o aluno não estudará com o mesmo aprofundamento do conteúdo principal e, portanto, conseguirá ler mais rápido o material.</td>
		    </tr>
		    <tr>
		      <td align="center">Links complementares (sites, biografias, etc.)</td>
		      <td align="center">Quantidade</td>
		      <td align="center">Cada link = 5 min a 10 min</td>
		      <td>Consideramos que esses tipos de link (como o “Saiba Mais”, por exemplo) são acessados de forma rápida para complementar o estudo ou despertar a curiosidade do aluno.</td>
		    </tr>
		  </tbody>
		</table>

		<table class="table-ead table-striped table-hover" cellpadding="10">
		  <caption class='table-title' style="background:#0CAF9C; color:#FFF !important">
		  <strong><span style="font-size:20px">Atividades avaliativas</span></strong>
		  </caption>
		  <thead>
		    <tr>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Item</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Parâmetros</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Valor</th>
		      <th style="text-align: center; width: 25%; background: #C8DAD7 !important; color: #000 !important; font-size:20px" class='table-title'>Considerações</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td align="center">Tipo de atividade</td>
		      <td align="center">Nível de interação e planejamento necessários </td>
		      <td align="left">Fórum = 2h <br />
		          Tarefa = 1h a 2h<br />
		          Questionário = 30 min<br />
		          Wiki = 30 min a 1h
		      </td>
		      <td>O tempo empregado em cada atividade deve considerar, além da tarefa em si, o planejamento e o acompanhamento por parte do aluno. Portanto, exercícios mais interativos levam, de forma geral, mais tempo para serem concluídos.</td>
		    </tr>
		  </tbody>
		</table>
	</div>


</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina22.php', 'aula3pagina24.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
