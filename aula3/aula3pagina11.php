<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','11', '26', 'aula3pagina10.php', 'aula3pagina12.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p><span class="semi-bold">Objetivo geral</span></p>
		<p>Diz respeito ao que esperamos com a realização do treinamento de maneira ampla. Veja o exemplo ao lado.</p>

		<p>Perceba os detalhes abordados na escrita desse objetivo geral: </p>
		<ul>
		  <li style="padding-bottom:10px"><span class="semi-bold">Público a que se destina o evento:</span> “servidores”.</li>
		  <li style="padding-bottom:10px"><span class="semi-bold">O que se espera que o aluno seja capaz ao final do treinamento?</span> “escrever material didático específico para a educação a distância de acordo com a área temática delimitada”.</li>
		  <li style="padding-bottom:10px"><span class="semi-bold">Em quais condições?</span> “empregar linguagem dialógica e recursos da virtualidade, por meio de um planejamento consistente”.</li>
		  <li style="padding-bottom:10px"><span class="semi-bold">Enfoque do curso:</span> pela descrição, notamos que é um curso voltado para a prática, não se fixa apenas nos aspectos teóricos: “com vistas à construção de conteúdos significativos, focados na aprendizagem dos alunos”.</li>
		</ul>
		<p>Com esse exemplo, salientamos que o objetivo geral deve ser completo, além de  trazer, em seu bojo, elementos suficientes para retratar a proposta do seu curso. Certo? </p>
	</div>
  	<div class="col-lg-12" >
		<div class="row well cor-ativo">
		  <p style="text-align:center; ">Como exemplo, vamos citar o objetivo geral deste curso. <br /><a href="javascript:void(0);" class="btn btn-info btn-small botao-pausa-1" style="text-align:center; text-indent:0em; margin-top:20px; ">Clique aqui para ver o objetivo geral</a> </p>
		  <div id="conteudo-pausa-1" style="display:none">
		    <p>Ao final do treinamento, os servidores deverão ser capazes de escrever material didático específico para a educação a distância, de acordo com a área temática delimitada, empregando linguagem dialógica e recursos da virtualidade, por meio de um planejamento consistente, com vistas à construção de conteúdos significativos, focados na aprendizagem dos alunos.</p>
		  </div>
		</div>
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina10.php', 'aula3pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
