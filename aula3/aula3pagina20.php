<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','20', '26', 'aula3pagina19.php', 'aula3pagina21.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/check.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 22px;    max-height: 98px;   margin-top: 9px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/check.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">F) ATIVIDADES AVALIATIVAS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Como posso ajudar o aluno a fixar os <br /> conhecimentos abordados no curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-6">
		<p>Você pode estar se questionando:</p>
        <div style="text-align:center;"><h3 class="titulo titulo-secondary">E, agora, por onde começar? Quais atividades são mais interessantes ou mais apropriadas para o ambiente online?</h3></div>
		<div class="bloco-pontilhado" style="margin-top:40px" class="">
		  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
		  <p style="text-align:center; font-weight:bold;"></p>
		  <div id="conteudo-cerebro" >
		    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		      <p style="text-align:center; text-indent:0em;">Para elaborar atividades coerentes, recorra aos objetivos específicos que você estabeleceu para o seu curso.</a> </p>                       
		    </div>
		  </div>
  </div>

	</div>

	<div class="col-lg-6">
		<p>Aqui, vamos retomar o objetivo que mencionamos anteriormente como exemplo, no item que trata da escrita dos objetivos, veja: </p>
		<p><em>Formular argumentos claros para expressar sua posição, a favor ou contra, sobre determinado assunto.</em></p>
		<p>Pela essência daquilo que se espera dos estudantes, qual atividade parece mais apropriada?</p>
		<ol>
		  <li>Exercício objetivo.</li>
		  <li>Redação discursiva.</li>
		</ol>
		<p>Na nossa perspectiva, a letra “b” se aproxima mais do objetivo proposto. Você concorda?</p>
		<p><span class="semi-bold">Para elaborar suas atividades avaliativas, pense no seguinte:</span> Qual a melhor maneira de os alunos fixarem os conteúdos estabelecidos, observando os objetivos específicos que foram delineados?</p>
		
	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina19.php', 'aula3pagina21.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
