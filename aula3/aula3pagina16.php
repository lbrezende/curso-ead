<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','16', '26', 'aula3pagina15.php', 'aula3pagina17.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/conclusao.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 63px; max-height:80px; margin-top:22px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/conclusao.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">E) CONTEÚDO PROGRAMÁTICO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quais assuntos são fundamentais <br /> para se atingir os objetivos traçados?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p>Esta parte do planejamento pode gerar alguns conflitos internos, pois, muitas vezes, não é possível abarcar todos os assuntos relacionados à temática no mesmo curso. </p>
		<p>Aí você se depara com um dilema: </p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:10px">Quais tópicos de conteúdo privilegiar?</h3></div>
		<p>Bom, as principais questões que devem permear seus pensamentos, nesse momento, são: </p>
		<p class="semi-bold">Por que este treinamento precisa ser realizado (justificativa)? </p>
		<p class="semi-bold">Quais objetivos foram delineados para esta ação educativa? </p>
		
	</div>
  	<div class="col-lg-12" style="margin-top:20px" >
		<div class="row well cor-ativo" style="text-align:justify"><span class="semi-bold">Note</span>: Se você já escreveu uma justificativa bem fundamentada e definiu os objetivos do curso com cuidado, delimitar os temas a serem abordados no curso será um pouco mais tranquilo.</div>
  	</div>

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina15.php', 'aula3pagina17.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
