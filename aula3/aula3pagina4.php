<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','4', '26', 'aula3pagina3.php', 'aula3pagina5.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Plano de elaboração do material didático</h3>
			<p>Vimos no item anterior que a entrega do <a href="docs/Plano_de_elaboracao_de_material_didatico.doc" title="acesse o plano de elaboração de material didático" target="_blank">Plano de Elaboração do Material Didático</a> é a primeira etapa do processo de produção de conteúdos no Tribunal.</p>
			<p>Por isso, agora, detalharemos os principais tópicos relacionados a esse planejamento, a fim de que você possua as informações necessárias para atuar nessa fase. </p>
			<p>Antes de seguirmos com os estudos, consideramos pertinente antecipar a descrição da Atividade Avaliativa deste módulo, tudo bem? Não se assuste! Apenas desejamos colocar a sua mente em alerta, vejamos:</p>
			<div class="bloco-pontilhado" style="margin-top:40px" class="">
			  <div style="text-align:center"><h3 class="titulo titulo-secondary">Atividade Avaliativa – Módulo II</h3></div>
			  <p>Ao final deste módulo, você deverá apresentar um Plano de Elaboração do Material Didático de um curso a distância, versando sobre tema de seu domínio e de interesse do STF. As orientações detalhadas sobre a atividade encontram-se no fim desta aula. Então, ao ler as explicações, aproveite para pensar em sua tarefa, ok?</p>
			</div>
	</div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina3.php', 'aula3pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
