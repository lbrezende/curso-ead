<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','6', '26', 'aula3pagina5.php', 'aula3pagina7.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/cracha.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 50px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/cracha.png"  title="Imagem ilustrativa de crachá." style="text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">A) NOME DO CURSO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Qual nome devo escolher para o curso? </strong></p>
					<p style="font-size: 20px"><strong>Qual a identidade dele?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>


<div class="row">
  	<div class="col-lg-6">
		<p>Vamos analisar, agora, alguns nomes de treinamento. Veja:</p>
		<div class="balao-esquerdo">“Redação Oficial”<a href="javascript:void(0)" style="float:right;" class="botao-questao-1 btn btn-info btn-small">Clique para ver a análise</a></div>
		    <div class="balao-direito conteudo-questao-1" style="display:none">
		      <p class="" style=" margin-bottom:0px; padding-bottom:0px; font-weight:100;">Esse é um tema específico e passível de ser tratado em apenas um curso a distância. Não há muito a acrescentar ao nome do curso. Ao nosso ver, ele atinge perfeitamente o público interessado.</p>
		    </div>

		    <div class="balao-esquerdo">“Direito Constitucional” <a href="javascript:void(0)" style="float:right;" class="botao-questao-2 btn btn-info btn-small">Clique para ver a análise</a></div>
		    <div class="balao-direito conteudo-questao-2" style="display:none">
		    <p class="" style=" margin-bottom:0px; padding-bottom:0px; font-weight:100;">A identificação do curso é muito ampla. Sabemos que existem diversos assuntos que podem ser abordados dentro do campo do direito constitucional; contudo, dificilmente todos serão desenvolvidos em apenas um evento online. Por isso, delimitar o nível de aprofundamento e/ou os temas principais que serão estudados facilitará a compreensão dos servidores sobre a proposta do curso.</p>
		  </div>

		<p>Para exemplificar, apresentamos o nome de uma ação de treinamento a distância, da área do direito constitucional, trabalhada aqui do STF: </p>
        <p><strong>“Introdução ao Direito Constitucional e ao Controle de Constitucionalidade”.</strong></p>

		<p>Perceba como fica evidente o foco do evento e o nível básico de aprofundamento. </p>

	</div>
  	<div class="col-lg-6">
    <div style="text-align:center"><h3 class="titulo titulo-secondary">Já começaram a surgir ideias interessantes para o nome do seu curso?</h3></div>
	  <div class="bloco-pontilhado" style="margin-top:40px" class="">
		  <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
		  <p style="text-align:center; font-weight:bold;"> </p>
		  <div id="conteudo-cerebro" >
		    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		      <p style="text-align:center; text-indent:0em;">Registre, sempre que possível, as ideias que aparecerem. Não confie apenas na sua memória, pois, em algumas situações, ela é volátil.  Anote o que conseguir. Dessa maneira, você reunirá mais elementos para tomar a melhor decisão a esse respeito.</p>                       
		    </div>
		  </div>
		</div>
	</div>	
</div>





<?php  configNavegacaoRodape('exibir', 'aula3pagina5.php', 'aula3pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
