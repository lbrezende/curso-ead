<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','12', '26', 'aula3pagina11.php', 'aula3pagina13.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-6">
		<p>Um método utilizado na definição de objetivos é o denominado ABCD, que possui quatro características, segundo o professor Regis Tractenberg em seu curso “Teoria e Prática do Design Instrucional”<span id="biblio-3" class="fonte-bibliografica-numero " name="biblio-abrir">3</span>:</p>
		<div class="fonte-bibliografica biblio-3-conteudo alert alert-warning" style="display:none;  "> 
		  <p><a href="http://www.livredocencia.com.br/home/" title="" target="_blank">http://www.livredocencia.com.br/home/</a>. Acesso com login e senha. </p>
		</div>
		<ol sytle="list-style-type:decimal;">
		  <li style="list-style-type:decimal;">Audiência (Audience – aprendiz): Deixa claro quem são os aprendizes.</li>
		  <li style="list-style-type:decimal;">Comportamento (Behavior): Indica um comportamento observável que, se realizado pelos aprendizes, fornece boa evidência de sua aprendizagem. </li>
		  <li style="list-style-type:decimal;">Condições (Conditions): Descreve as condições sob as quais os aprendizes devem desempenhar o comportamento observável.</li>
		  <li style="list-style-type:decimal;">Grau (Degree – critério): Especifica os critérios de avaliação do comportamento.</li>
		</ol>
		
	</div>
  	<div class="col-lg-6" >
		<p>O autor nos auxilia a compreender o modelo da seguinte maneira:</p>
		<p>“Eis um exemplo impreciso de objetivo de aprendizagem: <br />
		  Os alunos devem saber consertar um carro.<br />
		  Vejamos a mesma tarefa traduzida em um objetivo instrucional conforme o modelo ABCD:</p>
		<p><span style="color:#000080;"><strong>Os novos mecânicos contratados</strong>,</span><span style="color:#808000;"> <strong>dado um carro de fórmula 1 da Ferrari,</strong></span><span style="color:#800000;"> <strong>consertarão qualquer tipo de problema no pneu.</strong></span><span style="color:#808000;"> <strong>Eles contarão com todas as ferramentas necessárias mas não poderão consultar manuais técnicos nem deliberar sobre como realizar os consertos. </strong></span><span style="color:#548DD4;text-decoration:underline;"> Devem ser capazes de efetivar os reparos dentro de 20 segundos em 90% das tentativas.</span><br />
		  Legenda: <span style="color:#000080;"><strong>Aprendiz</strong></span> – <span style="color:#808000;"><strong>comportamento</strong></span> – <span style="color:#800000;"><strong>condições</strong></span> – <span style="color:#548DD4;text-decoration:underline;">critério”</span></p>
		
  	</div>
  	<div class="clear"></div>
  	<div class="col-lg-12" >
			<div class="bloco-pontilhado" style="margin-top:40px" class="">
			  <div style="text-align:center;">
				  <h3 class="titulo titulo-secondary">
				  	Então, ficou um pouco mais claro como o objetivo geral poderá ser escrito?
				  </h3>
			  </div>
			  <p>É importante salientar que não há rigidez quanto à redação do objetivo geral. Contudo, devemos contemplar na escrita os aspectos que deixem evidente a proposta do curso.</p>
			</div>  	
		
  	</div> 	

</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina11.php', 'aula3pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
