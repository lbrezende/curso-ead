<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','24', '26', 'aula3pagina23.php', 'aula3pagina25.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Considerações finais</h3>
	  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p><em>Você consegue perceber a importância de cada item do planejamento e como eles estão intimamente relacionados, como em uma engrenagem?</em></p>
		<p>Chegamos ao final desta aula e encerramos, juntamente com ela, o Módulo II. Nesta aula, conversamos sobre a importância do planejamento nas ações de cunho educativo, especialmente para a elaboração de materiais didáticos. Conhecemos o processo de produção de conteúdos para a EaD no STF e, por fim, analisamos cada um dos principais itens do Plano de Elaboração do Material Didático.</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Ainda restam dúvidas?</h3></div>
		<p>Se precisar de algum esclarecimento a respeito daquilo que dialogamos nesta aula, compartilhe no ambiente virtual de aprendizagem.</p>
		<p>No próximo módulo, conversaremos sobre as especificidades da escrita de materiais didáticos na educação a distância. </p>
		<p>Depois de todo esse estudo, você está preparado para colocar seus conhecimentos sobre planejamento em prática? Vamos à atividade de avaliação?</p>
	</div>


</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina23.php', 'aula3pagina25.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
