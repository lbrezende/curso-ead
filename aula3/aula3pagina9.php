<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','9', '26', 'aula3pagina8.php', 'aula3pagina10.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/eadpc.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 50px; max-height:80px; margin-top:18px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/eadpc.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">C) CARACTERIZAÇÃO DO PÚBLICO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quem são as pessoas que precisam <br /> participar do treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">
  	<div class="col-lg-6">
 		<p>Dessa forma, procure retratar com o máximo de detalhes as características do público, visto que isso ajudará a direcionar sua escrita nos seguintes termos: aprofundamento da abordagem, adequação da linguagem, enfoque que deverá ser dado ao curso e escolha dos recursos a serem empregados. </p>
		<p>Precisamos ressaltar que, no STF, há servidores com deficiências. Por isso, ao planejar a escrita do seu material, tenha em mente a diversidade dos leitores e a questão da <a href="http://goo.gl/9LXjxG" target="_blank">acessibilidade</a>.</p>
  	</div> 
  	<div class="col-lg-6">
 		<div class="bloco-pontilhado" style="margin-top:40px" class="">
		  <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
		  <p style="text-align:center; font-weight:bold;">Você conhece o projeto “STF Sem barreiras”?   </p>
		  <div id="conteudo-cerebro" >
		    <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
		      <p style="text-align:center; text-indent:0em;">Para obter mais informações a respeito, acesse a página do programa na intranet do Supremo: </p>
		      <p style="text-align:center; text-indent:0em;"><br />
		        <a href="http://intranetstf/intranetsis/?page_id=251"  class="btn btn-info btn-small" target="_blank" title="visite a página do stf sem barreiras">STF Sem Barreiras</a></p>
		    </div>
		  </div>
		</div>  			
  	</div>   
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina8.php', 'aula3pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
