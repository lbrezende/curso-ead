<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','22', '26', 'aula3pagina21.php', 'aula3pagina23.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/clock.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height: 100px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/clock.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">G) ESTIMATIVA DE CARGA HORÁRIA</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quanto tempo será necessário para o aluno estudar </br >todo o conteúdo e realizar as atividades do curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-12">
		<p>Determinar a carga horária de um curso a distância não é tarefa simples, pois exige a consideração de alguns fatores: quantidade de material a ser estudado pelo aluno, de leituras complementares sugeridas e de tarefas avaliativas a serem realizadas.</p>
		<p>De início, não conseguiremos propor números exatos, já que não teremos o material pronto para efetuar essa análise. Assim, na fase de planejamento, a carga horária será estimada vislumbrando-se a proposta do curso de maneira ampla. </p>
		<p>Isso é importante para o processo de contratação do conteudista. A Administração precisa saber quanto será, aproximadamente, o investimento com a ação, para decidir a favor ou contra o projeto. Como na instrutoria interna os valores são calculados com base na carga horária do treinamento, nesse momento, apenas poderemos indicar a estimativa.</p>
		<p>Ao final da produção do conteúdo, será, então, definida a carga horária que, de fato, será considerada para o curso. Nessa fase, também serão estabelecidos os valores pecuniários a serem pagos pela elaboração do material. </p>
		<p><span class="semi-bold">Atenção:</span> A carga horária total do curso, definida e validada pela SEEAD no final da elaboração do material didático, será a referência para todas as etapas seguintes do processo (revisão gramatical e transposição para o formato multimídia) e, ainda, para a tutoria, no momento da realização do treinamento. </p>
	</div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina21.php', 'aula3pagina23.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
