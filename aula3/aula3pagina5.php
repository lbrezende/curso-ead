<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','5', '26', 'aula3pagina4.php', 'aula3pagina6.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/cracha.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 50px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/cracha.png"  title="Imagem ilustrativa de crachá." style="text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">A) NOME DO CURSO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Qual nome devo escolher para o curso? </strong></p>
					<p style="font-size: 20px"><strong>Qual a identidade dele?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">
  	<div class="col-lg-6">
  		<p>A título de inspiração inicial, indicamos que assista ao vídeo a seguir, que aborda a temática da criação de nomes para empresas.</p>	
		<p>Apesar das ressalvas sobre o tipo de trabalho que iremos desenvolver, isto é, materiais didáticos para cursos a distância, o filme apresenta alguns princípios que podem nos auxiliar na escolha do nome do nosso curso.</p>
		<p>De acordo com o site “Consultório Etimológico”<span id="biblio-2" class="fonte-bibliografica-numero " name="biblio-abrir">2</span>, a palavra <em>nome</em> vem do Latim NOMEN e do Grego ONYMA ou ONOMA e significa: “palavra usada para identificar uma pessoa”.</p>
		<div class="fonte-bibliografica biblio-2-conteudo alert alert-warning" style="display:none;  "> 
		  Disponível em: <a href="http://origemdapalavra.com.br/site/pergunta/origem-da-palavra-nome/" title="acesse explicação da origem da palavra nome" target="_blank">http://origemdapalavra.com.br/site/pergunta/origem-da-palavra-nome/</a>. Acesso em abril de 2015.   
		</div>
		<p>No nosso caso, é de extrema relevância identificar com clareza a temática central do curso que pretendemos escrever e expressar de forma concisa as intenções do treinamento.</p>
		<p>Por isso, precisamos pensar em nomes que chamem a atenção do leitor, de tal forma que o motive a participar do evento. </p>
		<p>Ademais, quando possível, devemos deixar claro o nível de aprofundamento do treinamento, pois isso ajudará o aluno a avaliar se possui os conhecimentos necessários para participar da ação educativa com aproveitamento. </p>
		<p>Para definir a identificação do curso, podemos utilizar a criatividade, mas sempre atentos ao fato de que integramos uma instituição pública; assim, devemos ter cuidado com os exageros.</p>


	</div>
  	<div class="col-lg-6">
		<div class="bloco-pontilhado" style="">
		  <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
		  <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
		    <iframe width="420" height="315" src="https://www.youtube.com/embed/RwplGixBcsc" frameborder="0" allowfullscreen></iframe>                    
		  </div>
		</div>
	</div>	
</div>






<?php  configNavegacaoRodape('exibir', 'aula3pagina4.php', 'aula3pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
