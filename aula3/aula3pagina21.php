<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','21', '26', 'aula3pagina20.php', 'aula3pagina22.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/check.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 22px;    max-height: 98px;   margin-top: 9px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/check.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">F) ATIVIDADES AVALIATIVAS</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Como posso ajudar o aluno a fixar os <br /> conhecimentos abordados no curso?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>



<div class="row">

  	<div class="col-lg-12">
		<p>Vale ressaltar que não existem atividades certas ou erradas, e sim mais ou menos apropriadas em função das metas estabelecidas e do conteúdo estudado. </p>
		<p>Há várias opções. Para ajudá-lo nesse aspecto, listamos algumas atividades que podem ser utilizadas em cursos a distância e as ferramentas do Moodle a serem empregadas para viabilizar as tarefas no ambiente virtual de aprendizagem.</p>
		<p><strong>Note:</strong> Essas são apenas sugestões, você poderá criar novas propostas de acordo com o seu caso. </p>

		<table class="table-ead table-striped table-hover" cellpadding="10">
		  <thead>
		    <tr>
		      <th style="text-align: center; width: 45%; background: #FDCC82 !important; color: #000 !important; font-size:20px" class='table-title'>Tipo de atividade</th>
		      <th style="text-align: center; width: 15%; background: #FDCC82 !important; color: #000 !important; font-size:20px" class='table-title'>Ferramenta do Moodle</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Discussão/Debate de questões específicas:</li>
		          <ul>
		            <li>Expressar opiniões</li>
		            <li>Comentar as opiniões dos colegas</li>
		            <li>Defender um ponto de vista</li>
		          </ul>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconforum.png" title="Ícone de Fórum"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Pesquisas rápidas</li>
		          <li>Estimular reflexões em que o aluno escolha uma das alternativas propostas</li>
		          <li>Divisão da turma em grupos</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconescolha.png" title="Ícone de Escolha"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Exercícios objetivos:</li>
		          <ul>
		            <li>Verdadeiro ou Falso</li>
		            <li>Múltipla escolha</li>
		            <li>Associação, na qual o aluno deverá relacionar os itens presentes em duas colunas de opções </li>
		            <li>Resposta numérica</li>
		          </ul>
		          <li>Prova objetiva</li>
		          <li>Testes</li>
		          <li>Questões dissertativas para correção do tutor individualmente</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconquestionario.png" title="Ícone de Questionário"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Atividades desenvolvidas pelo aluno, criadas em arquivo – formato digital (word, excel, power point, pdf), que, posteriormente, são inseridas no Moodle para avaliação do tutor: </li>
		          <ul>
		            <li>Estudo de caso</li>
		            <li>Redação</li>
		            <li>Análise crítica para comparações e diferenças</li>
		            <li>Resumos</li>
		            <li>Resenhas</li>
		            <li>Artigos</li>
		            <li>Projetos</li>
		            <li>Relatórios</li>
		            <li>Mapas mentais</li>
		            <li>Portfólio</li>
		          </ul>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/icontarefa.png" title="Ícone de Tarefa"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Construção colaborativa de textos</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconwiki.png" title="Ícone de Wiki"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Discussão/Debate de questões específicas em tempo real, com hora marcada (síncrona)</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconchat.png" title="Ícone de Chat"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Esclarecer termos relacionados aos conteúdos do curso com a criação de um minidicionário (os alunos ajudam a construir e a redigir os termos)</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/iconglossario.png" title="Ícone de Glossário"></td>
		    </tr>
		    <tr>
		      <td class='table-title' style="background:#EFDAC1; text-align:left; color:#000 !important; text-shadow:none">
		        <ul>
		          <li>Resumos individuais dos alunos sobre os temas das aulas</li>
		          <li>Registros sobre o percurso de aprendizagem</li>
		        </ul>
		      </td>
		      <td class='table-title' style="background:#F7D29A; text-align:left; color:#000 !important; text-shadow:none; text-align:center"><img src="imagens/icondiario.png" title="Ícone de Diário"></td>
		    </tr>
		  </tbody>
	  </table>
		<p>No planejamento da elaboração do material didático, é importante propor pelo menos uma atividade a cada aula ou a cada conjunto de aulas, com temas que são complementares entre si. Também podem ser propostas atividades por módulos ou unidades. </p>
		<p>Durante o processo de escrita propriamente dito, novas ideias de atividades podem surgir em função daquilo que você escrever, não há óbice quanto a isso. O relevante é que a ideia da avaliação esteja com você desde o início do percurso de produção de materiais para a EaD.</p>
	</div>


</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina20.php', 'aula3pagina22.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
