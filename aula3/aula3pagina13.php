<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','13', '26', 'aula3pagina12.php', 'aula3pagina14.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/alvo.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 20px; max-height:80px; margin-top:6px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/alvo.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">D) OBJETIVOS</div>
	  			<div class="conteudo">
					<p  style="font-size: 20px"><strong>Aonde pretendemos chegar com este treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">

  	<div class="col-lg-6">
		<p><span class="semi-bold">Objetivos específicos</span></p>
		<p>Os objetivos específicos são um detalhamento do objetivo geral. Eles se referem ao que o aluno será capaz de realizar após o estudo de cada aula, isto é, a aprendizagem que ele deve apresentar ao final de cada tópico de conteúdo. Por isso, os objetivos específicos ajudam a definir o número de módulos e/ou aulas a serem escritas. </p>
		<p><span class="semi-bold">Atenção:</span> Os <span class="semi-bold">objetivos específicos</span> não são ações que descrevem atividades do conteudista ou do professor. Eles dizem respeito a <span class="semi-bold">aprendizagens esperadas dos alunos</span>.</p>
		<p>&nbsp;</p>
		


	</div>
  	<div class="col-lg-6" >
<div style="text-align:center;"><h3 class="titulo titulo-secondary" style="padding-top:15px">Você pode estar se perguntando: como escrever esses objetivos específicos?</h3></div>
		<ol sytle="list-style-type:decimal;">
		  <li style="padding-top:10px; list-style-type:decimal;">Inicie o seu objetivo com verbos no modo infinitivo. </li>
		  <li style="padding-top:10px; list-style-type:decimal;">Se possível, inclua condições e critérios: o que o aprendiz deve ser capaz de fazer? Em que condições deve fazê-lo? </li>
		  <li style="padding-top:10px; list-style-type:decimal;">Registre aquilo que o aluno será capaz de realizar após o estudo.</li>
		</ol>  	



  	</div>

</div>

<div class="row">
	
	<div class="col-lg-12">
		
		<div class="row well cor-ativo">
		  <p style="text-align:center; ">Veja alguns exemplos do nosso curso: <br /><a href="javascript:void(0);" class="btn btn-info btn-small botao-pausa-1" style="text-align:center; text-indent:0em; margin-top:20px; ">Clique aqui para ver os exemplos</a> </p>
		  <div id="conteudo-pausa-1" style="display:none">
		    <ul>
					  <li style="padding-top:10px">Identificar os principais fatos ligados à evolução histórica da EaD no Brasil e no mundo. </li>
					  <li style="padding-top:10px">Planejar o processo de elaboração de materiais para a EaD com base nos seus elementos essenciais.</li>
			</ul>
		  </div>
		</div>
		
	</div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula3pagina12.php', 'aula3pagina14.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
