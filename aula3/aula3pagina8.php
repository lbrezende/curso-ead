<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Por que planejar a escrita?', 'exibir', '3','8', '26', 'aula3pagina7.php', 'aula3pagina9.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  	<div class="col-lg-12">
	  	<h3 class="titulo titulo-primary">Principais itens do planejamento</h3>
	  	<img src="imagens/eadpc.png" class="hidden-xs"  title="Imagem ilustrativa de crachá." style="left: 50px; max-height:80px; margin-top:18px; position:absolute;" />
	  	<div style="text-align:center;" class="visible-xs"><img src="imagens/eadpc.png"  title="Imagem ilustrativa de crachá." style="margin-bottom:10px; text-align:center;" /></div>	  	
	  	<div class="tituloEspecialEad">
	  		<div class="conteudoTituloEspecialEad">
	  			<div class="titulo">C) CARACTERIZAÇÃO DO PÚBLICO</div>
	  			<div class="conteudo">
					<p style="font-size: 20px"><strong>Quem são as pessoas que precisam <br /> participar do treinamento?</strong></p>
	  			</div>
	  		</div>
	  	</div>  	
	</div>
</div>

<div class="row">
  	<div class="col-lg-6">
		<p>Imagine escrever um e-mail com vários destinatários definidos aleatoriamente. Agora, pense em redigir uma mensagem para pessoas que você conhece.</p>
		<div style="text-align:center;"><h3 class="titulo titulo-secondary">Existe alguma diferença em relação à forma como você organizará a sua escrita? <br /><br />Ou utilizará, em ambos os casos, as mesmas palavras?
		</h3></div>
		<p>Na primeira opção, você se empenhará para escrever um texto claro e dessa maneira alcançará seus objetivos, não temos dúvidas.</p>
		<p>Na segunda, pelo fato de conhecer a quem se destina a mensagem, provavelmente o seu texto será mais personalizado, pois você buscará expressões que fazem parte do contexto em que estão inseridos os leitores. Com isso, poderá aplicar, inclusive, uma escrita mais afetiva e menos carregada de formalismos e, ainda assim, logrará êxito nos seus propósitos.</p>
		<p>Ocorre algo semelhante na escrita de materiais didáticos: conhecer a quem se destina a produção textual poderá contribuir para a melhor escolha de palavras e expressões, bem como para o estabelecimento de um diálogo mais próximo e afetivo. Consequentemente, o seu texto poderá ser compreendido com mais facilidade pelos alunos.</p>
  	</div>
  	<div class="col-lg-6">
		
		<div class="bloco-pontilhado" style="margin-top:0px; padding:20px" class="">
		  <div style="text-align:center;"><h3 class="titulo titulo-secondary">Você deseja desenvolver uma escrita alinhada com o perfil do público a que se destina o treinamento? </h3></div>
		<p>O professor João Mattar (p. 57, 2014) nos ensina que: “Conhecer melhor nossos alunos permite adequar e modificar nossa estratégia de ensino em benefício da aprendizagem”.</p>  
		  <p style="text-align:center; font-weight:bold; margin-top:30px">Antes de escrever, é necessário saber:  <br>
		  <a href="javascript:void(0);" class="btn btn-info btn-small botao-cerebro" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para ver as dicas</a> </p>
		  <div id="conteudo-cerebro" style="display:none">
		    <div class="row" style=" margin-bottom:30px; padding:0px 30px">
		      <p style="text-indent:0em;">
		      <ul>
			      <li style="padding-top:10px">Qual o perfil dos estudantes? </li>
			      <li style="padding-top:10px">Teremos alunos que precisam de alguma atenção especial (pessoas com deficiência)? O que há de marcante neles que requer atenção especial?</li>
			      <li style="padding-top:10px">Quais os conhecimentos anteriores sobre o tema? Será necessário algum nivelamento de conhecimento?                     </li>
		      </ul>
		    </div>
		  </div>
		</div> 
  	</div>  	
</div>




<?php  configNavegacaoRodape('exibir', 'aula3pagina7.php', 'aula3pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 
