<?php 
//Configurar Aula
$aula = 4; 
$pagina = 12; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 
<style>
      .master-indent p {text-indent:  inherit}

</style>
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Correção gramatical</h3>

    <p>Para desenvolvermos um bom diálogo com nosso aluno, é imprescindível escrever com correção gramatical. Isso é claro, você pode pensar!</p>
    <p>O nosso propósito, aqui, não é discorrer sobre as regras da Língua Portuguesa. O que pretendemos enfatizar são algumas dicas que, se utilizadas no seu material didático, poderão facilitar o entendimento do texto pelos alunos.</p>
    <p>Dessa forma, sempre que possível, segundo nos orientam Abreu-Fialho e Meyohas (2007), procure:</p>
    <div class="master-indent">
      <div class="row">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor1 visible-xs"> 1 <img src="imagens/seta1.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor1 hidden-xs"> 1 <img src="imagens/seta1.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-1 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-1" style="display:none">
          <p class="titulo-seta"><strong>Redigir frases simples, orações curtas e períodos rápidos de ler.</strong></p>
          <p>(Se forem necessários períodos mais longos, utilize o bom senso e tente empregar a pontuação que favoreça a fluidez da leitura).</p>
          </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor2 visible-xs"> 2 <img src="imagens/seta2.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor2 hidden-xs"> 2 <img src="imagens/seta2.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-2 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-2" style="display:none">
          <p class="titulo-seta"><strong>Evitar frases com sentido negativo ou com dupla negação, pois elas dificultam a compreensão do leitor. </strong></p>
          <p><strong> Exemplo:</strong> </p>
          <p><em>Não é possível duvidar que este material didático não seja avaliado positivamente. </em></p>
          <p><strong> Sugestão de outra redação:</strong></p>
          <p><em>Este material didático será avaliado positivamente.</em></p>          
          </div>
        </div>
      </div>

      <div class="clear"> </div>  

      <div class="row">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor3 visible-xs"> 3 <img src="imagens/seta3.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor3 hidden-xs"> 3 <img src="imagens/seta3.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-3 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-3" style="display:none">
  <p><strong>Utilizar as conjunções conforme os sentidos que elas expressam.</strong></p>
   <p><strong> Exemplo:</strong> </p>
          <p><em>“embora” e “no entanto” para ideia de concessão e adversidade, respectivamente. </em></p>
  <p><a href="http://www.soportugues.com.br/secoes/morf/morf85.php" target="_blank">Mais sobre conjunções</a></p>
  </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor4 visible-xs"> 4 <img src="imagens/seta1.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor4 hidden-xs"> 4 <img src="imagens/seta1.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-4 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-4" style="display:none">
  <p><strong>Evitar o excesso de conjunções, adjetivos e advérbios.</strong></p>
  </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor5 visible-xs"> 5 <img src="imagens/seta2.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor5 hidden-xs"> 5 <img src="imagens/seta2.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-5 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-5" style="display:none">
            <p><strong>Utilizar a voz ativa dos verbos, quando possível.</strong></p>
            <p><strong> Voz ativa:</strong> quando o sujeito é agente, isto é, pratica a ação expressa pelo verbo.</p>
            <p><strong> Exemplo:</strong></p>
            <div class="row"> 
                <div class="col-lg-4"> 
                Ele
                </div>
                <div class="col-lg-4"> 
                fez
                </div>
                <div class="col-lg-4"> 
                o trabalho.
                </div>                                
            </div>
            <div class="row" style="font-style: italic;"> 
                <div class="col-lg-4"> 
                sujeito agente
                </div>
                <div class="col-lg-4"> 
                ação
                </div>
                <div class="col-lg-4"> 
                objeto (paciente)
                </div>                                
            </div>            
            <p style="padding-top:10px"><strong> Fonte: </strong><a href="http://www.soportugues.com.br/secoes/morf/morf72.php" target="_blank"> Só português</a></p>
            </div>
          </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row master-indent">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor6 visible-xs"> 6 <img src="imagens/seta3.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor6 hidden-xs"> 6 <img src="imagens/seta3.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-6 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-6" style="display:none">
  <p><strong>Ao empregar termos técnicos ou jargões, explicar com detalhes os significados.</strong></p>
  </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row master-indent">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor7 visible-xs"> 7 <img src="imagens/seta1.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor7 hidden-xs"> 7 <img src="imagens/seta1.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-7 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-7" style="display:none">
  <p><strong>Observar se a ordem das palavras mantém o significado que você deseja empregar na frase.</strong></p>
  <p><strong> Exemplo:</strong> </p>
  <p><strong> Frase 1</strong>: Apenas o conteudista pode escrever o texto.</p>
  <p><strong> Frase 2</strong>: O conteudista apenas pode escrever o texto.</p>
  <p>Na <strong>  primeira frase</strong>, o conteudista é o único que pode escrever o texto.</p>
  <p>Na <strong>  segunda</strong>, a única coisa que o conteudista pode fazer é escrever o texto.</p>
  <p><i class="italico">  Percebeu a diferença?</i></p>
  </div>
        </div>
      </div>

      <div class="clear"> </div>

      <div class="row master-indent">
        <div class="bloco-cinza-seta naoidenta  numeracao-cor8 visible-xs"> 8 <img src="imagens/seta2.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor8 hidden-xs"> 8 <img src="imagens/seta2.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-8 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-8" style="display:none">
  <p><strong>Evitar exageros na pontuação, em especial na aplicação das vírgulas de uso opcional.</strong></p>
  </div>
        </div>
      </div>

      <div class="clear"> </div>                        

      <div class="row master-indent">
       <div class="bloco-cinza-seta naoidenta numeracao-cor9 visible-xs"> 9 <img src="imagens/seta3.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">
        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor9 hidden-xs"> 9 <img src="imagens/seta3.jpg"> </div>
          <div><a href="javascript:void(0)" class="botao-questao-9 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-9" style="display:none">
  <p><strong>Utilizar dicionários sempre que desejar confirmar o significado de alguma palavra. Além disso, eles podem ser utilizados para a busca de sinônimos, o que ajuda a evitar repetição de palavras.</strong></p>
  </div>

        </div>
      </div>

      <div class="clear"> </div>      


      <div class="row master-indent">
      <div class="bloco-cinza-seta naoidenta numeracao-cor10 visible-xs"> 10 <img src="imagens/seta1.jpg"> </div>
        <div class="col-lg-8 col-lg-offset-2 bloco-cinza-seta ">

        <div class="bloco-cinza-seta naoidenta numeracao numeracao-cor10 hidden-xs"> 10 <img src="imagens/seta1.jpg"> </div>

          <div><a href="javascript:void(0)" class="botao-questao-10 btn btn-info">Clique para ver a dica</a></div>
          <div class="conteudo-questao-10" style="display:none">
  <p><strong>Empregar as regras do novo acordo ortográfico.</strong></p>
  <p>Descontrair é preciso...</p>  
  <a href="imagens/quadrinho.jpg" target="_blank"><img src="imagens/quadrinho.jpg" class="img-responsiva"></a>
  </div>

        </div>
      </div>

      <div class="clear"> </div>    

</div>
  </div>
</div>





<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

