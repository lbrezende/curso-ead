<?php 
//Configurar Aula
$aula = 4; 
$pagina = 4; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
   	<h3 class="titulo titulo-primary">A importância da comunicação na EaD</h3>

  </div>
</div>



<div class="row">
  <div class="col-lg-6">


      <p>Bom, diante de todas essas indagações, consideramos que as melhores palavras, na EaD, são aquelas que permitem ao aluno compreender, claramente, o que você (conteudista) escreveu.</p>
      <p>Nesse sentido, privilegie no seu material didático:</p>

      <ul class="ul">
        <li>Palavras simples. Se for necessário empregar algum vocábulo rebuscado, explique o significado dentro do contexto.</li>
        <li>Palavras que fazem parte do vocabulário do seu público-alvo.</li>
        <li>Palavras que fazem sentido no contexto do assunto das aulas.</li>
      </ul>
      <p>No mais, elimine:</p>
      <ul class="ul">
        <li>Palavras que geram duplicidade de sentido.</li>
        <li>Palavras que podem apresentar um tom preconceituoso.</li>
        <li>Gírias.</li>
      </ul>
 
  </div>

  <div class="col-lg-6">


    <div class="bloco-pontilhado" style="margin-top:0px" class="">
      <img src="imagens/dica.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
      <p style="text-align:center; font-weight:bold;">Procure escrever com simplicidade.    </p>
      <div id="conteudo-cerebro" >
        <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">

          <p class="center semidentacao">Veja o que diz o <a href="http://goo.gl/RwclPP" target="_blank">Dicionário <em>online</em> de Português</a>:</p>
          <p><strong>Simplicidade:</strong> “Característica do que não é complexo; desprovido de complicação; modo autêntico e espontâneo de se expressar (falar ou escrever); elegância.”</p>

        </div>
      </div>
    </div>


  </div>

</div>

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">

    <p>Inspire-se com a frase de Khalil Gibran:</p>
    <h3 class="titulo titulo-primary center"><img src="imagens/aspastop.jpg" style="margin-top:-50px;">A simplicidade é o último degrau da sabedoria.<img src="imagens/aspasbottom.jpg" style="margin-top:50px"></h3>

    <p>Portanto, seja simples e escolha palavras que facilitem a leitura. Com isso, você favorecerá a construção de significados à medida que o aluno interage com o seu texto.  </p>
     <h3 class="titulo titulo-secondary" style="text-align:center; ">Certo? Vamos continuar?</h3>


  </div>
</div>




<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

