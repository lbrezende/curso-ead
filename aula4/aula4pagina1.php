<?php 
//Configurar Aula
$aula = 4; 
$pagina = 1; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
  $paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
  $paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Olá, novamente!</h3>

      <p>Após estudarmos os fundamentos da educação a distância e, com base neles, planejarmos a escrita do nosso material didático, refletiremos, agora, acerca da linguagem que deverá ser empregada no texto.</p>
      <p>Pelo que vimos até aqui, você já deve ter percebido que, para atender às particularidades da EaD, a redação precisa ser diferenciada.</p>
      <p>Por isso, ao longo desta aula, conversaremos sobre algumas questões que, no seu conjunto, ajudarão a compor essa linguagem específica utilizada na elaboração de materiais para cursos <em>online</em>.</p>
      <p>Então, fique atento! Ao final, esperamos que você, conteudista, aplique os conhecimentos aqui construídos no texto das suas aulas. </p>
      <h3 class="titulo titulo-secondary" style="text-align:center">Tudo bem? Vamos começar?</h3>
        <div class="bloco-pontilhado" style="margin: 30px; 0px; padding-top:0px">
          <p class="semidentacao center"><strong >Observação sobre a atividade avaliativa do Módulo III</strong></p>
          <p>No final deste módulo, após o estudo das aulas 4, 5 e 6, você deverá <strong>apresentar o texto completo de uma aula</strong>, de acordo com o conteúdo programático do Plano de Elaboração do Material Didático que você elaborou recentemente.</p>
          <p>Isso significa que você terá a oportunidade de atuar como conteudista aqui no curso. Poderá praticar a escrita e, assim, ganhar mais confiança na produção de materiais didáticos para a EaD.</p>
      <h3 class="titulo titulo-secondary" style="text-align:center">Certo? O desafio está posto. Prepare-se! </h3>
        </div>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'fim', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

