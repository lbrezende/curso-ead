<?php 
//Configurar Aula
$aula = 4; 
$pagina = 8; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
<style>
	.poema {
		font-family: 'Dancing Script', cursive;
		font-size:30px;
	}
</style>
<!-- nova tela -->
<div class="row" >
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Linguagem dialógica</h3>

	<p>O que conversamos, até aqui, nos ajudará a compreender o assunto deste tópico, que tem como intenção consolidar uma comunicação efetiva com o nosso aluno.</p>
	<p >Para começar, que tal ler os versos do autor <a href="https://pt.wikipedia.org/wiki/Carlos_Drummond_de_Andrade" target="_blank">Carlos Drummond de Andrade</a>?</p>

  </div>
</div>

<div class="row">
  <div class="col-lg-offset-3 col-lg-6" style="background:url('imagens/carlos.png') no-repeat left top ; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">
  		<pre class="italic" style="    display: inline-table; font-weight:bold;   margin-top: 30px; font-size:28px !important; font-family:  'Dancing Script', cursive; background:none ">
  		
		<p class="center naoidenta " style="font-size:30px; margin-top:-30px"><a href="http://www.citador.pt/poemas/o-constante-dialogo-carlos-drummond-de-andrade" target="_blank"><span style="color:#376A92 !important;  margin:0px 20px; font-family:  'Dancing Script', cursive; background:none" class="center">O Constante Diálogo</span></a></p>
		Há tantos diálogos 
		Diálogo com o ser amado 
		                o semelhante 
		                o diferente 
		                o indiferente 
		                o oposto 
		                o adversário 
		                o surdo-mudo 
		                o possesso 
		                o irracional 
		                o vegetal 
		                o mineral 
		                o inominado 
		Diálogo consigo mesmo 
		                com a noite 
		                os astros 
		                os mortos 
		                as ideias 
		                o sonho 
		                o passado 
		                o mais que futuro 
		Escolhe teu diálogo 
		                           e 
		tua melhor palavra 
		                      ou 
		teu melhor silêncio. 
		Mesmo no silêncio e com o silêncio 
		dialogamos. 
		(no 'Discurso da Primavera')

		
		</pre>

  </div>
</div>



<div class="row">
  <div class="col-lg-12">

        <p>Após a leitura dos versos, medite conosco:</p>
        <h3 class="titulo titulo-secondary center">Qual a relação do diálogo com a educação a distância?</h3>
        <p>Vimos que, na EaD, a comunicação entre os participantes ocorre por meio das palavras escritas. De igual modo, reconhecemos que a relação pedagógica é baseada na horizontalidade, isto é, todos estão em “pé de igualdade” no processo educativo. Por consequência, o mais natural e saudável nesse ambiente é pautar a interação no diálogo. Por isso, utilizamos no material didático a linguagem dialógica ou conversacional.</p>

  </div>

</div>

<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

