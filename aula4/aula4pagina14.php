<?php 
//Configurar Aula
$aula = 4; 
$pagina = 14; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  <div class="col-lg-12">

		<h3 class="titulo titulo-primary">Considerações Finais</h3>

		<p class="center">Chegamos ao final da nossa quarta aula!</p>
		<p class="center"><strong>Como você se sente até aqui?</strong> </p>
		<p>Trocamos ideias sobre os principais pontos que devem ser observados em relação à escrita dos materiais didáticos para a educação a distância.</p>
		<p>Salientamos que, devido a todas as especificidades dessa modalidade de ensino, devemos empregar a linguagem dialógica na produção dos textos. </p>
		<p>Talvez essa proposta de escrita seja uma novidade para você. Contudo, poderemos praticá-la aqui no nosso curso em duas oportunidades:</p>
		<p><strong>a.</strong>  Veja a atividade avaliativa desta aula logo na sequência.</p>
		<p><strong>b.</strong>  No final do curso, está prevista, como atividade avaliativa, a escrita de uma aula completa de acordo com o conteúdo programático previsto no seu Plano de Elaboração do Material Didático.</p>

		<p>Se surgirem dúvidas sobre o assunto, utilize os canais de comunicação no nosso ambiente virtual de aprendizagem, tudo bem?</p>
		<p class="center">Vamos à atividade desta aula?</p>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

