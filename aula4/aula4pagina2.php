<?php 
//Configurar Aula
$aula = 4; 
$pagina = 2; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-6">
   <h3 class="titulo titulo-primary">A importância da comunicação na EaD</h3>
</div></div>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-6">

      <p>Você já passou por situações em que o emprego de alguma palavra gerou certo desconforto? Ou, então, tentou expressar uma ideia e o entendimento foi diferente daquele que pretendia?</p>
      <p>O fato é que palavras e expressões da nossa língua ganham significado dependendo do contexto em que são utilizadas. Além disso, a construção do significado sofre influência do ponto de vista e das experiências do leitor.</p>
      <p>Veja a ilustração seguinte:      </p>
      <p class="naoidenta center" ><a href="imagens/jogandofora.jpg" target="_blank"><img src="imagens/jogandofora.jpg" class="img-responsiva"></a></p>
   
  </div>
  <div class="col-lg-6">

      <p>Pense conosco:</p>
      <h3 class="titulo titulo-secondary" style="text-align:center; color:#555 !important">O que se passa pelo imaginário das pessoas? </h3>
      <h3 class="titulo titulo-secondary" style="text-align:center; color:#555 !important">Como elas interpretam as palavras? </h3>
      <h3 class="titulo titulo-secondary" style="text-align:center; color:#555 !important">Quais sentidos elas atribuem às expressões?</h3>
      <p>Na educação a distância, essas questões influenciam de maneira direta a comunicação, que se dá, basicamente, por meio da escrita. </p>
      <p>De acordo com Neder (2009), </p>
        <div class="col-lg-offset-2 col-lg-10">
            <p style="font-size: 14px; text-indent:initial;">a comunicação constitui um dos elementos centrais na EaD, tendo em vista, sobretudo, a relação professor-aluno, que não se estabelece mais face a face, mas, sim, pela mediação de textos, veiculados pelas tecnologias da informação e comunicação.</p>
        </div>  
  </div> 
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

