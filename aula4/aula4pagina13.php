<?php 
//Configurar Aula
$aula = 4; 
$pagina = 13; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Correção gramatical</h3>
   </div>
</div>

<div class="row">
  <div class="col-lg-6">

        <div class="bloco-pontilhado" style="margin-top:40px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <div id="conteudo-cerebro">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
              <p class="center"><strong>Você conhece o Vocabulário Ortográfico da Língua Portuguesa (VOLP)?</strong></p>
              <p>Esse documento, elaborado pela Academia Brasileira de Letras, reúne a ortografia oficial do Português.</p>
              <p>Caso tenha alguma dúvida sobre a escrita das palavras, consulte o <a href="http://www.academia.org.br/abl/cgi/cgilua.exe/sys/start.htm?sid=23" target="_blank">VOLP</a>. </p>
              
               
            </div>
          </div>
        </div>

  </div>  
  <div class="col-lg-6">

		 <p>Para concluir, nós, conteudistas, devemos sempre ter em mente os questionamentos: </p>
		    <h3 class="titulo titulo-secondary center">Será que o aluno compreenderá o que escrevi? </h3>
		    <h3 class="titulo titulo-secondary center">Posso efetuar alterações para deixar o texto mais claro?</h3>
		    <h3 class="titulo titulo-secondary center">Devo retirar algum trecho que está repetitivo?</h3>
		    <h3 class="titulo titulo-secondary center">Preciso explicar com mais detalhes algum termo?</h3>
		<p></p>
		<p>E lembre-se: você não estará ao lado do aluno no momento em que ele estiver lendo o material didático. Por isso, é importante o seu empenho para elaborar um texto claro, que apresente conteúdo consistente do ponto de vista conceitual e que contemple a linguagem dialógica.</p>
  </div>

</div>





<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

