<?php 
//Configurar Aula
$aula = 4; 
$pagina = 9; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Linguagem dialógica</h3>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">

        <img src="imagens/dialogo.jpg" style="float:right; padding-left:20px; padding-bottom:5px" >

        <p><strong>O que significa essa linguagem dialógica?</strong> </p>
        <p>Significa estabelecer uma conversa direta com o aluno (leitor) ou “trocar um dedinho de prosa”, se preferir. Alguns podem até achar estranho, mas é isso mesmo: escrever como se estivesse em um diálogo (bate-papo) com o estudante! Ressaltamos: DIÁLOGO, e não monólogo.</p>
        <p>O emprego dessa linguagem tem por objetivo aproximar o aluno do material didático e do seu autor, amenizar a falta de contato físico e favorecer a compreensão dos assuntos. Ademais, pretende-se a criação de um ambiente agradável para a aprendizagem, na qual o texto é o principal aliado.</p>
        <p>A literatura aponta que esse tipo de comunicação está diretamente relacionado com a qualidade da aprendizagem. De acordo com Moreno e Mayer (2000), citados por Piva Jr. e Freitas (2010), estudos indicam que a personalização do material didático permitiu melhorias significativas no desempenho e na aprendizagem de estudantes (26% de melhoria).</p>
        <p></p>
        <div class="row well cor-ativo"><p><strong>Exatamente aqui está o grande diferencial do material didático:</strong></p><p>A proposta de se desenvolver uma comunicação mais próxima e pessoal com o discente, sem perder a consistência teórico-conceitual.</p></div>

        <p></p>
       

  </div>

  <div class="col-lg-6">
	 <p><strong>Observação:</strong> Ressaltamos que dois dos quesitos avaliados pela Seção de Educação a Distância na escolha de profissionais conteudistas são, justamente, o conhecimento do tema e a aplicação dessa linguagem dialógica nos materiais produzidos.</p>
	 <p>&nbsp;</p>
	                
	        <div class="bloco-pontilhado" style="margin-top:40px" class="">
	          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
	          <div id="conteudo-cerebro">
	            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
	               <p>A título de conhecimento, destacamos os principais autores cujas ideias se complementam e ajudam a consolidar a compreensão sobre a linguagem dialógica: <a href="https://pt.wikipedia.org/wiki/Mikhail_Bakhtin" target="_blank">Bakhtin</a> e <a href="http://www.paulofreire.org/paulo-freire-patrono-da-educacao-brasileira" target="_blank">Paulo Freire</a>. Para aqueles que se interessarem pelo assunto, sugerimos a leitura da dissertação de mestrado de Emmanuela Vitorino Carvalho de Souza Blumer, na qual as contribuições desses pensadores são discutidas. </p>
	               <p class="center"><a href="http://goo.gl/uUz0MI" class="btn btn-info" target="_blank">Acesse aqui o arquivo</a></p>
	            </div>
	          </div>
	        </div>
  </div>

</div>





<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

