<?php 
//Configurar Aula
$aula = 4; 
$pagina = 6; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">O leitor na EaD</h3>

  </div>
</div>


<!-- nova tela -->
<div class="row">
  <div class="col-lg-6">

        <p>Após essa pitada filosófica, a relação que estabelecemos é a seguinte: um dos papéis do material didático é incentivar o leitor a sair da “caverna”. Em outras palavras, esperamos que o aluno saia do lugar atual em que se encontra (cognitivamente) e avance nos seus percursos de aprendizagem para vislumbrar ângulos diferentes da realidade e, assim, efetuar novas leituras do mundo à sua volta. </p>
        <p>Além disso, sabe quando paramos para pensar em algo e, de repente, suspiramos: <strong>Nossa, agora isso faz todo o sentido!</strong> Esperamos que o aluno exclame essa frase várias vezes enquanto estuda o material didático.</p>

 
        <p>O desafio é grande! Ainda mais pensando nas novas experiências comunicativas, advindas da consolidação da internet, que têm influenciado diretamente o comportamento dos leitores e a relação deles com o texto. </p>
        <p>Santaella (2004), citada por Possari (2009, p. 53), destaca que, no presente, o leitor é denominado <strong>imersivo ou virtual</strong>. Suas características são: envolvimento direto com a tecnologia; autonomia para escolher a direção da leitura de um texto não linear; e construção de seus próprios caminhos, já que tem a liberdade de optar por vias e nexos de acordo com suas preferências.</p>

	</div>
	<div class="col-lg-6">
		 <div class="bloco-pontilhado" style="margin-top:0px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <div id="conteudo-cerebro">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
               <a href="http://paulofreire.org" target="_blank"><img src="imagens/autorsaibamais.jpg" style="float:left; padding-right:10px"></a>
               <p>Para aqueles que gostam das contribuições de ilustres pensadores, indicamos o registro deixado por Paulo Freire sobre a temática da leitura:</p>
                       <div class="col-lg-offset-2 col-lg-10">
                          <p style="font-size: 14px;text-indent:inherit">A leitura do mundo precede a leitura da palavra, daí que a posterior leitura desta não pode prescindir da continuidade da leitura daquele (A palavra que eu digo sai do mundo que estou lendo, mas a palavra que sai do mundo que eu estou lendo vai além dele).  (...) Se for capaz de escrever minha palavra estarei, de certa forma transformando o mundo. O ato de ler o mundo implica uma leitura dentro e fora de mim. Implica na relação que eu tenho com esse mundo. <a href="http://www.ufscar.br/~crepa/crepa/alfabetizacao/OS_SUJEITOS_DO_PROCESSO_DE_ALFABETIZACAO.doc" target="_blank">Fonte: UFSCar</a></p>
                      </div>  


              
               
            </div>
          </div>
        </div>
	</div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

