<?php 
//Configurar Aula
$aula = 4; 
$pagina = 16; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  <div class="col-lg-12">
<h3 class="titulo titulo-primary">Referências</h3>

<p class="naoidenta break-word">ABREU-FIALHO, A. P.; MEYOHAS, J. <strong>Planejamento e elaboração de material didático impresso para EAD:</strong> elementos e estratégias de ensino. Aula 5: O uso da linguagem. Por que tanta preocupação e tanto cuidado? Fundação Cecierj, 2007. Disponível em: <<a target="_blank" href="http://teca.cecierj.edu.br/" target="_blank">http://teca.cecierj.edu.br</a>>. Acesso em: jun. 2015.</p>
<p class="naoidenta break-word">ANDRADE, Carlos D. <strong>O constante diálogo</strong>. Disponível em: <<a target="_blank" href="http://www.citador.pt/poemas/o-constante-dialogo-carlos-drummond-de-andrade">http://www.citador.pt/poemas/o-constante-dialogo-carlos-drummond-de-andrade</a>>. Acesso em: ago. 2015.</p>
<p class="naoidenta break-word">BELISÁRIO, Aluísio. O material didático na educação a distância e a constituição de propostas interativas. In: SILVA, Marco (org.).<strong> Educação online</strong>. São Paulo: Loyola (2003), p. 137-148.</p>
<p class="naoidenta break-word">BLUMER, Emmanuela Vitorino Carvalho de Souza. <strong>Linguagem dialógica na formação de professores para práxis:</strong> um estudo de caso na EAD/Dissertação de mestrado. Lavras: UFLA, 2013. Disponível em: <<a target="_blank" href="http://repositorio.ufla.br/bitstream/1/1431/1/DISSERTA%C3%87%C3%83O_Linguagem%20dial%C3%B3gica%20na%20forma%C3%A7%C3%A3o%20de%20professores%20para%20pr%C3%A1xis%20%20um%20estudo%20de%20caso%20na%20EAD.pdf">http://repositorio.ufla.br/bitstream/1/1431/1/DISSERTA%C3%87%C3%83O_Linguagem%20dial%C3%B3gica%20na%20forma%C3%A7%C3%A3o%20de%20professores%20para%20pr%C3%A1xis%20%20um%20estudo%20de%20caso%20na%20EAD.pdf</a>>. Acesso em: jun. 2015.</p>
<p class="naoidenta break-word">GRILLO, Marlene; ENRICONE, Délcia; BOCHESE, Jocelyne; FARIA, Elaine; HERNÁNDEZ, Ivane R. C.; NETO, Débora R. da S. <strong>Transposição didática:</strong> uma criação ou recriação cotidiana. Disponível em: <<a target="_blank" href="http://www.portalanpedsul.com.br/admin/uploads/1999/Formacao_De_Professores/Trabalho/02_29_00_TRANSPOSICAO_DIDATICA__UMA_CRIACAO_OU_RECRIACAO_COTIDIANA.pdf">http://www.portalanpedsul.com.br/admin/uploads/1999/Formacao_De_Professores/Trabalho/02_29_00_TRANSPOSICAO_DIDATICA__UMA_CRIACAO_OU_RECRIACAO_COTIDIANA.pdf</a>>. Acesso em: ago. 2015.</p>
<p class="naoidenta break-word">PIVA JR., Dilermando; FREITAS, Ricardo L. <strong>Linguagem dialógica instrucional:</strong> a (re) construção da linguagem para cursos <em>online</em>. Anais do XVI Workshop sobre Informática na Escola – WIE 2010. Disponível em: <<a target="_blank" href="http://www.br-ie.org/WIE2010/pdf/st04_05.pdf">http://www.br-ie.org/WIE2010/pdf/st04_05.pdf</a>>. Acesso em: jun. 2015.</p>
<p class="naoidenta break-word">POSSARI, L. H. V. <strong>Material didático para a EaD:</strong> processo de produção. EdUFMT, 2009.</p>
<p class="naoidenta break-word">PUZZO, Miriam Bauab. <strong>Teoria dialógica da linguagem:</strong> o ensino da gramática na perspectiva de Bakhtin. Disponível em: <<a target="_blank" href="http://www.revistas.usp.br/linhadagua/article/view/65163/71563">http://www.revistas.usp.br/linhadagua/article/view/65163/71563</a>>.  Acesso em: jun. 2015.</p>
<p class="naoidenta break-word">SARTORI, Ademilde Silveira; ROESLER, Jucimara. <strong>Comunicação e educação a distância: </strong>algumas reflexões sobre elaboração de materiais didáticos. Disponível em: <<a target="_blank" href="http://www.abed.org.br/seminario2003/texto13.doc">http://www.abed.org.br/seminario2003/texto13.doc</a>>. Acesso em: jun. 2015.</p>
<p class="naoidenta break-word">SILVA, Ivanda Martins. <strong>Educação a distância:</strong> uma abordagem dialógica na construção de materiais didáticos impressos. Disponível em: <<a target="_blank" href="http://www.seer.furg.br/redsis/article/view/1872/1209">http://www.seer.furg.br/redsis/article/view/1872/1209</a>>. Acesso em: jun. 2015.</p>
<p style="text-indent:0em">Imagens: <<a target="_blank" href="http://www.pixabay.com" target="_blank">http://www.pixabay.com</a>> Licença CCO Public Domain.</p>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

