<?php 
//Configurar Aula
$aula = 4; 
$pagina = 3; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos a para EaD');
?> 


<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
   	<h3 class="titulo titulo-primary">A importância da comunicação na EaD</h3>

  </div>
</div>


<div class="row">
  <div class="col-lg-12">

      <p>Diante disso, para escrever materiais didáticos, é necessário refletir sobre o uso das palavras no texto e no contexto, bem como o impacto delas além do texto.</p>
      <h3 class="titulo titulo-secondary center" style="margin-top:20px">Você já parou para pensar na importância das palavras?</h3>
      </div>
      </div>

  <div class="col-lg-6">
   <div class="bloco-pontilhado" style="margin-top:20px">
      <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
      <p>A nossa proposta é iluminar a sua visão a esse respeito. Então, assista ao vídeo “Receita para lavar palavra suja”, da filósofa Viviane Mosé:</p>
      <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
        <iframe width="420" height="315" src="https://www.youtube.com/embed/qUsdgtYSPP4" frameborder="0" allowfullscreen></iframe>                    
      </div>
    </div>
</div>    
  <div class="col-lg-6">
      <p style="margin-top:150px;text-align:center; text-indent:inherit">Após esse estímulo reflexivo, alguns questionamentos emergem:</p>
      <h3 class="titulo titulo-secondary center">Qual a “roupa certa” para a educação a distância?</h3>
      <h3 class="titulo titulo-secondary center">Quais palavras são mais apropriadas? </h3>
      <h3 class="titulo titulo-secondary center">Quais delas poderão nos ajudar a atingir os objetivos didáticos? Quais delas serão melhor compreendidas pelos alunos? </h3>
  </div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

