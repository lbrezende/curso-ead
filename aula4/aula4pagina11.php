<?php 
//Configurar Aula
$aula = 4; 
$pagina = 11; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Linguagem dialógica</h3>
  </div>
</div>


<div class="row">
  <div class="col-lg-12">


        <p class="center"><strong>Na prática, como escrever um texto com a linguagem dialógica?</strong></p>
        <p>Vamos propor um exemplo para você comparar o tipo de escrita empregado. Observe, na primeira coluna, um texto jornalístico. Na segunda coluna, um texto com as mesmas informações, mas escrito com a linguagem dialógica.</p>

<div class="row" style="margin-top:40px">
    <div class="col-lg-6">
        <div style="color:white; background: #2AAC9E !important; text-shadow:none; padding:10px; font-size:22px; height:80px;">
                <img src="imagens/news.png" class="table-img table-img-left">
                <p class="naoidenta" style="margin-top:15px; position: absolute;     right: 24%;"><strong>Texto Jornalístico</strong></p>
        </div>

        <div class="center" style="margin-top:30px;margin-bottom:60px;">
          <a href="javascript:void(0)" style="float:right;" class="botao-questao-2 btn btn-info btn-small">Clique para ver o texto</a>
        </div>
        

        <div class="conteudo-questao-2" style="display:none">
        <p style="margin-top:60px"><strong>“Quebra de sigilo não pode ocorrer sem fundamentação", decide Celso de Mello.</strong></p>
          <p>Por falta de fundamentação adequada e de indicação de fato concreto, o ministro Celso de Mello, do Supremo Tribunal Federal (STF), suspendeu liminarmente ordem de quebra de sigilos fiscal, bancário e telefônico determinados pela Comissão Parlamentar de Inquérito (CPI) das Próteses, instalada pelo Senado Federal. A decisão foi proferida no Mandado de Segurança (MS) 33635, no qual o empresário Francisco José Dambros, da empresa Importec, questionou determinação da comissão.</p>
          <p>No entendimento do ministro, há plausibilidade no pedido, pois, em exame preliminar, a deliberação da CPI carece de fundamentação adequada, limitando-se a fazer referência ao noticiário da imprensa e sustentando que tal fato justifica a quebra de sigilo. “A mera referência a notícias veiculadas pela imprensa e a busca de informações mediante quebra de sigilos bancário, fiscal e telefônico sem a correspondente e necessária indicação de fato concreto e específico que configure a existência de causa provável não bastam para justificar a medida.”</p>
          <p>(Extraído do <a href="http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=294365" target="_blank"><em>site</em> do STF</a>)</p>
        </div>
    </div>
    <div class="col-lg-6">
        
        <div class="naoidenta" style="color:white; background: #396E99; text-shadow:none !important; padding:10px; font-size:22px; height:80px;">
                <img src="imagens/note.png" class="table-img table-img-right">
                <p class="naoidenta" style="position: absolute;  margin-top:0px;    left: 16%;    text-align: center;"><strong> Texto para a EaD <br />
                     (Linguagem dialógica)</strong></p>
        </div>

        <div class="center" style="margin-top:30px; float:left">
          <a href="javascript:void(0)" style="float:right;" class="botao-questao-1 btn btn-info btn-small">Clique para ver o texto</a>
        </div>

        <div class="conteudo-questao-1" style="margin-top:60px; display:none">
          <p><strong>Seja bem-vindo!</strong></p>
          <p>Hoje, conversaremos sobre um tema que tem se destacado na imprensa nos últimos dias: a Comissão Parlamentar de Inquérito (CPI) das Próteses, que está aberta no Senado Federal.</p>
          <p>Você sabia que o Ministro Celso de Mello, do STF, suspendeu liminarmente a ordem de quebra dos sigilos fiscal, bancário e telefônico determinada pela CPI?</p>
          <p>Isso mesmo!</p>
          <p>Mas por que o Ministro tomou essa decisão?</p>
          <p>Bom, ao analisar o Mandado de Segurança 33.635, ele entendeu que a determinação da CPI, baseada em notícias veiculadas pela imprensa, não possuía a fundamentação necessária, isto é, não foi identificado fato concreto e específico que justificasse a medida. </p>
          <p>Ficou claro? Conseguiu compreender os motivos apontados pelo Ministro Celso de Mello?</p>
          <p>Continuaremos nossas reflexões na próxima aula. </p>
          <p>Até breve!</p>
        </div>

    </div>  
  </div>


        <h3 class="titulo titulo-secondary center" style="margin-top:30px">Percebeu as diferenças entre os textos?</h3>
        <p>Temos muitos motivos para empregar essa proposta de comunicação nos nossos materiais, não é mesmo?</p>
        <p>No próximo tópico, para finalizar esta aula, conversaremos sobre alguns aspectos da correção gramatical.  </p>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

