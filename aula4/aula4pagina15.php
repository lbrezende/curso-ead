<?php 
//Configurar Aula
$aula = 4; 
$pagina = 15; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 


<div class="row">
  <div class="col-lg-12">


<h3 class="titulo titulo-primary">Vamos Praticar</h3>

<p>Siga as orientações:</p>
<ul class="ul">
  <li>Utilize o seu diário “Meu percurso de Aprendizagem”, que se encontra no nosso ambiente virtual de aprendizagem.</li>
  <li>Escreva o título: “Atividade – Aula 4”.</li>
  <li>Reescreva o trecho abaixo e procure empregar a linguagem apropriada para a educação a distância (linguagem dialógica). Você também pode escolher outro texto de sua preferência para realizar a atividade.</li>
</ul>
<p></p>
        <div class="bloco-pontilhado" style="margin: 30px; 0px; padding-top:0px">
          <p class="center"><strong>REPERCUSSÃO GERAL RECONHECIDA E MÉRITO JULGADO – TEMA 376</strong> </p>
          <p class="center"><strong>“Direito Administrativo; Concurso Público </strong></p>
          <p>É constitucional a regra denominada ´cláusula de barreira´, inserida em edital de concurso público, que limita o número de candidatos participantes de cada fase da disputa, com o intuito de selecionar apenas os concorrentes mais bem classificados para prosseguir no certame. Discutia-se a legitimidade da aludida cláusula à luz do princípio da isonomia. De início, o Tribunal rejeitou questão de ordem no sentido de que a matéria estaria contida no RE 608.482 RG/RN, com repercussão geral reconhecida. Afirmou tratar-se de temas distintos. No mérito, a Corte explicou que o crescente número de candidatos ao ingresso em carreira pública provoca a criação de critérios editalícios que restrinjam a convocação de concorrentes de uma fase para outra dos certames. Nesse sentido, as regras restritivas subdividem-se em ´eliminatórias´ e ´cláusulas de barreira´. As eliminatórias preveem, como resultado de sua aplicação, o afastamento do candidato do concurso por insuficiência em algum aspecto de seu desempenho. Reputou comum a conjunção, com esta, da cláusula de barreira, que restringe o número de candidatos para a fase seguinte do certame, para determinar que, no universo de pessoas não excluídas pela regra eliminatória, participaria da etapa subsequente apenas número predeterminado de concorrentes, de modo a contemplar apenas os mais bem classificados. O Colegiado observou que a delimitação de número específico de candidatos é fator imprescindível para a realização de determinados certames, à luz da exigência constitucional de eficiência. Destacou, ainda, que as cláusulas de barreira, de modo geral, elegem critérios diferenciadores de candidatos em perfeita consonância com a Constituição, à luz do art. 37, caput e II. Apontou que o estabelecimento do número de candidatos aptos a participar de determinada etapa de concurso público também passa pelo critério de conveniência e oportunidade da Administração, e não infringe o princípio da isonomia quando o requisito de convocação é vinculado ao desempenho do concorrente em etapas anteriores. Acresceu que decisões judiciais ampliadoras do rol de participantes em determinada etapa de certame, no afã de atender à isonomia, desrespeitam esse postulado, porque ensejam a possível preterição de candidatos mais bem classificados. (RE 635.739/AL, Rel. Min. Gilmar Mendes, julgado em 19/2/2014, acórdão pendente de publicação.)”</p>
          <p>Disponível em:  <a class="break-word" href="http://www.stf.jus.br/arquivo/cms/publicacaoInformativoRG/anexo/Repercussao_Geral_3_12014.pdf" target="_blank">http://www.stf.jus.br/arquivo/cms/publicacaoInformativoRG/anexo/Repercussao_Geral_3_12014.pdf</a> </p>
        </div>

  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

