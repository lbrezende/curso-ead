<?php 
//Configurar Aula
$aula = 4; 
$pagina = 5; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">O leitor na EaD</h3>

  </div>
</div>

<!-- nova tela -->
<div class="row">
  <div class="col-lg-6">



        <p>Antes de dialogarmos a respeito do leitor, observe a definição de leitura, segundo Possari (2009, p. 48):</p>

        <div class="col-lg-offset-2 col-lg-10">
            <p style="font-size: 14px;text-indent:inherit">(...) processo pelo qual cada leitor atribuirá sentidos ao texto lido, seja ele de que natureza for e veiculados (sic) por qualquer mídia: impresso, TV, Rádio, CD, DVD, ou internet.</p>
        </div>  

        <img src="imagens/ipad.jpg" style="float:right; padding-left:20px; padding-bottom:5px">
        <h3 class="titulo titulo-secondary" style="text-align:center; ">Com base nessa definição, quem é o leitor na educação a distância? </h3>
        <p>A referida autora ressalta que o leitor é sujeito ativo, que interage com o texto; por consequência, é protagonista da construção de significados. </p>
        <p>Isso quer dizer que o material didático assume a responsabilidade de ser o ponto de partida para reflexões e aprendizagens. Como tal, sofrerá o impacto das leituras, releituras, interpretações e reinterpretações do leitor, o que torna a relação autor-leitor dinâmica. </p>
        <h3 class="titulo titulo-secondary" style="text-align:center; ">Como essa visão repercute no trabalho do conteudista?</h3>
        


  </div>
    <div class="col-lg-6">


        <div class="bloco-pontilhado" style="margin-top:0px">
          <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
          <p>Para auxiliar-nos a pensar sobre essa questão, assista a um breve vídeo, que retrata o &quot;Mito da Caverna&quot;, do filósofo grego <a href="https://goo.gl/pXRQmT" target="_blank">Platão</a>:</p>
          <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
            <iframe width="420" height="315" src="https://www.youtube.com/embed/73SJ0Mn86pQ" frameborder="0" allowfullscreen></iframe>                    
          </div>
        </div>

    </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

