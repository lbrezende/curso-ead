<?php 
//Configurar Aula
$aula = 4; 
$pagina = 10; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Linguagem dialógica</h3>
  </div>
</div>
<style>
  ul li {
    margin-top:10px;
    text-align:   justify;
  }
</style>

<div class="row">
  <div class="col-lg-12">
        <p class=center style="padding-top:10px"><strong>Como aplicar a linguagem dialógica na escrita dos materiais didáticos para a EaD?</strong> </p>  
  </div>
  <div class="col-lg-6">
        <p>Algumas ações podem nos ajudar a escrever com esse foco:</p>
        <ul  class="ul">
          <li >Utilizar linguagem leve e motivadora. </li>
          <li>Empregar um tratamento mais próximo do estudante (podemos chamá-lo de “você”!).</li>
          <li>Utilizar a 1ª pessoa do discurso (escolha entre 1ª pessoa do singular ou 1ª pessoa do plural e mantenha em todo o texto).</li>
          <li>Adequar o vocabulário (evitar rebuscamentos desnecessários).</li>
          <li>Utilizar exemplos.</li>
          <li>Contextualizar as informações. </li>
          <li>Destacar o que é mais importante. </li>
          <li>Propor questionamentos e reflexões.</li>
          <li>Antecipar dúvidas.</li>
        </ul>
        <p>De maneira complementar, Piva Jr. e Freitas (2010) sugerem:</p>    
            <ul class="ul">
          <li>Usar sentenças curtas e evitar períodos compostos.</li>
          <li>Evitar excesso de informações na sentença. </li>
          <li>Manter itens iguais ou equivalentes em paralelo e listar as condições separadamente. </li>
          <li>Usar exemplos familiares ao público-alvo. Adote um estilo de linguagem claro e que seja facilmente compreensível pelos alunos. </li>
          </ul>
    </div>    
  <div class="col-lg-6">
        <ul class="ul">
          
          <li>Evitar jargões e palavras difíceis ou desnecessárias. </li>
          <li>Utilizar termos técnicos somente quando necessários e, sempre que possível, devem vir acompanhados de explicações. </li>
          <li>Colocar as sentenças e parágrafos em uma sequência lógica: primeiro os tópicos que sensibilizam mais e, depois, aqueles com baixa sensibilização e contextualização; primeiro o geral, depois o específico; primeiro os conceitos permanentes, depois os temporários. </li>
          <li>Evitar sobrecarregar o texto com frases na negativa. </li>
          <li>Evitar o uso da voz passiva. Utilize verbos ativos e diretos. Ex.: Use “fez” (ativo) em vez de “fez-se” ou “foi feito” (passivo). </li>
          <li>Evitar usar em demasia palavras como “este”, “isso” ou “o qual”. </li>
          <li>Transformar palavras abstratas em verbos. Isso reduz a carga cognitiva necessária para interpretação das frases/textos. </li>
          <li>Ativar o conhecimento prévio do aluno. Ele deve ser utilizado a seu favor, melhorando, assim, a retenção de conteúdo e tornando a leitura mais agradável. </li>
          <li>Sempre que puder, incluir exemplos e estabelecer comparações entre diferentes situações ou abordagens.</li>
    </ul>



  </div>
  <div class="clear"> </div>
  <div class="col-lg-12" style="margin-top:40px"> 
        <div class="bloco-pontilhado" style="margin-top:0px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:1px;">
          <p class="center"><strong>Você já ouviu falar em “Transposição Didática”?</strong></p>
          <div id="conteudo-cerebro">
            <div class="row" style="text-align:center; margin-bottom:1px; padding:30px">
              <p>Esse é um conceito que foi definido por Ives Chevallard: “passagem de um conteúdo de saber preciso a uma versão didática deste objeto de saber ou ainda transformação de um objeto de saber a ensinar em objeto de ensino” <a href="http://goo.gl/9sW1KN" target="_blank">(GRILLO, et al., Portal Anped Sul)</a>. </p>
              <p>Em outras palavras, significa abordar o conhecimento científico de uma maneira mais didática, para que os educandos possam compreender com mais facilidade o assunto. É tornar o conhecimento palatável e inteligível para os alunos. É, portanto, transpor o conhecimento para uma versão didática.</p>
              <p>A linguagem dialógica encontra pontos de contato com essa definição, com certeza!</p>
            </div>
          </div>
        </div>   
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

