<?php 
//Configurar Aula
$aula = 4; 
$pagina = 7; 
$totalPaginas = 16;


//Automatização da navegação
$paginaAnterior = $pagina-1; 
$paginaProxima = $pagina+1; 

if ($pagina == 1) {
	$paginaAnterior = 1; 
}

if ($pagina == $totalPaginas) {
	$paginaProxima = $totalPaginas; 
}

require_once('../util/util.php'); 
configHeader('Linguagem para a EaD', 'exibir', $aula ,$pagina, $totalPaginas, 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php', 'Produção de Conteúdos para a EaD');
?> 

<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">O leitor na EaD</h3>

  </div>
</div>

<!-- nova tela -->
<div class="row">
  <div class="col-lg-6">


        <p>Segundo Possari (2009, p. 51), o impacto disso, nos materiais didáticos, é a ampliação do escopo da leitura, ou seja, os estudantes também são leitores de:</p>
         
          <ul class="ul">
            <li>imagem, desenho, pintura, foto;</li>
            <li>jornal e revista;</li>
            <li>gráficos, mapas e sistemas de notação;</li>
            <li>símbolos e sinais;</li>
            <li>cinema e vídeo.</li>
          </ul>

          <p>Nessa perspectiva, produzir textos educativos, hoje, exige muito além de palavras. É necessário pensar na interatividade e no perfil do leitor da atualidade. Logo, torna-se imprescindível agregar recursos variados a fim de proporcionar aos alunos alternativas para trilhar os seus próprios caminhos de leitura. </p>

  </div>
    <div class="col-lg-6">
        
        <h3 class="titulo titulo-secondary center" style="margin-top:0px; padding:0px;">Como assim?</h3>         
        <p>Escrevemos o texto principal, certo? Nele, incorporamos elementos para estabelecer conexões temáticas internas e externas, de modo a permitir que o aluno navegue no texto e além do texto, no ciberespaço.</p>
        <div class="row well cor-ativo"><p>Você consegue identificar algum recurso, nas nossas aulas, empregado segundo a perspectiva atual de leitura?</p></div>
        <p>Conversaremos sobre a aplicação desses elementos nas próximas aulas. Por enquanto, comece a pensar no seu material didático: o que você poderia utilizar para enriquecer o seu texto?</p>


  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula'.$aula.'pagina'.$paginaAnterior.'.php', 'aula'.$aula.'pagina'.$paginaProxima.'.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

