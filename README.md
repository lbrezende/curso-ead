# README #

Curso EaD do STF

### Como o curso é montado? ###

* Página aula n - com arquivos das páginas de cada aula e respectivas imagens
* Includes - css, js, e assets em geral
* Util - montagem do esqueleto da página

### How do I get set up? ###

* Montar servidor web - WAMP ou XAMPP
* Iniciar o git na pasta via linha de comando com 'git init'
* Baixar o repositório com o comando 'git clone https://lbrezende@bitbucket.org/lbrezende/curso-ead.git'

### Contribution guidelines ###

Para contribuir basta adicionar os arquivos alterados ao repositório, commitar e dar um push, lembrando-se sempre de rodar um pull antes

* git add -all
* git commit -am "Mensagem do commit"
* git pull
* git push origin master

### Who do I talk to? ###

* Leandro Rezende - lbrezende@gmail.com - 61 8138 8303