<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','8', '15', 'aula1pagina7.php', 'aula1pagina9.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <p> <h3 class="titulo titulo-primary">A EaD na atualidade</h3></p>
      <p>Antes de começarmos a discorrer sobre a educação a distância na atualidade, convidamos você a assistir ao vídeo a seguir, que sintetiza o que vimos até o momento. Trata-se de reportagem do <i class="italico">Jornal Nacional</i>, da Rede Globo de Televisão, que apresenta pontos importantes sobre a evolução da EaD.</p>
      <p>Todo o panorama descrito colaborou para que a EaD se firmasse como modalidade educativa. Essa nítida expansão foi, em grande medida, influenciada pela globalização e pelo avanço das tecnologias de informação e comunicação, especialmente pela internet.</p>
      <p>Percebemos que a internet tem influenciado sobremaneira o estilo de vida das pessoas em diversos aspectos. Estar conectado, agora, é sentir-se pertencente ao mundo.</p>
      <p style="text-align:center;"><h3 class="titulo titulo-secondary" style="text-align:center;">
        Você consegue se imaginar sem acessar a rede mundial de computadores por um longo período? <br /> Ou logo sofrerá de nomofobia<span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">4</span>?
      </h3>
      <div class="fonte-bibliografica biblio-4-conteudo alert alert-warning" style="display:none;  ">O nome nomofobia vem do inglês no-mobile-phone phobia. É o medo ou o pânico de ficar longe do próprio celular. Fonte: <a href="http://g1.globo.com/fantastico/noticia/2015/05/apaixonados-por-tecnologia-ficam-48h-sem-celulares-em-praia-detox.html" target="_blank">http://g1.globo.com/fantastico/noticia/2015/05/apaixonados-por-tecnologia-ficam-48h-sem-celulares-em-praia-detox.html</a>  </div> 
      </p>
    </p>
  </div>
  <div class="col-lg-6">
      <div class="bloco-pontilhado" style="">
        <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Veja a reportagem do Jornal Nacional sobre a evolução da EaD:</p>
        <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
          <iframe width="420" height="315" src="https://www.youtube.com/embed/5OKybl7vTqo" frameborder="0" allowfullscreen></iframe>                    
        </div>
      </div>
  </div>  
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina7.php', 'aula1pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


