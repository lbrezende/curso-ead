<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','10', '15', 'aula1pagina9.php', 'aula1pagina11.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <p> <h3 class="titulo titulo-primary">A EaD na atualidade</h3></p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <p><strong>a) Moore e Kearsley (2011)</strong></p>
    <div class="col-lg-offset-2 col-lg-10">
        <p>A ideia básica de educação a distância é muito simples: <strong> alunos e professores estão em locais diferentes </strong>durante todo ou grande parte do tempo em que aprendem e ensinam. Estando em locais distintos, eles <strong>  dependem de algum tipo de tecnologia</strong> para transmitir informações e lhes proporcionar um meio para interagir.</p>
        <div class="bloco-pontilhado" style="margin-bottom:10px" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
          <p style="text-align:center; font-weight:bold;">Você sabe o que é o <img src="imagens/moodle.jpg" alt="Logo do moodle" style="width:100px; margin-bottom:10px"> ?<a href="javascript:void(0);" class="btn btn-info btn-small botao-moodle" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
          <div id="conteudo-moodle" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
              <p style="text-align:center; text-indent:0em;">O texto que se encontra no link a seguir traz informações interessantes a respeito desse ambiente virtual de aprendizagem. <br><br> <a href="http://www.ead.edumed.org.br/file.php/1/PlataformaMoodle.pdf" target="_blank" class="btn btn-info btn-small" style="text-indent:0em;">Instituto EduMed</a><br><br><a href="http://www.moodle.org" target="_blank" class="btn btn-info btn-small" style="text-indent:0em;">Moodle.org</a></p>                       
            </div>
          </div>
        </div>        
    </div> 

  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-12">
            <div class="well conteudo-alunosprofessores" > 
                <p style="text-align:center; font-weight:bold; text-indent:0em"> “Alunos e professores estão em locais diferentes”</p>
                <p> Michel Moore desenvolveu o conceito de distância transacional, em que não importa a distância física, mas, sim, as relações pedagógicas e psicológicas que se estabelecem na EaD. O que afeta essa distância, isto é, o que a torna maior ou menor é o nível de interação entre os participantes. (MAIA e MATTAR, 2007, p. 15).</p>
                <p> Assim, a distância não é um empecilho para os processos educativos.</p>
            </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
            <div class="well conteudo-tecnologia" > 
                <p style="text-align:center; font-weight:bold; text-indent:0em"> “Dependem de algum tipo de tecnologia”</p>
                <p>Para viabilizar a oferta de cursos na modalidade a distância, diante de tantos recursos e possibilidades que os meios de informática proporcionam, foram desenvolvidos os Ambientes Virtuais de Aprendizagem (AVA). No STF, utilizamos a plataforma <i class="italico">Moodle</i>. (Leia sobre o <i class="italico">Moodle</i> no tópico Saiba Mais).</p>
                <p>Essas ferramentas permitem a realização de cursos de modo que os participantes estejam em uma sala de aula virtual. É um ambiente dinâmico, no qual são disponibilizados materiais didáticos, ocorrem a troca de informações entre os participantes, a realização de atividades diversas, a inclusão de arquivos de texto, vídeos, imagens, sons, enfim, uma série de recursos que podem tornar a aprendizagem mais rica e interativa.</p>
            </div>
      </div>
    </div>    
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina9.php', 'aula1pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


