<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','9', '15', 'aula1pagina8.php', 'aula1pagina10.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-12">
          <p> <h3 class="titulo titulo-primary">A EaD na atualidade</h3></p>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <p>Nos dias atuais, a educação a distância (igualmente denominada <i class="italico">e-learning</i>) se caracteriza pelo uso intensivo de tecnologias, microcomputador, multimídia, hipertexto e rede de computadores; pela integração de mídias e pelo uso da internet, segundo <strong>Maia e Mattar</strong> (2007).</p>
      
      <p>O grande diferencial da EaD, nesse novo cenário, é a possibilidade de interação entre todos os participantes do processo (professor/aluno, aluno/aluno), devido à oferta de diversas ferramentas de comunicação. Anteriormente, os meios utilizados favoreciam, apenas, o estudo individual.</p>
      <p style="text-align:center"> <h3 class="titulo titulo-secondary" style="text-align:center">Então, tendo em vista esse contexto atual, como a educação a distância pode ser definida?</h3></p>      
  </div>

  <div class="col-lg-6">
      <div class="botao-maiaMattar"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Maia e Mattar?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-maiaMattar btn btn-info">Conheça Maia e Mattar</a></p></div>
      <div class="conteudo-maiaMattar" style="display:none">
      <p style="background-color: #30BEC2; border-radius:10px; color: white; padding: 20px;"><img src="imagens/carmem.jpg" alt="Maia" style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          <strong style="color:#EECA71">Carmem Maia</strong> é jornalista, possui Doutorado em Comunicação e Semiótica, com Pós-Doutorado pelo Instituto de Educação da Universidade de Londres.
        </p>        
        <div class="clear"></div>
        <p style="background-color: #138DAC; border-radius:10px; color: white; padding: 20px;"><img src="imagens/mattar.jpg"  alt="Mattar" style="float: right; margin-left:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          <strong style="color:#EECA71">João Mattar</strong> possui Mestrado em <i class="italico">Educational Technology</i> (<i class="italico">Boise State University</i>/EUA), Doutorado em Literatura (USP) e Pós-Doutorado Interdisciplinar (Stanford University/EUA).
        </p>
      </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <p>Apresentamos, a seguir, alguns conceitos para solidificar sua compreensão sobre o assunto. Destacamos alguns termos nas definições e, em seguida, tecemos considerações a respeito deles, para ampliar sua visão sobre o tema. Nossa intenção é fazer com que esse vocabulário faça parte do seu dia a dia; afinal, você será integrado à equipe de profissionais que atuam na educação a distância do STF!</p>
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina8.php', 'aula1pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


