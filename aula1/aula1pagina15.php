<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','15', '15', 'aula1pagina14.php', 'fim', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-12 referencias">
    <p> <h3 class=" titulo titulo-primary">Referências</h3></p>
      <p>ALVES, Lucineia. <strong>Educação a distância</strong>: conceitos e história no Brasil e no mundo. Disponível em: <a href="http://www.abed.org.br/revistacientifica/Revista_PDF_Doc/2011/Artigo_07.pdf" target="_blank">&lt;http://www.abed.org.br/revistacientifica/Revista_PDF_Doc/2011/Artigo_07.pdf&gt;</a>. Acesso em mar. 2015. </p>
      <p>ASSOCIAÇÃO BRASILEIRA DE EDUCAÇÃO A DISTÂNCIA. <strong>Censo EAD.BR 2013</strong>: relatório analítico da aprendizagem a distância no Brasil. Disponível em: <a href="" target="_blank">&lt;http://www.abed.org.br/censoead2013/CENSO_EAD_2013_PORTUGUES.pdf&gt;</a>. Acesso em: mar. 2015.</p>
      <p>___________________. <strong>Dicionário de terminologia de educação a distância</strong>. Disponível em: </p>
      <p><a href="http://www.abed.org.br/revistacientifica/_brazilian/dicionario_terminologia_ead/dicionario.pdf" target="_blank">&lt;http://www.abed.org.br/revistacientifica/_brazilian/dicionario_terminologia_ead/dicionario.pdf&gt;</a>. Acesso em: mar. 2015.</p>
      <p>BENÍTE, I. M. S. <strong>História da educação a distância no Brasil e no mundo</strong>. Disponível em <a href="http://www.coladaweb.com/pedagogia/historia-da-educacao-a-distancia-no-brasil-e-no-mundo" target="_blank">&lt;http://www.coladaweb.com/pedagogia/historia-da-educacao-a-distancia-no-brasil-e-no-mundo&gt;</a>. Acesso em: mar. 2015. </p>
      <p>GOMES, S. G. S. <strong>Evolução histórica da EAD</strong>. E-TEC Brasil: tópicos em educação a distância. Disponível em: <a href="http://ftp.comprasnet.se.gov.br/sead/licitacoes/Pregoes2011/PE091/Anexos/Eventos_modulo_I/topico_ead/Aula_02.pdf" target="_blank">&lt;http://ftp.comprasnet.se.gov.br/sead/licitacoes/Pregoes2011/PE091/Anexos/Eventos_modulo_I/topico_ead/Aula_02.pdf&gt;</a>. Acesso em: mar. 2015. </p>
      <p>LOPES, M. A. L. P. et al. <strong>O processo histórico da educação a distância e suas implicações</strong>: desafios e possibilidades. Disponível em: <a href="http://www.histedbr.fe.unicamp.br/acer_histedbr/jornada/jornada7/_GT1%20PDF/O%20PROCESSO%20HIST%D3RICO%20DA%20EDUCA%C7%C3O%20A%20DIST%C2NCIA%20E%20SUAS%20IMPLICA%C7%D5ES.pdf" target="_blank">&lt;Acesse aqui&gt;</a>. Acesso em: mar. 2015.</p>
      <p>MACHADO, M; FERREIRA, S. M. B.; AQUINO, Vânia. <strong>A mediação pedagógica à luz do pensar complexo</strong>: uma análise comparativa da aula em ambiente virtual e presencial. Col@bora. <strong>Revista Digital da CVA. Ricesu</strong>, v. 6, n. 23, jul. 2010. Disponível em: <a href="http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116" target="_blank">&lt;http://pead.ucpel.tche.br/revistas/index.php/colabora/article/viewFile/133/116&gt;</a>. Acesso em: mar. 2015.</p>
      <p>MAIA, Carmem; MATTAR, João. <strong>ABC da EAD</strong>: a educação a distância hoje. São Paulo: Pearson Prentice Hall, 2007.</p>
      <p>MOORE, M. & KEARSLEY, G. <strong>Educação a distância</strong>: uma visão integrada. São Paulo: Thompson Learning, 2011.</p>
      <p>MORIN, J. M. Considerações para uma pedagogia da educação online. In: SILVA, Marco Antônio da. <strong>Educação online</strong>: teorias, práticas, legislação e formação corporativa. São Paulo: Loyola, 2003.</p>
      <p>SANTOS, Edméa Oliveira. <strong>Ambientes virtuais de aprendizagem</strong>: por autorias livres, plurais e gratuitas. <strong>Revista FAEBA</strong>, v. 12, n. 18.2003 (no prelo). Disponível em: <a href="http://www.comunidadesvirtuais.pro.br/hipertexto/home/ava.pdf " target="_blank">&lt;http://www.comunidadesvirtuais.pro.br/hipertexto/home/ava.pdf&gt;</a> . Acesso em: mar. 2015. </p>
      <p>VIDAL, E. M.; MAIA, J. E. B. <strong>Introdução a educação a distância</strong>. Disponível em:</p>
      <p><a href="http://www.fe.unb.br/catedraunescoead/areas/menu/publicacoes/livros-de-interesse-na-area-de-tics-na-educacao/introducao-a-educacao-a-distancia" target="_blank">&lt;http://www.fe.unb.br/catedraunescoead/areas/menu/publicacoes/livros-de-interesse-na-area-de-tics-na-educacao/introducao-a-educacao-a-distancia&gt;</a>. Acesso em: mar. 2015.</p>

  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina14.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


