<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','6', '15', 'aula1pagina5.php', 'aula1pagina7.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <p> <h3 class="titulo titulo-primary">A EaD no Brasil</h3></p>
      <p>Também no ramo dos cursos por correspondência, vale mencionar que, desde meados de 1940, o <a href="http://www.institutomonitor.com.br/Quem-somos.aspx" target="_blank">Instituto Monitor</a> e o <a href="http://www.institutouniversal.com.br/institucional/quem-somos" target="_blank">Instituto Universal Brasileiro</a> destacam-se no segmento de cursos profissionalizantes no País e atuam até hoje no mercado. </p>
      <p>O <strong>rádio</strong> começou a ser utilizado por volta de 1920, com a criação da <i class="italico">Rádio Sociedade do Rio de Janeiro</i><span id="biblio-3" class="fonte-bibliografica-numero " name="biblio-abrir">3</span>, que ofertava cursos em diversas áreas, entre as quais línguas estrangeiras. </p>
      <p>No âmbito governamental, a iniciativa mais proeminente de utilização do rádio com finalidade educativa foi o <i class="italico">Projeto Minerva </i>(1970), voltado para a inclusão social de adultos.</p>
      <div class="fonte-bibliografica biblio-3-conteudo alert alert-warning" style="display:none;  ">MAIA, Carmem; MATTAR, João. ABC da EAD: a educação a distância hoje. São Paulo: Pearson Prentice Hall, 2007. </div> 
  </div>
  <div class="col-lg-6">
      <div class="bloco-pontilhado" style="">
        <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Para conhecer o contexto em que o Projeto Minerva foi criado, convidamos você para assistir ao vídeo</p>
        <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
          <iframe width="420" height="315" src="https://www.youtube.com/embed/Mz0gtsk0gCs" frameborder="0" allowfullscreen></iframe>                    
        </div>
      </div>
  </div>  
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina5.php', 'aula1pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


