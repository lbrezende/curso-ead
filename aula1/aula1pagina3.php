<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','3', '15', 'aula1pagina2.php', 'aula1pagina4.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-12">

    <p> <h3 class="titulo titulo-primary">Breve histórico mundial da EaD</h3></p>


    <p style="margin-bottom:30px;">Avançando em direção aos <strong>séculos XIX e XX</strong>, surgiram iniciativas institucionais em vários países, com a utilização do rádio e da televisão, além dos cursos por correspondência. Mencionamos alguns exemplos logo abaixo<span id="biblio-2" class="fonte-bibliografica-numero" name="biblio-abrir">2</span> :</p>
    <div class="fonte-bibliografica biblio-2-conteudo alert alert-warning" style="display:none; margin-bottom:30px; margin-top:-10px; "> 2. <a href="http://www.abed.org.br/revistacientifica/Revista_PDF_Doc/2011/Artigo_07.pdf" target="_blank">http://www.abed.org.br/revistacientifica/Revista_PDF_Doc/2011/Artigo_07.pdf</a></div> 
    
    <div class="row"> 
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align:center; margin-bottom:10px"> 
            <img class="img-curso" style="max-height:90px" src="imagens/correio.jpg" alt="" title="Foto de correio" >
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"> 
          <p id="abrir-correspondencia" class="tracejado-arredondado">
            Sabe qual foi o primeiro curso por <strong> correspondência? </strong>  <br />
            <i class="italico">Sir Isaac Pitman Correspondence Colleges</i> (Reino Unido, 1840).
          </p>        
        </div>        
    </div>
    <div class="clear"> </div>

    <div class="row"> 
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align:center; margin-bottom:10px"> 
            <img class="img-curso" style="max-height:90px" src="imagens/radio.jpg" alt="" title="Foto de rádio antigo">
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"> 
          <p id="abrir-radio" class="tracejado-arredondado">
            Sabe qual foi o primeiro curso por <strong> rádio? </strong>  <br />
            <i class="italico">Japanese National Public Broadcasting Service</i> (Japão, 1935). 
          </p>
        </div>        
    </div>
    <div class="clear"> </div>

    <div class="row"> 
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align:center; margin-bottom:10px"> 
            <img class="img-curso" style="max-height:90px" src="imagens/tv.jpg" alt="" title="Foto de televisão" >
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"> 
          <p id="abrir-tv" class="tracejado-arredondado">
            Sabe qual foi o primeiro curso por <strong> TV? </strong>  <br />
            <i class="italico">Chicago TV College</i> (Estados Unidos,1956). 
          </p>        
 
        </div>        
    </div>   
    <div class="clear"> </div>    
       
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula1pagina2.php', 'aula1pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


