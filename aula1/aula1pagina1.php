<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','1', '15', 'index.php', 'aula1pagina2.php', 'Produção de Conteúdos para a EaD');
?> 

<p><?php $i = 1; ?></p>
<!-- nova tela -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="titulo titulo-primary">Seja bem-vindo à nossa primeira aula!</h3>

      <p>Para nos situarmos em relação à temática geral do curso, produção de conteúdos para a Educação a Distância (EaD), é necessário trabalhar o contexto mais amplo no qual ela está inserida. </p>
      <p>Por isso, o objetivo desta aula é possibilitar que você identifique os principais fatos ligados à evolução histórica da EaD no mundo e no Brasil, visto que isso facilitará a  compreensão das nuances e facetas que a permeiam nos dias de hoje – momento em que nos preparamos para escrever materiais didáticos. </p>
      <p>Para tanto, optamos por apresentar uma perspectiva histórica a partir dos meios de comunicação utilizados para difundir a modalidade. Separamos a realidade mundial da brasileira nos períodos iniciais, pois os marcos temporais não são coincidentes. Na sequência, abordamos essa dinâmica na atualidade de forma genérica, já que as características predominantes são as mesmas em ambos os casos.</p>
      <p>Esperamos que seja uma boa viagem pelo tempo.</p>

    <div style="text-align:center" alt=""><h3 class="titulo titulo-secondary">Preparados? Vamos começar?</h3> </div>
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'fim', 'aula1pagina2.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

