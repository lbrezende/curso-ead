<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','11', '15', 'aula1pagina10.php', 'aula1pagina12.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <p> <h3 class="titulo titulo-primary">A EaD na atualidade</h3></p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <p><strong>b) <a href="https://www.planalto.gov.br/ccivil_03/_Ato2004-2006/2005/Decreto/D5622.htm" target="_blank">Decreto 5.622/2005</a></strong></p>
    <div class="col-lg-offset-2 col-lg-10">
        <p>(...) caracteriza-se a educação a distância como modalidade educacional na qual a <strong class="botao-mediacao">mediação didático-pedagógica</strong> nos processos de ensino e aprendizagem ocorre com a utilização de meios e tecnologias de informação e comunicação, com estudantes e professores desenvolvendo atividades educativas em lugares ou tempos diversos. </p>
    </div> 

  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-12">
            <div class="well conteudo-mediacao" > 
                <p style="text-align:center; font-weight:bold; text-indent:0em"> “Mediação didático-pedagógica”</p>
                <p>“É um processo comunicacional, conversacional, de construção de significados, cujo objetivo é ampliar as possibilidades de diálogo e desenvolver a negociação significativa de processos e conteúdos a serem trabalhados nos ambientes educacionais, bem como incentivar a construção de um saber relacional, contextual, gerado na interação professor-aluno.</p>
                <p>A mediação pedagógica pressupõe, dessa forma, a ação de um docente que ajuda a desenvolver no aluno a curiosidade, a motivação, a autonomia e o gosto pelo aprender, seja no ambiente presencial ou no ambiente virtual.” (MACHADO et al, 2010). </p>
            </div>
      </div>
    </div>    
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina10.php', 'aula1pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


