<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','13', '15', 'aula1pagina12.php', 'aula1pagina14.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 

<div class="row">
  <div class="col-lg-12">
          <p> 
          <h3 class="titulo titulo-primary">EaD e capacitação continuada</h3></p>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
      <p>Ao trazer o paradigma da EaD para o âmbito da educação corporativa, <strong>Pierre Lévy</strong> (1999, p. 157) nos auxilia ao constatar o seguinte: “Pela primeira vez na história da humanidade, a maioria das competências adquiridas por uma pessoa no início de seu percurso profissional estarão obsoletas no fim de sua carreira”.</p>
      <p>Por essa razão, a capacitação continuada, hoje, é fundamental para todos os trabalhadores, independentemente do ramo de atuação. </p>
      
      <p>Pensando, então, na necessidade de atualização constante de conhecimentos e, ainda, na dificuldade praticamente generalizada dos trabalhadores quanto à disponibilidade de tempo para frequentar cursos com horários pré-determinados, a educação a distância <em>online</em> tem se firmado na área de qualificação profissional.</p>
      
      <p>A Administração Pública, campo a que se volta nossa reflexão, também tem buscado a profissionalização dos seus quadros de pessoal, pois vislumbra a prestação de serviços públicos cada vez mais eficiente. Para tanto, tem investido em programas de treinamento focados no desenvolvimento de seus servidores.</p>
      <p>O STF, por entender essa conjuntura, fomenta ações de capacitação <em>online</em> desde o ano de 2009. A demanda por cursos na modalidade cresceu significativamente desde então, a ponto de ser criada, em 2014, uma unidade específica para tratar do tema, a Seção de Educação a Distância. </p>
      <p>Veja, na tabela seguinte, o crescimento da EaD no Tribunal ao longo dos anos, tanto em relação à quantidade de cursos realizados como em relação ao número de alunos beneficiados:</p>
  </div>

  <div class="col-lg-6">
      <div class="botao-levy"><h3 class="titulo titulo-secondary" style="text-align:center">Já conhece Pierre Lévy?</h3><p style="text-indent:0em; text-align:center;"> <a href="javascript:void(0);" class="botao-levy btn btn-info">Conheça Pierre Lévy</a></p></div>
      <div class="conteudo-levy" style="display:none"> 
        <p style="background-color: #138DAC; border-radius:10px; color: white; padding: 20px;"><img src="imagens/pierre.jpg"  alt="Mattar" style="float: left; margin-right:15px; border-radius: 100px; max-width: 100px; border: 3px solid #fff;">
          <strong style="color:#EECA71">Pierre Lévy</strong> é um filósofo francês da cultura virtual contemporânea. Vive em Paris e leciona no Departamento de Hipermídia da Universidade de Paris. Principais obras: <i class="italico">As tecnologias da inteligência, As árvores de conhecimento, Cibercultura e O que é virtual?</i>
          <a href="http://pt.wikipedia.org/wiki/Pierre_L%C3%A9vy" target="_blank" style="text-indent:0em;" class="btn btn-white btn-small">Ver na Wikipedia</a>
          <br>

        </p>
      </div>  
        <div class="bloco-pontilhado" style="" class="">
          <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px; margin-bottom:10px;">
          <p style="text-align:center; font-weight:bold;">Saiba mais sobre a EaD no Brasil <br><a href="javascript:void(0);" class="btn btn-info btn-small botao-eadbrasil" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
          <div id="conteudo-eadbrasil" style="display:none">
            <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
              <p style="text-align:center; text-indent:0em;">Se quiser ampliar sua percepção sobre o desenvolvimento da EaD no Brasil, em termos numéricos, indicamos a leitura do Censo EaD Brasil 2013, realizado pela Associação Brasileira de Educação a Distância (ABED), disponível no link: <br><br> <a href="http://www.abed.org.br/censoead2013/CENSO_EAD_2013_PORTUGUES.pdf" class="btn btn-info btn-small" target="_blank">Censo EaD</a> </p>                       
            </div>
          </div>
        </div>         
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
    <div style="text-align:center; margin-top:10px"><img src="imagens/tabela.jpg" class="img-curso" title="Tabela mostrando que de 2009 para 2014 o número de cursos realizados subiu de 1 para 10, e o número de participantes EaD subiu de 80 para 419 pessoas."><br><span style="font-size:14px">Fonte: Seção de Educação a Distância do STF</span></div>
  </div>

  <div class="col-lg-6">
    <p style="text-align:center"> <h3 style="text-align:center" class="titulo titulo-secondary">Números expressivos, não é mesmo? </h3></p>
    <p style="text-align:center"> <h3 style="text-align:center" class="titulo titulo-secondary">Será que você faz parte desses dados?</h3></p>
    <p style="text-align:center"> <h3 style="text-align:center" class="titulo titulo-secondary">Já participou de algum outro curso a distância oferecido pelo Tribunal ou esta é sua primeira experiência?</h3></p>
  </div>      
</div>  
<br><br>
<div class="row">
  <div class="col-lg-12">
      <div>
        <p>Podemos dizer que a EaD é um caminho sem volta. Pelas suas características na atualidade, ela revela alguns desafios que temos como autores de conteúdos: </p>
        <p>• desenvolvimento de materiais didáticos de qualidade que, de fato, contribuam para a aprendizagem dos alunos, sejam significativos e façam sentido dentro do contexto institucional do STF;</p>
        <p>• atualização constante quanto às possibilidades que a tecnologia oferece, com vistas a incrementar o estudo on-line, sem perder o foco educativo.</p>
        <p>Mas não se assuste! Juntos construiremos caminhos, passo a passo, para desenvolver o melhor trabalho possível. Por essa razão, queremos, desde já, estabelecer uma parceria. Você aceita? </p>
      </div>      
  </div>
</div>




<?php  configNavegacaoRodape('exibir', 'aula1pagina12.php', 'aula1pagina14.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


