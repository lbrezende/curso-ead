<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','2', '15', 'aula1pagina1.php', 'aula1pagina3.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <h3 class="titulo titulo-primary">Breve histórico mundial da EaD</h3>

    <p>Falando em história, convidamos você a imaginar o contexto mundial há alguns séculos. </p>
    <p>Pegue o seu controle remoto e dê um pause no <strong>século XVIII</strong>, exatamente em 1728. Nesse ano, nos Estados Unidos, foi oferecido o curso de Taquigrafia por correspondência, desenvolvido pelo professor <a href="http://webcache.googleusercontent.com/search?q=cache:2_q6hmhzHVEJ:www.taquigrafia.emfoco.nom.br/variedades_seis_mat%25C3%25A9rias/caleb_phillips_um_professor_de_taquigrafia_pioneiro.doc+&cd=1&hl=pt-BR&ct=clnk&gl=br" target="_blank">Caleb Phillips</a>. Essa ação é considerada um marco para a educação a distância.  </p>
    <p>Leia, à direita, o anúncio do curso publicado no jornal <i class="italico">Boston Gazette</i><span id="biblio-1" class="fonte-bibliografica-numero " name="biblio-abrir">1</span> (Gazeta de Boston) da época: </p>
    <div class="fonte-bibliografica biblio-1-conteudo alert alert-warning" style="display:none;  ">1. Evolução Histórica da EAD, p. 25. Disponível em: <a href="http://ftp.comprasnet.se.gov.br/sead/licitacoes/Pregoes2011/PE091/Anexos/Eventos_modulo_I/topico_ead/Aula_02.pdf" target="_blank">http://ftp.comprasnet.se.gov.br/sead/licitacoes/Pregoes2011/ PE091/Anexos/Eventos_modulo_I/topico_ead/Aula_02.pdf</a>  Acesso em março de 2015 </div> 
    
        <h3 class="titulo titulo-secondary" style="text-align:center; margin-top:50px">
      O que achou do anúncio? <br />Você acha que ficaria interessado em participar desse curso?
    </h3>
       
  </div>

<div class="row">
  <div class="col-lg-6">
   <div style="text-align:center"><img class="img-curso" src="imagens/anuncio.jpg" alt="" title="Toda pessoa da região, desejosa de aprender esta arte, pode receber em sua casa várias lições semanalmente e ser perfeitamente instruída, como as pessoas que vivem em Boston">   </div>
  </div>  

   
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina1.php', 'aula1pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


