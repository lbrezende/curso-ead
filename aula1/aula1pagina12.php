<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','12', '15', 'aula1pagina11.php', 'aula1pagina13.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
    <p> <h3 class="titulo titulo-primary">A EaD na atualidade</h3></p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <p><strong>c) Marco Silva (2006)</strong></p>
    <div class="col-lg-offset-2 col-lg-10">
        <p>(...) a educação online é um fenômeno da <strong>cibercultura</strong>, um conjunto interrelacionado de técnicas, práticas, atitudes, modos de pensamento e valores. (...) é um novo ambiente de comunicação, de sociabilidade, de organização, de conhecimento e de educação que surge com a interconexão mundial de computadores.  </p>
    </div> 

  </div>
  <div class="col-lg-6">
    <div class="row">
      <div class="col-lg-12">
            <div class="well conteudo-cibercultura" > 
                <p style="text-align:center; font-weight:bold; text-indent:0em"> “Cibercultura”</p>
                <p>Cibercultura, termo proposto por Pierre Lévy, é a cultura que surgiu, ou surge, a partir do uso da rede de computadores, da comunicação por meio de computadores, da indústria do entretenimento e do comércio eletrônico. </p>
                <p>É também o estudo de vários fenômenos sociais associados à internet e outras novas formas de comunicação em rede, como as comunidades on-line, jogos de multiusuários, jogos sociais, mídias sociais, realidade aumentada, mensagens de texto, e inclui questões relacionadas à identidade, à privacidade e à formação de rede<span id="biblio-5" class="fonte-bibliografica-numero " name="biblio-abrir">5</span>. </p>
            </div>
            <div class="fonte-bibliografica biblio-5-conteudo alert alert-warning" style="display:none;  ">5. Disponível em: <a href="http://pt.wikipedia.org/wiki/Cibercultura" target="_blank">http://pt.wikipedia.org/wiki/Cibercultura</a> - com adaptações. Acesso em março de 2015.</div> 
      </div>
    </div>    
  </div>
</div>



<?php  configNavegacaoRodape('exibir', 'aula1pagina11.php', 'aula1pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


