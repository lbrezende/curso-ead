<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','14', '15', 'aula1pagina13.php', 'aula1pagina15.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-12">
    <p> <h3 class="titulo titulo-primary">Considerações Finais</h3></p>
      <p>Para resgatarmos rapidamente os temas lançados nesta aula: conversamos sobre os aspectos históricos da EaD até chegarmos aos dias de hoje. Percebemos que a modalidade é fundamental para promover ações educativas desde épocas anteriores. Ainda, observamos que ela evoluiu no tempo, acompanhou os avanços das tecnologias e, hoje, vivencia grande expansão, notadamente no campo da educação continuada de profissionais. </p>
      <p>Feitas essas considerações, na próxima aula trocaremos ideias a respeito do estudo <i class="italico">online</i> e das particularidades da aprendizagem na educação a distância. </p>

  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <p> <h3 class="titulo titulo-primary">Vamos Praticar?</h3></p>
      <p>
        <p>Após tudo o que dialogamos aqui, o que mais chamou sua atenção no histórico da EaD? Quais são suas percepções, opiniões, dúvidas, conclusões, análises a respeito do tema tratado nesta aula?</p>
        <p>Registre suas ideias no nosso Ambiente Virtual de Aprendizagem – o<i class="italico"> Moodle</i>. Lá disponibilizamos o espaço <strong>“Meu percurso de aprendizagem”</strong>. Utilizamos, para tanto, a ferramenta “Diário”.</p>
        <p>Esse espaço foi criado para registros individuais, por isso apenas você e o tutor têm acesso a ele. Então, fique livre para se expressar!</p>
        <p>O objetivo é que você escreva, passo a passo, o que aprendeu em cada aula estudada. Ao final, terá uma visão geral sobre todo o trajeto percorrido.</p>
        <p>Aguardamos suas reflexões lá no <i class="italico"> Moodle</i>!</p>
      </p>
    <p> <h3 class="titulo titulo-secondary">Vamos prosseguir?</h3></p>
  </div>
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina13.php', 'aula1pagina15.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


