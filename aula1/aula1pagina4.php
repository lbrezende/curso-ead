<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','4', '15', 'aula1pagina3.php', 'aula1pagina5.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-12">
  <p> <h3 class="titulo titulo-primary">Breve histórico mundial da EaD</h3></p>
  </div>
</div>


<div class="row">
  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
      <p class="well" style="text-indent:0em;  ">
        <span id="abrir-tvaberta"><a class="btn btn-info btn-small botao-tvaberta"  href="javascript:void(0);" class="btn btn-info btn-small">Conheça Universidade Aberta</a></span>
        <span id="conteudo-tvaberta" style="display:none"><strong> Universidade Aberta:</strong><br /> Originalmente, era o nome da Universidade Aberta da Inglaterra (<i class="italico">Open University</i>), mas agora é usado como termo genérico para qualquer instituição de ensino superior que possua as características da aprendizagem aberta (<i class="italico">open learning</i>).
        <br /><span style="font-size:12px;">Fonte: <a href="http://www.abed.org.br/rbaad/dicionario.pdf" target="_blank">Dicionário de Terminologia de Educação a Distância (ABED)  </a></span>.</span>
      </p>

  </div>
  <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12">
      <p>Na segunda metade do século XX, várias <strong>universidades abertas</strong> foram criadas. A partir desse momento, a EaD começou a ganhar relevância na área acadêmica mundo afora, com a oferta de cursos superiores por correspondência na Suécia, no Reino Unido, na Espanha, na Venezuela, entre outros países. Isso demonstra a ampliação do alcance da modalidade em diferentes realidades econômicas e sociais pelo mundo.</p>
      <div class="saibamais bloco-pontilhado" style="" class="">
        <img src="imagens/saibamais.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Sabe o que estava acontecendo pelo mundo nos séculos XVIII, XIX e XX? <a href="javascript:void(0);" class="btn btn-info btn-small botao-saibamaisSeculo" style="text-align:center; text-indent:0em; margin-top:20px; margin-bottom:30px">Clique aqui para descobrir!</a> </p>
        <div id="conteudo-saibamaisSeculo" style="display:none">
          <div class="row" style="text-align:center; margin-bottom:30px; padding:30px">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <a href="http://www1.uol.com.br/bibliot/linhadotempo/index4.htm" target="_blank" class="btn btn-info btn-block" style="margin-bottom:5px">Século XVIII</a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <a href="http://www1.uol.com.br/bibliot/linhadotempo/index5.htm" target="_blank" class="btn btn-info btn-block" style="margin-bottom:5px">Século XIX</a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <a href="http://www1.uol.com.br/bibliot/linhadotempo/index6.htm" target="_blank" class="btn btn-info btn-block" style="margin-bottom:5px">Século XX</a>
            </div>                        
          </div>
        </div>
      </div>
  </div>  
</div>



<?php  configNavegacaoRodape('exibir', 'aula1pagina3.php', 'aula1pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


