<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','5', '15', 'aula1pagina4.php', 'aula1pagina6.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <p> <h3 class="titulo titulo-primary">A EaD no Brasil</h3></p>
      <p>Na realidade brasileira, a educação a distância surgiu “em decorrência do iminente processo de industrialização cuja trajetória gerou uma demanda por políticas educacionais que formassem o trabalhador para a ocupação industrial”, segundo Lopes et al (2015).
      <p>Seguindo essa expoente demanda, os principais meios utilizados para veicular a modalidade por aqui, à medida de sua consolidação, também foram: correspondência, rádio e televisão. </p>
      <p>Diferentemente do que ocorreu no cenário mundial, os primeiros registros de oferta de cursos a distância em terras nacionais apareceram apenas no início do século XX. Entre eles, em 1904, foi anunciado nos classificados do Jornal do Brasil o curso profissionalizante de Datilografia por <strong>correspondência</strong>.</p>     
    </p>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">      
      <div class="bloco-pontilhado" style="padding:30px">
        <p class="titulo titulo-secondary" style="text-align:center; font-weight:bold; text-indent:0em;">Você conhece a letra de máquina de escrever? <br /><img src="imagens/maquina.jpg" alt="Foto de máquina de escrever" title="Foto de máquina de escrever" style="margin-top:20px"><br><a href="javascript:void(0);" class="btn btn-info btn-small botao-maquina" style="text-align:center; text-indent:0em; margin-top:20px; ">Clique aqui para conhecer!</a> </p>
        <div id="conteudo-maquina" style="display:none">
         <p class="maquina-de-escrever">Dependendo da geração na qual você nasceu, a máquina de escrever figurou como um instrumento fundamental para trabalho com a escrita, semelhante ao que ocorre com o computador hoje. Contudo, muitos entre nós, possivelmente, nunca viram um exemplar e nem tiveram o prazer de utilizá-la.</p>
        </div>
      </div>      
    </p>
  </div>  
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina4.php', 'aula1pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


