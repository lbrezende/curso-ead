<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contextualização da Educação a Distância', 'exibir', '1','7', '15', 'aula1pagina6.php', 'aula1pagina8.php', 'Planejamento das Contratações:buscando a gestão efetiva dos gastos públicos');
?> 


<div class="row">
  <div class="col-lg-6">
        <p> <h3 class="titulo titulo-primary">A EaD no Brasil</h3></p>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
      <p>E a <strong>televisão?</strong></p>
      <p>A televisão começou a ser utilizada logo em seguida, também na década de 70. Um projeto bastante conhecido, realizado até os dias atuais, é o <a href="http://educacao.globo.com/telecurso/noticia/2014/11/historico.html" target="_blank">Telecurso</a>. Ele foi criado para oferecer ensino regular aos jovens e adultos moradores de municípios e comunidades distantes. </p>
      <div style="" class="bloco-pontilhado">
        <img src="imagens/video.png" alt="" title="saiba mais" style="margin-top:-40px; margin-left:-30px;">
        <p style="text-align:center; font-weight:bold;">Que tal assistir a uma aula do Telecurso?</p>
        <div id="" class="fitvids" style="text-align:center; margin-bottom:20px; padding:20px">
          <iframe width="420" height="315" src="https://www.youtube.com/embed/mYUp5e9mwZA" frameborder="0" allowfullscreen></iframe>                    
        </div>
      </div>
  </div>
  <div class="col-lg-6">
      <p style="text-indent:0em; text-align:center">
        <br class="visible-xs">
        <img src="imagens/palavra.jpg" alt="Palavra da conteudista" class="img-curso" title="Palavra da Conteudista"><br>
        <a href="javascript:void(0);" class="btn btn-info btn-small botao-conteudista-1" style="margin-top:20px;">Clique para ler</a>
        <div class="conteudo-conteudista-1 well" style="display:none">
          <p>Você pode estar se questionando: qual a importância, para a análise do tema central do nosso curso, de se conhecer os aspectos históricos da EaD, a partir da perspectiva dos meios utilizados para a sua veiculação?</p>
          <p>Primeiramente, a intenção é permitir que você fique familiarizado com alguns aspectos históricos que permeiam a modalidade, bem como perceba que a educação a distância tem sido uma necessidade desde tempos antigos. </p>
          <p>Segundo, desejamos que você observe como a EaD tem acompanhado a dinâmica das transformações da sociedade e, nesse movimento, evoluído no que se refere aos meios de comunicação utilizados para difundir a modalidade. Podemos dizer que ela aprimorou-se, alcançou novos horizontes educativos, comunicativos e tecnológicos e, assim, consolidou-se ao longo da história.</p>
        </div>
      </p>
  </div>  
</div>


<?php  configNavegacaoRodape('exibir', 'aula1pagina6.php', 'aula1pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


